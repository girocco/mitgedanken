#!/bin/sh

# This script uses the current values of Girocco::Config to
# convert apache.conf.in into apache.conf.
#
# It is run automatically by "make" or "make apache.conf" but
# may be run separately if desired.

set -e

. ./shlib.sh

trap "rm -f 'apache.conf.$$'" EXIT
trap 'exit 130' INT
trap 'exit 143' TERM

__girocco_conf="$GIROCCO_CONF"
[ -n "$__girocco_conf" ] || __girocco_conf="Girocco::Config"
perl -I"$PWD" -M"$__girocco_conf" -- - apache.conf.in >apache.conf.$$ <<'EOT'
#line 21 "make-apache-conf.sh"
use strict;
use warnings;

my $frombegin = '# ---- BEGIN LINES TO DUPLICATE ----';
my $fromend   = '# ---- END LINES TO DUPLICATE ----';
my $tobegin   = '# ---- BEGIN DUPLICATE LINES ----';
my $toend     = '# ---- END DUPLICATE LINES ----';

open IN, '<', $ARGV[0] or die "could not open $ARGV[0] for reading: $!\n";
my $input;
{
	local $/;
	$input = <IN>;
}
close IN;
$input =~ s/^##.*(?:\n|$)//gm;
my $repl = sub {
	no warnings;
	if (eval '$Girocco::Config::'.$_[1]) {
		$_[0] ? "Girocco_Config_$_[1]_Disabled" : "!Girocco_Config_$_[1]_Disabled";
	} else {
		$_[0] ? "!Girocco_Config_$_[1]_Enabled" : "Girocco_Config_$_[1]_Enabled";
	}
};
$input =~ s/^(\s*<IfDefine\s+)(!?)\@\@([a-zA-Z][a-zA-Z0-9_]*)\@\@>/
	$1 . &$repl($2, $3) . '>'/gmexi;
{
	no warnings;
	$input =~ s/\@\@([a-zA-Z][a-zA-Z0-9_]*)\@\@/eval
		"\$Girocco::Config::$1 ne ''?\$Girocco::Config::$1:'\@\@'.\"$1\".'\@\@'"/gsex;
}
if ($input =~ /(?:^|\n)$frombegin[^\n]*\n(.*)(?<=\n)$fromend/s) {
	my $dupelines = $1;
	if ($input =~ /^((?:.+\n)?$tobegin[^\n]*\n).*((?<=\n)$toend.*)$/s) {
		$input = $1 . $dupelines . $2;
	}
}
printf "%s", $input;
EOT
mv -f apache.conf.$$ apache.conf
