#!/bin/sh
#
# This is generic shell library for all the scripts used by Girocco;
# most importantly, it introduces all the $cfg_* shell variables.

# hash patterns
hexdig='[0-9a-f]'
octet="$hexdig$hexdig"
octet4="$octet$octet$octet$octet"
octet19="$octet4$octet4$octet4$octet4$octet$octet$octet"
octet20="$octet4$octet4$octet4$octet4$octet4"
# tab (single \t between single quotes)
tab='	'

# set a sane umask that never excludes any user or group permissions
umask $(printf '0%03o' $(( $(umask) & ~0770 )) )

vcmp() {
	# Compare $1 to $2 each of which must match \d+(\.\d+)*
	# An empty string ('') for $1 or $2 is treated like 0
	# Outputs:
	#  -1 if $1 < $2
	#   0 if $1 = $2
	#   1 if $1 > $2
	# Note that `vcmp 1.8 1.8.0.0.0.0` correctly outputs 0.
	while
		_a="${1%%.*}"
		_b="${2%%.*}"
		[ -n "$_a" ] || [ -n "$_b" ]
	do
		if [ "${_a:-0}" -lt "${_b:-0}" ]; then
			echo -1
			return
		elif [ "${_a:-0}" -gt "${_b:-0}" ]; then
			echo 1
			return
		fi
		_a2="${1#$_a}"
		_b2="${2#$_b}"
		set -- "${_a2#.}" "${_b2#.}"
	done
	echo 0
}

unset orig_path
get_girocco_config_pm_var_list() (
	# Export all the variables from Girocco::Config to suitable var= lines
	# prefixing them with 'cfg_'. E.g. $cfg_admin is admin's mail address now
	# and also setting a 'defined_cfg_' prefix to 1 if they are not undef.
	__girocco_conf="$GIROCCO_CONF"
	[ -n "$__girocco_conf" ] || __girocco_conf="Girocco::Config"
	[ -z "$basedir" ] || __girocco_extrainc="-I$basedir"
	inc_basedir=@basedir@
	[ "@basedir@" != '@'basedir'@' ] || inc_basedir="$PWD"
	[ -z "$orig_path" ] || { PATH="$orig_path" && export PATH; }
	perl -I"$inc_basedir" $__girocco_extrainc -M$__girocco_conf -le \
		'foreach (sort {uc($a) cmp uc($b)} keys %Girocco::Config::) {
			my $val = ${$Girocco::Config::{$_}}; defined($val) or $val="";
			$val =~ s/([\\"\$\`])/\\$1/gos;
			$val =~ s/(?:\r\n|\r|\n)$//os;
			print "cfg_$_=\"$val\"";
			print "defined_cfg_$_=",
				(defined(${$Girocco::Config::{$_}})?"1":"");
		}'
)

# Returns full command path for "$1" if it's a valid command otherwise returns "$1"
_fcp() {
	if _fp="$(command -v "$1" 2>/dev/null)"; then
		printf '%s\n' "$_fp"
	else
		printf '%s\n' "$1"
	fi
}

get_girocco_config_var_list() (
	# Same as get_girocco_config_pm_var_list except that
	# the following variables (all starting with var_) are added:
	# var_group		cfg_owning_group if defined otherwise `id -gn`
	# var_git_ver		The version number part from `git version`
	# var_git_exec_path	The result of $cfg_git_bin --exec-dir
	# var_sh_bin		Full path to the posix sh interpreter to use
	# var_perl_bin		Full path to the perl interpreter to use
	# var_gzip_bin		Full path to the gzip executable to use
	# var_nc_openbsd_bin	Full path to the netcat (nc) with -U support
	# var_have_git_171	Set to 1 if git version >= 1.7.1 otherwise ''
	# var_have_git_172	Set to 1 if git version >= 1.7.2 otherwise ''
	# var_have_git_173	Set to 1 if git version >= 1.7.3 otherwise ''
	# var_have_git_1710	Set to 1 if git version >= 1.7.10 otherwise ''
	# var_have_git_185	Set to 1 if git version >= 1.8.5 otherwise ''
	# var_have_git_210	Set to 1 if git version >= 2.1.0 otherwise ''
	# var_have_git_235	Set to 1 if git version >= 2.3.5 otherwise ''
	# var_have_git_260	Set to 1 if git version >= 2.6.0 otherwise ''
	# var_have_git_2101	Set to 1 if git version >= 2.10.1 otherwise ''
	# var_window_memory	Value to use for repack --window-memory=
	# var_big_file_threshold Value to use for core.bigFileThreshold
	# var_redelta_threshold Recompute deltas if no more than this many objs
	# var_upload_window	If not "", pack.window to use for upload-pack
	# var_log_window_size	Value to use for git-svn --log-window-size=
	# var_utf8_locale	Value to use for a UTF-8 locale if available
	# var_xargs_r		A "-r" if xargs needs it to behave correctly
	# var_du_exclude	Option to exclude PATTERN from du if available
	# var_du_follow		Option to follow command line sym links if available
	_cfg_vars="$(get_girocco_config_pm_var_list)"
	eval "$_cfg_vars"
	printf '%s\n' "$_cfg_vars"
	printf 'var_group=%s\n' "${cfg_owning_group:-$(id -gn)}"
	_gver="$("$cfg_git_bin" version 2>/dev/null |
		LC_ALL=C sed -ne 's/^[^0-9]*\([0-9][0-9]*\(\.[0-9][0-9]*\)*\).*$/\1/p')"
	printf 'var_git_ver=%s\n' "$_gver"
	printf 'var_git_exec_path="%s"\n' "$("$cfg_git_bin" --exec-path 2>/dev/null)"
	printf 'var_sh_bin="%s"\n' "$(_fcp "${cfg_posix_sh_bin:-/bin/sh}")"
	printf 'var_perl_bin="%s"\n' "$(_fcp "${cfg_perl_bin:-$(unset -f perl; command -v perl)}")"
	printf 'var_gzip_bin="%s"\n' "$(_fcp "${cfg_gzip_bin:-$(unset -f gzip; command -v gzip)}")"
	printf 'var_nc_openbsd_bin="%s"\n' "$(_fcp "${cfg_nc_openbsd_bin:-$(unset -f nc; command -v nc)}")"
	printf 'var_have_git_171=%s\n' "$([ $(vcmp "$_gver" 1.7.1) -ge 0 ] && echo 1)"
	printf 'var_have_git_172=%s\n' "$([ $(vcmp "$_gver" 1.7.2) -ge 0 ] && echo 1)"
	printf 'var_have_git_173=%s\n' "$([ $(vcmp "$_gver" 1.7.3) -ge 0 ] && echo 1)"
	printf 'var_have_git_1710=%s\n' "$([ $(vcmp "$_gver" 1.7.10) -ge 0 ] && echo 1)"
	printf 'var_have_git_185=%s\n' "$([ $(vcmp "$_gver" 1.8.5) -ge 0 ] && echo 1)"
	printf 'var_have_git_210=%s\n' "$([ $(vcmp "$_gver" 2.1.0) -ge 0 ] && echo 1)"
	printf 'var_have_git_235=%s\n' "$([ $(vcmp "$_gver" 2.3.5) -ge 0 ] && echo 1)"
	printf 'var_have_git_260=%s\n' "$([ $(vcmp "$_gver" 2.6.0) -ge 0 ] && echo 1)"
	printf 'var_have_git_2101=%s\n' "$([ $(vcmp "$_gver" 2.10.1) -ge 0 ] && echo 1)"
	__girocco_conf="$GIROCCO_CONF"
	[ -n "$__girocco_conf" ] || __girocco_conf="Girocco::Config"
	[ -z "$basedir" ] || __girocco_extrainc="-I$basedir"
	inc_basedir=@basedir@
	[ "@basedir@" != '@'basedir'@' ] || inc_basedir="$PWD"
	printf "var_window_memory=%s\n" \
		"$(perl -I"$inc_basedir" $__girocco_extrainc -M$__girocco_conf \
		-MGirocco::Util -e 'print calc_windowmemory')"
	printf "var_big_file_threshold=%s\n" \
		"$(perl -I"$inc_basedir" $__girocco_extrainc -M$__girocco_conf \
		-MGirocco::Util -e 'print calc_bigfilethreshold')"
	printf "var_redelta_threshold=%s\n" \
		"$(perl -I"$inc_basedir" $__girocco_extrainc -M$__girocco_conf \
		-MGirocco::Util -e 'print calc_redeltathreshold')"
	if [ -n "$cfg_upload_pack_window" ] && [ "$cfg_upload_pack_window" -ge 2 ] &&
	   [ "$cfg_upload_pack_window" -le 50 ]; then
		printf "var_upload_window=%s\n" "$cfg_upload_pack_window"
	else
		printf "var_upload_window=%s\n" ""
	fi
	printf 'var_log_window_size=%s\n' "${cfg_svn_log_window_size:-250}"
	# We parse the output of `locale -a` and select a suitable UTF-8 locale.
	_guess_locale="$(locale -a | LC_ALL=C grep -viE '^(posix|c)(\..*)?$' |
		LC_ALL=C grep -iE '\.utf-?8$' | LC_ALL=C sed -e 's/\.[Uu][Tt][Ff]-*8$//' |
		LC_ALL=C sed -e '/en_US/ s/^/0 /; /en_US/ !s/^/1 /' | LC_ALL=C sort |
		head -n 1 | LC_ALL=C cut -d ' ' -f 2)"
	[ -z "$_guess_locale" ] || printf 'var_utf8_locale=%s.UTF-8\n' "$_guess_locale"
	# On some broken platforms running xargs without -r and empty input runs the command
	printf 'var_xargs_r=%s\n' "$(</dev/null command xargs printf %s -r)"
	# The disk usage report produces better numbers if du has an exclude option
	_x0="${0##*/}"
	_x0="${_x0%?}?*"
	for _duopt in --exclude -I; do
		if _test="$(du $_duopt 's?lib.s*' $_duopt "$_x0" "$0" 2>/dev/null)" && [ -z "$_test" ]; then
			printf 'var_du_exclude=%s\n' "$_duopt"
			break
		fi
	done
	if _test="$(du -H "$0" 2>/dev/null)" && [ -n "$_test" ]; then
		printf 'var_du_follow=%s\n' "-H"
		break
	fi
)

# If basedir has been replaced, and shlib_vars.sh exists, get the config
# definitions from it rather than running Perl.
if [ "@basedir@" = '@'basedir'@' ] || ! [ -r "@basedir@/shlib_vars.sh" ]; then
	# Import all the variables from Girocco::Config to the local environment,
	eval "$(get_girocco_config_var_list)"
else
	# Import the variables from shlib_vars.sh which avoids needlessly
	# running another copy of Perl
	. "@basedir@/shlib_vars.sh"
fi

# git_add_config "some.var=value"
# every ' in value must be replaced with the 4-character sequence '\'' before
# calling this function or Git will barf.  Will not be effective unless running
# Git version 1.7.3 or later.
git_add_config() {
	GIT_CONFIG_PARAMETERS="${GIT_CONFIG_PARAMETERS:+$GIT_CONFIG_PARAMETERS }'$1'"
	export GIT_CONFIG_PARAMETERS
}

# Make sure we have a reproducible environment by using a controlled HOME dir
XDG_CONFIG_HOME="$cfg_chroot/var/empty"
HOME="$cfg_chroot/etc/girocco"
TMPDIR="/tmp"
GIT_CONFIG_NOSYSTEM=1
GIT_ATTR_NOSYSTEM=1
GIT_NO_REPLACE_OBJECTS=1
GIT_TERMINAL_PROMPT=0
GIT_PAGER="cat"
PAGER="cat"
GIT_ASKPASS="$cfg_basedir/bin/git-askpass-password"
GIT_SVN_NOTTY=1
GIROCCO_SUPPRESS_AUTO_GC_UPDATE=1
GIT_SSH="$cfg_basedir/bin/git-ssh"
SVN_SSH="$cfg_basedir/bin/git-ssh"
export XDG_CONFIG_HOME
export HOME
export TMPDIR
export GIT_CONFIG_NOSYSTEM
export GIT_ATTR_NOSYSTEM
export GIT_NO_REPLACE_OBJECTS
export GIT_TERMINAL_PROMPT
export GIT_PAGER
export PAGER
export GIT_ASKPASS
export GIT_SVN_NOTTY
export GIROCCO_SUPPRESS_AUTO_GC_UPDATE
export GIT_SSH
export SVN_SSH
unset GIT_USER_AGENT
unset GIT_HTTP_USER_AGENT
if [ -n "$defined_cfg_git_client_ua" ]; then
	GIT_USER_AGENT="$cfg_git_client_ua"
	export GIT_USER_AGENT
fi
unset GIT_CONFIG_PARAMETERS
git_add_config "core.ignoreCase=false"
git_add_config "core.pager=cat"
if [ -n "$cfg_git_no_mmap" ]; then
	# Just like compiling with NO_MMAP
	git_add_config "core.packedGitWindowSize=1m"
else
	# Always use the 32-bit default (32m) even on 64-bit to avoid memory blowout
	git_add_config "core.packedGitWindowSize=32m"
fi
[ -z "$var_big_file_threshold" ] ||
	git_add_config "core.bigFileThreshold=$var_big_file_threshold"
git_add_config "gc.auto=0"

# Make sure any sendmail.pl config is always available
unset SENDMAIL_PL_HOST
unset SENDMAIL_PL_PORT
unset SENDMAIL_PL_NCBIN
unset SENDMAIL_PL_NCOPT
[ -z "$cfg_sendmail_pl_host"  ] || { SENDMAIL_PL_HOST="$cfg_sendmail_pl_host"   && export SENDMAIL_PL_HOST; }
[ -z "$cfg_sendmail_pl_port"  ] || { SENDMAIL_PL_PORT="$cfg_sendmail_pl_port"   && export SENDMAIL_PL_PORT; }
[ -z "$cfg_sendmail_pl_ncbin" ] || { SENDMAIL_PL_NCBIN="$cfg_sendmail_pl_ncbin" && export SENDMAIL_PL_NCBIN; }
[ -z "$cfg_sendmail_pl_ncopt" ] || { SENDMAIL_PL_NCOPT="$cfg_sendmail_pl_ncopt" && export SENDMAIL_PL_NCOPT; }

# Set PATH and PYTHON to the values set by Config.pm, if any
unset PYTHON
[ -z "$cfg_python" ] || { PYTHON="$cfg_python" && export PYTHON; }
[ -z "$cfg_path" ] || { orig_path="$PATH" && PATH="$cfg_path" && export PATH; }

# Extra GIT variables that generally ought to be cleared, but whose clearing
# could potentially interfere with the correct operation of hook scripts so
# they are segregated into a separate function for use as appropriate
clean_git_env() {
	unset GIT_ALTERNATE_OBJECT_DIRECTORIES
	unset GIT_CONFIG
	unset GIT_DIR
	unset GIT_GRAFT_FILE
	unset GIT_INDEX_FILE
	unset GIT_OBJECT_DIRECTORY
	unset GIT_NAMESPACE
}

# We cannot use a git() {} or nc_openbsd() {} function to redirect git
# and nc_openbsd to the desired executables because when using
# "ENV_VAR=xxx func" the various /bin/sh implementations behave in various
# different and unexpected ways:
#   a) treat "ENV_VAR=xxx" like a separate, preceding "export ENV_VAR=xxx"
#   b) treat "ENV_VAR=xxx" like a separate, prededing "ENV_VAR=xxx"
#   c) treat "ENV_VAR=xxx" like a temporary setting only while running func
# None of these are good.  We want a temporary "export ENV_VAR=xxx"
# setting only while running func which none of the /bin/sh's do.
#
# Instead we'd like to use an alias that provides the desired behavior without
# any of the bad (a), (b) or (c) effects.
#
# However, unfortunately, some of the crazy /bin/sh implementations do not
# recognize alias expansions when preceded by variable assignments!
#
# So we are left with git() {} and nc_openbsd() {} functions and in the
# case of git() {} we can compensate for (b) and (c) failing to export
# but not (a) and (b) persisting the values so the caller will simply
# have to beware and explicitly unset any variables that should not persist
# beyond the function call itself.

git() (
	[ z"${GIT_DIR+set}" != z"set" ] || export GIT_DIR
	[ z"${GIT_SSL_NO_VERIFY+set}" != z"set" ] || export GIT_SSL_NO_VERIFY
	[ z"${GIT_TRACE_PACKET+set}" != z"set" ] || export GIT_TRACE_PACKET
	[ z"${GIT_USER_AGENT+set}" != z"set" ] || export GIT_USER_AGENT
	[ z"${GIT_HTTP_USER_AGENT+set}" != z"set" ] || export GIT_HTTP_USER_AGENT
	exec "$cfg_git_bin" "$@"
)

# Since we do not yet require at least Git 1.8.5 this is a compatibility function
# that allows us to use git update-ref --stdin where supported and the slow shell
# script where not, but only the "delete" operation is currently supported.
git_updateref_stdin() {
	if [ -n "$var_have_git_185" ]; then
		git update-ref --stdin
	else
		while read -r _op _ref; do
			case "$_op" in
				delete)
					git update-ref -d "$_ref"
					;;
				*)
					echo "bad git_updateref_stdin op: $_op" >&2
					exit 1
					;;
			esac
		done
	fi
}

# see comments for git() -- callers must explicitly export all variables
# intended for the commands these functions run before calling them
perl() { command "${var_perl_bin:-perl}" "$@"; }
gzip() { command "${var_gzip_bin:-gzip}" "$@"; }

nc_openbsd() { command "$var_nc_openbsd_bin" "$@"; }

list_packs() { command "$cfg_basedir/bin/list_packs" "$@"; }

readlink() { command "$cfg_basedir/bin/readlink" "$@"; }

strftime() { command "$cfg_basedir/bin/strftime" "$@"; }

# Some platforms' broken xargs runs the command always at least once even if
# there's no input unless given a special option.  Automatically supply the
# option on those platforms by providing an xargs function.
xargs() { command xargs $var_xargs_r "$@"; }

_addrlist() {
	_list=
	for _addr in "$@"; do
		[ -z "$_list" ] || _list="$_list, "
		_list="$_list$_addr"
	done
	echo "$_list"
}

_sendmail() {
	_mailer="${cfg_sendmail_bin:-/usr/sbin/sendmail}"
	if [ -n "$cfg_sender" ]; then
		"$_mailer" -i -f "$cfg_sender" "$@"
	else
		"$_mailer" -i "$@"
	fi
}

# First argument is an id WITHOUT surrounding '<' and '>' to use in a
# "References:" header.  It may be "" to suppress the "References" header.
# Following arguments are just like mail function
mailref() {
	_references=
	if [ $# -ge 1 ]; then
		_references="$1"
		shift
	fi
	_subject=
	if [ "$1" = "-s" ]; then
		shift
		_subject="$1"
		shift
	fi
	{
		echo "From: \"$cfg_name\" ($cfg_title) <$cfg_admin>"
		echo "To: $(_addrlist "$@")"
		[ -z "$_subject" ] || echo "Subject: $_subject"
		echo "MIME-Version: 1.0"
		echo "Content-Type: text/plain; charset=utf-8; format=fixed"
		echo "Content-Transfer-Encoding: 8bit"
		[ -z "$_references" ] || echo "References: <$_references>"
		[ -n "$cfg_suppress_x_girocco" ] || echo "X-Girocco: $cfg_gitweburl"
		echo "Auto-Submitted: auto-generated"
		echo ""
		cat
	} | _sendmail "$@"
}

# Usage: mail [-s <subject>] <addr> [<addr>...]
mail() {
	mailref "" "$@"
}

# bang CMD... will execute the command with well-defined failure mode;
# set bang_action to string of the failed action ('clone', 'update', ...);
# re-define the bang_trap() function to do custom cleanup before bailing out
bang() {
	bang_errcode=
	bang_catch "$@"
	[ "${bang_errcode:-0}" = "0" ] || bang_failed
}

bang_catch() {
	bang_active=1
	bang_cmd="$*"
	bang_errcode=0
	if [ "${show_progress:-0}" != "0" ]; then
		exec 3>&1
		read -r bang_errcode <<-EOT || :
		$(
			exec 4>&3 3>&1 1>&4 4>&-
			{ "$@" 3>&- || echo $? >&3; } 2>&1 | tee -i -a "$bang_log"
		)
		EOT
		exec 3>&-
		if [ -z "$bang_errcode" ] || [ "$bang_errcode" = "0" ]; then
			# All right. Cool.
			bang_active=
			bang_cmd=
			return;
		fi
	else
		if "$@" >>"$bang_log" 2>&1; then
			# All right. Cool.
			bang_active=
			bang_cmd=
			return;
		else
			bang_errcode="$?"
		fi
	fi
}

bang_failed() {
	bang_active=
	unset GIT_DIR
	>.banged
	cat "$bang_log" >.banglog
	echo "" >>.banglog
	echo "$bang_cmd failed with error code $bang_errcode" >>.banglog
	! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :
	if [ "${show_progress:-0}" != "0" ]; then
		echo ""
		echo "$bang_cmd failed with error code $bang_errcode"
	fi
	if [ -e .bangagain ]; then
		git config --remove-section girocco.bang 2>/dev/null || :
		rm -f .bangagain
	fi
	bangcount="$(git config --int girocco.bang.count 2>/dev/null)" || :
	bangcount=$(( ${bangcount:-0} + 1 ))
	git config --int girocco.bang.count $bangcount
	if [ $bangcount -eq 1 ]; then
		git config girocco.bang.firstfail "$(TZ=UTC date "+%Y-%m-%d %T UTC")"
	fi
	if [ $bangcount -ge $cfg_min_mirror_failure_message_count ] &&
	   [ "$(git config --bool girocco.bang.messagesent 2>/dev/null || :)" != "true" ] &&
	   ! check_interval "girocco.bang.firstfail" $cfg_min_mirror_failure_message_interval; then
		bangmailok="$(git config --bool gitweb.statusupdates 2>/dev/null || echo true)"
		bangaddrs=
		[ "$bangmailok" = "false" ] || [ -z "$mail" ] || bangaddrs="$mail"
		[ -z "$cfg_admincc" ] || [ "$cfg_admincc" = "0" ] || [ -z "$cfg_admin" ] ||
		if [ -z "$bangaddrs" ]; then bangaddrs="$cfg_admin"; else bangaddrs="$bangaddrs,$cfg_admin"; fi
		rsubj=
		[ $bangcount -le 1 ] || rsubj=" repeatedly"
		[ -z "$bangaddrs" ] ||
		{
			echo "$bang_cmd failed with error code $bang_errcode"
			echo ""
			rsubj=
			if [ $bangcount -gt 1 ]; then
				echo "$bangcount consecutive update failures have occurred since $(config_get girocco.bang.firstfail)"
				echo ""
			fi
			echo "you will not receive any more notifications until recovery"
			echo "this status message may be disabled on the project admin page"
			echo ""
			echo "Log follows:"
			echo ""
			cat "$bang_log"
		} | mailref "update@$cfg_gitweburl/$proj.git" -s "[$cfg_name] $proj $bang_action failed$rsubj" "$bangaddrs"
		git config --bool girocco.bang.messagesent true
	fi
	bangthrottle=
	[ $bangcount -lt 15 ] ||
		check_interval "girocco.bang.firstfail" $(( $cfg_min_mirror_interval * 3 / 2 )) ||
		bangthrottle=1
	bang_trap $bangthrottle
	[ -n "$bang_errcode" ] && [ "$bang_errcode" != "0" ] || bang_errcode=1
	exit $bang_errcode
}

# bang_eval CMD... will evaluate the command with well-defined failure mode;
# Identical to bang CMD... except the command is eval'd instead of executed.
bang_eval() {
	bang eval "$*"
}

# Default bang settings:
bang_setup() {
	bang_active=
	bang_action="lame_programmer"
	bang_trap() { :; }
	bang_tmpdir="${TMPDIR:-/tmp}"
	bang_tmpdir="${bang_tmpdir%/}"
	bang_log="$(mktemp "${bang_tmpdir:-/tmp}/repomgr-XXXXXX")"
	is_git_dir . || {
		echo "bang_setup called with current directory not a git directory" >&2
		exit 1
	}
	trap 'rm -f "$bang_log"' EXIT
	trap '[ -z "$bang_active" ] || { bang_errcode=130; bang_failed; }; exit 130' INT
	trap '[ -z "$bang_active" ] || { bang_errcode=143; bang_failed; }; exit 143' TERM
}

# Remove banged status
bang_reset() {
	rm -f .banged .bangagain .banglog
	git config --remove-section girocco.bang 2>/dev/null || :
}

# Check to see if banged status
is_banged() {
	[ -e .banged ]
}

# Check to see if banged message was sent
was_banged_message_sent() {
	[ "$(git config --bool girocco.bang.messagesent 2>/dev/null || :)" = "true" ]
}

# Progress report - if show_progress is set, shows the given message.
progress() {
	[ "${show_progress:-0}" = "0" ] || echo "$*"
}

# Project config accessors; must be run in project directory
config_get() {
	case "$1" in
	*.*)
		git config "$1";;
	*)
		git config "gitweb.$1";;
	esac
}

config_set() {
	git config "gitweb.$1" "$2" && chgrp $var_group config && chmod g+w config
}

config_set_raw() {
	git config "$1" "$2"        && chgrp $var_group config && chmod g+w config
}

config_get_date_seconds() {
	_dt="$(config_get "$1")" || :
	[ -n "$_dt" ] || return 1
	_ds="$(perl -I@basedir@ -MGirocco::Util -e "print parse_any_date('$_dt')")"
	[ -n "$_ds" ] || return 1
	echo "$_ds"
}

# Tool for checking whether given number of seconds has not passed yet
check_interval() {
	os="$(config_get_date_seconds "$1")" || return 1
	ns="$(date +%s)"
	[ $ns -lt $(($os+$2)) ]
}

# Check if we are running with effective root permissions
is_root() {
	[ "$(id -u 2>/dev/null)" = "0" ]
}

# Check to see if the single argument (default ".") is a Git directory
is_git_dir() {
	# Just like Git's test except we ignore GIT_OBJECT_DIRECTORY
	# And we are slightly more picky (must be refs/.+ not refs/.*)
	[ $# -ne 0 ] || set -- "."
	[ -d "$1/objects" ] && [ -x "$1/objects" ] || return 1
	[ -d "$1/refs" ] && [ -x "$1/refs" ] || return 1
	if [ -L "$1/HEAD" ]; then
		_hr="$(readlink "$1/HEAD")"
		case "$_hr" in "refs/"?*) :;; *) return 1;; esac
	fi
	[ -f "$1/HEAD" ] && [ -r "$1/HEAD" ] || return 1
	read -r _hr <"$1/HEAD" || return 1
	case "$_hr" in
		$octet20*)
			[ "${_hr#*[!0-9a-f]}" = "$_hr" ] || return 1
			return 0;;
		ref:refs/?*)
			return 0;;
		ref:*)
			_hr="${_hr##ref:*[ $tab]}"
			case "$_hr" in "refs/"?*) return 0;; esac
	esac
	return 1
}

# Check to see if the single argument (default ".") is a directory with no refs
is_empty_refs_dir() {
	[ $# -ne 0 ] || set -- "."
	if [ -s "$1/packed-refs" ]; then
		# could be a packed-refs file with just a '# pack-refs ..." line
		# null hash lines and peel lines do not count either
		_refcnt="$(( $(LC_ALL=C sed <"$1/packed-refs" \
			-e "/^00* /d" \
			-e "/^$octet20$hexdig* refs\/[^ $tab]*\$/!d" | wc -l) ))"
		[ "${_refcnt:-0}" -eq 0 ] || return 1
	fi
	if [ -d "$1/refs" ]; then
		# quick and dirty check, doesn't try to validate contents
		# or ignore embedded symbolic refs
		_refcnt="$(( $(find -L "$1/refs" -type f -print 2>/dev/null | head -n 1 | LC_ALL=C wc -l) ))"
		[ "${_refcnt:-0}" -eq 0 ] || return 1
	fi
	# last chance a detached HEAD (we ignore any linked working trees though)
	[ -s "$1/HEAD" ] && read -r _hr <"$1/HEAD" && [ -n "$_hr" ] || return 0
	[ "${_hr#*[!0-9a-f]}" != "$_hr" ] || [ "${_hr#*[!0]}" = "$_hr" ] || [ "${#_hr}" -lt 40 ] || return 1
	return 0
}

# List all Git repositories, with given prefix if specified, one-per-line
# All project names starting with _ are always excluded from the result
get_repo_list() {
	if [ -n "$1" ]; then
		LC_ALL=C cut -d : -f 1,3 "$cfg_chroot"/etc/group | LC_ALL=C grep "^$1"
	else
		LC_ALL=C cut -d : -f 1,3 "$cfg_chroot"/etc/group
	fi | while IFS=: read name id; do
		[ $id -lt 65536 ] || case "$name" in _*) :;; ?*) echo "$name"; esac
	done
}

# set the variable named by the first argument to the project part (i.e. WITH
# the trailing ".git" but WITHOUT the leading $cfg_reporoot) of the directory
# specified by the second argument.
# This function cannot be fooled by symbolic links.
# If the second argument is omitted (or empty) use $(pwd -P) instead.
# The directory specified by the second argument must exist.
v_get_proj_from_dir() {
	[ -n "$2" ] || set -- "$1" "$(pwd -P)"
	[ -d "$2" ] || return 1
	case "$2" in
	"$cfg_reporoot/"?*)
		# Simple case that does not need any fancy footwork
		_projpart="${2#$cfg_reporoot/}"
		;;
	*)
		_absrr="$(cd "$cfg_reporoot" && pwd -P)"
		_abspd="$(cd "$2" && pwd -P)"
		case "$_abspd" in
		"$_absrr/"?*)
			# The normal case
			_projpart="${_abspd#$_absrr/}"
			;;
		*)
			# Must have been reached via a symbolic link, but
			# we have no way to know the source, so use a
			# generic "_external" leader combined with just the
			# trailing directory name
			_abspd="${_abspd%/}"
			_abspd="${_abspd%/.git}"
			_projpart="_external/${_abspd##*/}"
			;;
		esac
	esac
	eval "$1="'"$_projpart"'
}

# Returns success if "$1" does not exist or contains only blank lines and comments
# The parsing rules are in Git's sha1-file.c parse_alt_odb_entry function;
# the format for blank lines and comments has been the same since Git v0.99.5
is_empty_alternates_file() {
	[ -n "$1" ] || return 0
	[ -e "$1" ] && [ -f "$1" ] && [ -s "$1" ] || return 0
	[ -r "$1" ] || return 1
	LC_ALL=C awk <"$1" '!/^$/ && !/^#/ {exit 1}'
}

# Return success if the given project name has at least one immediate child fork
# that has a non-empty alternates file
has_forks_with_alternates() {
	_prj="${1%.git}"
	[ -n "$_prj" ] || return 1
	[ -d "$cfg_reporoot/$_prj" ] || return 1
	is_git_dir "$cfg_reporoot/$_prj.git" || return 1
	if
		get_repo_list "$_prj/[^/:][^/:]*:" |
		while read -r _prjname && [ -n "$_prjname" ]; do
			is_empty_alternates_file "$cfg_reporoot/$_prjname.git/objects/info/alternates" ||
			exit 1 # will only exit implicit subshell created by '|'
		done
	then
		return 1
	fi
	return 0
}

# returns empty string and error for empty string otherwise one of
#   m  =>  normal Git mirror
#   s  =>  mirror from svn source
#   d  =>  mirror from darcs source
#   b  =>  mirror from bzr source
#   h  =>  mirror from hg source
#   w  =>  mirror from mediawiki source
#   f  =>  mirror from other fast-import source
# note that if the string is non-empty and none of s, d, b or h match the
# return will always be type m regardless of whether it's a valid Git URL
get_url_mirror_type() {
	case "$1" in
		"")
			return 1
			;;
		svn://* | svn+http://* | svn+https://* | svn+file://* | svn+ssh://*)
			echo 's'
			;;
		darcs://* | darcs+http://* | darcs+https://*)
			echo 'd'
			;;
		bzr://*)
			echo 'b'
			;;
		hg+http://* | hg+https://* | hg+file://* | hg+ssh://* | hg::*)
			echo 'h'
			;;
		mediawiki::*)
			echo 'w'
			;;
		*)
			echo 'm'
			;;
	esac
	return 0
}

# returns false for empty string
# returns true if the passed in url is a mirror using git fast-import
is_gfi_mirror_url() {
	[ -n "$1" ] || return 1
	case "$(get_url_mirror_type "$1" 2>/dev/null || :)" in
		d|b|h|w|f)
			# darcs, bzr, hg and mediawiki mirrors use git fast-import
			# and so do generic "f" fast-import mirrors
			return 0
			;;
		*)
			# Don't think git-svn currently uses git fast-import
			# And Git mirrors certainly do not
			return 1
			;;
	esac
	# assume it does not use git fast-import
	return 1
}

# returns false for empty string
# returns true if the passed in url is a mirror using git-svn
is_svn_mirror_url() {
	[ -n "$1" ] || return 1
	[ "$(get_url_mirror_type "$1" 2>/dev/null || :)" = "s" ]
}

# returns mirror url for gitweb.baseurl of git directory
# (GIT_DIR) passed in as the argument (which defaults to "." if omitted)
# will fail if the directory does not have .nofetch and gitweb.baseurl
# comes back empty -- otherwise .nofetch directories succeed with a "" return
# automatically strips any leading "disabled " prefix before returning result
get_mirror_url() {
	_gitdir="${1:-.}"
	# always return empty for non-mirrors
	! [ -e "$_gitdir/.nofetch" ] || return 0
	_url="$(GIT_DIR="$_gitdir" config_get baseurl 2>/dev/null)" || :
	_url="${_url##* }"
	[ -n "$_url" ] || return 1
	printf '%s\n' "$_url"
	return 0
}

# returns get_url_mirror_type for gitweb.baseurl of git directory
# (GIT_DIR) passed in as the argument (which defaults to "." if omitted)
# will fail if the directory does not have .nofetch and gitweb.baseurl
# comes back empty -- otherwise .nofetch directories succeed with a "" return
# automatically strips any leading "disabled " prefix before testing
get_mirror_type() {
	_url="$(get_mirror_url "$@")" || return 1
	[ -n "$_url" ] || return 0
	get_url_mirror_type "$_url"
}

# returns true if the passed in git dir (defaults to ".") is a mirror using git fast-import
is_gfi_mirror() {
	_url="$(get_mirror_url "$@")" || return 1
	is_gfi_mirror_url "$_url"
}

# returns true if the passed in git dir (defaults to ".") is a mirror using git-svn
is_svn_mirror() {
	_url="$(get_mirror_url "$@")" || return 1
	is_svn_mirror_url "$_url"
}

# current directory must already be set to Git repository
# if girocco.headok is already true succeeds without doing anything
# if rev-parse --verify HEAD succeeds sets headok=true and succeeds
# otherwise tries to set HEAD to a symbolic ref to refs/heads/master
# then refs/heads/trunk and finally the first top-level head from
# refs/heads/* (i.e. only two slashes in the name) and finally any
# existing refs/heads.  The first one to succeed wins and sets headok=true
# and then a successful exit.  Otherwise headok is left unset with a failure exit
# We use the girocco.headok flag to make sure we only force a valid HEAD symref
# when the repository is being set up -- if the HEAD is later deleted (through
# a push or fetch --prune) that's no longer our responsibility to fix
check_and_set_head() {
	[ "$(git config --bool girocco.headok 2>/dev/null || :)" != "true" ] || return 0
	if git rev-parse --verify --quiet HEAD >/dev/null; then
		git config --bool girocco.headok true
		return 0
	fi
	for _hr in refs/heads/master refs/heads/trunk; do
		if git rev-parse --verify --quiet "$_hr" >/dev/null; then
			_update_head_symref "$_hr"
			return 0
		fi
	done
	git for-each-ref --format="%(refname)" refs/heads 2>/dev/null |
	while read -r _hr; do
		case "${_hr#refs/heads/}" in */*) :;; *)
			_update_head_symref "$_hr"
			exit 1 # exit subshell created by "|"
		esac
	done || return 0
	_hr="$(git for-each-ref --format="%(refname)" refs/heads 2>/dev/null | head -n 1)" || :
	if [ -n "$_hr" ]; then
		_update_head_symref "$_hr"
		return 0
	fi
	return 1
}
_update_head_symref() {
	git symbolic-ref HEAD "$1"
	git config --bool girocco.headok true
	! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :
}

# current directory must already be set to Git repository
# if the directory needs to have gc run and .needsgc is not already set
# then .needsgc will be set triggering a "mini" gc at the next opportunity
# Girocco shouldn't generate any loose objects but we check for that anyway
check_and_set_needsgc() {
	# If there's a .needspack file and ANY loose objects with a newer timestamp
	# then also set .needsgc otherwise remove it.  The only caller that may set
	# .needspack is a mirror therefore we don't have to worry about removing a
	# .needspack out from under a simultaneous creator.  We always do this and
	# do it first to try and avoid leaving a stale .needspack lying around.
	if [ -e .needspack ]; then
		_objfiles=
		_objfiles="$(( $(find -L objects/$octet -maxdepth 1 -newer .needspack -name "$octet19*" -type f -print 2>/dev/null |
			 head -n 1 | LC_ALL=C wc -l) +0 ))"
		if [ "${_objfiles:-0}" = "0" ]; then
			rm -f .needspack
		else
			[ -e .needsgc ] || >.needsgc
		fi
	fi
	! [ -e .needsgc ] || return 0
	_packs=
	{ _packs="$(list_packs --quiet --count --exclude-no-idx --exclude-keep objects/pack || :)" || :; } 2>/dev/null
	if [ "${_packs:-0}" -ge 20 ]; then
		>.needsgc
		return 0
	fi
	_logfiles=
	{ _logfiles="$(($(find -L reflogs -maxdepth 1 -type f -print | wc -l || :)+0))" || :; } 2>/dev/null
	if [ "${_logfiles:-0}" -ge 50 ]; then
		>.needsgc
		return 0
	fi
	# Truly git gc only checks the number of objects in the objects/17 directory
	# We check for -ge 10 which should make the probability of having more than
	# 5120 (20*256) loose objects present when there are less than 10 in
	# objects/17 vanishingly small (20 is the threshold we use for pack files)
	_objfiles=
	! [ -d objects/17 ] ||
	{ _objfiles="$(($(find -L objects/17 -type f -name "$octet19*" -print | wc -l || :)+0))" || :; } 2>/dev/null
	if [ "${_objfiles:-0}" -ge 10 ]; then
		>.needsgc
		return 0
	fi
}

# A well-known UTF-8 locale is required for some of the fast-import providers
# in order to avoid mangling characters.  Ideally we could use "POSIX.UTF-8"
# but that is not reliably UTF-8 but rather usually US-ASCII.
# We parse the output of `locale -a` and select a suitable UTF-8 locale at
# install time and store that in $var_utf8_locale if one is found.
# If we cannot find one in the `locale -a` output then we just use a well-known
# UTF-8 locale and hope for the best.  We set LC_ALL to our choice and export
# it.  We only set this temporarily when running the fast-import providers.
set_utf8_locale() {
	LC_ALL="${var_utf8_locale:-en_US.UTF-8}"
	export LC_ALL
}

# hg-fast-export | git fast-import with error handling in current directory GIT_DIR
git_hg_fetch() (
	set_utf8_locale
	_python="${PYTHON:-python}"
	rm -f hg2git-marks.old hg2git-marks.new
	if [ -f hg2git-marks ] && [ -s hg2git-marks ]; then
		LC_ALL=C sed 's/^:\([^ ][^ ]*\) \([^ ][^ ]*\)$/\2 \1/' <hg2git-marks | {
			if [ -n "$var_have_git_185" ]; then
				git cat-file --batch-check=':%(rest) %(objectname)'
			else
				LC_ALL=C sed 's/^\([^ ][^ ]*\) \([^ ][^ ]*\)$/:\2 \1/'
			fi
		} | LC_ALL=C sed '/ missing$/d' >hg2git-marks.old
		if [ -n "$var_have_git_171" ] &&
		   git rev-parse --quiet --verify refs/notes/hg >/dev/null; then
			if [ -z "$var_have_git_185" ] ||
			   ! LC_ALL=C cmp -s hg2git-marks hg2git-marks.old; then
				_nm='hg-fast-export'
				GIT_AUTHOR_NAME="$_nm"
				GIT_COMMITTER_NAME="$_nm"
				GIT_AUTHOR_EMAIL="$_nm"
				GIT_COMMITTER_EMAIL="$_nm"
				export GIT_AUTHOR_NAME
				export GIT_COMMITTER_NAME
				export GIT_AUTHOR_EMAIL
				export GIT_COMMITTER_EMAIL
				git notes --ref=refs/notes/hg prune
				unset GIT_AUTHOR_NAME
				unset GIT_COMMITTER_NAME
				unset GIT_AUTHOR_EMAIL
				unset GIT_COMMITTER_EMAIL
			fi
		fi
	else
		>hg2git-marks.old
	fi
	_err1=
	_err2=
	exec 3>&1
	{ read -r _err1 || :; read -r _err2 || :; } <<-EOT
	$(
		exec 4>&3 3>&1 1>&4 4>&-
		{
			_e1=0
			_af="$(git config hg.authorsfile)" || :
			_cmd='GIT_DIR="$(pwd)" "$_python" "$cfg_basedir/bin/hg-fast-export.py" \
				--repo    "$(pwd)/repo.hg" \
				--marks   "$(pwd)/hg2git-marks.old" \
				--mapping "$(pwd)/hg2git-mapping" \
				--heads   "$(pwd)/hg2git-heads" \
				--status  "$(pwd)/hg2git-state" \
				-U unknown --force --flatten --hg-hash'
			[ -z "$_af" ] || _cmd="$_cmd"' --authors "$_af"'
			eval "$_cmd" 3>&- || _e1=$?
			echo $_e1 >&3
		} |
		{
			_e2=0
			git fast-import \
				--import-marks="$(pwd)/hg2git-marks.old" \
				--export-marks="$(pwd)/hg2git-marks.new" \
				--export-pack-edges="$(pwd)/gfi-packs" \
				--force 3>&- || _e2=$?
			echo $_e2 >&3
		}
	)
	EOT
	exec 3>&-
	[ "$_err1" = 0 ] && [ "$_err2" = 0 ] || return 1
	mv -f hg2git-marks.new hg2git-marks
	rm -f hg2git-marks.old
	git for-each-ref --format='%(refname) %(objectname)' refs/heads |
	LC_ALL=C sed -e 's,^refs/heads/,:,' >hg2git-heads
)
