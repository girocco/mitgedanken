#!/bin/sh
# The Girocco installation script
# We will OVERWRITE basedir!

set -e

[ -n "$MAKE" ] || MAKE="$(MAKEFLAGS= make -s gnu_make_command_name | grep '^gnu_make_command_name=' | sed 's/^[^=]*=//')"
if [ -z "$MAKE" ]; then
	echo "ERROR: cannot determine name of the GNU make command" >&2
	echo "Please set MAKE to the name of the GNU make executable" >&2
	exit 1
fi

# Run perl module checker
if ! [ -f toolbox/check-perl-modules.pl ] || ! [ -x toolbox/check-perl-modules.pl ]; then
	echo "ERROR: missing toolbox/check-perl-modules.pl!" >&2
	exit 1
fi

# What Config should we use?
[ -n "$GIROCCO_CONF" ] || GIROCCO_CONF=Girocco::Config
echo "*** Initializing using $GIROCCO_CONF..."

# First run Girocco::Config consistency checks
perl -I"$PWD" -M$GIROCCO_CONF -e ''

. ./shlib.sh
umask 0022
"$var_perl_bin" toolbox/check-perl-modules.pl

# Config.pm already checked $cfg_reporoot to require an absolute path, but
# we also require it does not contain a : or ; that would cause problems when
# used in GIT_ALTERNATE_OBJECT_DIRECTORIES
probch=':;'
case "$cfg_reporoot" in *[$probch]*)
	echo "fatal: \$Girocco::Config::reporoot may not contain ':' or ';' characters" >&2
	exit 1
esac

warn() { printf >&2 '%s\n' "$*"; }
die() { warn "$@"; exit 1; }

# Either we must run as root (but preferably not if disable_jailsetup is true)
# or the mirror_user (preferred choice for disable_jailsetup).
if [ "$(id -u)" -eq 0 ]; then
	if [ "${cfg_disable_jailsetup:-0}" != "0" ]; then
		cat <<'EOT'

***
*** WARNING: $Girocco::Config::disable_jailsetup has been enabled
*** WARNING: but installation is being performed as the superuser
***

You appear to have disabled jailsetup which is perfectly fine for installations
that will not be using an ssh jail.  However, in that case, running the install
process as the superuser is highly discouraged.

Instead, running it as the configured $Girocco::Config::mirror_user is much
preferred.

The install process will now pause for 10 seconds to give you a chance to abort
it before continuing to install a disable_jailsetup config as the superuser.

EOT
		sleep 10 || die "install aborted"
	fi
else
	[ -n "$cfg_mirror_user" ] || die 'Girocco::Config.pm $mirror_user must be set'
	curuname="$(id -un)"
	[ -n "$curuname" ] || die "Cannot determine name of current user"
	if [ "$cfg_mirror_user" != "$curuname" ]; then
		warn "ERROR: install must run as superuser or Config.pm's \$mirror_user ($cfg_mirror_user)"
		die  "ERROR: install is currently running as $curuname"
	fi
fi

# $1 must exist and be a dir
# $2 may exist but must be a dir
# $3 must not exist
# After call $2 will be renamed to $3 (if $2 existed)
# And $1 will be renamed to $2
quick_move() {
	[ -n "$1" ] && [ -n "$2" ] && [ -n "$3" ] || { echo "fatal: quick_move: bad args: '$1' '$2' '$3'" >&2; exit 1; }
	! [ -e "$3" ] || { echo "fatal: quick_move: already exists: $3" >&2; exit 1; }
	[ -d "$1" ] || { echo "fatal: quick_move: no such dir: $1" >&2; exit 1; }
	! [ -e "$2" ] || [ -d "$2" ] || { echo "fatal: quick_move: not a dir: $2" >&2; exit 1; }
	perl -e 'rename($ARGV[1], $ARGV[2]) or die "rename failed: $!\n" if -d $ARGV[1];
		rename($ARGV[0], $ARGV[1]) or die "rename failed: $!\n"; exit 0;' "$1" "$2" "$3" || {
		echo "fatal: quick_move: rename failed" >&2
		exit 1
	}
	! [ -d "$1" ] && [ -d "$2" ] || {
		echo "fatal: quick_move: rename failed" >&2
		exit 1
	}
}

check_sh_builtin() (
	"unset" -f command
	"command" "$var_sh_bin" -c '{ "unset" -f unalias command "$1" || :; "unalias" "$1" || :; } >/dev/null 2>&1; "command" -v "$1"' "$var_sh_bin" "$1"
) 2>/dev/null

owngroup=
[ -z "$cfg_owning_group" ] || owngroup=":$cfg_owning_group"
if [ -n "$cfg_httpspushurl" ] && [ -z "$cfg_certsdir" ]; then
	echo "ERROR: \$httpspushurl is set but \$certsdir is not!" >&2
	echo "ERROR: perhaps you have an incorrect Config.pm?" >&2
	exit 1
fi


# Check for extra required tools
if [ "${cfg_xmllint_readme:-0}" != "0" ] && ! command -v xmllint >/dev/null; then
	echo "ERROR: \$xmllint_readme set but xmllint not in \$PATH!" >&2
	exit 1
fi


echo "*** Checking for compiled utilities..."
if ! [ -f src/can_user_push ] || ! [ -x src/can_user_push ]; then
	echo "ERROR: src/can_user_push is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/can_user_push_http ] || ! [ -x src/can_user_push_http ]; then
	echo "ERROR: src/can_user_push_http is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/getent ] || ! [ -x src/getent ]; then
	echo "ERROR: src/getent is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/get_user_uuid ] || ! [ -x src/get_user_uuid ]; then
	echo "ERROR: src/get_user_uuid is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/list_packs ] || ! [ -x src/list_packs ]; then
	echo "ERROR: src/list_packs is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/peek_packet ] || ! [ -x src/peek_packet ]; then
	echo "ERROR: src/peek_packet is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/rangecgi ] || ! [ -x src/rangecgi ]; then
	echo "ERROR: src/rangecgi is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/readlink ] || ! [ -x src/readlink ]; then
	echo "ERROR: src/readlink is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/strftime ] || ! [ -x src/strftime ]; then
	echo "ERROR: src/strftime is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/throttle ] || ! [ -x src/throttle ]; then
	echo "ERROR: src/throttle is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi
if ! [ -f src/ulimit512 ] || ! [ -x src/ulimit512 ]; then
	echo "ERROR: src/ulimit512 is not built! Did you _REALLY_ read INSTALL?" >&2
	echo "ERROR: perhaps you forgot to run make?" >&2
	exit 1
fi


echo "*** Checking for ezcert..."
if ! [ -f ezcert.git/CACreateCert ] || ! [ -x ezcert.git/CACreateCert ]; then
	echo "ERROR: ezcert.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi


echo "*** Checking for git..."
case "$cfg_git_bin" in /*) :;; *)
	echo 'ERROR: $Girocco::Config::git_bin must be set to an absolute path' >&2
	exit 1
esac
if ! [ -f "$cfg_git_bin" ] || ! [ -x "$cfg_git_bin" ]; then
	echo "ERROR: $cfg_git_bin does not exist or is not executable" >&2
	exit 1
fi
if ! git_version="$("$cfg_git_bin" version)" || [ -z "$git_version" ]; then
	echo "ERROR: $cfg_git_bin version failed" >&2
	exit 1
fi
case "$git_version" in
	[Gg]"it version "*) :;;
	*)
		echo "ERROR: '$cfg_git_bin version' output does not start with 'git version '" >&2
		exit 1
esac
echo "Found $cfg_git_bin $git_version"
git_vernum="$(echo "$git_version" | sed -ne 's/^[^0-9]*\([0-9][0-9]*\(\.[0-9][0-9]*\)*\).*$/\1/p')"
echo "*** Checking Git $git_vernum for compatibility..."
if [ "$(vcmp "$git_vernum" 1.6.6)" -lt 0 ]; then
	echo 'ERROR: $Girocco::Config::git_bin must be at least Git version 1.6.6'
	exit 1
fi
if [ "$(vcmp "$git_vernum" 1.6.6.3)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 1.6.6.3, clients will not see useful error messages'
fi
if [ "$(vcmp "$git_vernum" 1.7.3)" -lt 0 ]; then
	cat <<'EOT'

***
*** SEVERE WARNING: $Girocco::Config::git_bin is set to a version of Git before 1.7.3
***

Some Girocco functionality will be gracefully disabled and other things will
just not work at all such as race condition protection against simultaneous
client pushes and server garbage collections.

EOT
fi
if [ -n "$cfg_mirror" ] && [ "$(vcmp "$git_vernum" 1.7.5)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 1.7.5 and mirroring enabled, some sources can cause an infinite fetch loop'
fi
if [ "$(vcmp "$git_vernum" 1.7.6.6)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 1.7.6.6, performance may be degraded'
fi
if [ "$(uname -m 2>/dev/null)" = "x86_64" ] && [ "$(vcmp "$git_vernum" 1.7.11)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.12.0)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version >= 1.7.11 and < 2.12.0 and x86_64, make sure Git built WITHOUT XDL_FAST_HASH'
	echo 'WARNING: See https://lore.kernel.org/git/20141222041944.GA441@peff.net/ for details'
fi
if [ "$(vcmp "$git_vernum" 1.8.4.2)" -ge 0 ] && [ -n "$cfg_mirror" ] && [ "$(vcmp "$git_vernum" 2)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version >= 1.8.4.2 and < 2.0.0, git-daemon needs write access for shallow clones'
	echo 'WARNING: $Girocco::Config::git_bin version >= 1.8.4.2 and < 2.0.0, shallow clones will leave repository turds'
fi
if [ "$(vcmp "$git_vernum" 1.8.4.3)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 1.8.4.3, clients will not receive symref=HEAD:refs/heads/...'
fi
if [ "$(vcmp "$git_vernum" 2.1)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 2.1.0, pack bitmaps will not be available'
fi
if [ "$(vcmp "$git_vernum" 2.1)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.1.3)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version >= 2.1.0 and < 2.1.3, pack bitmaps may not be reliable, please upgrade to at least Git version 2.1.3'
fi
if [ "$(vcmp "$git_vernum" 2.2)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.3.2)" -lt 0 ]; then
	cat <<'EOT'

***
*** ERROR: $Girocco::Config::git_bin is set to an incompatible version of Git
***

Git versions starting with 2.2.0 and continuing up through 2.3.1 are incompatible
with Girocco due to various unresolved issues.  Please either downgrade to 2.1.4
or earlier or, more preferred, upgrade to 2.3.2 (ideally 2.4.11) or later.

In order to bypass this check you will have to modify install.sh in which case
USE THE SELECTED GIT BINARY AT YOUR OWN RISK!

EOT
	exit 1
fi
if [ "$(vcmp "$git_vernum" 2.3.3)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 2.3.3, performance will be sub-optimal'
fi
if [ "$(vcmp "$git_vernum" 2.4.4)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version < 2.4.4, many refs smart HTTP fetches can deadlock'
fi
if [ "$(vcmp "$git_vernum" 2.10.1)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.12.3)" -lt 0 ]; then
	echo 'WARNING: $Girocco::Config::git_bin version >= 2.10.1 and < 2.12.3, --pickaxe-regex can segfault'
	echo 'WARNING: If gitweb pickaxe regular expression searches are enabled, --pickaxe-regex will be used'
	echo 'WARNING: See the fix at http://repo.or.cz/git.git/f53c5de29cec68e3 for details'
	echo 'WARNING: The fix is trivial and easily cherry-picked into a custom 2.10.1 - 2.12.2 build'
	echo 'WARNING: Leaving the gitweb/gitweb_config.perl "regexp" feature off as recommended avoids the issue'
fi
secmsg=
if [ "$(vcmp "$git_vernum" 2.4.11)" -lt 0 ]; then
	secmsg='prior to 2.4.11'
fi
if [ "$(vcmp "$git_vernum" 2.5)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.5.5)" -lt 0 ]; then
	secmsg='2.5.x prior to 2.5.5'
fi
if [ "$(vcmp "$git_vernum" 2.6)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.6.6)" -lt 0 ]; then
	secmsg='2.6.x prior to 2.6.6'
fi
if [ "$(vcmp "$git_vernum" 2.7)" -ge 0 ] && [ "$(vcmp "$git_vernum" 2.7.4)" -lt 0 ]; then
	secmsg='2.7.x prior to 2.7.4'
fi
if [ -n "$secmsg" ]; then
	cat <<EOT

***
*** SEVERE WARNING: \$Girocco::Config::git_bin is set to a version of Git $secmsg
***

Security issues exist in Git versions prior to 2.4.11, 2.5.x prior to 2.5.5,
2.6.x prior to 2.6.6 and 2.7.x prior to 2.7.4.

Besides the security fixes included in later versions, versions prior to
2.2.0 may accidentally prune unreachable loose objects earlier than
intended.  Since Git version 2.4.11 is the minimum version to include all
security fixes to date, it should be considered the absolute minimum
version of Git to use when running Girocco.

This is not enforced, but Git is easy to build from the git.git submodule
and upgrading to GIT VERSION 2.4.11 OR LATER IS HIGHLY RECOMMENDED.

We will now pause for a moment so you can reflect on this warning.

EOT
	sleep 60
fi
if [ -n "$cfg_mirror" ] && [ "$cfg_mirror" != 0 ] && grep -q ns_parserr "$cfg_git_bin"; then
	cat <<'EOT'

***
*** WARNING: $Girocco::Config::git_bin is set to a questionable Git binary
***

You appear to have enabled mirroring and the Git binary you have selected
appears to contain an experimental patch that cannot be disabled.  This
patch can generate invalid network DNS traffic and/or cause long delays
when fetching using the "git:" protocol when no port number is specified.
It may also end up retrieving repsitory contents from a host other than
the one specified in the "git:" URL when the port is omitted.

You are advised to either build your own version of Git (the problem patch
is not part of the official Git repository) or disable mirroring (via the
$Girocco::Config:mirror setting) to avoid these potential problems.

USE THE SELECTED GIT BINARY AT YOUR OWN RISK!

EOT
	sleep 5
fi

test_nc_U() {
	[ -n "$1" ] || return 1
	_cmdnc="$(command -v "$1" 2>/dev/null)" || :
	[ -n "$_cmdnc" ] && [ -f "$_cmdnc" ] && [ -x "$_cmdnc" ] || return 1
	_tmpdir="$(mktemp -d /tmp/nc-u-XXXXXX)"
	[ -n "$_tmpdir" ] && [ -d "$_tmpdir" ] || return 1
	>"$_tmpdir/output"
	(sleep 3 | "$_cmdnc" -l -U "$_tmpdir/socket" 2>/dev/null >"$_tmpdir/output" || >"$_tmpdir/failed")&
	_bgpid="$!"
	sleep 1
	echo "testing" | "$_cmdnc" -w 1 -U "$_tmpdir/socket" >/dev/null 2>&1 || >"$_tmpdir/failed"
	sleep 1
	kill "$_bgpid" >/dev/null 2>&1 || :
	read -r _result <"$_tmpdir/output" || :
	_bad=
	! [ -e "$_tmpdir/failed" ] || _bad=1
	rm -rf "$_tmpdir"
	[ -z "$_bad" ] && [ "$_result" = "testing" ]
} >/dev/null 2>&1

echo "*** Verifying \$Girocco::Config::nc_openbsd_bin supports -U option..."
test_nc_U "$var_nc_openbsd_bin" || {
	echo "ERROR: invalid Girocco::Config::nc_openbsd_bin setting" >&2
	echo "ERROR: \"$var_nc_openbsd_bin\" does not grok the -U option" >&2
	uname_s="$(uname -s 2>/dev/null | tr A-Z a-z 2>/dev/null)" || :
	case "$uname_s" in
	*dragonfly*)
		echo "ERROR: see the src/dragonfly/README file for a solution" >&2;;
	*kfreebsd*|*linux*)
		echo "ERROR: try installing the package named 'netcat-openbsd'" >&2;;
	esac
	exit 1
}

echo "*** Verifying selected POSIX sh is sane..."
shbin="$var_sh_bin"
[ -n "$shbin" ] && [ -f "$shbin" ] && [ -x "$shbin" ] && [ "$("$shbin" -c 'echo sh $(( 1 + 1 ))' 2>/dev/null)" = "sh 2" ] || {
	echo 'ERROR: invalid $Girocco::Config::posix_sh_bin setting' >&2
	exit 1
}
[ "$(check_sh_builtin command)" = "command" ] || {
	echo 'ERROR: invalid $Girocco::Config::posix_sh_bin setting (does not understand command -v)' >&2
	exit 1
}
sh_not_builtin=
sh_extra_chroot_installs=
badsh=
for sbi in cd pwd read umask unset unalias; do
	if [ "$(check_sh_builtin "$sbi")" != "$sbi" ]; then
		echo "ERROR: invalid \$Girocco::Config::posix_sh_bin setting (missing built-in $sbi)" >&2
		badsh=1
	fi
done
[ -z "$badsh" ] || exit 1
for sbi in '[' echo printf test; do
	if ! extra="$(check_sh_builtin "$sbi")"; then
		echo "ERROR: invalid \$Girocco::Config::posix_sh_bin setting (missing command $sbi)" >&2
		badsh=1
		continue
	fi
	if [ "$extra" != "$sbi" ]; then
		case "$extra" in /*) :;; *)
			echo "ERROR: invalid \$Girocco::Config::posix_sh_bin setting (bad command -v $sbi result: $extra)" >&2
			badsh=1
			continue
		esac
		withspc=
		case "$extra" in *" "*) withspc=1; esac
		[ -z "$withspc" ] && [ -f "$extra" ] && [ -r "$extra" ] && [ -x "$extra" ] || {
			echo "ERROR: invalid \$Girocco::Config::posix_sh_bin setting (unusable command -v $sbi result: $extra)" >&2
			badsh=1
			continue
		}
		echo "WARNING: slow \$Girocco::Config::posix_sh_bin setting (not built-in $sbi)" >&2
		sh_not_builtin="$sh_not_builtin $sbi"
		sh_extra_chroot_installs="$sh_extra_chroot_installs $extra"
	fi
done
[ -z "$badsh" ] || exit 1
[ -z "$sh_extra_chroot_installs" ] || {
	echo "WARNING: the selected POSIX sh implements these as non-built-in:$sh_not_builtin" >&2
	echo "WARNING: as a result it will run slower than necessary" >&2
	echo "WARNING: consider building and switching to dash which can be found at:" >&2
	echo "WARNING:   http://gondor.apana.org.au/~herbert/dash/" >&2
	echo "WARNING: (download a tarball from the files section or clone the Git repository" >&2
	echo "WARNING:  and checkout the latest tag, run autogen.sh, configure and build)" >&2
	echo "WARNING: dash is licensed under the 3-clause BSD license" >&2
}

echo "*** Verifying xargs is sane..."
_xargsr="$(</dev/null command xargs printf %s -r)" || :
xtest1="$(</dev/null command xargs $_xargsr printf 'test %s ' 2>&1)" || :
xtest2="$(printf '%s\n' one two | command xargs $_xargsr printf 'test %s ' 2>&1)" || :
[ -z "$xtest1" ] && [ "$xtest2" = "test one test two " ] || {
	echo 'ERROR: xargs is unusable' >&2
	echo 'ERROR: either `test -z "$(</dev/null xargs echo test 2>&1)"`' >&2
	echo 'ERROR: or `test -z "$(</dev/null xargs -r echo test 2>&1)"`' >&2
	echo 'ERROR: must be true, but neither is' >&2
	exit 1
}

echo "*** Verifying selected perl is sane..."
perlbin="$var_perl_bin"
[ -n "$perlbin" ] && [ -f "$perlbin" ] && [ -x "$perlbin" ] && [ "$("$perlbin" -wle 'print STDOUT "perl ", + ( 1 + 1 )' 2>/dev/null)" = "perl 2" ] || {
	echo 'ERROR: invalid $Girocco::Config::perl_bin setting' >&2
	exit 1
}

echo "*** Verifying selected gzip is sane..."
gzipbin="$var_gzip_bin"
[ -n "$gzipbin" ] && [ -f "$gzipbin" ] && [ -x "$gzipbin" ] && "$gzipbin" -V 2>&1 | grep -q gzip &&
    [ "$(echo Girocco | "$gzipbin" -c -n -9 | "$gzipbin" -c -d)" = "Girocco" ] || {
	echo 'ERROR: invalid $Girocco::Config::gzip_bin setting' >&2
	exit 1
}

echo "*** Verifying basedir, webroot and cgiroot paths..."
# Make sure $cfg_basedir, $cfg_webroot and $cfg_cgiroot are absolute paths
case "$cfg_basedir" in /*) :;; *)
	echo "ERROR: invalid Girocco::Config::basedir setting" >&2
	echo "ERROR: \"$cfg_basedir\" must be an absolute path (start with '/')" >&2
	exit 1
esac
case "$cfg_webroot" in /*) :;; *)
	echo "ERROR: invalid Girocco::Config::webroot setting" >&2
	echo "ERROR: \"$cfg_webroot\" must be an absolute path (start with '/')" >&2
	exit 1
esac
case "$cfg_cgiroot" in /*) :;; *)
	echo "ERROR: invalid Girocco::Config::cgiroot setting" >&2
	echo "ERROR: \"$cfg_cgiroot\" must be an absolute path (start with '/')" >&2
	exit 1
esac

# return the input with trailing slashes stripped but return "/" for all "/"s
striptrsl() {
	[ -n "$1" ] || return 0
	_s="${1##*[!/]}"
	[ "$_s" != "$1" ] || _s="${_s#?}"
	printf "%s\n" "${1%$_s}"
}

# a combination of realpath + dirname where the realpath of the deepest existing
# directory is returned with the rest of the non-existing components appended
# and trailing slashes and multiple slashes are removed
realdir() {
	_d="$(striptrsl "$1")"
	if [ "$_d" = "/" ] || [ -z "$_d" ]; then
		echo "$_d"
		return 0
	fi
	_c=""
	while ! [ -d "$_d" ]; do
		_c="/$(basename "$_d")$_c"
		_d="$(dirname "$_d")"
		[ "$_d" != "/" ] || _c="${_c#/}"
	done
	printf "%s%s\n" "$(cd "$_d" && pwd -P)" "$_c"
}

# Use basedir, webroot and cgiroot for easier control of filesystem locations
# Wherever we are writing/copying/installing files we use these, but where we
# are editing, adding config settings or printing advice we always stick to the
# cfg_xxx Config variable versions.  These are like a set of DESTDIR variables.
# Only the file system directories that could be asynchronously accessed (by
# the web server, jobd.pl, taskd.pl or incoming pushes) get these special vars.
# The chroot is handled specially and does not need one of these.
# We must be careful to allow cgiroot and/or webroot to be under basedir in which
# case the prior contents of cgiroot and/or webroot are discarded.
rbasedir="$(realdir "$cfg_basedir")"
rwebroot="$(realdir "$cfg_webroot")"
rcgiroot="$(realdir "$cfg_cgiroot")"
case "$rbasedir" in "$rwebroot"/?*)
	echo "ERROR: invalid Girocco::Config::basedir setting; must not be under webroot" >&2
	exit 1
esac
case "$rbasedir" in "$rcgiroot"/?*)
	echo "ERROR: invalid Girocco::Config::basedir setting; must not be under cgiroot" >&2
	exit 1
esac
if [ "$rwebroot" = "$rcgiroot" ]; then
	echo "ERROR: invalid Girocco::Config::webroot and Girocco::Config::cgiroot settings; must not be the same" >&2
	exit 1
fi
case "$rcgiroot" in "$rwebroot"/?*)
	echo "ERROR: invalid Girocco::Config::cgiroot setting; must not be under webroot" >&2
	exit 1
esac
case "$rwebroot" in "$rcgiroot"/?*)
	echo "ERROR: invalid Girocco::Config::webroot setting; must not be under cgiroot" >&2
	exit 1
esac
basedir="$rbasedir-new"
case "$rwebroot" in
	"$rbasedir"/?*)
		webroot="$basedir${rwebroot#$rbasedir}"
		webrootsub=1
		;;
	*)
		webroot="$rwebroot-new"
		webrootsub=
		;;
esac
case "$rcgiroot" in
	"$rbasedir"/?*)
		cgiroot="$basedir${rcgiroot#$rbasedir}"
		cgirootsub=1
		;;
	*)
		cgiroot="$rcgiroot-new"
		cgirootsub=
		;;
esac

echo "*** Setting up basedir..."

chown_make() {
	if [ "$LOGNAME" = root ] && [ -n "$SUDO_USER" ] && [ "$SUDO_USER" != root ]; then
		find -H "$@" -user root -exec chown "$SUDO_USER:$(id -gn "$SUDO_USER")" '{}' + 2>/dev/null || :
	elif [ "$LOGNAME" = root ] && { [ -z "$SUDO_USER" ] || [ "$SUDO_USER" = root ]; }; then
		echo "*** WARNING: running make as root w/o sudo may leave root-owned: $*"
	fi
}

"$MAKE" --no-print-directory --silent apache.conf
chown_make apache.conf
"$MAKE" --no-print-directory --silent -C src
chown_make src
rm -fr "$basedir"
mkdir -p "$basedir" "$basedir/gitweb" "$basedir/cgi"
cp cgi/*.cgi "$basedir/cgi"
cp -pR Girocco jobd taskd html jobs toolbox hooks apache.conf shlib.sh bin screen "$basedir"
find -H "$basedir" -type l -exec rm -f '{}' +
cp -p src/can_user_push src/can_user_push_http src/get_user_uuid src/list_packs src/peek_packet \
	src/rangecgi src/readlink src/strftime src/throttle src/ulimit512 \
	ezcert.git/CACreateCert cgi/authrequired.cgi cgi/snapshot.cgi \
	"$basedir/bin"
cp -p gitweb/*.sh gitweb/*.perl "$basedir/gitweb"
if [ -n "$cfg_httpspushurl" ]; then
	[ -z "$cfg_pretrustedroot" ] || rm -f "$basedir"/html/rootcert.html
else
	rm -f "$basedir"/html/rootcert.html "$basedir"/html/httpspush.html
fi
[ -n "$cfg_mob" ] || rm -f "$basedir"/html/mob.html

# Put the correct Config in place
[ "$GIROCCO_CONF" = "Girocco::Config" ] || cp "$(echo "$GIROCCO_CONF" | sed 's#::#/#g; s/$/.pm/')" "$basedir/Girocco/Config.pm"

# Create symbolic links to selected binaries
ln -s "$cfg_git_bin" "$basedir/bin/git"
ln -s "$shbin" "$basedir/bin/sh"
ln -s "$perlbin" "$basedir/bin/perl"
ln -s "$gzipbin" "$basedir/bin/gzip"

echo "*** Preprocessing scripts..."
SHBIN="$shbin" && export SHBIN
PERLBIN="$perlbin" && export PERLBIN
perl -I"$PWD" -M$GIROCCO_CONF -i -p \
	-e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
	-e 's/^#!.*sh/#!$ENV{SHBIN}/ if $. == 1;' \
	-e 's/(?<!")\@basedir\@/"$Girocco::Config::basedir"/g;' \
	-e 's/(?<=")\@basedir\@/$Girocco::Config::basedir/g;' \
	-e 's/__BASE''DIR__/$Girocco::Config::basedir/g;' \
	-e 's/\@reporoot\@/"$Girocco::Config::reporoot"/g;' \
	-e 's/\@shbin\@/"$ENV{SHBIN}"/g;' \
	-e 's/\@perlbin\@/"$ENV{PERLBIN}"/g;' \
	-e 's/\@jailreporoot\@/"$Girocco::Config::jailreporoot"/g;' \
	-e 's/\@chroot\@/"$Girocco::Config::chroot"/g;' \
	-e 's/\@webadmurl\@/"$Girocco::Config::webadmurl"/g;' \
	-e 's/\@screen_acl_file\@/"$Girocco::Config::screen_acl_file"/g;' \
	-e 's/\@mob\@/"$Girocco::Config::mob"/g;' \
	-e 's/\@autogchack\@/"$Girocco::Config::autogchack"/g;' \
	-e 's/\@git_server_ua\@/"$Girocco::Config::git_server_ua"/g;' \
	-e 's/\@defined_git_server_ua\@/defined($Girocco::Config::git_server_ua)/ge;' \
	-e 's/\@git_no_mmap\@/"$Girocco::Config::git_no_mmap"/g;' \
	-e 's/\@big_file_threshold\@/"'"$var_big_file_threshold"'"/g;' \
	-e 's/\@upload_pack_window\@/"'"$var_upload_window"'"/g;' \
	-e 's/\@fetch_stash_refs\@/"$Girocco::Config::fetch_stash_refs"/g;' \
	-e 'close ARGV if eof;' \
	"$basedir"/jobs/*.sh "$basedir"/jobd/*.sh \
	"$basedir"/taskd/*.sh "$basedir"/gitweb/*.sh \
	"$basedir"/shlib.sh "$basedir"/hooks/* \
	"$basedir"/toolbox/*.sh "$basedir"/toolbox/*.pl \
	"$basedir"/toolbox/reports/*.sh \
	"$basedir"/bin/git-* "$basedir"/bin/*.sh \
	"$basedir"/bin/create-* "$basedir"/bin/update-* \
	"$basedir"/bin/*.cgi "$basedir"/screen/*
perl -I"$PWD" -M$GIROCCO_CONF -i -p \
	-e 's/__BASE''DIR__/$Girocco::Config::basedir/g;' \
	"$basedir"/cgi/*.cgi "$basedir"/gitweb/*.perl \
	"$basedir"/jobd/*.pl "$basedir"/taskd/*.pl
perl -i -p \
	-e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
	-e 'close ARGV if eof;' \
	"$basedir"/jobd/jobd.pl "$basedir"/taskd/taskd.pl \
	"$basedir"/bin/sendmail.pl "$basedir"/bin/CACreateCert
perl -i -p \
	-e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
	-e 's/^#!.*sh/#!$ENV{SHBIN}/ if $. == 1;' \
	-e 'close ARGV if eof;' \
	"$basedir"/bin/format-readme "$basedir/cgi"/*.cgi
unset PERLBIN
unset SHBIN

# Dump all the cfg_ and defined_ variables to shlib_vars.sh
get_girocco_config_var_list >"$basedir"/shlib_vars.sh

echo "*** Setting up darcs-fast-export from bzr-fastimport.git..."
if ! [ -f bzr-fastimport.git/exporters/darcs/darcs-fast-export ] ||
   ! [ -x bzr-fastimport.git/exporters/darcs/darcs-fast-export ]; then
	echo "ERROR: bzr-fastimport.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi
mkdir -p "$basedir"/bin
cp bzr-fastimport.git/exporters/darcs/darcs-fast-export "$basedir"/bin

echo "*** Setting up hg-fast-export from fast-export.git..."
if ! [ -f fast-export.git/hg-fast-export.py ] || ! [ -f fast-export.git/hg2git.py ]; then
	echo "ERROR: fast-export.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi
mkdir -p "$basedir"/bin
cp fast-export.git/hg-fast-export.py fast-export.git/hg2git.py "$basedir"/bin

echo "*** Setting up markdown from markdown.git..."
if ! [ -f markdown.git/Markdown.pl ]; then
	echo "ERROR: markdown.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi
mkdir -p "$basedir"/bin
(PERLBIN="$perlbin" && export PERLBIN &&
	perl -p -e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
		markdown.git/Markdown.pl >"$basedir"/bin/Markdown.pl.$$ &&
	chmod a+x "$basedir"/bin/Markdown.pl.$$ &&
	mv -f "$basedir"/bin/Markdown.pl.$$ "$basedir"/bin/Markdown.pl)
test $? -eq 0

# Some permission sanity on basedir/bin just in case
find -H "$basedir"/bin -type f -exec chmod go-w '{}' +
chown -R -h "$cfg_mirror_user""$owngroup" "$basedir"/bin

if [ -n "$cfg_mirror" ]; then
	echo "--- Remember to start $cfg_basedir/taskd/taskd.pl"
fi
echo "--- Also remember to either start $cfg_basedir/jobd/jobd.pl, or add this"
echo "--- to the crontab of $cfg_mirror_user (adjust frequency on number of repos):"
echo "*/30 * * * * /usr/bin/nice -n 18 $cfg_basedir/jobd/jobd.pl -q --all-once"


echo "*** Setting up repository root..."
[ -d "$cfg_reporoot" ] || {
	mkdir -p "$cfg_reporoot"
	chown "$cfg_mirror_user""$owngroup" "$cfg_reporoot" ||
	echo "WARNING: Cannot chown $cfg_mirror_user$owngroup $cfg_reporoot"
}
[ -z "$cfg_owning_group" ] ||
	chgrp "$cfg_owning_group" "$cfg_reporoot" || echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_reporoot"
chmod 02775 "$cfg_reporoot" || echo "WARNING: Cannot chmod $cfg_reporoot properly"
mkdir -p "$cfg_reporoot/_recyclebin" "$cfg_reporoot/_global/hooks" "$cfg_reporoot/_global/empty"
chown "$cfg_mirror_user""$owngroup" "$cfg_reporoot/_recyclebin" "$cfg_reporoot/_global" "$cfg_reporoot/_global/hooks" "$cfg_reporoot/_global/empty" ||
	echo "WARNING: Cannot chown $cfg_mirror_user$owngroup $cfg_reporoot/{_recyclebin,_global} properly"
if [ "$cfg_owning_group" ]; then
	chgrp "$cfg_owning_group" "$cfg_reporoot/_recyclebin" || echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_reporoot/_recyclebin"
	chgrp -R "$cfg_owning_group" "$cfg_reporoot/_global" || echo "WARNING: Cannot chgrp -R $cfg_owning_group $cfg_reporoot/_global"
fi
chmod 02775 "$cfg_reporoot/_recyclebin" || echo "WARNING: Cannot chmod $cfg_reporoot/_recyclebin properly"
chmod 00755 "$cfg_reporoot/_global" "$cfg_reporoot/_global/hooks" "$cfg_reporoot/_global/empty" || echo "WARNING: Cannot chmod $cfg_reporoot/_global properly"


if [ "${cfg_disable_jailsetup:-0}" = "0" ] && [ -n "$cfg_chrooted" ]; then
	echo "*** Setting up chroot jail for pushing..."
	if [ "$(id -u)" -eq 0 ]; then
		# jailsetup may install things from $cfg_basedir/bin into the
		# chroot so we do a mini-update of just that portion now
		mkdir -p "$cfg_basedir"
		rm -rf "$cfg_basedir/bin-new"
		cp -pR "$basedir/bin" "$cfg_basedir/bin-new" >/dev/null 2>&1
		rm -rf "$cfg_basedir/bin-old"
		quick_move "$cfg_basedir/bin-new" "$cfg_basedir/bin" "$cfg_basedir/bin-old"
		rm -rf "$cfg_basedir/bin-old"
		if [ -n "$sh_extra_chroot_installs" ]; then
			GIROCCO_CHROOT_EXTRA_INSTALLS="$sh_extra_chroot_installs"
			export GIROCCO_CHROOT_EXTRA_INSTALLS
		fi
		./jailsetup.sh
		unset GIROCCO_CHROOT_EXTRA_INSTALLS
	else
		echo "WARNING: Skipping jail setup, not root"
	fi
fi


echo "*** Setting up jail configuration (project database)..."
[ "$(id -u)" -eq 0 ] || ./jailsetup.sh dbonly
mkdir -p "$cfg_chroot" "$cfg_chroot/etc"
touch "$cfg_chroot/etc/passwd" "$cfg_chroot/etc/group"
chown "$cfg_mirror_user""$owngroup" "$cfg_chroot/etc" ||
	echo "WARNING: Cannot chown $cfg_mirror_user$owngroup $cfg_chroot/etc"
chown "$cfg_cgi_user""$owngroup" "$cfg_chroot/etc/passwd" "$cfg_chroot/etc/group" ||
	echo "WARNING: Cannot chown $cfg_cgi_user$owngroup the etc/passwd and/or etc/group files"
chmod g+w "$cfg_chroot/etc/passwd" "$cfg_chroot/etc/group" ||
	echo "WARNING: Cannot chmod g+w the etc/passwd and/or etc/group files"
chmod 02775 "$cfg_chroot/etc" || echo "WARNING: Cannot chmod 02775 $cfg_chroot/etc"


echo "*** Setting up global hook scripts..."
# It is absolutely CRUCIAL that hook script replacements are done atomically!
# Otherwise an incoming push might slip in and fail to run the hook script!
# The underlying rename(2) function call provides this and mv will use it.
# First add hook scripts
hooks="pre-auto-gc pre-receive post-commit post-receive update"
for hook in $hooks; do
	cat "$basedir/hooks/$hook" >"$cfg_reporoot/_global/hooks/$hook.$$"
	chown "$cfg_mirror_user""$owngroup" "$cfg_reporoot/_global/hooks/$hook.$$" ||
		echo "WARNING: Cannot chown $cfg_reporoot/_global/hooks/$hook"
	chmod 0755 "$cfg_reporoot/_global/hooks/$hook.$$"
	mv -f "$cfg_reporoot/_global/hooks/$hook.$$" "$cfg_reporoot/_global/hooks/$hook"
done
# Then remove any hook scripts that do not belong
for hook in "$cfg_reporoot/_global/hooks"/*; do
	hook="${hook##*/}"
	[ -f "$cfg_reporoot/_global/hooks/$hook" ] || continue
	case " $hooks " in *" $hook "*);;*)
		rm -f "$cfg_reporoot/_global/hooks/$hook" ||
			echo "WARNING: Cannot remove extraneous $cfg_reporoot/_global/hooks/$hook"
	esac
done


echo "*** Setting up gitweb from git.git..."
if ! [ -f git.git/Makefile ]; then
	echo "ERROR: git.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi

# We do not wholesale replace either webroot or cgiroot unless they are under
# basedir so if they exist and are not we make a copy to start working on them.
# We make a copy using -p which can result in some warnings so we suppress
# error output as it's of no consequence in this case.
rm -rf "$webroot" "$cgiroot"
[ -n "$webrootsub" ] || ! [ -d "$rwebroot" ] || cp -pR "$rwebroot" "$webroot" >/dev/null 2>&1 || :
[ -n "$cgirootsub" ] || ! [ -d "$rcgiroot" ] || cp -pR "$rcgiroot" "$cgiroot" >/dev/null 2>&1 || :
mkdir -p "$webroot" "$cgiroot"

(
	cd git.git &&
	"$MAKE" --no-print-directory --silent NO_SUBDIR=: bindir="$(dirname "$cfg_git_bin")" \
		GITWEB_CONFIG_COMMON="" GITWEB_CONFIG_SYSTEM="" \
		GITWEB_CONFIG="$cfg_basedir/gitweb/gitweb_config.perl" SHELL_PATH="$shbin" gitweb &&
	chown_make gitweb &&
	PERLBIN="$perlbin" && export PERLBIN &&
	perl -p -e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
		-e 's/^(\s*use\s+warnings\s*;.*)$/#$1/;' gitweb/gitweb.cgi >"$cgiroot"/gitweb.cgi.$$ &&
	chmod a+x "$cgiroot"/gitweb.cgi.$$ &&
	chown_make "$cgiroot"/gitweb.cgi.$$ &&
	mv -f "$cgiroot"/gitweb.cgi.$$ "$cgiroot"/gitweb.cgi &&
	cp gitweb/static/*.png gitweb/static/*.css gitweb/static/*.js "$webroot"
)
test $? -eq 0


echo "*** Setting up git-browser from git-browser.git..."
if ! [ -f git-browser.git/git-browser.cgi ]; then
	echo "ERROR: git-browser.git is not checked out! Did you _REALLY_ read INSTALL?" >&2
	exit 1
fi
mkdir -p "$webroot"/git-browser "$cgiroot"
(
	cd git-browser.git &&
	CFG="$cfg_basedir/gitweb/git-browser.conf" && export CFG &&
	PERLBIN="$perlbin" && export PERLBIN && perl -p \
		-e 's/^#!.*perl/#!$ENV{PERLBIN}/ if $. == 1;' \
		-e 's/"git-browser\.conf"/"$ENV{"CFG"}"/' git-browser.cgi >"$cgiroot"/git-browser.cgi.$$ &&
	chmod a+x "$cgiroot"/git-browser.cgi.$$ &&
	chown_make "$cgiroot"/git-browser.cgi.$$ &&
	mv -f "$cgiroot"/git-browser.cgi.$$ "$cgiroot"/git-browser.cgi &&
	cp -r *.html *.js *.css js.lib "$webroot"/git-browser &&
	cp -r JSON "$cgiroot"
)
test $? -eq 0
rm -f "$webroot"/git-browser/index.html
cat >"$basedir/gitweb"/git-browser.conf.$$ <<-EOT
	gitbin: $cfg_git_bin
	warehouse: $cfg_reporoot
	doconfig: $cfg_basedir/gitweb/gitbrowser_config.perl
EOT
chown_make "$basedir/gitweb"/git-browser.conf.$$
mv -f "$basedir/gitweb"/git-browser.conf.$$ "$basedir/gitweb"/git-browser.conf
cat >"$webroot"/git-browser/GitConfig.js.$$ <<-EOT
	cfg_gitweb_url="$cfg_gitweburl/"
	cfg_browsercgi_url="$cfg_webadmurl/git-browser.cgi"
EOT
chown_make "$webroot"/git-browser/GitConfig.js.$$
mv -f "$webroot"/git-browser/GitConfig.js.$$ "$webroot"/git-browser/GitConfig.js


echo "*** Setting up our part of the website..."
mkdir -p "$webroot" "$cgiroot"
cp "$basedir"/bin/snapshot.cgi "$basedir/cgi"
cp "$basedir"/bin/authrequired.cgi "$basedir/cgi"
[ -n "$cfg_httpspushurl" ] || rm -f "$basedir/cgi"/usercert.cgi "$cgiroot"/usercert.cgi
cp "$basedir/cgi"/*.cgi "$cgiroot"
rm -rf "$basedir/cgi"
[ -z "$cfg_webreporoot" ] || { rm -f "$cfg_webreporoot" && ln -s "$cfg_reporoot" "$cfg_webreporoot"; }
if [ -z "$cfg_httpspushurl" ] || [ -n "$cfg_pretrustedroot" ]; then
	grep -v 'rootcert[.]html' gitweb/indextext.html >"$basedir/gitweb/indextext.html"
else
	cp gitweb/indextext.html "$basedir/gitweb"
fi
mv "$basedir"/html/*.css "$basedir"/html/*.js "$webroot"
cp mootools.js "$webroot"
cp htaccess "$webroot/.htaccess"
cp cgi/htaccess "$cgiroot/.htaccess"
cp git-favicon.ico "$webroot/favicon.ico"
cp robots.txt "$webroot"
cat gitweb/gitweb.css >>"$webroot"/gitweb.css


if [ -n "$cfg_httpspushurl" ]; then
	echo "*** Setting up SSL certificates..."
	bits=2048
	if [ "$cfg_rsakeylength" -gt "$bits" ] 2>/dev/null; then
	    bits="$cfg_rsakeylength"
	fi
	mkdir -p "$cfg_certsdir"
	[ -d "$cfg_certsdir" ]
	wwwcertcn=
	if [ -e "$cfg_certsdir/girocco_www_crt.pem" ]; then
		wwwcertcn="$(
			openssl x509 -in "$cfg_certsdir/girocco_www_crt.pem" -noout -subject |
			sed -e 's,[^/]*,,'
		)"
	fi
	wwwcertdns=
	if [ -n "$cfg_wwwcertaltnames" ]; then
		for dnsopt in $cfg_wwwcertaltnames; do
			wwwcertdns="${wwwcertdns:+$wwwcertdns }--dns $dnsopt"
		done
	fi
	wwwcertdnsfile=
	if [ -r "$cfg_certsdir/girocco_www_crt.dns" ]; then
		wwwcertdnsfile="$(cat "$cfg_certsdir/girocco_www_crt.dns")"
	fi
	needroot=
	[ -e "$cfg_certsdir/girocco_client_crt.pem" ] &&
	[ -e "$cfg_certsdir/girocco_client_key.pem" ] &&
	[ -e "$cfg_certsdir/girocco_www_key.pem" ] &&
	[ -e "$cfg_certsdir/girocco_www_crt.pem" ] && [ "$wwwcertcn" = "/CN=$cfg_httpsdnsname" ] &&
	[ -e "$cfg_certsdir/girocco_root_crt.pem" ] || needroot=1
	if [ -n "$needroot" ] && ! [ -e "$cfg_certsdir/girocco_root_key.pem" ]; then
		rm -f "$cfg_certsdir/girocco_root_crt.pem" "$cfg_certsdir/girocco_root_key.pem"
		umask 0077
		openssl genrsa -f4 -out "$cfg_certsdir/girocco_root_key.pem" $bits
		chmod 0600 "$cfg_certsdir/girocco_root_key.pem"
		rm -f "$cfg_certsdir/girocco_root_crt.pem"
		umask 0022
		echo "Created new root key"
	fi
	if ! [ -e "$cfg_certsdir/girocco_root_crt.pem" ]; then
		"$basedir/bin/CACreateCert" --root --key "$cfg_certsdir/girocco_root_key.pem" \
			--out "$cfg_certsdir/girocco_root_crt.pem" "girocco $cfg_nickname root certificate"
		rm -f "$cfg_certsdir/girocco_www_crt.pem" "$cfg_certsdir/girocco_www_chain.pem"
		rm -f "$cfg_certsdir/girocco_client_crt.pem" "$cfg_certsdir/girocco_client_suffix.pem"
		rm -f "$cfg_certsdir/girocco_mob_user_crt.pem"
		rm -f "$cfg_chroot/etc/sshcerts"/*.pem
		echo "Created new root certificate"
	fi
	if ! [ -e "$cfg_certsdir/girocco_www_key.pem" ]; then
		umask 0077
		openssl genrsa -f4 -out "$cfg_certsdir/girocco_www_key.pem" $bits
		chmod 0600 "$cfg_certsdir/girocco_www_key.pem"
		rm -f "$cfg_certsdir/girocco_www_crt.pem"
		umask 0022
		echo "Created new www key"
	fi
	if ! [ -e "$cfg_certsdir/girocco_www_crt.pem" ] ||
		[ "$wwwcertcn" != "/CN=$cfg_httpsdnsname" ] || [ "$wwwcertdns" != "$wwwcertdnsfile" ]; then
		openssl rsa -in "$cfg_certsdir/girocco_www_key.pem" -pubout |
		"$basedir/bin/CACreateCert" --server --key "$cfg_certsdir/girocco_root_key.pem" \
			--cert "$cfg_certsdir/girocco_root_crt.pem" $wwwcertdns \
			--out "$cfg_certsdir/girocco_www_crt.pem" "$cfg_httpsdnsname"
		printf '%s\n' "$wwwcertdns" >"$cfg_certsdir/girocco_www_crt.dns"
		echo "Created www certificate"
	fi
	if ! [ -e "$cfg_certsdir/girocco_www_chain.pem" ]; then
		cat "$cfg_certsdir/girocco_root_crt.pem" >"$cfg_certsdir/girocco_www_chain.pem"
		echo "Created www certificate chain file"
	fi
	if ! [ -e "$cfg_certsdir/girocco_client_key.pem" ]; then
		umask 0037
		openssl genrsa -f4 -out "$cfg_certsdir/girocco_client_key.pem" $bits
		chmod 0640 "$cfg_certsdir/girocco_client_key.pem"
		rm -f "$cfg_certsdir/girocco_client_crt.pem"
		umask 0022
		echo "Created new client key"
	fi
	if ! [ -e "$cfg_certsdir/girocco_client_crt.pem" ]; then
		openssl rsa -in "$cfg_certsdir/girocco_client_key.pem" -pubout |
		"$basedir/bin/CACreateCert" --subca --key "$cfg_certsdir/girocco_root_key.pem" \
			--cert "$cfg_certsdir/girocco_root_crt.pem" \
			--out "$cfg_certsdir/girocco_client_crt.pem" "girocco $cfg_nickname client authority"
		rm -f "$cfg_certsdir/girocco_client_suffix.pem"
		rm -f "$cfg_certsdir/girocco_mob_user_crt.pem"
		rm -f "$cfg_chroot/etc/sshcerts"/*.pem
		echo "Created client certificate"
	fi
	if ! [ -e "$cfg_certsdir/girocco_client_suffix.pem" ]; then
		cat "$cfg_certsdir/girocco_client_crt.pem" >"$cfg_certsdir/girocco_client_suffix.pem"
		echo "Created client certificate suffix file"
	fi
	if [ -z "$cfg_pretrustedroot" ]; then
		cat "$cfg_rootcert" >"$webroot/${cfg_nickname}_root_cert.pem"
	else
		rm -f "$webroot/${cfg_nickname}_root_cert.pem"
	fi
	if [ -n "$cfg_mob" ]; then
		if ! [ -e "$cfg_certsdir/girocco_mob_user_key.pem" ]; then
			openssl genrsa -f4 -out "$cfg_certsdir/girocco_mob_user_key.pem" $bits
			chmod 0644 "$cfg_certsdir/girocco_mob_user_key.pem"
			rm -f "$cfg_certsdir/girocco_mob_user_crt.pem"
			echo "Created new mob user key"
		fi
		if ! [ -e "$cfg_certsdir/girocco_mob_user_crt.pem" ]; then
			openssl rsa -in "$cfg_mobuserkey" -pubout |
			"$basedir/bin/CACreateCert" --client --key "$cfg_clientkey" \
				--cert "$cfg_clientcert" \
				--out "$cfg_certsdir/girocco_mob_user_crt.pem" 'mob'
			echo "Created mob user client certificate"
		fi
		cat "$cfg_mobuserkey" >"$webroot/${cfg_nickname}_mob_key.pem"
		cat "$cfg_mobusercert" "$cfg_clientcertsuffix" >"$webroot/${cfg_nickname}_mob_user.pem"
	else
		rm -f "$webroot/${cfg_nickname}_mob_key.pem" "$webroot/${cfg_nickname}_mob_user.pem"
	fi
else
	rm -f "$webroot/${cfg_nickname}_root_cert.pem"
	rm -f "$webroot/${cfg_nickname}_mob_key.pem" "$webroot/${cfg_nickname}_mob_user.pem"
fi


echo "*** Finalizing permissions and moving into place..."
chown -R -h "$cfg_mirror_user""$owngroup" "$basedir" "$webroot" "$cgiroot"
[ -z "$cfg_httpspushurl" ] || chown -R -h "$cfg_mirror_user""$owngroup" "$cfg_certsdir"

# This should always be the very last thing install.sh does
rm -rf "$rbasedir-old" "$rwebroot-old" "$rcgiroot-old"
quick_move "$basedir" "$rbasedir" "$rbasedir-old"
[ -n "$webrootsub" ] || quick_move "$webroot" "$rwebroot" "$rwebroot-old"
[ -n "$cgirootsub" ] || quick_move "$cgiroot" "$rcgiroot" "$rcgiroot-old"
rm -rf "$rbasedir-old" "$rwebroot-old" "$rcgiroot-old"
echo "--- Update hooks and config with $cfg_basedir/toolbox/update-all-projects.sh"
! [ -S "$cfg_chroot/etc/taskd.socket" ] || {
	echo "*** Requesting graceful restart of running taskd (and, if running, jobd)..."
	touch "$cfg_chroot/etc/taskd.restart"
	chown_make "$cfg_chroot/etc/taskd.restart"
	trap ':' PIPE
	echo "nop" | nc_openbsd -w 5 -U "$cfg_chroot/etc/taskd.socket" || :
	trap - PIPE
}
