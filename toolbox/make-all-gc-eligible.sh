#!/bin/sh

# Unset gitweb.lastreceive in all projects so that the next time
# each project reaches its gc interval it will run gc

set -e

. @basedir@/shlib.sh

umask 002

base="$cfg_reporoot"
cut -d : -f 1 <"$cfg_chroot/etc/group" | grep -v "^_repo" |
(
	count=0
	while read proj; do
		projdir="$base/$proj.git"
		if git --git-dir="$projdir" config --unset gitweb.lastreceive 2>/dev/null; then
			count=$(( $count + 1 ))
			if [ $(( $count % 10 )) = 0 ]; then
				printf '%s ' $count
			fi
		fi
	done
	if [ $(( $count % 10 )) != 0 ]; then
		printf '%s ' $count
	fi
	printf '%s\n' 'done'
)
