#!/bin/sh

# Report on project disk use.
# Output can be sent as email to admin with -m
# Automatically runs with nice and ionice (if available)

# Usage: project-disk-use [-m] [top-n-only]

# With -m mail the report to $cfg_admin instead of sending it to stdout

# Shows total disk usage in K bytes for $cfg_reporoot
# The top-n-only (all if not given) repos are then listed by
# decreasing order of disk space.

# Note that any *.git directories that are found in $cfg_reporoot that are
# not listed in $cfg_chroot/etc/group ARE included and have a '!' suffix added.

set -e

datefmt='%Y-%m-%d %H:%M:%S %z'
startdate="$(date "+$datefmt")"

. @basedir@/shlib.sh

mailresult=
if [ "$1" = "-m" ]; then
	shift
	mailresult=1
fi

hasnice=
! command -v nice >/dev/null || hasnice=1
hasionice=
! command -v ionice >/dev/null || hasionice=1

nl='
'

fmtcomma() {
	# remove leading 0s
	_y="${1%%[1-9]*}"; _x="${1#$_y}"; _x="${_x:-0}"
	# add commas after each group of 3
	_y=
	while [ $_x -gt 999 ]; do
		_y="$(printf '%03d' $(($_x - $_x / 1000 * 1000)))${_y:+,$_y}"
		_x="$(($_x/1000))"
	done
	[ $_x -eq 0 ] || _y="$_x${_y:+,$_y}"
	echo "${_y:-0}"
}

fmtpct() {
	if [ -z "$1" ] || [ "$1" = "0" ]; then return; fi
	if [ -z "$2" ] || [ "$2" = "0" ]; then return; fi
	_fmtpct=$(( ( $1 * 100 + $2 / 2 ) / $2 ))
	if [ $_fmtpct -le 100 ]; then
		_of=
		[ -z "$4" ] || _of=" of $(fmtcomma $2)"
		echo "${3:- }($_fmtpct%$_of)"
	fi
}

get_use_k() {
	_targ="$1"
	shift
	_cmd="du $var_du_follow -k -s"
	if [ -n "$var_du_exclude" ]; then
		while [ -n "$1" ]; do
			_cmd="$_cmd $var_du_exclude \"$1\""
			shift
		done
	elif [ $# -ne 0 ]; then
		echo "get_use_k: error: no du exclude option available" >&2
		exit 1
	fi
	_cmd="$_cmd \"$_targ\" 2>/dev/null | cut -f 1"
	[ -z "$hasionice" ] || _cmd="ionice -c 3 $_cmd"
	[ -z "$hasnice" ] || _cmd="nice -n 19 $_cmd"
	eval "$_cmd"
}

is_active() (
	cd "$cfg_reporoot/$1.git" || return 1
	check_interval lastchange 2592000 # 30 days
)

projlist="$(cut -d : -f 1 <"$cfg_chroot/etc/group")"

is_listed_proj() {
  echo "$projlist" | grep -q -e "^$1$"
}

totaluse="$(get_use_k "$cfg_reporoot")"
globaluse="$(get_use_k "$cfg_reporoot/_global")"
totaluse="$(( $totaluse - $globaluse ))"
if [ -n "$var_du_exclude" ]; then
	# Attribute all hard-linked stuff in _recyclebin to non-_recyclebin
	inuse="$(get_use_k "$cfg_reporoot" "_recyclebin")"
	inuse="$(( $inuse - $globaluse ))"
	binned="$(( $totaluse - $inuse ))"
else
	# No du exclude option, if binned contains hard links into other repos
	# its size may appear larger than it actually is and the kpct will be off
	binned="$(get_use_k "$cfg_reporoot/_recyclebin")"
	inuse="$(( $totaluse - $binned ))"
fi

cd "$cfg_reporoot"
absroot="$(pwd -P)"
total=0
ktotal=0
howmany=0
orphans=0
banged=0
bangsent=0
mirrors=0
forks=0
mactive=0
pactive=0
results=
while IFS='' read -r proj; do
	external=
	absproj="$(cd "$proj" && pwd -P)" && [ -n "$absproj" ] && [ -d "$absproj" ] || continue
	if [ -L "$proj" ]; then
		# symlinks to elsewhere under $cfg_reporoot are ignored
		# symlinks to outside there are reported on unless orphaned
		case "$absproj" in "$absroot"/*)
			continue
		esac
	fi
	case "$absproj" in "$absroot"/*);;*)
		external=1
	esac
	proj="${proj#./}"
	proj="${proj%.git}"
	x=
	a=
	if ! is_listed_proj "$proj"; then
		[ -z "$external" ] || continue
		x='!'
		orphans="$(( $orphans + 1 ))"
	fi
	case "$proj" in */*) forks="$(( $forks + 1 ))"; esac
	mirror="$(get_mirror_type "$proj.git" 2>/dev/null)" || :
	[ -z "$mirror" ] || mirrors="$(( $mirrors + 1 ))"
	if is_active "$proj"; then
		a='*'
		if [ -n "$mirror" ]; then
			mactive="$(( $mactive + 1 ))"
		else
			pactive="$(( $pactive + 1 ))"
		fi
	fi
	: ${mirror:=M}
	usek="$(get_use_k "$proj.git")"
	repok=
	girocco_reposizek=
	girocco_bang_count=
	girocco_bang_firstfail=
	girocco_bang_messagesent=
	[ -L "$proj.git/objects" ] || ! [ -d "$proj.git/objects" ] ||
	eval "$(git --git-dir="$cfg_reporoot/$proj.git" config --get-regexp \
		'^girocco\.((bang\.(count|firstfail|messagesent))|reposizek)$' |
		LC_ALL=C awk '{gsub(/[.]/,"_",$1); $0 ~ / / || sub(/$/," "); sub(/ /,"=\042"); print $0 "\042"}')" || :
	b=
	if [ -n "$girocco_bang_count" ] && [ "${girocco_bang_count#*[!0-9]}" = "$girocco_bang_count" ] && [ "$girocco_bang_count" -gt 0 ]; then
		banged=$(( $banged + 1 ))
		girocco_bang_count=$(( 0 + $girocco_bang_count ))
		b="~(${girocco_bang_firstfail:+$girocco_bang_firstfail/}$girocco_bang_count"
		if [ "$girocco_bang_messagesent" = "true" ] || [ "$girocco_bang_messagesent" = "1" ]; then
			b="$b/sent"
			bangsent=$(( $bangsent + 1 ))
		fi
		b="$b)"
	fi
	repok="$girocco_reposizek"
	repokpct=
	case "$repok" in
		[0-9]*)
			repok="${repok%%[!0-9]*}"
			repokpct=$(( ( $repok * 100 + $usek / 2 ) / $usek ))
			if [ $repokpct -le 100 ]; then
				repokpct="$repokpct%"
			else
				repokpct="100+"
			fi;;
		*)
			repok=0
			repokpct=-;;
	esac
	if [ -z "$external" ]; then
		ktotal=$(( $ktotal + $repok ))
		total="$(( $total + $usek ))"
	fi
	howmany="$(( $howmany + 1 ))"
	line="$usek $repokpct $mirror $proj$a$x$b$nl"
	results="$results$line"
done <<EOT
$(find -L . -type d \( -path ./_recyclebin -o -path ./_global -o -name '*.git' -print \) -prune 2>/dev/null)
EOT

kpct=$(( ( $ktotal * 100 + $inuse / 2 ) / $inuse ))
enddate="$(date "+$datefmt")"
domail=cat
[ -z "$mailresult" ] || domail='mailref "diskuse@$cfg_gitweburl" -s "[$cfg_name] Project Disk Use Report" "$cfg_admin"'
{
	cat <<EOT
Project Disk Use Report
=======================

       Start Time: $startdate
         End Time: $enddate

  Repository Root: $cfg_reporoot
Unbinned Disk Use: $(fmtcomma "$inuse") (1024-byte blocks)
  reposizek Total: $(fmtcomma "$ktotal") ($kpct%)

 Recycle Bin Root: $cfg_reporoot/_recyclebin
  Binned Disk Use: $(fmtcomma "$binned") (1024-byte blocks)

   Total Disk Use: $(fmtcomma "$totaluse") (1024-byte blocks)

 Repository Count: $(fmtcomma "$howmany")
            Forks: $(fmtcomma "$forks")$(fmtpct "$forks" "$howmany")
          Mirrors: $(fmtcomma "$mirrors")$(fmtpct "$mirrors" "$howmany")
          ~Banged: $(fmtcomma "$banged")$(fmtpct "$banged" "$howmany")/($(( $banged - $bangsent )) unsent)
         Orphaned: $(fmtcomma "$orphans")
 Repository Total: $(fmtcomma "$total") (1024-byte blocks)
 
   *30-Day Active: $(fmtcomma "$(( $pactive + $mactive ))")$(fmtpct "$(( $pactive + $mactive ))" "$howmany")
             Push: $(fmtcomma "$pactive")$(fmtpct "$pactive" "$howmany")$(fmtpct "$pactive" "$(( $pactive + $mactive ))" "/" 1)$(fmtpct "$pactive" "$(( $howmany - $mirrors ))" "/" 1)
           Mirror: $(fmtcomma "$mactive")$(fmtpct "$mactive" "$howmany")$(fmtpct "$mactive" "$(( $pactive + $mactive ))" "/" 1)$(fmtpct "$mactive" "$mirrors" "/" 1)

$(df -h "$cfg_reporoot")
EOT

	if [ $# -lt 1 ] || [ $1 != "0" ]; then
		topn=cat
		message="Individual Repository Use"
		case "${1%%[!0-9]*}" in ?*)
			message="Individual Repository Use (Top $1)"
			topn="head -n ${1%%[!0-9]*}"
		esac
		echo ""
		echo ""
		echo "$message"
		echo "$message" | tr -c '\n' -
		echo ""
		printf '%s' "$results" | sort -k1,1nr -k4,4 |
		while read -r a b c d; do
			printf "%10s %4s  %s  %s\n" "$(fmtcomma "$a")" "$b" "$c" "$d"
		done |
		sed -e 's/ [M-] /   /g' | $topn
	fi
} | eval "$domail"
