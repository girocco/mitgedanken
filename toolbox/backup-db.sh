#!/bin/sh

# This script makes a rotating backup of the very
# important database files $chroot/etc/passwd and
# $chroot/etc/group and $chroot/etc/sshkeys.
# Backup files are rotated dropping .9.$ext and
# renaming the others to the next higher number
# and then finally making a copy of the original
# to a .1 (not compressed for passwd and group).

# Running this regularly from a cron entry is
# highly recommended.  It may be run manually
# from the $basedir/toolbox directory any time
# after make install has been run to create an
# immediate backup.

# Backups retain the modification date of the file
# they are backing up at the time the backup was
# made.  If restoration is required this can be
# used to determine the time period from which data
# would be lost if the backup were to be used.

set -e

. @basedir@/shlib.sh

# rotate_file basename suffix
rotate_file2_9() {
	! [ -f "$1.8.$2" ] || mv -f "$1.8.$2" "$1.9.$2"
	! [ -f "$1.7.$2" ] || mv -f "$1.7.$2" "$1.8.$2"
	! [ -f "$1.6.$2" ] || mv -f "$1.6.$2" "$1.7.$2"
	! [ -f "$1.5.$2" ] || mv -f "$1.5.$2" "$1.6.$2"
	! [ -f "$1.4.$2" ] || mv -f "$1.4.$2" "$1.5.$2"
	! [ -f "$1.3.$2" ] || mv -f "$1.3.$2" "$1.4.$2"
	! [ -f "$1.2.$2" ] || mv -f "$1.2.$2" "$1.3.$2"
	return 0
}

# backup_file basename
backup_file() {
	rotate_file2_9 "$1" gz
	if [ -f "$1.1" ]; then
		rm -f "$1.2.gz"
		gzip -n9 <"$1.1" >"$1.2.gz" &&
		touch -r "$1.1" "$1.2.gz" &&
		chmod a-w "$1.2.gz"
	fi
	if [ -f "$1" ]; then
		cp -pf "$1" "$1.1" &&
		chmod a-w "$1.1"
	fi
	return 0
}

# badkup_dir basename
backup_dir() (
	set -e
	rotate_file2_9 "$1" tar.gz
	! [ -f "$1.1.tar.gz" ] || mv -f "$1.1.tar.gz" "$1.2.tar.gz"
	if [ -d "$1" ]; then
		cd "$(dirname "$1")"
		base="$(basename "$1")"
		rm -f "$base.1.tar.gz"
		tar -c -f - $base | gzip -n9 >"$base.1.tar.gz"
		chmod a-w "$base.1.tar.gz"
	fi
	return 0
)

# Be paranoid
if [ -z "$cfg_chroot" ] || [ "$cfg_chroot" = "/" ]; then
  echo 'Config.pm chroot setting is invalid' >&2
  exit 1
fi

backup_file "$cfg_chroot/etc/passwd"
backup_file "$cfg_chroot/etc/group"
backup_dir "$cfg_chroot/etc/sshkeys"

exit 0
