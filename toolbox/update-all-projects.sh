#!/bin/sh

# Update all current projects hooks and config

# If one or more project names are given, just update those instead

# update-all-projects [--dry-run] [projname]...

set -e

. @basedir@/shlib.sh

force=
dryrun=
[ "$1" != "--force" ] && [ "$1" != "-f" ] || { force=1; shift; }
[ "$1" != "--dry-run" ] && [ "$1" != "-n" ] || { dryrun=1; shift; }
case "$1" in -*) echo "Invalid options: $1" >&2; exit 1;; esac

umask 002

if [ -z "$cfg_owning_group" ]; then
	if [ -z "$force" ]; then
		echo "No owning_group set, refusing to run without --force" >&2
		exit 1
	fi
fi

mydir="$(cd "$(dirname "$0")" && pwd -P)"
required='update-all-hooks.sh update-all-config.pl'
bad=
for r in $required; do
	if ! [ -x "$mydir/$r" ]; then
		echo "missing required script: $mydir/$r" >&2
		bad=1
	fi
done
[ -z "$bad" ] || exit 1
echo "Running update-all-hooks..."
"$mydir/update-all-hooks.sh" ${force:+--force} ${dryrun:+--dry-run} "$@"
echo "Running update-all-config..."
"$mydir/update-all-config.pl" ${force:+--force} ${dryrun:+--dry-run} "$@"
