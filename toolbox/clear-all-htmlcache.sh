#!/bin/sh

# Remove all files present in the htmlcache subdirectory of each project.
# A count of projects that had files to be removed is displayed.

set -e

. @basedir@/shlib.sh

umask 002

base="$cfg_reporoot"
cut -d : -f 1 <"$cfg_chroot/etc/group" | grep -v "^_repo" |
(
	count=0
	while read proj; do
		projdir="$base/$proj.git"
		if cd "$projdir/htmlcache" 2>/dev/null; then
			if [ "$(echo *)" != '*' ]; then
				rm -f * 2>/dev/null
				count=$(( $count + 1 ))
				if [ $(( $count % 10 )) = 0 ]; then
					printf '%s ' $count
				fi
			fi
		fi
	done
	if [ $(( $count % 10 )) != 0 ]; then
		printf '%s ' $count
	fi
	printf '%s\n' 'done'
)
