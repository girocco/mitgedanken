#!/bin/sh

# Update all out-of-date hooks in all current projects and install missing ones

# If one or more project names are given, just update those instead

set -e

. @basedir@/shlib.sh

force=
dryrun=
[ "$1" != "--force" ] && [ "$1" != "-f" ] || { force=1; shift; }
[ "$1" != "--dry-run" ] && [ "$1" != "-n" ] || { dryrun=1; shift; }
case "$1" in -*) echo "Invalid options: $1" >&2; exit 1;; esac

if is_root; then
	printf '%s\n' "$(basename "$0"): refusing to run as root -- bad things would happen"
	exit 1
fi

umask 002

base="${cfg_reporoot%/}"
globalhooks="$base/_global/hooks"
hookbin="$cfg_basedir/hooks"
cmd='cut -d : -f 1 <"$cfg_chroot/etc/group" | grep -v ^_repo'
[ $# -eq 0 ] || cmd='printf "%s\n" "$@"'
eval "$cmd" |
(
	while read -r proj; do
		proj="${proj#$base/}"
		proj="${proj%.git}"
		projdir="$base/$proj.git"
		[ -d "$projdir" ] || { echo "$proj: does not exist -- skipping"; continue; }
		! [ -e "$projdir/.nohooks" ] || { echo "$proj: .nohooks found -- skipping"; continue; }
		updates=
		for hook in pre-auto-gc pre-receive post-commit post-receive update; do
			doln=
			if [ -f "$projdir/hooks/$hook" ]; then
				if
					! [ -L "$projdir/hooks/$hook" ] ||
					[ "$(readlink "$projdir/hooks/$hook")" != "$globalhooks/$hook" ]
				then
					doln=1
					updates="$updates $hook"
				fi
			elif ! [ -e "$projdir/hooks/$hook" ]; then
				[ -n "$dryrun" ] || [ -d "$projdir/hooks" ] || mkdir "$projdir/hooks"
				doln=1
				updates="$updates +$hook"
			fi
			if [ -z "$dryrun" ] && [ -n "$doln" ]; then
				ln -sfn "$globalhooks/$hook" "$projdir/hooks/$hook"
			fi
		done
		for hook in post-update; do
			if [ -e "$projdir/hooks/$hook" ]; then
				[ -n "$dryrun" ] || rm -f "$projdir/hooks/$hook"
				updates="$updates -$hook"
			fi
		done
		if
			[ -d "$projdir/mob/hooks" ] &&
			{
				! [ -L "$projdir/mob/hooks" ] ||
				[ "$(readlink "$projdir/mob/hooks")" != "../hooks" ]
			}
		then
			if [ -z "$dryrun" ]; then
				[ -L "$projdir/mob/hooks" ] || rm -rf "$projdir/mob/hooks"
				ln -sfn ../hooks "$projdir/mob/hooks"
			fi
			updates="$updates mob/hooks@ -> ../hooks"
		fi
		[ -z "$updates" ] || echo "$proj:$updates" ${dryrun:+(dryrun)}
	done
)
