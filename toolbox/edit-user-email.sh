#!/bin/sh

set -e

. @basedir@/shlib.sh

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
	echo "Usage: $0 <username> <old e-mail> <new e-mail>"
	exit 1
fi
ETC="$cfg_chroot/etc"
COUNT="$(grep -E -c "^$1:" "$ETC/passwd")"
if [ "$COUNT" -ne "1" ]; then
	echo "fatal: user '$1' doesn't appear to exist (or exists multiple times, or contains regexpy characters)."
	exit 1
fi
if echo "$2" | grep -q '[,:]'; then
	echo "fatal: e-mail '$2' has illegal characters ([,:])."
	exit 1
fi
if ! echo "$3" | grep -E -q '^[a-zA-Z0-9+._-]+@[a-zA-Z0-9.-]+$'; then
	echo "fatal: e-mail '$3' is not valid (^[a-zA-Z0-9+._-]+@[a-zA-Z0-9.-]+$)."
	exit 1
fi
sed \
	-e "s/^$1\(:[^:]*:[^:]*:[^:]*:\)$2\([,:]\)/$1\\1$3\\2/" \
	-e "t show" \
	-e "b" \
	-e ": show" \
	-e "w /dev/stderr" \
	"$ETC/passwd" >"$ETC/passwd.$$"
mv -f "$ETC/passwd.$$" "$ETC/passwd"
rm -f "$ETC/passwd.$$"
if [ -n "$cfg_update_pwd_db" ] && [ "$cfg_update_pwd_db" != "0" ]; then
	"$cfg_basedir/bin/update-pwd-db" "$ETC/passwd" "$1"
fi
echo "All changed lines listed above."
