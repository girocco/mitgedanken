#!/usr/bin/perl

# Show projects' pack and/or object counts
# Use "show-pack-counts.pl --sorted" to see in pack count order
# Use the --objects option to show pack object counts instead of pack counts

use strict;
use warnings;
use vars qw($VERSION);
BEGIN {*VERSION = \'2.0'}
use File::Basename;
use Getopt::Long;
use Pod::Usage;
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::Project;
use Girocco::CLIUtil;

my $shbin;
BEGIN {
    $shbin = $Girocco::Config::posix_sh_bin;
    defined($shbin) && $shbin ne "" or $shbin = "/bin/sh";
}

$| = 1;
select((select(STDERR), $| = 1)[0]);
exit(&main(@ARGV)||0);

my ($quiet, $progress);

sub die_usage {
	pod2usage(-exitval => 2);
}

sub die_usage_msg {
	warn(@_);
	pod2usage(-exitval => 2);
}

sub do_help {
	pod2usage(-verbose => 2, -exitval => 0);
}

sub do_version {
	print basename($0), " version ", $VERSION, "\n";
	exit 0;
}

sub defval {
	return defined($_[0]) ? $_[0] : $_[1];
}

my ($lastpct, $lastts);

sub progress {
	return unless $progress;
	my ($prog, $total, $prefix) = @_;
	$total or $total = 100;
	defined $prefix or $prefix = "";
	my $now = time();
	defined $lastts or $lastts = $now + 1;
	if ($now > $lastts) {
		$lastts = $now;
		defined $lastpct or $lastpct = "";
		my $pct = int($prog * 100 / $total);
		$pct = 100 if $pct > 100;
		if ($pct ne $lastpct) {
			$lastpct = $pct;
			$prefix ne "" and $prefix .= " ";
			printf STDERR "\r%s%3d%%", $prefix, $pct;
		}
	}
}

sub progress_done {
	return unless $progress && defined($lastpct);
	my $prefix = $_[0];
	defined $prefix or $prefix = "";
	$prefix ne "" and $prefix .= " ";
	printf STDERR "\r%s100%%, done.\n", $prefix;
}

my ($packs, $packobjs, $loose, $sorton);

sub main {
	local *ARGV = \@_;
	my ($help, $version, $asc);

	close(DATA) if fileno(DATA);
	$progress = -t 2;
	Getopt::Long::Configure('bundling');
	$_ eq "--sorted" || $_ eq "--sort" and $_ .= "=" foreach @_;
	GetOptions(
		'help|h'		=> sub {do_help},
		'version|V'		=> sub {do_version},
		'quiet|q'		=> sub {$progress = 0},
		'progress|P'		=> \$progress,
		'objects|packed'	=> sub {$packobjs = 1},
		'no-objects|no-packed'	=> sub {$packobjs = 0},
		'packs'			=> sub {$packs = 1},
		'no-packs'		=> sub {$packs = 0},
		'loose'			=> sub {$loose = 1},
		'no-loose'		=> sub {$loose = 0},
		'full'			=> sub {$packs = $packobjs = $loose = 1;
					$sorton = "objects"},
		'sorted|sort:s'		=> \$sorton,
	) or die_usage;
	$packs = 1 if !defined($packs) && !defval($packobjs,0) && !defval($loose,0);
	$packobjs = 1 if !defined($packobjs) && defined($packs) && !$packs && !defval($loose,0);
	$packobjs || $packs || $loose or
		die_usage_msg "At least one of --packs, --objects or --loose must be active\n";
	if (defined($sorton)) {
		$sorton =~ /^\+/ and $asc = 1;
		$sorton =~ s/^[+-]//;
		$sorton eq "" and $sorton = $packs ? "packs" : "objects";
		$sorton =~ tr/A-Z/a-z/;
		if ($sorton eq "packs") {
			$packs or die_usage_msg "Sorting on \"packs\" requires the --packs option\n";
		} elsif ($sorton eq "loose") {
			$loose or die_usage_msg "Sorting on \"loose\" requires the --loose option\n";
		} elsif ($sorton eq "packed") {
			$packobjs or die_usage_msg "Sorting on \"packed\" requires the --objects option\n";
		} elsif ($sorton eq "objects") {
			$loose || $packobjs or
				die_usage_msg "Sorting on \"objects\" requires the --objects and/or --loose option\n";
			if ($loose && !$packobjs) {
				$sorton = "loose";
			} elsif ($packobjs && !$loose) {
				$sorton = "packed";
			}
		} else {
			die_usage_msg "Unknown sort field: $sorton\n";
		}
	} else {
		$progress = 0;
	}

	nice_me();
	my @projlist = ();
	my $reporoot = $Girocco::Config::reporoot;
	if (@_) {
		my %projs = map {($_ => 1)} Girocco::Project::get_full_list;
		foreach (@_) {
			s,/+$,,;
			my $p = $_;
			exists($projs{$p}) and push(@projlist, $p), next;
			$p =~ s/\.git$//i && exists($projs{$p}) and push(@projlist, $p), next;
			$p =~ s,^\Q$reporoot\E/,, && exists($projs{$p}) and push(@projlist, $p), next;
			warn "Ignoring unknown project: $_\n";
		}
		@projlist or exit 1;
	} else {
		@projlist = sort({lc($a) cmp lc($b)} Girocco::Project::get_full_list);
	}

	my @results = ();
	my $pmsg = "Inspecting projects:";
	foreach my $proj (@projlist) {
		my ($pcks, $pobjs, $lobjs) =
			inspect_proj("$reporoot/$proj.git", $packs, $packobjs, $loose);
		if ($sorton) {
			push(@results, [$proj, $pcks, $pobjs, $lobjs]);
			progress(scalar(@results), scalar(@projlist), $pmsg);
		} else {
			show_one($proj, $pcks, $pobjs, $lobjs);
		}
	}
	if ($sorton) {
		progress_done($pmsg);
		my $sorter;
		my $desc = $asc ? 1 : -1;
		$sorton eq "packs" and $sorter = sub {
			my $x = $$a[1] <=> $$b[1];
			$x *= $desc;
			$x or $x = lc($$a[0]) cmp lc($$b[0]);
			return $x;
		};
		$sorton eq "packed" and $sorter = sub {
			my $x = $$a[2] <=> $$b[2];
			$x *= $desc;
			$x or $x = lc($$a[0]) cmp lc($$b[0]);
			return $x;
		};
		$sorton eq "loose" and $sorter = sub {
			my $x = $$a[3] <=> $$b[3];
			$x *= $desc;
			$x or $x = lc($$a[0]) cmp lc($$b[0]);
			return $x;
		};
		$sorton eq "objects" and $sorter = sub {
			my $x = ($$a[2] + $$a[3]) <=> ($$b[2] + $$b[3]);
			$x *= $desc;
			$x or $x = lc($$a[0]) cmp lc($$b[0]);
			return $x;
		};
		foreach my $info (sort($sorter @results)) {
			show_one(@$info);
		}
	}
	exit 0;
}

sub show_one {
	my ($name, $pcks, $pobjs, $lobjs) = @_;
	my $line = "";
	if (defined($sorton)) {
		$sorton eq "packs" and $line .= $pcks;
		$sorton eq "packed" and $line .= $pobjs;
		$sorton eq "loose" and $line .= $lobjs;
		$sorton eq "objects" and $line .= ($pobjs + $lobjs);
	} else {
		if ($packs) {
			$line .= $pcks;
		} elsif ($packobjs && $loose) {
			$line .= ($pobjs + $lobjs);
		} elsif ($packobjs) {
			$line .= $pobjs;
		} else {
			$line .= $lobjs;
		}
	}
	$line .= "\t$name";
	my @info = ();
	$packs and push(@info, "packs: " . $pcks);
	$packobjs and push(@info, "packed: " . $pobjs);
	$loose and push(@info, "loose: " . $lobjs);
	if (@info > 1) {
		length($name) < 8 and $line .= "\t";
		length($name) < 16 and $line .= "\t";
		length($name) < 24 and $line .= "\t";
		$line .= " \t(" . join(", ", @info) . ")";
	}
	print $line, "\n";
}

my ($lpbin, @lpcmd);
BEGIN {
  $lpbin = $Girocco::Config::basedir . "/bin/list_packs";
  @lpcmd = ($lpbin, "--exclude-no-idx");
}

sub count_proj_packs {
	my $projdir = shift;
	my $objs = shift;
	my $pd = $projdir . "/objects/pack";
	-d $pd or warn("no such project path (anymore) $pd\n"), return undef;
	open LPCMD, '-|', @lpcmd, ($objs ? "--count-objects" : "--count"), $pd or
		warn("failed to run list_packs for project dir $pd\n"), return undef;
	my $pcount = <LPCMD>;
	chomp($pcount);
	close LPCMD;
	$pcount =~ /^\d+$/ or
		warn("list_packs produced invalid output for project dir $pd\n"), return undef;
	return 0 + $pcount;
}

my ($octet, $octet19);
BEGIN {
    $octet = '[0-9a-f][0-9a-f]';
    $octet19 = $octet x 19;
}

sub count_proj_loose {
	my $projdir = shift;
	my $od = $projdir . "/objects";
	-d $od or warn("no such project path (anymore) $od\n"), return undef;
	open FINDCMD, '-|', $shbin, '-c', "cd " . quotemeta($od) . " && " .
		"find -L $octet -maxdepth 1 -name '$octet19*' -print 2>/dev/null | wc -l" or
		warn("failed to run find for project dir $od\n"), return undef;
	my $ocount = <FINDCMD>;
	$ocount =~ s/\s+//gs;
	close FINDCMD;
	$ocount =~ /^\d+$/ or
		warn("find + wc produced invalid output for project dir $od\n"), return undef;
	return 0 + $ocount;
}

sub inspect_proj {
	my $path = shift;
	my ($pcks, $pobjs, $lobjs);
	$pcks = count_proj_packs($path) || 0 if $packs;
	$pobjs = count_proj_packs($path, 1) || 0 if $packobjs;
	$lobjs = count_proj_loose($path) || 0 if $loose;
	return ($pcks, $pobjs, $lobjs);
}

__END__

=head1 NAME

show-pack-counts.pl - show projects' pack and/or object counts

=head1 SYNOPSIS

show-pack-counts.pl [<options>] [<projname>...]

 Options:
   -h | --help                  detailed instructions
   -V | --version               show version
   -q | --quiet                 suppress progress messages
   -P | --progress              show progress messages (default)
   --sorted[=[+]<field>]        sort output on <field>
   --objects                    count objects in packs instead of packs
   --no-objects                 omit objects in packs count (default)
   --packs                      always include pack count
   --no-packs                   always omit pack count
   --loose                      count loose objects (implies --no-packs)
   --no-loose                   omit loose objects count (default)
   --full                       shortcut meta option that sets
                                --packs --objects --loose --sorted=objects

 <field> can be loose, packed, packs or objects and may be preceded by an
 optional + to sort in ascending rather than descending order.
 If no sorted option is given output will be shown unsorted as it's gathered.
 The default sort field depends on the options being packs if pack counts are
 included and objects otherwise.  Using a field name of + changes the
 default sorting order to ascending without changing the default sort field.

 <projname>...                  if given, only inspect these projects

=head1 OPTIONS

=over 8

=item B<-h>, B<--help>

Print the full description of show-pack-counts.pl's options.

=item B<-V>, B<--version>

Print the version of show-pack-counts.pl.

=item B<-q>, B<--quiet>

Suppress progress messages.  There are never any progress messages unless
sorted output is active.

=item B<-P>, B<--progress>

Show progress information to STDERR while collecting sorted output.  This is
the default unless STDERR is not a TTY.

=item B<--sorted[=>I<<fieldE<gt>>B<]>

Collect all the output first and then show it in sorted order.

Possible I<<fieldE<gt>> names are:

=over 8

=item B<[+]loose>

Sort by number of loose objects (requires B<--loose> option too)

=item B<[+]packed>

Sort by total number of objects in packs (requires B<--objects> option too)

=item B<[+]packs>

Sort by total number of packs (requires possibly implied B<--packs> option)

=item B<[+]<objects>

Sort by total objects counted (requires B<--objects> and/or B<--loose>)

=item B<[+]<packs>

Sort by total number of packs (requires possibly implied B<--packs> option)

=item B<+>

Keep the default sort field but sort in ascending order rather than descending
order.  When prefixed to one of the other sort field names, sort on that field
but in ascending rather than descending order.

=back

If the B<--sorted=[+]<field>> option is given more than once then later options
override earlier ones (there is no multi-field sorting).  This can be used to
change the sorting order used with B<--full>.

If B<--sorted> is used (or B<--sorted=+>) the sort field is chosen based on
what information is being collected.  If packs are being counted the sort will
be on the number of packs (B<--sorted=packs>).

If packs are not being counted the sort will be on the total number of objects
in each project being either objects in packs, loose objects or the sum of
both objects in packs and loose objects depending on what options have been
given (B<--sorted=objects>).

=item B<--objects>

Instead of counting packs, count objects in them.  Implies B<--no-packs>
unless an explicit B<--packs> is given too.

=item B<--no-objects>

Omit the count of objects in packs.  Included for completeness.  Reactivates
implicit B<--packs> if given.

=item B<--packs>

Count the number of packs present.  This is implied by default but may be
given explicitly to always include the packs count.

=item B<--no-packs>

Omit the count of packs.  At least one other counting option must be specified
with this (either B<--objects> or B<--loose>).

=item B<--loose>

Count loose objects.

=item B<--no-loose>

Do not count loose objects.

=item B<--full>

A shortcut meta option that behaves the same as replacing it with B<--packs>
B<--objects> B<--loose> B<--sorted=objects> at that point.

=item I<<projnameE<gt>>

If no project names are specified then I<all> projects are processed.

If one or more project names are specified then only those projects are
processed.  Specifying non-existent projects produces a warning for them,
but the rest of the projects specified will still be processed.

Each B<projname> may be either a full absolute path starting with
$Girocco::Config::reporoot or just the project name part with or without
a trailing C<.git>.

Any explicitly specified projects that do exist but are not known to
Girocco will be skipped (with a warning).

=back

=head1 DESCRIPTION

Count the number of packs, objects contained in packs and/or loose objects
and show the results.

Output can be sorted and the projects inspected can be limited.

=cut
