#!/usr/bin/perl

# Remove all projects' ctags that are not a valid_tag (--dry-run shows invalid tags)

use strict;
use warnings;
use lib "__BASEDIR__";

use Storable;
use Girocco::Config;
use Girocco::Util qw(valid_tag);

my $rr = $Girocco::Config::reporoot;
-d $rr or die "No such directory $rr\n";
my $gwic = $Girocco::Config::projlist_cache_dir . "/gitweb.index.cache";
my $cache;
eval { $cache = retrieve($gwic); 1; } or die "Could not load cache file $gwic\n";

# Format of cache is:
# [ "format string", [[project hash refs in project name order], {project name => hashref}]]

# Each project hash ref has:
#  owner => project owner name
#  descr_long => project one liner description (from description file)
#  age_epoch => time of last change (seconds since epoch)
#  ctags => hash ref of tag => count (not present if ctags not enabled)
#  path => $Girocco::Config::reporoot relative path to repository
#  descr => truncated version of descr_long (typically 29-34 characters)

@ARGV == 0 || @ARGV == 1 && $ARGV[0] eq "--dry-run"
	or die "Usage: clean-tags.pl [--dry-run]\n";

my $dryrun = @ARGV;

my $killcount = 0;

sub kill_tag {
	if (-f "$rr/$_[1]/ctags/$_[0]") {
		++$killcount;
		if ($dryrun) {
			my $p = $_[1];
			$p =~ s/\.git$//;
			printf "%s:\t%s\n", $p, $_[0];
		} else {
			unlink("$rr/$_[1]/ctags/$_[0]")
				or warn "failed to remove '$rr/$_[1]/ctags/$_[0]': $!\n";
		}
	}
}

foreach my $proj (@{$cache->[1][0]}) {
	ref($proj) eq 'HASH' && ref($proj->{ctags}) eq 'HASH' or next;
	while (my ($k,$v) = each(%{$proj->{ctags}})) {
		valid_tag($k) or kill_tag($k, $proj->{path});
	}
}

printf "%s %d tag file(s)\n", ($dryrun?"Would remove":"Removed"), $killcount;
exit 0;
