#!/usr/bin/perl

# usertool.pl - command line Girocco user maintenance tool
# Copyright (C) 2016,2017 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;
use vars qw($VERSION);
BEGIN {*VERSION = \'1.0'}
use File::Basename;
use POSIX qw(strftime);
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::Util;
use Girocco::SSHUtil;
use Girocco::CLIUtil;
use Girocco::User;
use Girocco::Project;

exit(&main(@ARGV)||0);

our $help;
BEGIN {$help = <<'HELP'}
Usage: %s [--quiet] <command> <options>

       list [--sort=lcname|name|email|uid|push|no] [--email] [<regex>]
              list all users (default is --sort=lcname)
              limit to users matching <regex> if given
              match <regex> against email instead of user name with --email

       create [--force] [--force] [--keep-keys] [--dry-run] <user>
              create new user <user>
              retain pre-existing keys (but not auth) with --keep-keys
              show all info/warnings but don't actually create with --dry-run

       remove [--force] <user>
              remove user <user>

       show [--force] [--load] [--id] <user>
              show user <user>
              with --load actually load the user forcing a UUID if needed
              with --id interpret <user> as a uid instead of a name
              with --force attempt to show normally "invisible" users

       listkeys [--force] [--verbose] [--urls] [--raw] <user>
              list user <user> key info
              with --urls show https push cert download urls if any
              with --force attempt to show normally "invisible" users
              with --verbose include public key data (authorized_keys compat)
              with --raw produce unannotated authorized_keys output

       listprojs [--regex] [--email] <userinfo>
              list all push projects that have a user matching <userinfo>
              match <userinfo> against user email instead of name with --email
              treat <userinfo> as a regex with --regex

       [set]email [--force] <user> <newemail>
              set user <user> email to <newemail>
              without "set" and only 1 arg, just show current user email

       [set]keys [--force] <user> <newkeys>
              set user <user> ssh authorized keys to <newkeys>
              <newkeys> is -|[@]filename
              without "set" and only 1 arg, like listkeys --raw

       get [--force] <user> <fieldname>
              show user <user> field <fieldname>
              <fieldname> is email|uid|uuid|creationtime|pushtime|keys

       set [--force] <user> <fieldname> <newfieldvalue>
              set user <user> field <fieldname> to <newfieldvalue>
              <fieldname> is email|keys
              <newfieldvalue> same as for corresponding set... command
HELP

our $quiet;
our $setopt;
sub die_usage {
	my $sub = shift || diename;
	if ($sub) {
		die "Invalid arguments to $sub command -- try \"help\"\n";
	} else {
		die "Invalid arguments -- try \"help\"\n";
	}
}

# Should be contents of a sshkeys file or {keys} member
# return array of arrayref for each key:
#   [0] = key line number in file
#   [1] = type either "RSA" or "DSA"
#   [2] = number of bits in key
#   [3] = key comment (nickname)
#   [4] = md5 key fingerprint as shown by ssh-keygen -l
#   [5] = raw public key line (starting with ssh-... and with comment but no \n)
sub key_info_list {
	my $data = shift;
	my @keys = ();
	defined($data) or $data = "";
	my %types = ('ssh-dss' => 'DSA', 'ssh-rsa' => 'RSA');
	my $line = 0;
	foreach (split(/\n/, Girocco::User::_trimkeys($data))) {
		if (/^(?:no-pty )?(ssh-(?:dss|rsa) .*)$/) {
			++$line;
			my $raw = $1;
			my ($type, $bits, $fingerprint, $comment) = sshpub_validate($raw);
			next unless $type && $types{$type};
			push(@keys, [$line, $types{$type}, $bits, $comment, $fingerprint, $raw]);
		}
	}
	@keys;
}

sub get_username_for_id {
	my $id = shift;
	defined($id) && $id =~ /^-?\d+$/ or return undef;
	$id = 0 + $id;
	my %usersbyid = map {((0 + $$_[3]) => $_)} get_full_users;
	return defined($usersbyid{$id}->[1]) && $usersbyid{$id}->[1] ne "" ?
		$usersbyid{$id}->[1] : undef;
}

sub get_user_forcefully {
	my ($username, $load) = @_;
	defined($username) && $username ne "" or return undef;
	my %users = map {($$_[1] => $_)} get_full_users;
	exists($users{$username}) && defined($users{$username}->[3]) &&
		$users{$username}->[3] =~ /^-?\d+$/
		or die "No such user: \"$username\"\n";
	my $user;
	my $uref = $users{$username};
	$user = {
		name  => $username,
		uid   => $$uref[3],
		email => $$uref[5]->[0],
		uuid  => $$uref[5]->[1],
		creationtime => $$uref[5]->[2],
	};
	bless $user, "Girocco::User";
	($user->{keys}, $user->{auth}, $user->{authtype}) = $user->_sshkey_load
		if -f jailed_file($user->_sshkey_path);
	$user;
}

sub get_user_carefully {
	my ($username, $load, $force) = @_;
	defined($username) && $username ne "" or return undef;
	$force || Girocco::User::does_exist($username, 1) or die "No such user: \"$username\"\n";
	my $user;
	if (!$load) {
		{
			my %users = map {($$_[1] => $_)} get_all_users;
			exists($users{$username}) && $users{$username}->[2] or last;
			my $uref = $users{$username};
			$user = {
				name  => $username,
				uid   => $$uref[2],
				email => $$uref[4],
				uuid  => $$uref[5],
				creationtime => $$uref[6],
			};
			bless $user, "Girocco::User";
			($user->{keys}, $user->{auth}, $user->{authtype}) = $user->_sshkey_load
				if -f jailed_file($user->_sshkey_path);
		}
		$user || !$force or $user = get_user_forcefully($username);
	}
	$user = get_user($username) if $load || !defined($user);
	$user;
}

sub get_clean_user {
	my $user = get_user_carefully(@_);
	delete $user->{email} unless $user->{email};
	delete $user->{uuid} unless $user->{uuid};
	delete $user->{creationtime} unless $user->{creationtime};
	delete $user->{auth} unless $user->{auth};
	delete $user->{authtype} unless $user->{authtype};
	if ($user->{keys}) {
		my @keys = key_info_list($user->{keys});
		$user->{key_list} = [map({"$$_[0]: $$_[1] $$_[2] \"$$_[3]\""} @keys)] if @keys;
	}
	delete $user->{keys};
	my ($pushuser, $pushtime) = (stat(jailed_file('/etc/sshactive/'.$user->{name})))[4,9];
	if (defined($pushuser) && defined($pushtime)) {
		$pushuser = $pushuser eq $user->{uid} ? 'ssh' : 'https';
		my $jailtime = (stat(jailed_file('/etc/sshactive/_jailsetup')))[9];
		$user->{push_access} = $pushuser if !defined($jailtime) || $pushtime > $jailtime;
		$pushtime = strftime("%a %d %b %Y %T %Z", localtime($pushtime));
		$user->{push_time} = $pushtime;
	}
	return $user;
}

sub get_all_users_with_push {
	my %users = map {($$_[1] => $_)} get_all_users;
	my $jailtime = (stat(jailed_file('/etc/sshactive/_jailsetup')))[9];
	defined($jailtime) or $jailtime = 0;
	opendir my $dh, jailed_file('/etc/sshactive') or die "opendir failed: $!\n";
	local $_;
	while ($_ = readdir($dh)) {
		next unless exists $users{$_};
		my ($pushuser, $pushtime) = (stat(jailed_file('/etc/sshactive/'.$_)))[4,9];
		next unless defined($pushuser) && defined($pushtime);
		$users{$_}->[7] = $pushtime;
		my $pushtype = '';
		$pushtype = $pushuser eq $users{$_}->[2] ? "ssh" : "https" if $pushtime > $jailtime;
		$users{$_}->[8] = $pushtype;
	}
	closedir $dh;
	values(%users);
}

sub parse_options {
	Girocco::CLIUtil::_parse_options(
		sub {
			warn((($_[0]eq'?')?"unrecognized":"missing argument for")." option \"$_[1]\"\n")
				unless $quiet;
			die_usage;
		}, @_);
}

sub cmd_list {
	my %sortsub = (
		lcname => sub {lc($$a[1]) cmp lc($$b[1])},
		name => sub {$$a[1] cmp $$b[1]},
		uid => sub {$$a[2] <=> $$b[2]},
		email => sub {lc($$a[4]) cmp lc($$b[4]) || lc($$a[1]) cmp lc($$b[1])},
		push => sub {($$b[7]||0) <=> ($$a[7]||0) || lc($$a[1]) cmp lc($$b[1])},
		no => sub {$$a[0] <=> $$b[0]},
	);
	my $sortopt = 'lcname';
	my ($verbose, $email);
	parse_options(":sort" => \$sortopt, verbose => \$verbose, email => \$email);
	my $regex;
	if (@ARGV) {
		my $val = shift @ARGV;
		$regex = qr($val) or die "bad regex \"$val\"\n";
	}
	!@ARGV && exists($sortsub{$sortopt}) or die_usage;
	my $sortsub = $sortsub{$sortopt};
	my $grepsub = defined($regex) ? ($email ? sub {$$_[4] =~ /$regex/} : sub {$$_[1] =~ /$regex/}) : sub {1};
	my @users = sort($sortsub grep {&$grepsub} get_all_users_with_push);
	my $fmtpush = sub {
		my $u = shift;
		return wantarray ? () : "" unless defined($$u[7]);
		return (wantarray ? "" : ' (') . ($$u[8] ? $$u[8] : "push") .
			(wantarray ?
				'@' . strftime("%Y%m%d_%H%M%S%z", localtime($$u[7])) :
				' ' . strftime("%a %d %b %Y %T %Z", localtime($$u[7])) . ')');
	};
	my $fmtinfo = sub {
		my @fields = ();
		if (defined($$_[4])) {
			$fields[0] = $$_[4];
		} elsif (defined($$_[5]) || defined($$_[6])) {
			$fields[0] = "";
		}
		if (defined($$_[5])) {
			$fields[1] = $$_[5];
		} elsif (defined($$_[6])) {
			$fields[1] = "";
		}
		$fields[2] = $$_[6] if defined($$_[6]);
		join(",", @fields);
	};
	if ($verbose) {
		print map(sprintf("%s\n", join(":", $$_[1], $$_[2], &$fmtinfo, &$fmtpush($_))), @users);
	} else {
		print map(sprintf("%s: %s%s\n", $$_[1], $$_[4], scalar(&$fmtpush($_))), @users);
	}
	return 0;
}

sub cmd_create {
	my ($force, $keepkeys, $dryrun);
	$force = 0;
	parse_options(force => sub{++$force}, "keep-keys" => \$keepkeys, "dry-run" => \$dryrun);
	@ARGV == 1 or die_usage;
	my $username = $ARGV[0];
	$force >= 2 || !Girocco::User::does_exist($username,1) or die "User \"$username\" already exists\n";
	$force || Girocco::User::valid_name($username) or die "Invalid user name: $username\n";
	$username =~ /^[a-zA-Z0-9_][a-zA-Z0-9+._-]*$/ or die "Invalid characters in user name: $username\n";
	my %users = map {($$_[1] => $_)} get_full_users;
	!exists($users{$username}) or die "User \"$username\" already has passwd entry\n";
	my $kf = jailed_file('/etc/sshkeys/'.$username);
	if (-e $kf) {
		my $size = "s";
		my $w = 0;
		-f $kf and $size = "ing @{[-s $kf]} byte file", $w = 1;
		if ($force >= 2 && $w) {
			warn "Ignoring already exist$size: \$chroot/etc/sshkeys/$username\n" unless $quiet;
		} else {
			die "Already exist$size: \$chroot/etc/sshkeys/$username\n";
		}
	}
	my $uobj;
	if ($force) {
		# force initialization
		$uobj = { name => $username };
		bless $uobj, "Girocco::User";
	} else {
		# normal "nice" initialization
		$uobj = Girocco::User->ghost($username);
	}
	$keepkeys && -f $kf and ($uobj->{keys}) = $uobj->_sshkey_load;
	$uobj or die "Could not initialize new user object\n";
	my $email;
	{
		$email = prompt_or_die("Email/info for user $username");
		unless (valid_email($email)) {
			unless ($force) {
				warn "Your email sure looks weird...?\n";
				redo;
			}
			warn "Allowing invalid email with --force\n" unless $quiet;
		}
		if (length($email) > 96) {
			unless ($force) {
				warn "Your email is longer than 96 characters.  Do you really need that much?\n";
				redo;
			}
			warn "Allowing email longer than 96 characters with --force\n" unless $quiet;
		}
	}
	$uobj->{email} = $email;
	my $kcnt = scalar(@{[split(/\n/, $uobj->{keys}||'')]});
	warn "Preserved $kcnt key@{[$kcnt==1?'':'s']} from sshkeys file\n" if $kcnt and !$quiet;
	$uobj->conjure unless $dryrun;
	return 0;
}

sub cmd_remove {
	my ($force);
	parse_options(force => \$force);
	@ARGV or die "Please give user name on command line.\n";
	@ARGV == 1 or die_usage;
	my $uobj;
	if ($force) {
		my %users = map {($$_[1] => $_)} get_full_users;
		exists($users{$ARGV[0]}) or die "User \"$ARGV[0]\" does not have passwd entry\n";
		$uobj = { name => $ARGV[0], uid => $users{$ARGV[0]}->[3] };
		bless $uobj, "Girocco::User";
	} else {
		$uobj = get_user_carefully($ARGV[0]);
	}
	defined $uobj->{uid} && $uobj->{uid} =~ /^\d+/ or die "User \"$ARGV[0]\" failed to load\n";
	0 + $uobj->{uid} >= 65540 or die "User \"$ARGV[0]\" with uid $$uobj{uid} < 65540 cannot be removed\n";
	my $old;
	my $oldname = $uobj->{name};
	open my $fd, '<', jailed_file("/etc/passwd") or die "user remove failed: $!";
	my $r = qr/^\Q$oldname\E:/;
	foreach (grep /$r/, <$fd>) {
		chomp;
		$old = $_ and last if defined($_) && $_ ne "";
	}
	close $fd;
	$uobj->remove;
	warn "Successfully removed user \"$oldname\":\n$old\n" unless $quiet;
	return 0;
}

sub cmd_show {
	use Data::Dumper;
	my ($force, $load, $id);
	parse_options(force => \$force, load => \$load, id => \$id);
	@ARGV == 1 or die_usage;
	my $username = $ARGV[0];
	if ($id) {
		defined($username) && $username =~ /^-?\d+$/ or die "Invalid user id: $username\n";
		$username = get_username_for_id($username);
		defined($username) or die "No such user id: $ARGV[0]\n";
	}
	my $user = get_clean_user($username, $load, $force);
	my %info = %$user;
	my $d = Data::Dumper->new([\%info], ['*'.$user->{name}]);
	$d->Sortkeys(sub {[sort({lc($a) cmp lc($b)} keys %{$_[0]})]});
	print $d->Dump([\%info], ['*'.$user->{name}]);
	return 0;
}

sub cmd_listkeys {
	my ($force, $verbose, $urls, $raw);
	parse_options(force => \$force, verbose => \$verbose, urls => \$urls, raw => \$raw);
	@ARGV == 1 or die_usage;
	my $username = $ARGV[0];
	my $user = get_user_carefully($username, 0, $force);
	if ($user->{keys}) {
		my @keys = key_info_list($user->{keys});
		$user->{key_info} = \@keys if @keys;
		#$user->{key_desc} = [map({"$$_[0]: $$_[1] $$_[2] \"$$_[3]\""} @keys)] if @keys;
	}
	$verbose = $urls = 0 if $raw;
	my $v = $verbose ? "# " : "";
	my $vln = $verbose ? "\n" : "";
	foreach (@{$user->{key_info}}) {
		my $line = $$_[0];
		my $prefix = $v . (" " x length("" . $line . ": "));
		print "$v$$_[0]: $$_[1] $$_[2] \"$$_[3]\"\n" unless $raw;
		print $prefix, "fingerprint $$_[4]\n" unless $raw;
		print $prefix, $Girocco::Config::webadmurl,
			"/usercert.cgi/$username/$line/",
			$Girocco::Config::nickname,
			"_${username}_user_$line.pem", "\n"
			if $urls && $$_[1] eq "RSA";
		print $$_[5], "\n$vln" if $verbose || $raw;
	}
	return 0;
}

sub cmd_listprojs {
	my %sortsub = (
		lcname => sub {lc($$a[1]) cmp lc($$b[1])},
		name => sub {$$a[1] cmp $$b[1]},
		gid => sub {$$a[3] <=> $$b[3]},
		owner => sub {lc($$a[4]) cmp lc($$b[4]) || lc($$a[1]) cmp lc($$b[1])},
		no => sub {$$a[0] <=> $$b[0]},
	);
	my ($regex, $email, $sortopt);
	$sortopt = 'lcname';
	parse_options(regex => \$regex, email => \$email, ":sort" => \$sortopt);
	exists($sortsub{$sortopt}) or die_usage;
	@ARGV == 1 or die_usage;
	my @users = ();
	my @allusers = get_all_users;
	push(@allusers, [undef, "everyone"]) unless $email || $regex;
	push(@allusers, [undef, "mob"]) unless $email || $regex || $Girocco::Config::mob ne "mob";
	if ($regex) {
		my $val = $ARGV[0];
		my $uregex = qr($val) or die "bad regex \"$val\"\n";
		my $select = $email ? sub {$$_[4] =~ /$uregex/} : sub {$$_[1] =~ /$uregex/};
		push(@users, map({$$_[1]} grep {&$select} @allusers));
		@users or $quiet or warn "No matching users found\n";
		@users or return 0;
	} else {
		my $type;
		my %userslookup;
		if ($email) {
			$type = "email";
			%userslookup = map {($$_[4] => $_)} @allusers;
		} else {
			$type = "name";
			%userslookup = map {($$_[1] => $_)} @allusers;
		}
		exists($userslookup{$ARGV[0]}) or die "Unknown user $type: $ARGV[0]\n";
		push(@users, $userslookup{$ARGV[0]}->[1]);
	}
	my $regexstr = '(?:^|,)' . join("|", map(quotemeta($_), sort @users)) . '(?:,|$)';
	my $regexcomp = qr/$regexstr/;
	my $sortsub = $sortsub{$sortopt};
	my $grepsub = sub {$$_[5] =~ /$regexcomp/};
	my @projects = sort($sortsub grep {&$grepsub} get_all_projects);
	print map(sprintf("%s: %s\n", $$_[1], $$_[5] =~ /^:/ ? "<mirror>" : $$_[5]), @projects);
	return 0;
}

sub cmd_getval {
	my ($force);
	parse_options(force => \$force);
	@ARGV == 2 or die_usage;
	my $username = $ARGV[0];
	my $field = $ARGV[1];
	$field = "push_time" if $field eq "pushtime" || $field eq "push";
	my $user = get_clean_user($username, 0, $force);
	print $user->{$field}, "\n" if defined($user->{$field});
	return defined($user->{$field}) ? 0 : 1;
}

sub cmd_setemail {
	my $force = 0;
	shift(@ARGV), $force=1 if @ARGV && $ARGV[0] eq '--force';
	@ARGV == 2 || (@ARGV == 1 && !$setopt) or die_usage;
	my $user = get_user_carefully($ARGV[0], 0, $force && @ARGV==1);
	if (@ARGV == 2 && !valid_email($ARGV[1])) {
		die "invalid email/info (use --force to accept): \"$ARGV[1]\"\n"
			unless $force;
		warn "using invalid email/info with --force\n" unless $quiet;
	}
	if (@ARGV == 2 && length($ARGV[1]) > 96) {
		die "email/info longer than 96 chars (use --force to accept): \"$ARGV[1]\"\n"
			unless $force;
		warn "using longer than 96 char email/info with --force\n" unless $quiet;
	}
	my $old = $user->{email};
	if (@ARGV == 1) {
		print "$old\n" if defined($old);
		return 0;
	}
	if (defined($old) && $old eq $ARGV[1]) {
		warn $user->{name}, ": skipping update of email/info to same value\n" unless $quiet;
	} else {
		$user = get_user($ARGV[0]);
		$user->{email} = $ARGV[1];
		$user->_passwd_update;
		warn $user->{name}, ": email/info updated to \"$ARGV[1]\" (was \"$old\")\n" unless $quiet;
	}
	return 0;
}

sub cmd_setkeys {
	my $force = 0;
	shift(@ARGV), $force=1 if @ARGV && $ARGV[0] eq '--force';
	@ARGV == 2 || (@ARGV == 1 && !$setopt) or die_usage;
	if (@ARGV == 1) {
		unshift(@ARGV, '--raw');
		unshift(@ARGV, '--force') if $force;
		return cmd_listkeys(@ARGV);
	}
	my $user = get_user_carefully($ARGV[0]);
	my $old = $user->{keys} || "";
	my ($new, $newname);
	if ($ARGV[1] eq "-") {
		local $/;
		$new = <STDIN>;
		$newname = "contents of <STDIN>";
	} else {
		my $fn = $ARGV[1];
		$fn =~ s/^\@//;
		die "missing filename for new keys\n" unless $fn ne "";
		die "no such file: \"$fn\"\n" unless -f $fn && -r $fn;
		open F, '<', $fn or die "cannot open \"$fn\" for reading: $!\n";
		local $/;
		$new = <F>;
		close F;
		$newname = "contents of \"$fn\"";
	}
	defined($new) or $new = '';
	$new = Girocco::User::_trimkeys($new);
	if ($old eq $new) {
		warn $user->{name}, ": skipping update of keys to same value\n" unless $quiet;
		return 0;
	}
	if (length($new) > 9216) {
		die "The list of keys is more than 9kb. Do you really need that much?\n" unless $force;
		warn "Allowing keys list of length @{[length($new)]} > 9216 with --force\n" unless $quiet;
	}
	my $minlen = $Girocco::Config::min_key_length;
	defined($minlen) && $minlen =~ /^\d+/ && $minlen >= 512 or $minlen = 512;
	foreach my $key (split /\r?\n/, $new) {
		my $linestart = substr($key, 0, 50);
		$linestart .= "..." if length($linestart) > length($key);
		my ($type, $bits, $fingerprint, $comment);
		($type, $bits, $fingerprint, $comment) = sshpub_validate($key)
			if $key =~ /^ssh-(?:dss|rsa) [0-9A-Za-z+\/=]+ \S+$/;
		$type or die "Invalid keys line: $linestart\n";
		if ($Girocco::Config::disable_dsa && $type eq 'ssh-dss') {
			die "ssh-dss keys are disabled: $linestart\n" unless $force;
			warn "Allowing disabled ssh-dss key with --force\n" unless $quiet;
		}
		if ($bits > 16384) {
			die "max key bits is 16384 but found $bits: $linestart\n" unless $force;
			warn "Allowing $bits bit key greater than maximum 16384 bits with --force\n" unless $quiet;
		}
		if ($bits < $minlen) {
			die "min key bits is $minlen but found $bits: $linestart\n" unless $force;
			warn "Allowing $bits bit key less than minimum $minlen bits with --force\n" unless $quiet;
		}
	}
	$user = get_user($ARGV[0]);
	$user->{keys} = $new;
	$user->_sshkey_save;
	warn $user->{name}, ": keys updated to $newname\n" unless $quiet;
	return 0;
}

our %fieldnames;
BEGIN {
	%fieldnames = (
		email         => [\&cmd_setemail,  0],
		keys          => [\&cmd_setkeys,   0],
		uid           => [\&cmd_getval,    1],
		uuid          => [\&cmd_getval,    1],
		creationtime  => [\&cmd_getval,    1],
		push          => [\&cmd_getval,    1],
		push_time     => [\&cmd_getval,    1],
		pushtime      => [\&cmd_getval,    1],
	);
}

sub do_getset {
	$setopt = shift;
	my @newargs = ();
	push(@newargs, shift) if @_ && $_[0] eq '--force';
	my $field = $_[1];
	(($setopt && @_ >= 3) || @_ == 2) && exists($fieldnames{$field}) or die_usage;
	!$setopt || @_ != 2 || !${$fieldnames{$field}}[1] or die_usage;
	push(@newargs, shift);
	shift unless ${$fieldnames{$field}}[1];
	push(@newargs, @_);
	diename(($setopt ? "set " : "get ") . $field);
	@ARGV = @newargs;
	&{${$fieldnames{$field}}[0]}(@ARGV);
}

sub cmd_get {
	do_getset(0, @_);
}

sub cmd_set {
	do_getset(1, @_);
}

our %commands;
BEGIN {
	%commands = (
		list => \&cmd_list,
		create => \&cmd_create,
		remove => \&cmd_remove,
		delete => \&cmd_remove,
		show => \&cmd_show,
		listkeys => \&cmd_listkeys,
		listprojs => \&cmd_listprojs,
		listprojects => \&cmd_listprojs,
		projects => \&cmd_listprojs,
		setemail => \&cmd_setemail,
		setkeys => \&cmd_setkeys,
		get => \&cmd_get,
		set => \&cmd_set,
	);
}

sub dohelp {
	my $bn = basename($0);
	printf "%s version %s\n\n", $bn, $VERSION;
	printf $help, $bn;
	exit 0;
}

sub main {
	local *ARGV = \@_;
	shift, $quiet=1 if @ARGV && $ARGV[0] =~ /^(?:-q|--quiet)$/i;
	dohelp if !@ARGV || @ARGV && $ARGV[0] =~ /^(?:-h|-?-help|help)$/i;
	my $command = shift;
	diename($command);
	$setopt = 1;
	if (!exists($commands{$command}) && exists($commands{"set".$command})) {
		$setopt = 0;
		$command = "set" . $command;
	}
	exists($commands{$command}) or die "Unknown command \"$command\" -- try \"help\"\n";
	dohelp if @ARGV && $ARGV[0] =~ /^(?:-h|-?-help|help)$/i && !Girocco::User::does_exist("help",1);
	&{$commands{$command}}(@ARGV);
}
