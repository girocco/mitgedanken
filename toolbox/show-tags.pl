#!/usr/bin/perl

# Show all projects ctags using the gitweb cache
# Use "show-tags.pl | sort -k2,2nr -k1,1f" to see in tag count order

use strict;
use warnings;
use lib "__BASEDIR__";

use Storable;
use Girocco::Config;

my $gwic = $Girocco::Config::projlist_cache_dir . "/gitweb.index.cache";
my $cache;
eval { $cache = retrieve($gwic); 1; } or die "Could not load cache file $gwic\n";

# Format of cache is:
# [ "format string", [[project hash refs in project name order], {project name => hashref}]]

# Each project hash ref has:
#  owner => project owner name
#  descr_long => project one liner description (from description file)
#  age_epoch => time of last change (seconds since epoch)
#  ctags => hash ref of tag => count (not present if ctags not enabled)
#  path => $Girocco::Config::reporoot relative path to repository
#  descr => truncated version of descr_long (typically 29-34 characters)

my %ctags = ();

foreach my $proj (@{$cache->[1][0]}) {
	ref($proj) eq 'HASH' && ref($proj->{ctags}) eq 'HASH' or next;
	while (my ($k,$v) = each(%{$proj->{ctags}})) {
		$v =~ /^\d+$/ && $v or $v = 1;
		$ctags{$k} += $v;
	}
}

foreach my $tag (sort({lc($a) cmp lc($b) || $a cmp $b} keys(%ctags))) {
	printf "%s\t%s\n", $tag, $ctags{$tag};
}
exit 0;
