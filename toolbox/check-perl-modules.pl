#!/usr/bin/env perl

# check-perl-modules.pl -- check for modules used by Girocco
# Copyright (c) 2013,2019 Kyle J. McKay.  All rights reserved.
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

# Attempt to require all the modules used by Perl anywhere in Girocco and
# report the failures.  There is special handling for the SHA-1 functions
# and some other optional modules.

# These ones are not errors

eval {require Proc::ProcessTable; 1} or
print "Note: missing Perl module Proc::ProcessTable, deprecated ".
	"toolbox/kill-stale-daemons.pl will not function\n";

eval {require HTML::Entities; 1} or
print "Warning: missing Perl module HTML::Entities, gitweb ".
	"email obfuscation will be partially disabled\n";

eval {require HTML::TagCloud; 1} or
print "Warning: missing Perl module HTML::TagCloud, optional gitweb tag cloud ".
	"display will be all one size\n";

eval {require HTTP::Date; 1} ||
eval {require Time::ParseDate; 1} or
print "Warning: missing Perl module HTTP::Date and Time::ParseDate, gitweb ".
	"will always return true to if-modified-since feed requests\n";

eval {require Compress::Zlib; 1} or
print "Warning: missing Perl module Compress::Zlib, 'git svn gc' will not ".
	"fully gc\n";

# These ones are required

my $errors = 0;

# For SHA1, any of Digest::SHA, Digest::SHA1 or Digest::SHA::PurePerl will do

eval {require Digest::SHA; 1} ||
eval {require Digest::SHA1; 1} ||
eval {require Digest::SHA::PurePerl; 1} or
++$errors,
print STDERR "One of the Perl modules Digest::SHA or Digest::SHA1 or "
. "Digest::SHA::PurePerl must be available\n";

# For XML, either of XML::Simple or XML::Parser will do

eval {require XML::Simple; 1} ||
eval {require XML::Parser; 1} or
++$errors,
print STDERR "One of the Perl modules XML::Simple or "
. "XML::Parser must be available\n";

my @modules = qw(
	CGI
	CGI::Carp
	CGI::Util
	Digest::MD5
	Encode
	Errno
	Fcntl
	File::Basename
	File::Find
	File::Spec
	File::stat
	Getopt::Long
	IO::Handle
	IO::Socket
	IO::Socket::UNIX
	IPC::Open2
	JSON
	LWP::UserAgent
	MIME::Base64
	Pod::Usage
	POSIX
	RPC::XML
	RPC::XML::Client
	Socket
	Storable
	Time::HiRes
	Time::Local
);

my %imports = (
	Time::HiRes => [qw(gettimeofday tv_interval)],
);

foreach my $module (@modules) {
	my $imports = '';
	$imports = " qw(".join(' ', @{$imports{$module}}).")"
		if $imports{$module};
	eval "use $module$imports; 1" or
	++$errors, print STDERR "Missing required Perl module $module$imports\n";
}

eval {require JSON; 1} &&
eval {require LWP::UserAgent; 1} &&
eval {require RPC::XML; 1} &&
eval {require RPC::XML::Client; 1} or
print STDERR "Note: Perl modules JSON, LWP::UserAgent, RPC::XML and RPC::".
	"XML::Client are required by the ref-change notification mechanism\n";

my $fcgi_ok = 1;
eval {require FCGI; 1} or $fcgi_ok = 0,
print STDERR "Note: running gitweb.cgi in FastCGI mode requires ".
	"the missing Perl module FCGI\n";
eval {require CGI::Fast; 1} or $fcgi_ok = 0,
print STDERR "Note: running gitweb.cgi in FastCGI mode requires ".
	"the missing Perl module CGI::Fast\n";
!$fcgi_ok || eval {require FCGI::ProcManager; 1} or
print STDERR "Note: gitweb.cgi running in FastCGI mode requires ".
	"missing FCGI::ProcManager to support the --nproc option\n";

!$errors or print STDERR "\nFatal: required perl modules are missing!\n\n";

exit($errors ? 1 : 0);
