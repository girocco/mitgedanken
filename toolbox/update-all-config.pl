#!/usr/bin/perl

# update-all-config.pl - Update all out-of-date config

use strict;
use warnings;
use vars qw($VERSION);
BEGIN {*VERSION = \'2.0'}
use File::Basename;
use File::Spec;
use Cwd qw(realpath);
use POSIX qw();
use Getopt::Long;
use Pod::Usage;
use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::Util;
use Girocco::CLIUtil;
use Girocco::Project;

my $shbin;
BEGIN {
    $shbin = $Girocco::Config::posix_sh_bin;
    defined($shbin) && $shbin ne "" or $shbin = "/bin/sh";
}

exit(&main(@ARGV)||0);

my ($dryrun, $force, $quiet);

sub die_usage {
	pod2usage(-exitval => 2);
}

sub do_help {
	pod2usage(-verbose => 2, -exitval => 0);
}

sub do_version {
	print basename($0), " version ", $VERSION, "\n";
	exit 0;
}

my ($dmode, $dperm, $drwxmode, $fmode, $fmodeoct, $fperm, $wall);
BEGIN {
	$dmode=02775;
	$dperm='drwxrwsr-x';
	$drwxmode='ug+rwx,o+rx';
	$fmode=0664;
	$fmodeoct='0664';
	$fperm='-rw-rw-r--';
	$wall=0;
}

my $owning_group_id;
my $htmlcache_owning_group_id;
my $ctags_owning_group_id;

sub main {
	local *ARGV = \@_;
	my ($help, $version);

	umask 002;
	close(DATA) if fileno(DATA);
	Getopt::Long::Configure('bundling');
	GetOptions(
		'help|h'	=> sub {do_help},
		'version|V'	=> sub {do_version},
		'dry-run|n'	=> \$dryrun,
		'quiet|q'	=> \$quiet,
		'force|f'	=> \$force,
	) or die_usage;
	$dryrun and $quiet = 0;

	-f jailed_file("/etc/group") or
		die "Girocco group file not found: " . jailed_file("/etc/group") . "\n";

	if (!defined($Girocco::Config::owning_group) || $Girocco::Config::owning_group eq "") {
		die "\$Girocco::Config::owning_group unset, refusing to run without --force\n" unless $force;
		$dmode=02777;
		$dperm='drwxrwsrwx';
		$drwxmode='a+rwx';
		$fmode=0666;
		$fmodeoct='0666';
		$fperm='-rw-rw-rw-';
		$wall=1;
		warn "Mode 666 in effect\n" unless $quiet;
	} elsif (($owning_group_id = scalar(getgrnam($Girocco::Config::owning_group))) !~ /^\d+$/) {
		die "\$Girocco::Config::owning_group invalid ($Girocco::Config::owning_group), refusing to run\n";
	}
	if (defined($owning_group_id) && $Girocco::Config::htmlcache_owning_group) {
		die "\$Girocco::Config::htmlcache_owning_group invalid ($Girocco::Config::htmlcache_owning_group), refusing to run\n"
		unless ($htmlcache_owning_group_id = scalar(getgrnam($Girocco::Config::htmlcache_owning_group))) =~ /^\d+$/;
	}
	if (defined($owning_group_id) && $Girocco::Config::ctags_owning_group) {
		die "\$Girocco::Config::ctags_owning_group invalid ($Girocco::Config::ctags_owning_group), refusing to run\n"
		unless ($ctags_owning_group_id = scalar(getgrnam($Girocco::Config::ctags_owning_group))) =~ /^\d+$/;
	}

	my @allprojs = Girocco::Project::get_full_list;
	my @projects = ();

	my $root = $Girocco::Config::reporoot;
	$root or die "\$Girocco::Config::reporoot is invalid\n";
	$root =~ s,/+$,,;
	$root ne "" or $root = "/";
	$root = realpath($root);
	if (@ARGV) {
		my %projnames = map {($_ => 1)} @allprojs;
		foreach (@ARGV) {
			s,/+$,,;
			$_ or $_ = "/";
			-d $_ and $_ = realpath($_);
			s,^\Q$root\E/,,;
			s,\.git$,,;
			if (!exists($projnames{$_})) {
				warn "$_: unknown to Girocco (not in etc/group)\n"
					unless $quiet;
				next;
			}
			push(@projects, $_);
		}
	} else {
		@projects = sort {lc($a) cmp lc($b)} @allprojs;
	}

	nice_me(18);
	my $bad = 0;
	foreach (@projects) {
		my $projdir = "$root/$_.git";
		if (! -d $projdir) {
			warn "$_: does not exist -- skipping\n" unless $quiet;
			next;
		}
		if (!is_git_dir($projdir)) {
			warn "$_: is not a .git directory -- skipping\n" unless $quiet;
			next;
		}
		if (-e "$projdir/.noconfig") {
			warn "$_: found .noconfig -- skipping\n" unless $quiet;
			next;
		}
		if (!chdir($projdir)) {
			warn "$_: chdir to project directory failed: $!\n" unless $quiet;
			next;
		}
		process_one_project($_) or $bad = 1;
	}

	return $bad ? 1 : 0;
}

my (@mkdirs, @mkfiles);
my (@fixdpermsdirs, @fixdpermsrwx, @fixfpermsfiles, @fixfpermsdirs);
BEGIN {
	@mkdirs = qw(refs info hooks ctags htmlcache bundles reflogs objects objects/info);
	@mkfiles = qw(config info/lastactivity);
	@fixdpermsdirs = qw(. refs info ctags htmlcache bundles reflogs objects objects/info);
	@fixdpermsrwx = qw(refs objects);
	@fixfpermsfiles = qw(HEAD config description packed-refs README.html info/lastactivity
		info/alternates info/http-alternates info/packs);
	@fixfpermsdirs = qw(ctags);
}

my (@boolvars, @falsevars, @false0vars, @truevars);
BEGIN {
	@boolvars = qw(gitweb.statusupdates);
	@falsevars = qw(core.ignorecase receive.denynonfastforwards);
	@false0vars = qw(receive.autogc);
	@truevars = qw(receive.fsckobjects receive.updateserverinfo repack.writebitmaps transfer.fsckobjects);
}

my $hdr;

sub defval($$) {
	return defined($_[0]) ? $_[0] : $_[1];
}

sub openfind_ {
	my $noe = shift;
	my $duperr;
	if ($noe) {{
		open $duperr, '>&2' or last;
		my $errfd = POSIX::open(File::Spec->devnull, &POSIX::O_RDWR);
		defined($errfd) or close($duperr), $duperr = undef, last;
		POSIX::dup2($errfd, 2) or close($duperr), $duperr = undef;
		POSIX::close($errfd);
	}}
	my $fd;
	my $ans = open $fd, '-|', "find", @_;
	if ($noe && defined($duperr) && defined(fileno($duperr))) {
		POSIX::dup2(fileno($duperr), 2);
		close($duperr);
	}
	$ans or die "find failed: $!\n";
	return $fd;
}

sub openfind { return openfind_(0, @_); }
sub openfindne { return openfind_(1, @_); }

sub all_remotes
{
	my $config = shift;
	return map({
		my ($i,$r) = (index($_,"."),rindex($_,"."));
		substr($_,$i+1,$r-$i-1);
	} grep(/^remote\.[^.].*\.url$/i, keys(%$config)));
}

sub has_default_fetch_spec
{
	my $config = shift;
	my $default = $config->{'remotes.default'};
	my @remotes = defined($default) ? split(' ', $default) : all_remotes($config);
	foreach (@remotes) {
		defval($config->{"remote.$_.url"},"") ne "" or next;
		!defined($default) && git_bool($config->{"remote.$_.skipdefaultupdate"}) and next;
		defval($config->{"remote.$_.fetch"},"") ne "" and return 1;
	}
	return 0;
}

sub is_native_git_mirror_url
{
	my $bu = shift;
	defined($bu) && $bu ne "" or return 0;
	# All current or former natively supported by Git URLs return true:
	#  1. rsync: (removed in 2.8.0, also recognize rsync+ and rsync::)
	#  2. ftp:/ftps: (strongly discouraged)
	#  3. git:
	#  4. http:/https: (smart and non-smart)
	#  5. ssh:
	#  6. scp-like ssh syntax [user@]host:[^:/]
	return $bu =~ /^(?:
		rsync[:+]	|
		ftps?:		|
		git:		|
		https?:		|
		ssh:		|
		(?:[^\s:\@]+\@)?[^\s:\@+]+:(?!\/\/)[^\s:\\]
	)/xi;
}

sub process_one_project
{
	my ($proj) = @_;
	my $bad = 0;
	my $reallybad = 0;
	$hdr = 0;
	do {
		if (! -d $_) {
			if (-e $_) {
				warn "$proj: bypassing project, exists but not directory: $_\n" unless $quiet;
				$reallybad = $bad = 1;
				last;
			} else {
				my $grpid = $owning_group_id;
				$grpid = $htmlcache_owning_group_id
					if $htmlcache_owning_group_id && $_ eq "htmlcache";
				$grpid = $ctags_owning_group_id
					if $ctags_owning_group_id && $_ eq "ctags";
				do_mkdir($proj, $_, $grpid) or $bad = 1, last;
			}
		}
	} foreach (@mkdirs);
	return 0 if $reallybad;

	-d $_ && check_dperm($proj, $_) or $bad = 1 foreach (@fixdpermsdirs);
	my $fp = openfindne(@fixdpermsrwx, qw(-xdev -type d ( ! -path objects/?? -o -prune ) ! -perm), "-$drwxmode", "-print");
	while (<$fp>) {
		chomp;
		change_dpermrwx($proj, $_) or $bad = 1;
	}
	close($fp) or $bad = 1;
	$fp = openfind(qw(. -xdev -type d ( ! -path ./objects/?? -o -prune ) ! -perm -a+rx -print));
	while (<$fp>) {
		chomp;
		change_dpermrx($proj, $_) or $bad = 1;
	}
	close($fp) or $bad = 1;

	do {
		if (-e $_) {
			if (! -f $_) {
				warn "$proj: bypassing project, exists but not file: $_\n" unless $quiet;
				$reallybad = $bad = 1;
				last;
			}
		} else {
			my $result = "(dryrun)";
			if (!$dryrun) {
				$result = "";
				my $tf;
				open($tf, '>', $_) && close ($tf) or $result = "FAILED", $bad = 1;
			}
			pmsg($proj, "$_: created", $result) unless $quiet;
		}
	} foreach(@mkfiles);
	return 0 if $reallybad;

	$dryrun || check_fperm($proj, "config") or $bad = 1;
	my $config = read_config_file_hash("config", !$quiet);
	if (!defined($config)) {
		warn "$proj: could not read config file -- skipping\n" unless $quiet;
		return 0;
	}

	my $do_config = sub {
		my ($item, $val) = @_;
		my $oldval = defval($config->{$item},"");
		my $result = "(dryrun)";
		if (!$dryrun) {
			$result = "";
			system($Girocco::Config::git_bin, "config", "--file", "config", "--replace-all", $item, $val) == 0 or
				$result = "FAILED", $bad = 1;
		}
		if (!exists($config->{$item})) {
			pmsg($proj, "config $item: created \"$val\"", $result) unless $quiet;
		} else {
			pmsg($proj, "config $item: \"$oldval\" -> \"$val\"", $result) unless $quiet;
		}
	};
	my $do_config_unset = sub {
		my ($item, $msg) = @_;
		defined($msg) or $msg = "";
		$msg eq "" or $msg = " " . $msg;
		my $oldval = defval($config->{$item},"");
		my $result = "(dryrun)";
		if (!$dryrun) {
			$result = "";
			system($Girocco::Config::git_bin, "config", "--file", "config", "--unset-all", $item) == 0 or
				$result = "FAILED", $bad = 1;
		}
		pmsg($proj, "config $item: removed$msg \"$oldval\"", $result) unless $quiet;
	};

	my $repovers = $config->{'core.repositoryformatversion'};
	if (!defined($repovers)) {
		$repovers = "";
	} elsif ($repovers =~ /^[2345]$/) {
		pmsg($proj, "WARNING: unknown core.repositoryformatversion value left unchanged: \"$repovers\"");
	} elsif ($repovers !~ /^[01]$/) {
		pmsg($proj, "WARNING: replacing invalid core.repositoryformatversion value: \"$repovers\"") unless $quiet;
		$repovers = "";
	}
	&$do_config('core.repositoryformatversion', 0) if $repovers eq "";
	my $hookspath = $Girocco::Config::reporoot . "/_global/hooks";
	my $cfghooks = defval($config->{'core.hookspath'},"");
	if ($cfghooks ne $hookspath) {
		my $updatehookspath = 1;
		$hookspath = $Girocco::Config::reporoot . "/$proj.git/hooks" if $Girocco::Config::localhooks;
		if ($cfghooks =~ m{^/[^/]} && -d $cfghooks && -d "hooks") {
			# tolerate this situation provided the realpath of $cfghooks
			# matches the realpath of the hooks subdirectory and the hooks
			# subdirectory exists; actually making sure the correct symlinks
			# are present remains up to update-all-hooks not us
			if (realpath($cfghooks) eq realpath("hooks")) {
				# we do, however, insist that it be stored exactly
				# as $reporoot/<project_name>.git/hooks in this case because
				# that's the only guaranteed version that works in the chroot
				$hookspath = $Girocco::Config::reporoot . "/$proj.git/hooks";
				$cfghooks eq $hookspath and $updatehookspath = 0;
			}
		}
		&$do_config('core.hookspath', $hookspath) if $updatehookspath;
	}
	my $cmplvl = defval($config->{'core.compression'},"");
	if ($cmplvl !~ /^-?\d+$/ || $cmplvl < -1 || $cmplvl > 9 || "" . (0 + $cmplvl) ne "" . $cmplvl) {
		pmsg($proj, "WARNING: replacing invalid core.compression value: \"$cmplvl\"") unless $cmplvl eq "" || $quiet;
		$cmplvl = ""; 
	} elsif ($cmplvl != 5) {
		pmsg($proj, "WARNING: suboptimal core.compression value left unchanged: \"$cmplvl\"") unless $quiet;
	}
	$cmplvl ne "" or &$do_config('core.compression', 5);
	my $grpshr = defval($config->{'core.sharedrepository'},"");
	if ($grpshr eq "" || (valid_bool($grpshr) && !git_bool($grpshr))) {
		&$do_config('core.sharedrepository', 1);
	} elsif (!(valid_bool($grpshr) && git_bool($grpshr))) {
		pmsg($proj, "WARNING: odd core.sharedrepository value left unchanged: \"$grpshr\"");
	}
	if (git_bool($config->{'core.bare'})) {
		my $setlaru = 1;
		my $laru = $config->{'core.logallrefupdates'};
		if (defined($laru)) {
			if (valid_bool($laru)) {
				$setlaru = 0;
				if (git_bool($laru)) {
					pmsg($proj, "WARNING: core.logallrefupdates is true (left unchanged)")
						unless $quiet || -d "worktrees";
				}
			} else {
				pmsg($proj, "WARNING: replacing non-boolean core.logallrefupdates value") unless $quiet;
			}
		}
		!$setlaru or &$do_config('core.logallrefupdates', 'false');
	} else {
		pmsg($proj, "WARNING: core.bare is not true (left unchanged)") unless $quiet;
	}
	my $precious = defval($config->{'extensions.preciousobjects'},"");
	valid_bool($precious) && git_bool($precious) or &$do_config('extensions.preciousobjects', 'true');
	defval($config->{'transfer.unpacklimit'},"") eq "1" or &$do_config('transfer.unpacklimit', 1);
	lc(defval($config->{'receive.denydeletecurrent'},"")) eq "warn" or &$do_config('receive.denydeletecurrent', 'warn');
	do {
		!exists($config->{$_}) || valid_bool(defval($config->{$_},"")) or &$do_config_unset($_, "(not a boolean)");
	} foreach (@boolvars);
	do {
		(valid_bool(defval($config->{$_},"")) && !git_bool($config->{$_})) or &$do_config($_, "false");
	} foreach (@falsevars);
	do {
		(valid_bool(defval($config->{$_},"")) && !git_bool($config->{$_})) or &$do_config($_, 0);
	} foreach (@false0vars);
	do {
		(valid_bool(defval($config->{$_},"")) && git_bool($config->{$_})) or &$do_config($_, "true");
	} foreach (@truevars);

	if (defined($Girocco::Config::owning_group) && $Girocco::Config::owning_group ne "") {
		$fp = openfind(qw(. -xdev ( -type d -o -type f ) ! -group), $Girocco::Config::owning_group, "-print");
		while (<$fp>) {
			chomp;
			my $grpid = $owning_group_id;
			$grpid = $htmlcache_owning_group_id if $htmlcache_owning_group_id && m{^\./htmlcache(?:/|$)}i;
			$grpid = $ctags_owning_group_id if $ctags_owning_group_id && m{^\./ctags(?:/|$)}i;
			change_group($proj, $_, $grpid) or $bad = 1;
		}
		close($fp) or $bad = 1;
	}
	foreach (@fixfpermsfiles) {
		if (-e $_) {
			if (! -f $_) {
				warn "$proj: bypassing project, exists but not file: $_\n" unless $quiet;
				$reallybad = $bad = 1;
				last;
			}
			check_fperm($proj, $_) or $bad = 1;
		}
	}
	return 0 if $reallybad;

	$fp = openfindne(@fixfpermsdirs, qw(-xdev -type f ! -perm), $fmodeoct, "-print");
	while (<$fp>) {
		chomp;
		check_fperm($proj, $_) or $bad = 1;
	}
	close($fp) or $bad = 1;
	$fp = openfind(qw(. -xdev -type f ! -perm -a+r -print));
	while (<$fp>) {
		chomp;
		check_fpermr($proj, $_) or $bad = 1;
	}
	close($fp) or $bad = 1;
	$fp = openfind(qw(. -xdev -type d ( -path ./hooks -o -path ./mob/hooks ) -prune -o -type f -perm +a+x -print));
	while (<$fp>) {
		chomp;
		check_fpermnox($proj, $_) or $bad = 1;
	}
	close($fp) or $bad = 1;

	my $bu = defval($config->{'gitweb.baseurl'},"");
	if (-e ".nofetch") {
		$bu eq "" or pmsg($proj, "WARNING: .nofetch exists but gitweb.baseurl is not empty ($bu)") unless $quiet;
	} else {
		if ($bu eq "") {
			if (has_default_fetch_spec($config)) {
				pmsg($proj, "WARNING: gitweb.baseurl is empty and .nofetch does not exist but fetch spec does") unless $quiet;
			} else {
				pmsg($proj, "WARNING: gitweb.baseurl is empty and .nofetch does not exist") unless $quiet;
			}
		} elsif (is_native_git_mirror_url($bu) && !has_default_fetch_spec($config)) {
			pmsg($proj, "WARNING: gitweb.baseurl is not empty but fetch spec is") unless $quiet;
		}
	}

	return !$bad;
}

sub do_mkdir
{
	my ($proj, $subdir, $grpid) = @_;
	my $result = "";
	if (!$dryrun) {
		mkdir($subdir) && -d "$subdir" or $result = "FAILED";
		if ($grpid && $grpid != $owning_group_id) {
			my @info = stat($subdir);
			if (@info < 6 || $info[2] eq "" || $info[4] eq "" || $info[5] eq "") {
				$result = "FAILED";
			} elsif ($info[5] != $grpid) {
				if (!chown($info[4], $grpid, $subdir)) {
					$result = "FAILED";
					warn "chgrp: ($proj) $subdir: $!\n" unless $quiet;
				} elsif (!chmod($info[2] & 07777, $subdir)) {
					$result = "FAILED";
					warn "chmod: ($proj) $subdir: $!\n" unless $quiet;
				}
			}
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$subdir/: created", $result);
	return $result ne "FAILED";
}

sub check_dperm {
	my ($proj, $subdir) = @_;
	my $oldmode = (stat($subdir))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $subdir: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = ($oldmode & ~07777) | $dmode;
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $subdir)) {
			$result = "FAILED";
			warn "chmod: ($proj) $subdir: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$subdir/:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub change_dpermrwx {
	my ($proj, $subdir) = @_;
	my $oldmode = (stat($subdir))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $subdir: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = $oldmode | ($wall ? 0777 : 0775);
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $subdir)) {
			$result = "FAILED";
			warn "chmod: ($proj) $subdir: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$subdir/:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub change_dpermrx {
	my ($proj, $subdir) = @_;
	$subdir =~ s,^\./,,;
	my $oldmode = (stat($subdir))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $subdir: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = $oldmode | 0555;
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $subdir)) {
			$result = "FAILED";
			warn "chmod: ($proj) $subdir: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$subdir/:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub check_fperm {
	my ($proj, $file) = @_;
	my $oldmode = (stat($file))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $file: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = ($oldmode & ~07777) | $fmode;
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $file)) {
			$result = "FAILED";
			warn "chmod: ($proj) $file: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$file:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub check_fpermr {
	my ($proj, $file) = @_;
	$file =~ s,^\./,,;
	my $oldmode = (stat($file))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $file: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = $oldmode | 0444;
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $file)) {
			$result = "FAILED";
			warn "chmod: ($proj) $file: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$file:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub check_fpermnox {
	my ($proj, $file) = @_;
	$file =~ s,^\./,,;
	my $oldmode = (stat($file))[2];
	if (!defined($oldmode) || $oldmode eq "") {
		warn "chmod: ($proj) $file: No such file or directory\n" unless $quiet;
		return 0;
	}
	my $newmode = $oldmode & ~0111;
	$newmode == $oldmode and return 1;	
	my $result = "";
	if (!$dryrun) {
		if (!chmod($newmode & 07777, $file)) {
			$result = "FAILED";
			warn "chmod: ($proj) $file: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	pmsg($proj, "$file:", get_mode_perm($oldmode), '->', get_mode_perm($newmode), $result);
	return $result ne "FAILED";
}

sub change_group {
	my ($proj, $item, $grpid) = @_;
	$item =~ s,^\./,,;
	my @info = stat($item);
	if (@info < 6 || $info[2] eq "" || $info[4] eq "" || $info[5] eq "") {
		warn "chgrp: ($proj) $item: No such file or directory\n" unless $quiet;
		return 0;
	}
	$info[5] == $grpid and return 1;
	my $result = "";
	if (!$dryrun) {
		if (!chown($info[4], $grpid, $item)) {
			$result = "FAILED";
			warn "chgrp: ($proj) $item: $!\n" unless $quiet;
		} elsif (!chmod($info[2] & 07777, $item)) {
			$result = "FAILED";
			warn "chmod: ($proj) $item: $!\n" unless $quiet;
		}
	} else {
		$result = "(dryrun)";
	}
	my $isdir = ((($info[2] >> 12) & 017) == 004) ? '/' : '';
	pmsg($proj, "$item$isdir: group", get_grp_nam($info[5]), '->', get_grp_nam($grpid), $result);
	return $result ne "FAILED";
}

my $wrote; BEGIN {$wrote = ""}
sub pmsg {
	my $proj = shift;
	my $msg = join(" ", @_);
	$msg =~ s/\s+$//;
	my $prefix = "";
	if (!$hdr) {
		$prefix = $wrote . $proj . ":\n";
		$hdr = 1;
	}
	print $prefix, "  ", join(' ', @_), "\n";
	$wrote = "\n";
}

my %ftypes;
BEGIN {%ftypes = (
	000 => '?',
	001 => 'p',
	002 => 'c',
	003 => '?',
	004 => 'd',
	005 => '?',
	006 => 'b',
	007 => '?',
	010 => '-',
	011 => '?',
	012 => 'l',
	013 => '?',
	014 => 's',
	015 => '?',
	016 => 'w',
	017 => '?'
)}
my %fperms;
BEGIN {%fperms = (
	0 => '---',
	1 => '--x',
	2 => '-w-',
	3 => '-wx',
	4 => 'r--',
	5 => 'r-x',
	6 => 'rw-',
	7 => 'rwx'
)}

sub get_mode_perm {
	my $mode = $_[0];
	my $str = $ftypes{($mode >> 12) & 017} .
		$fperms{($mode >> 6) & 7} .
		$fperms{($mode >> 3) & 7} .
		$fperms{$mode & 7};
	substr($str,3,1) = ($mode & 0100) ? 's' : 'S' if $mode & 04000;
	substr($str,6,1) = ($mode & 0010) ? 's' : 'S' if $mode & 02000;
	substr($str,9,1) = ($mode & 0001) ? 't' : 'T' if $mode & 01000;
	return $str;
}

sub get_perm {
	my $mode = (stat($_[0]))[2];
	defined($mode) or return '??????????';
	return get_mode_perm($mode);
}

sub get_grp_nam {
	my $grpid = $_[0];
	defined($grpid) or return '?';
	my $grpnm = scalar(getgrgid($grpid));
	return defined($grpnm) && $grpnm ne "" ? $grpnm : $grpid;
}

sub get_grp {
	my $grp = (stat($_[0]))[5];
	defined($grp) or return '?';
	return get_grp_nam($grp);
}

__END__

=head1 NAME

update-all-config.pl - Update all projects' config settings

=head1 SYNOPSIS

update-all-config.pl [<options>] [<projname>]...

 Options:
   -h | --help                  detailed instructions
   -V | --version               show version
   -n | --dry-run               show what would be done but don't do it
   -f | --force                 run without a Config.pm owning_group
   -q | --quiet                 suppress change messages		

 <projname>                     if given, only operate on these projects

=head1 OPTIONS

=over 8

=item B<-h>, B<--help>

Print the full description of update-all-config.pl's options.

=item B<-V>, B<--version>

Print the version of update-all-config.pl.

=item B<-n>, B<--dry-run>

Do not actually make any changes, just show what would be done without
actually doing it.

=item B<-q>, B<--quiet>

Suppress the messages about what's actually being changed.  This option
is ignored if B<--dry-run> is in effect.

The warnings about missing and unknown-to-Girocco projects are also
suppressed by this option.

=item B<-f>, B<--force>

Allow running without a $Girocco::Config::owning_group set.  This is not
recommended as it results in world-writable items being used (instead of
just world-readable).

=item B<<projname>>

If no project names are specified then I<all> projects are processed.

If one or more project names are specified then only those projects are
processed.  Specifying non-existent projects produces a warning for them,
but the rest of the projects specified will still be processed.

Each B<projname> may be either a full absolute path starting with
$Girocco::Config::reporoot or just the project name part with or without
a trailing C<.git>.

Any explicitly specified projects that do exist but are not known to
Girocco will be skipped (with a warning).

=back

=head1 DESCRIPTION

Inspect the C<config> files of Girocco projects (i.e. $GIT_DIR/config) and
look for anomalies and out-of-date settings.

Additionally check the existence and permissions on various files and
directories in the project.

If an explicity specified project is located under $Girocco::Config::reporoot
but is not actually known to Girocco (i.e. it's not in the etc/group file)
then it will be skipped.

By default, any anomalies or out-of-date settings will be corrected with a
message to that effect.  However using B<--dry-run> will only show the
correction(s) which would be made without making them and B<--quiet> will make
the correction(s) without any messages.

Any projects that have a C<$GIT_DIR/.noconfig> file are always skipped (with a
message unless B<--quiet> is used).

=cut
