#!/bin/sh

set -e

. @basedir@/shlib.sh

if [ -z "$1" ]; then
	echo "Usage: $0 <username>"
	exit 1
fi
u="$1"
qu="$(printf '%s' "$u" | sed -e 's/\./\\./g' -e 's/+/[+]/g')"
ETC="$cfg_chroot/etc"
COUNT="$(grep -E -c "^$qu:" "$ETC/passwd")" || :
if [ "$COUNT" -ne "1" ]; then
	echo "fatal: user '$u' doesn't appear to exist (or exists multiple times, or contains regexpy characters)."
	exit 1
fi
ENTRY="$(grep -E "^$qu:" "$ETC/passwd" | cut -d : -f 1-5)"
GRPS="$( (grep -E '^[^:]+:[^:]+:[^:]+.*(:|,)'"$qu"'(,|:|$)' "$ETC/group" || :) | cut -d : -f 1 )" || :
if [ "$GRPS" ]; then
	echo "User '$u' is still part of these groups:" $GRPS
	echo "fatal: this simplistic script cannot remove users from groups."
	exit 1
fi
sed -e "/^$qu:/ d" "$ETC/passwd" > "$ETC/passwd.$$"
mv -f "$ETC/passwd.$$" "$ETC/passwd" || :
rm -f "$ETC/passwd.$$" || :
rm -f "$ETC/sshkeys/$u" || :
rm -f "$ETC/sshcerts/$cfg_nickname"_"$u"_user_*.pem || :
rm -f "$ETC/sshactive/$u" || :
rm -f "$ETC/sshactive/$u",* || :
! [ -e "$ETC/sshkeys/$u" ] || echo "Warning: unable to remove $ETC/sshkeys/$u" >&2
[ "$(echo "$ETC/sshcerts/$cfg_nickname"_"$u"_user_*.pem)" = "$ETC/sshcerts/$cfg_nickname"_"$u"_user_"*".pem ] ||
	echo "Warning: unable to remove $(echo "$ETC/sshcerts/$cfg_nickname"_"$u"_user_*.pem)" >&2
if [ -n "$cfg_update_pwd_db" ] && [ "$cfg_update_pwd_db" != "0" ]; then
	"$cfg_basedir/bin/update-pwd-db" "$ETC/passwd" "$u"
fi
echo "User \"$ENTRY\" (+SSH key/certs) removed."
