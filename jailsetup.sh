#!/bin/sh
# The Girocco jail setup script
#
# If the first parameter is "dbonly", setup the database only
#
# We are designed to set up the chroot based on the output of
# `uname -s` by sourcing a suitable system-specific script.
# Unrecognized systems will generate an error.  When using
# "dbonly" the setup of the chroot binaries is skipped so the
# output of `uname -s` does not matter in that case.

set -e

curdir="$(pwd)"
srcdir="$curdir/src"
getent="$srcdir/getent"
. ./shlib.sh

# find_std_utility should always come up with the full path to the standard
# version of the utility whose name is passed as "$1"
getconf="/usr/bin/getconf"
[ -x "$getconf" ] || getconf="/bin/getconf"
[ -x "$getconf" ] || getconf="getconf"
stdpath="$("unset" -f command; "command" "$getconf" "PATH" 2>/dev/null)" || :
":" "${stdpath:=/bin:/usr/bin}"
stdpath="$stdpath:/sbin:/usr/sbin"
find_std_utility() (
	"unset" -f unalias command "$1" >/dev/null 2>&1 || :
	"unalias" -a >/dev/null 2>&1 || :
	PATH="$stdpath" && "export" PATH || :
	"command" -v "$1"
) 2>/dev/null

dbonly=
[ "$1" != "dbonly" ] || dbonly=1

reserved_users="root sshd _sshd mob git lock bundle nobody everyone $cfg_cgi_user $cfg_mirror_user"

# Require either sshd or _sshd user unless "dbonly"
sshd_user=sshd
if ! "$getent" passwd sshd >/dev/null && ! "$getent" passwd _sshd >/dev/null; then
	if [ -n "$dbonly" ]; then
		if ! [ -s etc/passwd ]; then
			# Only complain on initial etc/passwd creation
			echo "WARNING: no sshd or _sshd user, omitting entries from chroot etc/passwd"
		fi
		sshd_user=
	else
		echo "*** Error: You do not have required sshd or _sshd user in system." >&2
		exit 1
	fi
else
	"$getent" passwd sshd >/dev/null || sshd_user=_sshd
fi

# Verify we have all we need
if ! "$getent" passwd "$cfg_mirror_user" >/dev/null; then
	echo "*** Error: You do not have \"$cfg_mirror_user\" user in system yet." >&2
	exit 1
fi
if ! "$getent" passwd "$cfg_cgi_user" >/dev/null; then
	echo "*** Error: You do not have \"$cfg_cgi_user\" user in system yet." >&2
	exit 1
fi
if [ -n "$dbonly" ] && [ -z "$cfg_owning_group" ]; then
	cfg_owning_group="$("$getent" passwd "$cfg_mirror_user" | cut -d : -f 4)"
elif ! "$getent" group "$cfg_owning_group" >/dev/null; then
	echo "*** Error: You do not have \"$cfg_owning_group\" group in system yet." >&2
	exit 1
fi

# One last paranoid check before we go writing all over everything
if [ -z "$cfg_chroot" ] || [ "$cfg_chroot" = "/" ]; then
	echo "*** Error: chroot location is not set or is invalid." >&2
	echo "*** Error: perhaps you have an incorrect Config.pm?" >&2
	exit 1
fi

umask 022
mkdir -p "$cfg_chroot"
cd "$cfg_chroot"
chmod 755 "$cfg_chroot" ||
	echo "WARNING: Cannot chmod $cfg_chroot"

mkdir -p var/empty
chmod 0555 var/empty ||
	echo "WARNING: Cannot chmod a=rx $cfg_chroot/var/empty"

# Set up basic user/group configuration; if there isn't any already
mobpass=
[ -n "$cfg_mob" ] || mobpass='x'
mkdir -p etc
if ! [ -s etc/passwd ]; then
	cat >etc/passwd <<EOT
root:x:0:0:system administrator:/var/empty:/bin/false
nobody:x:$("$getent" passwd nobody | cut -d : -f 3-4):unprivileged user:/var/empty:/bin/false
EOT
	[ -z "$sshd_user" ] || cat >>etc/passwd <<EOT
sshd:x:$("$getent" passwd $sshd_user | cut -d : -f 3-4):privilege separation:/var/empty:/bin/false
_sshd:x:$("$getent" passwd $sshd_user | cut -d : -f 3-4):privilege separation:/var/empty:/bin/false
EOT
	[ "$cfg_cgi_user" = "$cfg_mirror_user" ] || cat >>etc/passwd <<EOT
$cfg_cgi_user:x:$("$getent" passwd "$cfg_cgi_user" | cut -d : -f 3-5):/:/bin/true
EOT
	cat >>etc/passwd <<EOT
$cfg_mirror_user:x:$("$getent" passwd "$cfg_mirror_user" | cut -d : -f 3-5):/:/bin/true
everyone:x:65537:$("$getent" group "$cfg_owning_group" | cut -d : -f 3):every user:/:/bin/false
mob:$mobpass:65538:$("$getent" group "$cfg_owning_group" | cut -d : -f 3):the mob:/:/bin/git-shell-verify
git::65539:$("$getent" passwd nobody | cut -d : -f 4):read-only access:/:/bin/git-shell-verify
EOT
elif [ -z "$dbonly" ]; then
	# Make sure an sshd entry is present
	if ! grep -q '^sshd:' etc/passwd; then
		echo "*** Error: chroot etc/passwd exists but lacks sshd entry." >&2
		exit 1
	fi
fi

if ! [ -s etc/group ]; then
	cat >etc/group <<EOT
_repo:x:$("$getent" group "$cfg_owning_group" | cut -d : -f 3):$cfg_mirror_user
EOT
fi

# Set up basic default Git configuration
# Initialize one if none exists or update critical variables for an existing one
mkdir -p etc/girocco
didchmod=
if [ -e etc/girocco/.gitconfig ] && ! [ -f etc/girocco/.gitconfig ]; then
	echo "*** Error: chroot etc/girocco/.gitconfig exists but is not a file." >&2
	exit 1
fi
if [ -f etc/girocco/.gitconfig ]; then
	gcerr=0
	x="$(git config --file etc/girocco/.gitconfig --get "no--such--section.no  such  subsection.no--such--key")" || gcerr=$?
	if [ $gcerr -gt 1 ]; then
		echo "*** Error: chroot etc/girocco/.gitconfig exists but is corrupt." >&2
		echo "*** Error: either remove it or edit it to correct the problem." >&2
		exit 1
	fi
fi
if ! [ -s etc/girocco/.gitconfig ]; then
	chmod u+w etc/girocco
	didchmod=1
	cat >etc/girocco/.gitconfig <<EOT
# Any values set here will take effect whenever Girocco runs a git command

EOT
fi
# $1 => name, $2 => value, $3 => overwrite_flag
# if $3 is "2" and $2 is "" value will be unset
update_config_item() {
	_existsnot=
	_oldval=
	_oldval="$(git config --file etc/girocco/.gitconfig --get "$1")" || _existsnot=1
	if [ -n "$_existsnot" ]; then
		[ -n "$2" ] || [ "$3" != "2" ] || return 0
	else
		[ -n "$3" ] || return 0
		[ "$_oldval" != "$2" ] || { [ "$3" = "2" ] && [ -z "$2" ]; } || return 0
	fi
	[ -n "$didchmod" ] || { chmod u+w etc/girocco; didchmod=1; }
	if [ "$3" = "2" ] && [ -z "$2" ]; then
		git config --file etc/girocco/.gitconfig --unset "$1"
	else
		git config --file etc/girocco/.gitconfig "$1" "$2"
	fi
	if [ -n "$_existsnot" ]; then
		echo "chroot: etc/girocco/.gitconfig: config $1: (created) \"$2\""
	elif [ "$3" = "2" ] && [ -z "$2" ]; then
		echo "chroot: etc/girocco/.gitconfig: config $1: (removed)"
	else
		echo "chroot: etc/girocco/.gitconfig: config $1: \"$_oldval\" -> \"$2\""
	fi
}
if [ -n "$cfg_git_no_mmap" ]; then
	update_config_item core.packedGitWindowSize 1m 1
else
	update_config_item core.packedGitWindowSize 32m 1
fi
if [ -n "$var_window_memory" ]; then
	update_config_item pack.windowMemory "$var_window_memory" 1
fi
if [ -n "$cfg_jgit_compatible_bitmaps" ]; then
	update_config_item pack.writeBitmapHashCache false 1
else
	update_config_item pack.writeBitmapHashCache true 1
fi
update_config_item core.pager "cat" 1
update_config_item core.compression 5
update_config_item diff.renameLimit 250
update_config_item transfer.unpackLimit 1 1
update_config_item http.lowSpeedLimit 1
update_config_item http.lowSpeedTime 600
update_config_item receive.advertisePushOptions false 1
update_config_item receive.maxInputSize "${cfg_max_receive_size:-0}" 1
update_config_item girocco.notifyHook "${cfg_default_notifyhook}" 2
if [ -n "$defined_cfg_git_client_ua" ]; then
	update_config_item http.userAgent "$cfg_git_client_ua" 1
else
	update_config_item http.userAgent "" 2
fi

# set up some default ssh client config just in case
if [ -e etc/girocco/.ssh ] && ! [ -d etc/girocco/.ssh ]; then
	echo "*** Error: chroot etc/girocco/.ssh exists but is not a directory." >&2
	exit 1
fi
if [ -e etc/girocco/.ssh/config ] && ! [ -f etc/girocco/.ssh/config ]; then
	echo "*** Error: chroot etc/girocco/.ssh/config exists but is not a file." >&2
	exit 1
fi
if ! [ -s etc/girocco/.ssh/config ]; then
	chmod u+w etc/girocco
	didchmod=1
	[ -d etc/girocco/.ssh ] || mkdir etc/girocco/.ssh
	cat >etc/girocco/.ssh/config <<EOT
# Any values set here will take effect whenever Girocco runs an ssh client command
BatchMode yes
StrictHostKeyChecking no
CheckHostIP no
UserKnownHostsFile /dev/null
EOT
fi

[ -z "$didchmod" ] || chmod a-w etc/girocco

mkdir -p etc/sshkeys etc/sshcerts etc/sshactive
for ruser in $reserved_users; do
	touch etc/sshkeys/$ruser
done
chgrp $cfg_owning_group etc etc/sshkeys etc/sshcerts etc/sshactive ||
	echo "WARNING: Cannot chgrp $cfg_owning_group the etc directories"
chgrp $cfg_owning_group etc/passwd ||
	echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_chroot/etc/passwd"
chgrp $cfg_owning_group etc/group ||
	echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_chroot/etc/group"
chgrp $cfg_owning_group etc/girocco etc/girocco/.gitconfig ||
	echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_chroot/etc/girocco"
chgrp $cfg_owning_group etc/girocco/.ssh etc/girocco/.ssh/config ||
	echo "WARNING: Cannot chgrp $cfg_owning_group $cfg_chroot/etc/.ssh"
chmod g+s etc etc/sshkeys etc/sshcerts etc/sshactive ||
	echo "WARNING: Cannot chmod g+s the etc directories"
chmod g+w etc etc/sshkeys etc/sshcerts etc/sshactive ||
	echo "WARNING: Cannot chmod g+w the etc directories"
chmod g+w etc/passwd etc/group ||
	echo "WARNING: Cannot chmod g+w the etc/passwd and/or etc/group files"
chmod go-w etc/passwd etc/girocco etc/girocco/.gitconfig ||
	echo "WARNING: Cannot chmod go-w etc/girocco and/or etc/girocco/.gitconfig"
chmod go-w etc/girocco/.ssh etc/girocco/.ssh/config ||
	echo "WARNING: Cannot chmod go-w etc/girocco/.ssh and/or etc/girocco/.ssh/config"
chmod go-rwx etc/girocco/.ssh/config ||
	echo "WARNING: Cannot chmod go-rwx etc/girocco/.ssh/config"
chmod a-w etc/girocco/.ssh ||
	echo "WARNING: Cannot chmod a-w etc/girocco/.ssh"
chmod a-w etc/girocco ||
	echo "WARNING: Cannot chmod a-w etc/girocco"
chmod -R g+w etc/sshkeys etc/sshcerts etc/sshactive 2>/dev/null ||
	echo "WARNING: Cannot chmod g+w the sshkeys, sshcerts and/or sshactive files"

# Note time of last install
>etc/sshactive/_install

[ -z "$dbonly" ] || exit 0

# Make sure the system type is supported for chroot
sysname="$(uname -s | tr A-Z a-z)" || :
: ${sysname:=linux}
nosshdir=
# These equivalents may need to be expanded at some point
case "$sysname" in
	*kfreebsd*)
		sysname=linux;;
	*darwin*)
		sysname=darwin;;
	*dragonfly*)
		sysname=dragonfly;;
	*freebsd*)
		sysname=freebsd;;
	*linux*)
		sysname=linux;;
esac

chrootsetup="$curdir/chrootsetup_$sysname.sh"
if ! [ -f "$chrootsetup" ] || ! [ -r "$chrootsetup" ] || ! [ -s "$chrootsetup" ]; then
	echo "*** Error: $chrootsetup not found" >&2
	echo "*** Error: creating a chroot for a $(uname -s) system is not supported" >&2
	exit 1
fi

# validate reporoot, chroot, jailreporoot and sshd_bin before doing anything more

# validates the passed in dir if a second argument is not empty dir must NOT
# start with / otherwise it must.  A trailing '/' is removed and any duplicated
# // are removed and a sole / or empty is disallowed.
make_valid_dir() {
	_check="$(echo "$1" | tr -s /)"
	_check="${_check%/}"
	[ -n "$_check" ] && [ "$_check" != "/" ] || return 1
	if [ -z "$2" ]; then
		# must start with '/'
		case "$_check" in /*) :;; *) return 1; esac
	else
		# must NOT start with '/'
		case "$_check" in /*) return 1; esac
	fi
	echo "$_check"
}

if ! reporoot="$(make_valid_dir "$cfg_reporoot")"; then
	echo "*** Error: invalid Config::reporoot: $cfg_reporoot" >&2
	echo "*** Error: MUST start with '/' and MUST NOT be '/'" >&2
	exit 1
fi
if ! chroot="$(make_valid_dir "$cfg_chroot")"; then
	echo "*** Error: invalid Config::chroot: $cfg_chroot" >&2
	echo "*** Error: MUST start with '/' and MUST NOT be '/'" >&2
	exit 1
fi
if ! jailreporoot="$(make_valid_dir "$cfg_jailreporoot" 1)"; then
	echo "*** Error: invalid Config::jailreporoot: $cfg_jailreporoot" >&2
	echo "*** Error: MUST NOT start with '/' and MUST NOT be ''" >&2
	exit 1
fi

# chroot MUST NOT be reporoot
if [ "$chroot" = "$reporoot" ]; then
	echo "*** Error: invalid Config::reporoot: $cfg_reporoot" >&2
	echo "*** Error: invalid Config::chroot: $cfg_chroot" >&2
	echo "*** Error: reporoot and chroot MUST NOT be the same" >&2
	exit 1
fi

# chroot MUST NOT be a subdirectory of reporoot
case "$chroot" in "$reporoot"/*)
	echo "*** Error: invalid Config::reporoot: $cfg_reporoot" >&2
	echo "*** Error: invalid Config::chroot: $cfg_chroot" >&2
	echo "*** Error: chroot MUST NOT be a subdirectory of reporoot" >&2
	exit 1
esac

# chroot/jailreporoot MUST NOT be a subdirectory of reporoot
case "$chroot/$jailreporoot" in "$reporoot"/*)
	echo "*** Error: invalid Config::reporoot: $cfg_reporoot" >&2
	echo "*** Error: invalid Config::chroot: $cfg_chroot" >&2
	echo "*** Error: invalid Config::jailreporoot: $cfg_jailreporoot" >&2
	echo "*** Error: chroot/jailreporoot MUST NOT be a subdirectory of reporoot" >&2
	exit 1
esac

# reporoot MUST NOT be a subdirectory of chroot/jailreporoot
case "$reporoot" in "$chroot/$jailreporoot"/*)
	echo "*** Error: invalid Config::reporoot: $cfg_reporoot" >&2
	echo "*** Error: invalid Config::chroot: $cfg_chroot" >&2
	echo "*** Error: invalid Config::jailreporoot: $cfg_jailreporoot" >&2
	echo "*** Error: reporoot MUST NOT be a subdirectory of chroot/jailreporoot" >&2
	exit 1
esac

# sshd_bin MUST be undef (or empty) or a full absolute path
sshd_bin_bad=
case "$cfg_sshd_bin" in *"/../"*) sshd_bin_bad=1;; ""|/?*) :;; *) sshd_bin_bad=1;; esac
[ -z "$sshd_bin_bad" ] || {
	echo "*** Error: invalid Config::sshd_bin $cfg_sshd_bin" >&2
	echo "*** Error: if set, sshd_bin must be an absolute path" >&2
	exit 1
}
sshd_bin="$cfg_sshd_bin"
[ -n "$sshd_bin" ] || sshd_bin="$(find_std_utility "sshd")" || {
	echo "*** Error: Config::sshd_bin is not set and no sshd could be found" >&2
	echo "*** Error: please set Config::sshd_bin to an absolute path to sshd" >&2
	exit 1
}
[ -x "$sshd_bin" ] && [ -r "$sshd_bin" ] && [ -f "$sshd_bin" ] || {
	echo "*** Error: the selected sshd ('$sshd_bin') was not found, not readable or not executable" >&2
	exit 1
}

# Set the user and group on the top of the chroot before creating anything else
chown 0:0 "$chroot"

# When we create a fork, the alternates always have an absolute path.
# If reporoot is not --bind mounted at the same location in chroot we must
# create a suitable symlink so the absolute path alternates continue to work
# in the ssh chroot or else forks will be broken in there.
if [ "$reporoot" != "/$jailreporoot" ]; then
	mkdirp="$(dirname "${reporoot#/}")"
	[ "$mkdirp" = "." ] && mkdirp=
	lnback=
	[ -z "$mkdirp" ] || lnback="$(echo "$mkdirp/" | sed -e 's,[^/]*/,../,g')"
	[ -z "$mkdirp" ] || mkdir -p "$chroot/$mkdirp"
	(umask 0; ln -s -f -n "$lnback$jailreporoot" "$chroot$reporoot")
	[ $? -eq 0 ] || exit 1
fi

# First, setup basic platform-independent directory structure
mkdir -p bin dev etc lib sbin var/empty var/run "$jailreporoot"
chmod 0555 var/empty
rm -rf usr local
ln -s . usr
ln -s . local

# Now source the platform-specific script that is responsible for dev device
# setup, proc setup (if needed), lib64 setup (if needed) and basic library
# installation to make a chroot operational.  Additionally it will define a
# pull_in_bin function that can be used to add executables and their library
# dependencies to the chroot and finally will install a suitable nc.openbsd
# compatible version of netcat that supports connections to unix sockets.
. "$chrootsetup"

# Now, bring in sshd, sh etc.
# The $chrootsetup script should have already provided a suitable nc.openbsd
install -p "$cfg_basedir/bin/git-shell-verify" bin/git-shell-verify.new
install -p "$cfg_basedir/bin/git-askpass-password" bin/git-askpass-password.new
perl -i -p \
	-e 's|^#!.*|#!/bin/sh| if $. == 1;' \
	-e 'close ARGV if eof;' \
	bin/git-shell-verify.new bin/git-askpass-password.new
mv -f bin/git-askpass-password.new bin/git-askpass-password
mv -f bin/git-shell-verify.new bin/git-shell-verify
pull_in_bin "$cfg_basedir/bin/can_user_push" bin
pull_in_bin "$cfg_basedir/bin/list_packs" bin
pull_in_bin "$cfg_basedir/bin/strftime" bin
pull_in_bin "$cfg_basedir/bin/ulimit512" bin
pull_in_bin "$var_sh_bin" bin/sh
# be paranoid since these are going into the chroot and make sure
# that we get the "standard" versions of them (they are all standard "POSIX"
# utilities) not some wayward version picked up by a haphazard PATH
pull_in_bin "$(find_std_utility cat	)" bin
pull_in_bin "$(find_std_utility chmod	)" bin
pull_in_bin "$(find_std_utility date	)" bin
pull_in_bin "$(find_std_utility find	)" bin
pull_in_bin "$(find_std_utility mkdir	)" bin
pull_in_bin "$(find_std_utility mv	)" bin
pull_in_bin "$(find_std_utility rm	)" bin
pull_in_bin "$(find_std_utility sleep	)" bin
pull_in_bin "$(find_std_utility sort	)" bin
pull_in_bin "$(find_std_utility touch	)" bin
pull_in_bin "$(find_std_utility tr	)" bin
pull_in_bin "$(find_std_utility wc	)" bin
# this one's already been validated and might be in a non-standard location
pull_in_bin "$sshd_bin" sbin

# ...and the bits of git we need,
# being sure to use the configured git and its --exec-path to find the pieces
for i in git git-index-pack git-receive-pack git-shell git-update-server-info \
	git-upload-archive git-upload-pack git-unpack-objects git-config \
	git-for-each-ref git-rev-list git-rev-parse git-symbolic-ref; do
	pull_in_bin "$var_git_exec_path/$i" bin git
done

# ...and any extras identified by install.sh
# these are also all standard "POSIX" utilities
# ones that a decent sh implementation would have built-in already...
if [ -n "$GIROCCO_CHROOT_EXTRA_INSTALLS" ]; then
	for i in $GIROCCO_CHROOT_EXTRA_INSTALLS; do
		pull_in_bin "$(find_std_utility "$(basename "$i")")" bin
	done
fi

# Note time of last jailsetup
>etc/sshactive/_jailsetup

# Update permissions on the database files
chown $cfg_cgi_user:$cfg_owning_group etc/passwd etc/group
chown -R $cfg_cgi_user:$cfg_owning_group etc/sshkeys etc/sshcerts etc/sshactive
chown $cfg_mirror_user:$cfg_owning_group etc etc/girocco etc/girocco/.gitconfig
chown $cfg_mirror_user:$cfg_owning_group etc/girocco/.ssh etc/girocco/.ssh/config

# Set up basic sshd configuration:
if [ -n "$nosshdir" ]; then
	rm -rf etc/ssh
	ln -s . etc/ssh
	! [ -f /etc/moduli ] || { cp -p /etc/moduli etc/; chown 0:0 etc/moduli; }
else
	! [ -e etc/ssh ] || [ -d etc/ssh ] || rm -rf etc/ssh
	mkdir -p etc/ssh
	! [ -f /etc/ssh/moduli ] || { cp -p /etc/ssh/moduli etc/ssh/; chown 0:0 etc/ssh/moduli; }
fi
mkdir -p var/run/sshd
if ! [ -s etc/ssh/sshd_config ]; then
	cat >etc/ssh/sshd_config <<EOT
Protocol		2
Port			$cfg_sshd_jail_port
UsePAM			no
X11Forwarding		no
AllowAgentForwarding	no
AllowTcpForwarding	no
PermitTunnel		no
IgnoreUserKnownHosts	yes
PrintLastLog		no
PrintMotd		no
UseDNS			no
PermitRootLogin		no
UsePrivilegeSeparation	yes

HostKey			/etc/ssh/ssh_host_rsa_key
EOT
if [ -z "$cfg_disable_dsa" ]; then
	cat >>etc/ssh/sshd_config <<EOT
HostKey			/etc/ssh/ssh_host_dsa_key
EOT
fi
	cat >>etc/ssh/sshd_config <<EOT
AuthorizedKeysFile	/etc/sshkeys/%u
StrictModes		no

# mob and git users:
PermitEmptyPasswords	yes
ChallengeResponseAuthentication no
PasswordAuthentication	yes
EOT
fi
if ! [ -s etc/ssh/ssh_host_rsa_key ]; then
	bits=2048
	if [ "$cfg_rsakeylength" -gt "$bits" ] 2>/dev/null; then
		bits="$cfg_rsakeylength"
	fi
	yes | ssh-keygen -b "$bits" -t rsa -N "" -C Girocco -f etc/ssh/ssh_host_rsa_key
fi
if [ -z "$cfg_disable_dsa" ] && ! [ -s etc/ssh/ssh_host_dsa_key ]; then
	# ssh-keygen can only create 1024 bit DSA keys
	yes | ssh-keygen -b 1024 -t dsa -N "" -C Girocco -f etc/ssh/ssh_host_dsa_key
fi

# Set the final permissions on the binaries and perform any final twiddling
chroot_update_permissions

# Change the owner of the sshd-related files
chown 0:0 etc/ssh/ssh_* etc/ssh/sshd_*

echo "--- Add to your boot scripts: mount --bind $reporoot $chroot/$jailreporoot"
echo "--- Add to your boot scripts: mount --bind /proc $chroot/proc"
echo "--- Add to your syslog configuration: listening on socket $chroot/dev/log"
echo "--- To restart a running jail's sshd: sudo kill -HUP \$(cat $chroot/var/run/sshd.pid)"
