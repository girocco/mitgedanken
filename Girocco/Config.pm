package Girocco::Config;

use strict;
use warnings;


#
##  --------------
##  Basic settings
##  --------------
#

# Name of the service (typically a single word or a domain name)
# (no spaces allowed)
our $name = "GiroccoEx";

# Nickname of the service (undef for initial part of $name upto first '.')
# (no spaces allowed)
our $nickname = undef;

# Title of the service (as shown in gitweb)
# (may contain spaces)
our $title = "Example Girocco Hosting";

# Path to the Git binary to use (you MUST set this, even if to /usr/bin/git!)
our $git_bin = '/usr/bin/git';

# Path to the git-daemon binary to use (undef to use default)
# If $gitpullurl is undef this will never be used (assuming no git inetd
# service has been set up in that case).
# The default if this is undef is `$git_bin --exec-path`/git-daemon
our $git_daemon_bin = undef;

# Path to the git-http-backend binary to use (undef to use default)
# If both $httppullurl and $httpspushurl are undef this will never be used
# The default if this is undef is `$git_bin --exec-path`/git-http-backend
our $git_http_backend_bin = undef;

# Name (if in $PATH) or full path to netcat executable that accepts a -U option
# to connect to a unix socket.  This may simply be 'nc' on many systems.
# See the ../src/dragonfly/README file for a DragonFly BSD nc with -U support.
our $nc_openbsd_bin = 'nc.openbsd';

# Path to POSIX sh executable to use.  Set to undef to use /bin/sh
our $posix_sh_bin = undef;

# Path to Perl executable to use.  Set to undef to use Perl found in $PATH
our $perl_bin = undef;

# Path to gzip executable to use.  Set to undef to use gzip found in $PATH
our $gzip_bin = undef;

# Path to the sendmail instance to use.  It should understand the -f <from>, -i and -t
# options as well as accepting a list of recipient addresses in order to be used here.
# You MUST set this, even if to '/usr/sbin/sendmail'!
# Setting this to 'sendmail.pl' is special and will automatically be expanded to
# a full path to the ../bin/sendmail.pl executable in this Girocco installation.
# sendmail.pl is a sendmail-compatible script that delivers the message directly
# using SMTP to a mail relay host.  This is the recommended configuration as it
# minimizes the information exposed to recipients (no sender account names or uids),
# can talk to an SMTP server on another host (eliminating the need for a working
# sendmail and/or SMTP server on this host) and avoids any unwanted address rewriting.
# By default it expects the mail relay to be listening on localhost port 25.
# See the sendmail.pl section below for more information on configuring sendmail.pl.
our $sendmail_bin = 'sendmail.pl';

# E-mail of the site admin
our $admin = 'admin@example.org';

# Sender of emails
# This is the SMTP 'MAIL FROM:' value
# It will be passed to $sendmail_bin with the -f option
# Some sites may not allow non-privileged users to pass the -f option to
# $sendmail_bin.  In that case set this to undef and no -f option will be
# passed which means the 'MAIL FROM:' value will be the user the mail is
# sent as (either $cgi_user or $mirror_user depending on the activity).
# To avoid having bounce emails go to $admin, this may be set to something
# else such as 'admin-noreply@example.org' and then the 'admin-noreply' address
# may be redirected to /dev/null.  Setting this to '' or '<>' is not
# recommended because that will likely cause the emails to be marked as SPAM
# by the receiver's SPAM filter.  If $sendmail_bin is set to 'sendmail.pl' this
# value must be acceptable to the receiving SMTP server as a 'MAIL FROM:' value.
# If this is set to undef and 'sendmail.pl' is used, the 'MAIL FROM:' value will
# be the user the mail is sent as (either $cgi_user or $mirror_user).
our $sender = $admin;

# Copy $admin on failure/recovery messages?
our $admincc = 1;

# Girocco branch to use for html.cgi view source links (undef for HEAD)
our $giroccobranch = undef;

# PATH adjustments
# If the PATH needs to be customized to find required executables on
# the system, it can be done here.
# For example something like this:
#$ENV{PATH} = substr(`/usr/bin/getconf PATH`,0,-1).":/usr/local/bin";


#
##  ----------------------
##  Git user agent strings
##  ----------------------
#

# Git clients (i.e. fetch/clone) always send a user agent string when fetching
# over HTTP.  Since version 1.7.12.1 an 'agent=' capability string is included
# as well which affects git:, smart HTTP and ssh: protocols.

# These settings allow the default user agent string to be changed independently
# for fetch/clone operations (only matters if $mirror is true) and server
# operations (some other Git client fetching from us).  Note that it is not
# possible to suppress the capability entirely although it can be set to an
# empty string.  If these values are not set, the default user agent string
# will be used.  Typically (unless Git was built with non-standard options) the
# default is "git/" plus the version.  So for example "git/1.8.5.6" or
# "git/2.1.4" might be seen.

# One might want to change the default user agent strings in order to prevent
# an attacker from learning the exact Git version being used to avoid being
# able to quickly target any version-specific vulnerabilities.  Note that
# no matter what's set here, an attacker can easily determine whether a server
# is running JGit, libgit2 or Git and for Git whether it's version 1.7.12.1 or
# later.  A reasonable value to hide the exact Git version number while
# remaining compatible with servers that require a "Git/" user agent string
# would be something like "git/2" or even just "git/".

# The GIT_USER_AGENT value to use when acting as a client (i.e. clone/fetch)
# This value is only used if $mirror is true and at least one mirror is set up.
# Setting this to the empty string will suppress the HTTP User-Agent header,
# but will still include an "agent=" capability in the packet protocol.  The
# empty string is not recommended because some servers match on "git/".
# Leave undef to use the default Git user agent string
# IMPORTANT: some server sites will refuse to serve up Git repositories unless
# the client user agent string contains "Git/" (matched case insensitively)!
our $git_client_ua = undef;

# The GIT_USER_AGENT value to use when acting as a server (i.e. some Git client
# is fetching/cloning from us).
# Leave undef to use the default Git user agent string
our $git_server_ua = undef;


#
##  -------------
##  Feature knobs
##  -------------
#

# Enable mirroring mode if true (see "Foreign VCS mirrors" section below) 
our $mirror = 1;

# Enable push mode if true
our $push = 1;

# If both $mirror and $push are enabled, setting this to 'mirror' pre-selects
# mirror mode on the initial regproj display, otherwise 'push' mode will be
# pre-selected.  When forking the initial mode will be 'push' if $push enabled.
our $initial_regproj_mode = 'mirror';

# Enable user management if true; this means the interface for registering
# user accounts and uploading SSH keys. This implies full chroot.
our $manage_users = 1;

# Minimum key length (in bits) for uploaded SSH RSA/DSA keys.
# If this is not set (i.e. undef) keys as small as 512 bits will be allowed.
# Nowadays keys less than 2048 bits in length should probably not be allowed.
# Note, however, that versions of OpenSSH starting with 4.3p1 will only generate
# DSA keys of exactly 1024 bits in length even though that length is no longer
# recommended.  (OpenSSL can be used to generate DSA keys with lengths > 1024.)
# OpenSSH does not have any problem generating RSA keys longer than 1024 bits.
# This setting is only checked when new keys are added so setting it/increasing it
# will not affect existing keys.  For maximum compatibility a value of 1024 may
# be used however 2048 is recommended.  Setting it to anything other than 1024,
# 2048 or 3072 may have the side effect of making it very difficult to generate
# DSA keys that satisfy the restriction (but RSA keys should not be a problem).
# Note that no matter what setting is specified here keys smaller than 512 bits
# will never be allowed via the reguser.cgi/edituser.cgi interface.
# RECOMMENDED VALUE: 2048 (ok) or 3072 (better)
our $min_key_length = 3072;

# Disable DSA public keys?
# If this is set to 1, adding DSA keys at reguser.cgi/edituser.cgi time will be
# prohibited.  If $pushurl is undef then this is implicitly set to 1 since DSA
# keys are not usable with https push.
# OpenSSH will only generate 1024 bit DSA keys starting with version 4.3p1.
# Even if OpenSSL is used to generate a longer DSA key (which can then be used
# with OpenSSH), the SSH protocol itself still forces use of SHA-1 in the DSA
# signature blob which tends to defeat the purpose of going to a longer key in
# the first place.  So it may be better from a security standpoint to simply
# disable DSA keys especially if $min_key_length and $rsakeylength have been set
# to something higher such as 3072 or 4096.  This setting is only checked when
# new keys are added so setting it/increasing it will not affect existing keys.
# There is no way to disable DSA keys in the OpenSSH server config file itself.
# If this is set to 1, no ssh_host_dsa_key will be generated or used with the
# sshd running in the jail (but if the sshd_config has already been generated
# in the jail, it must be removed and 'sudo make install' run again or otherwise
# the sshd_config needs to be edited by hand for the change to take effect).
# RECOMMENDED VALUE: 1
our $disable_dsa = 1;

# Enable the special 'mob' user if set to 'mob'
our $mob = "mob";

# Let users set admin passwords; if false, all password inputs are assumed empty.
# This will make new projects use empty passwords and all operations on them
# unrestricted, but you will be able to do no operations on previously created
# projects you have set a password on.
our $project_passwords = 1;

# How to determine project owner; 'email' adds a form item asking for their
# email contact, 'source' takes realname of owner of source repository if it
# is a local path (and empty string otherwise). 'source' is suitable in case
# the site operates only as mirror of purely local-filesystem repositories.
our $project_owners = 'email';

# Which project fields to make editable, out of 'shortdesc', 'homepage', 'README',
# 'cleanmirror', 'notifymail', 'reverseorder', 'summaryonly', 'notifytag' and 'notifyjson'
# 'notifycia' was used by the now defunct CIA service and while allowing it to
# be edited does work and the value is saved, the value is totally ignored by Girocco
our @project_fields = qw(cleanmirror homepage shortdesc README notifymail reverseorder summaryonly notifytag notifyjson);

# Minimal number of seconds to pass between two updates of a project.
our $min_mirror_interval = 3600; # 1 hour

# Minimal number of seconds to pass between two garbage collections of a project.
our $min_gc_interval = 604800; # 1 week

# Minimal number of seconds to pass after first failure before sending failure email.
# A mirror update failed message will not be sent until mirror updates have been
# failing for at least this long.  Set to 0 to send a failure message right away
# (provided the $min_mirror_failure_message_count condition has been met).
our $min_mirror_failure_message_interval = 216000; # 2.5 days

# Minimal number of consecutive failures required before sending failure email.
# A mirror update failed message will not be sent until mirror updates have failed
# for this many consecutive updates.  Set to 0 to send a failure message right away
# (provided the $min_mirror_failure_message_interval condition has been met).
our $min_mirror_failure_message_count = 3;

# Maximum window memory size when repacking.  If this is set, it will be used
# instead of the automatically computed value if it's less than that value.
# May use a 'k', 'm', or 'g' suffix otherwise value is in bytes.
our $max_gc_window_memory_size = undef;

# Maximum big file threshold size when repacking.  If this is set, it will be
# used instead of the automatically computed value if it's less than that value.
# May use a 'k', 'm', or 'g' suffix otherwise value is in bytes.
our $max_gc_big_file_threshold_size = undef;

# Whether or not to run the ../bin/update-pwd-db script whenever the etc/passwd
# database is changed.  This is typically needed (i.e. set to a true value) for
# FreeBSD style systems when using an sshd chroot jail for push access.  So if
# $pushurl is undef or the system Girocco is running on is not like FreeBSD
# (e.g. a master.passwd file that must be transformed into pwd.db and spwd.db), then
# this setting should normally be left false (i.e. 0).  See comments in the
# provided ../bin/update-pwd-db script about when and how it's invoked.
our $update_pwd_db = 0;

# Port the sshd running in the jail should listen on
# Be sure to update $pushurl to match
# Not used if $pushurl is undef
our $sshd_jail_port = 22;

# If this is true then host names used in mirror source URLs will be checked
# and any that are not DNS names (i.e. IPv4 or IPv6) or match one of the DNS
# host names in any of the URL settings below will be rejected.
our $restrict_mirror_hosts = 1;

# If $restrict_mirror_hosts is enabled this is the minimum number of labels
# required in a valid dns name.  Normally 2 is the correct value, but if
# Girocco is being used internally where a common default or search domain
# is set for everyone then this should be changed to 1 to allow a dns name
# with a single label in it.  No matter what is set here at least 1 label
# is always required when $restrict_mirror_hosts is enabled.
our $min_dns_labels = 2;

# If $xmllint_readme is true then the contents of the README.html section
# will be passed through xmllint and any errors must be corrected before
# it can be saved.  If this is set to true then xmllint must be in the $PATH.
# RECOMMENDED VALUE: 1
our $xmllint_readme = 0;

# If defined, pass this value to format-readme as its `-m` option
# When format-readme is formatting an automatic readme, it will skip
# anything larger than this.  The default is 32768 if unset.
# See `bin/format-readme -h` for details.
our $max_readme_size = undef;

# Maximum size of any single email sent by mail.sh in K (1024-byte) units
# If message is larger it will be truncated with a "...e-mail trimmed" line
# RECOMMENDED VALUE: 256 - 5120 (.25M - 5M)
our $mailsh_sizelimit = 512;


#
##  -------------------
##  Foreign VCS mirrors
##  -------------------
#

# Note that if any of these settings are changed from true to false, then
# any pre-existing mirrors using the now-disabled foreign VCS will stop
# updating, new mirrors using the now-disabled foreign VCS will be disallowed
# and attempts to update ANY project settings for a pre-existing project that
# uses a now-disabled foreign VCS source URL will also be disallowed.

# If $mirror is true and $mirror_svn is true then mirrors from svn source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form svn://... or svn+http://... or svn+https://...
# Note that for this to work the "svn" command line command must be available
# in PATH and the "git svn" commands must work (which generally requires both
# Perl and the subversion perl bindings be installed).
our $mirror_svn = 1;

# Prior to Git v1.5.1, git-svn always used a log window size of 1000.
# Starting with Git v1.5.1, git-svn defaults to using a log window size of 100
# and provides a --log-window-size= option to change it.  Starting with Git
# v2.2.0, git-svn disconnects and reconnects to the server every log window size
# interval to attempt to reduce memory use by git-svn.  If $svn_log_window_size
# is undefined, Girocco will use a log window size of 250 (instead of the
# the default 100).  If $svn_log_window_size is set, Girocco will use that
# value instead.  Beware that setting it too low (i.e. < 50) will almost
# certainly cause performance issues if not failures.  Unless there are concerns
# about git-svn memory use on a server with extremely limited memory, the
# value of 250 that Girocco uses by default should be fine.  Obviously if
# $mirror or $mirror_svn is false this setting is irrelevant.
our $svn_log_window_size = undef;

# If $mirror is true and $mirror_darcs is true then mirrors from darcs source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form darcs+http://... darcs+https://... (and deprecated darcs://...)
# Note that for this to work the "darcs" command line command must be available
# in PATH and so must python (required to run the darcs-fast-export script).
our $mirror_darcs = 1;

# If $mirror is true and $mirror_bzr is true then mirrors from bzr source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form bzr://...
# Note that for this to work the "bzr" command line command must be available
# in PATH (it's a python script so python is required as well).
our $mirror_bzr = 1;

# If $mirror is true and $mirror_hg is true then mirrors from hg source
# repositories will be allowed (and be converted to Git).  These URLs have
# the form hg+http://... or hg+https://...
# Note that for this to work the "hg" command line command must be available
# in PATH and so must python (required to run the hg-fast-export.py script).
# Note that if the PYTHON environment variable is set that will be used instead
# of just plain "python" to run the hg-fast-export.py script (which needs to
# be able to import from mercurial).
our $mirror_hg = 1;


#
##  -----
##  Paths
##  -----
#

# Path where the main chunk of Girocco files will be installed
# This will get COMPLETELY OVERWRITTEN by each make install!!!
# MUST be an absolute path
our $basedir = '/home/repo/repomgr';

# Path where the automatically generated non-user certificates will be stored
# (The per-user certificates are always stored in $chroot/etc/sshcerts/)
# This is preserved by each make install and MUST NOT be under $basedir!
# Not used unless $httpspushurl is defined
# MUST be an absolute path
our $certsdir = '/home/repo/certs';

# The repository collection
# "$reporoot/_recyclebin" will also be created for use by toolbox/trash-project.pl
# MUST be an absolute path
our $reporoot = "/srv/git";

# The repository collection's location within the chroot jail
# Normally $reporoot will be bind mounted onto $chroot/$jailreporoot
# Should NOT start with '/'
our $jailreporoot = "srv/git";

# The chroot for ssh pushing; location for project database and other run-time
# data even in non-chroot setups
# MUST be an absolute path
our $chroot = "/home/repo/j";

# An installation that will never run a chrooted sshd should set this
# to a true value (e.g. 1) to guarantee that jailsetup for a chrooted
# sshd never takes place no matter what user runs `make install`.
# Note that the "jailsetup.sh" script will still run to do the database
# setup that's stored in $chroot regardless of this setting, it will just
# always run in "dbonly" mode when this setting is true.
our $disable_jailsetup = 0;

# The gitweb files web directory (corresponds to $gitwebfiles)
# Note that it is safe to place this under $basedir since it's set up after
# $basedir is completely replaced during install time.  Be WARNED, however,
# that normally the install process only adds/replaces things in $webroot,
# but if $webroot is under $basedir then it will be completely removed and
# rebuilt each time "make install" is run.  This will make gitweb/git-browser
# web services very briefly unavailable while this is happening.
# MUST be an absolute path
our $webroot = "/home/repo/www";

# The CGI-enabled web directory (corresponds to $gitweburl and $webadmurl)
# This will not be web-accessible except that if any aliases point to
# a *.cgi file in here it will be allowed to run as a cgi-script.
# Note that it is safe to place this under $basedir since it's set up after
# $basedir is completely replaced during install time.  Be WARNED, however,
# that normally the install process only adds/replaces things in $cgiroot,
# but if $cgiroot is under $basedir then it will be completely removed and
# rebuilt each time "make install" is run.  This will make gitweb/git-browser
# web services very briefly unavailable while this is happening.
# MUST be an absolute path
our $cgiroot = "/home/repo/cgibin";

# A web-accessible symlink to $reporoot (corresponds to $httppullurl, can be undef)
# If using the sample apache.conf (with paths suitably updated) this is not required
# to serve either smart or non-smart HTTP repositories to the Git client
# MUST be an absolute path if not undef
our $webreporoot = "/home/repo/www/r";

# The location to store the project list cache, gitweb project list and gitweb
# cache file.  Normally this should not be changed.  Note that it must be in
# a directory that is writable by $mirror_user and $cgi_user (just in case the
# cache file is missing).  The directory should have its group set to $owning_group.
# Again, this setting should not normally need to be changed.
# MUST be an absolute path
our $projlist_cache_dir = "$chroot/etc";


#
##  ----------------------------------------------------
##  Certificates (only used if $httpspushurl is defined)
##  ----------------------------------------------------
#

# path to root certificate (undef to use automatic root cert)
# this certificate is made available for easy download and should be whatever
# the root certificate is for the https certificate being used by the web server
our $rootcert = undef;

# The certificate to sign user push client authentication certificates with (undef for auto)
# The automatically generated certificate should always be fine
our $clientcert = undef;

# The private key for $clientcert (undef for auto)
# The automatically generated key should always be fine
our $clientkey = undef;

# The client certificate chain suffix (a pemseq file to append to user client certs) (undef for auto)
# The automatically generated chain should always be fine
# This suffix will also be appended to the $mobusercert before making it available for download
our $clientcertsuffix = undef;

# The mob user certificate signed by $clientcert (undef for auto)
# The automatically generated certificate should always be fine
# Not used unless $mob is set to 'mob'
# The $clientcertsuffix will be appended before making $mobusercert available for download
our $mobusercert = undef;

# The private key for $mobusercert (undef for auto)
# The automatically generated key should always be fine
# Not used unless $mob is set to 'mob'
our $mobuserkey = undef;

# Server alt names to embed in the auto-generated girocco_www_crt.pem certificate.
# The common name (CN) in the server certificate is the host name from $httpspushurl.
# By default no server alt names are embedded (not even the host from $httpspushurl).
# If the web server configuration is not using this auto-generated server certificate
# then the values set here will have no impact and this setting can be ignored.
# To embed server alternative names, list each (separated by spaces).  The names
# may be DNS names, IPv4 addresses or IPv6 addresses (NO surrounding '[' ']' please).
# If ANY DNS names are included here be sure to also include the host name from
# the $httpspushurl or else standards-conforming clients will fail with a host name
# mismatch error when they attempt to verify the connection.
#our $wwwcertaltnames = 'example.com www.example.com git.example.com 127.0.0.1 ::1';
our $wwwcertaltnames = undef;

# The key length for automatically generated RSA private keys (in bits).
# These keys are then used to create the automatically generated certificates.
# If undef or set to a value less than 2048, then 2048 will be used.
# Set to 3072 to generate more secure keys/certificates.  Set to 4096 (or higher) for
# even greater security.  Be warned that setting to a non-multiple of 8 and/or greater
# than 4096 could negatively impact compatibility with some clients.
# The values 2048, 3072 and 4096 are expected to be compatible with all clients.
# Note that OpenSSL has no problem with > 4096 or non-multiple of 8 lengths.
# See also the $min_key_length setting above to restrict user key sizes.
# This value is also used when generating the ssh_host_rsa_key for the chroot jail sshd.
# RECOMMENDED VALUE: 3072
our $rsakeylength = 3072;


#
##  -------------
##  URL addresses
##  -------------
#

# URL of the gitweb.cgi script (must be in pathinfo mode).  If the sample
# apache.conf configuration is used, the trailing "/w" is optional.
our $gitweburl = "http://repo.or.cz/w";

# URL of the extra gitweb files (CSS, .js files, images, ...)
our $gitwebfiles = "http://repo.or.cz";

# URL of the Girocco CGI web admin interface (Girocco cgi/ subdirectory)
# e.g. reguser.cgi, edituser.cgi, regproj.cgi, editproj.cgi etc.
our $webadmurl = "http://repo.or.cz";

# URL of the Girocco CGI bundles information generator (Girocco cgi/bundles.cgi)
# If the sample apache.conf configuration is used, the trailing "/b" is optional.
# This is different from $httpbundleurl.  This URL lists all available bundles
# for a project and returns that as an HTML page.
our $bundlesurl = "http://repo.or.cz/b";

# URL of the Girocco CGI html templater (Girocco cgi/html.cgi)
# If mod_rewrite is enabled and the sample apache.conf configuration is used,
# the trailing "/h" is optional when the template file name ends in ".html"
# (which all the provided ones do).
our $htmlurl = "http://repo.or.cz/h";

# HTTP URL of the repository collection (undef if N/A)
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
our $httppullurl = "http://repo.or.cz/r";

# HTTP URL of the repository collection when fetching a bundle (undef if N/A)
# Normally this will be the same as $httppullurl, but note that the bundle
# fetching logic is located in git-http-backend-verify so whatever URL is
# given here MUST end up running the git-http-backend-verify script!
# For example, if we're fetching the 'clone.bundle' for the 'girocco.git'
# repository, the final URL will be "$httpbundleurl/girocco.git/clone.bundle"
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
# This is different from $bundlesurl.  This URL fetches a single Git-format
# .bundle file that is only usable with the 'git bundle' command.
our $httpbundleurl = "http://repo.or.cz/r";

# HTTPS push URL of the repository collection (undef if N/A)
# If this is defined, the openssl command must be available
# The sample apache.conf configuration requires mod_ssl, mod_authn_anon and
# mod_rewrite be enabled to support https push operations.
# Normally this should be set to $httppullurl with http: replaced with https:
# If the sample apache.conf configuration is used, the trailing "/r" is optional.
our $httpspushurl = undef;

# Git URL of the repository collection (undef if N/A)
# (You need to set up git-daemon on your system, and Girocco will not
# do this particular thing for you.)
our $gitpullurl = "git://repo.or.cz";

# Pushy SSH URL of the repository collection (undef if N/A)
# Note that the "/$jailreporoot" portion is optional and will be automatically
# added if appropriate when omitted by the client so this URL can typically
# be made the same as $gitpullurl with git: replaced with ssh:
our $pushurl = "ssh://repo.or.cz/$jailreporoot";

# URL of gitweb of this Girocco instance (set to undef if you're not nice
# to the community)
our $giroccourl = "$Girocco::Config::gitweburl/girocco.git";


#
##  -------------------
##  Web server controls
##  -------------------
#

# If true then non-smart HTTP access will be disabled
# There's normally no reason to leave non-smart HTTP access enabled
# since downloadable bundles are provided.  However, in case the
# non-smart HTTP access is needed for some reason, this can be set to undef or 0.
# This affects access via http: AND https: and processing of apache.conf.in.
# Note that this setting does not affect gitweb, ssh: or git: URL access in any way.
# RECOMMENDED VALUE: 1
our $SmartHTTPOnly = 1;

# If true, the https <VirtualHost ...> section in apache.conf.in will be
# automatically enabled when it's converted to apache.conf by the conversion
# script.  Do NOT enable this unless the required Apache modules are present
# and loaded (mod_ssl, mod_rewrite, mod_authn_anon) AND $httpspushurl is
# defined above otherwise the server will fail to start (with various errors)
# when the resulting apache.conf is used.
our $TLSHost = 0;

# If true, the information about configuring a Git client to trust
# a Girocco-generated TLS root will be suppressed presuming that some other
# means (such as LetsEncrypt.org) has been used to generate a TLS web
# certificate signed by a pre-trusted root.  This does NOT affect the
# information on how to configure https push certificates as those are still
# required in order to push over https regardless of what web server certificate
# may be in use.
# RECOMMENDED VALUE: 0 (for girocco-generated root & server certificates)
# RECOMMENDED VALUE: 1 (for LetsEncrypt etc. generated server certificates)
our $pretrustedroot = 0;


#
##  ------------------------
##  Some templating settings
##  ------------------------
#

# Legal warning (on reguser and regproj pages)
our $legalese = <<EOT;
<p>By submitting this form, you are confirming that you will mirror or push
only what we can store and show to anyone else who can visit this site without
breaking any law, and that you will be nice to all small furry animals.
<sup class="sup"><span><a href="/h/about.html">(more details)</a></span></sup>
</p>
EOT

# Pre-configured mirror sources (set to undef for none)
# Arrayref of name - record pairs, the record has these attributes:
#	label: The label of this source
# 	url: The template URL; %1, %2, ... will be substituted for inputs
#	desc: Optional VERY short description
#	link: Optional URL to make the desc point at
#	inputs: Arrayref of hashref input records:
#		label: Label of input record 
#		suffix: Optional suffix
#	If the inputs arrayref is undef, single URL input is shown,
#	pre-filled with url (probably empty string).
our $mirror_sources = [
	{
		label => 'Anywhere',
		url => '',
		desc => 'Any HTTP/Git/rsync pull URL - bring it on!',
		inputs => undef
	},
	{
		label => 'GitHub',
		url => 'https://github.com/%1/%2.git',
		desc => 'GitHub Social Code Hosting',
		link => 'https://github.com/',
		inputs => [ { label => 'User:' }, { label => 'Project:', suffix => '.git' } ]
	},
	{
		label => 'GitLab',
		url => 'https://gitlab.com/%1/%2.git',
		desc => 'Engulfed the Green and Orange Boxes',
		link => 'https://gitlab.com/',
		inputs => [ { label => 'Project:' }, { label => 'Repository:', suffix => '.git' } ]
	}
];

# You can customize the gitweb interface widely by editing
# gitweb/gitweb_config.perl


#
##  -------------------
##  Permission settings
##  -------------------
#

# Girocco needs some way to manipulate write permissions to various parts of
# all repositories; this concerns three entities:
# - www-data: the web interface needs to be able to rewrite few files within
# the repository
# - repo: a user designated for cronjobs; handles mirroring and repacking;
# this one is optional if not $mirror
# - others: the designated users that are supposed to be able to push; they
# may have account either within chroot, or outside of it

# There are several ways how to use Girocco based on a combination of the
# following settings.

# (Non-chroot) UNIX user the CGI scripts run on; note that if some non-related
# untrusted CGI scripts run on this account too, that can be a big security
# problem and you'll probably need to set up suexec (poor you).
# This must always be set.
our $cgi_user = 'www-data';

# (Non-chroot) UNIX user performing mirroring jobs; this is the user who
# should run all the daemons and cronjobs and
# the user who should be running make install (if not root).
# This must always be set.
our $mirror_user = 'repo';

# (Non-chroot) UNIX group owning the repositories by default; it owns whole
# mirror repositories and at least web-writable metadata of push repositories.
# If you undefine this, all the data will become WORLD-WRITABLE.
# Both $cgi_user and $mirror_user should be members of this group!
our $owning_group = 'repo';

# Whether to use chroot jail for pushing; this must be always the same
# as $manage_users.
# TODO: Gitosis support for $manage_users and not $chrooted?
our $chrooted = $manage_users;

# How to control permissions of push-writable data in push repositories:
# * 'Group' for the traditional model: The $chroot/etc/group project database
#   file is used as the UNIX group(5) file; the directories have gid appropriate
#   for the particular repository and are group-writable. This works only if
#   $chrooted so that users are put in the proper groups on login when using
#   SSH push.  Smart HTTPS push does not require a chroot to work -- simply
#   run "make install" as the non-root $mirror_user user, but leave
#   $manage_users and $chrooted enabled.
# * 'ACL' for a model based on POSIX ACL: The directories are coupled with ACLs
#   listing the users with push permissions. This works for both chroot and
#   non-chroot setups, however it requires ACL support within the filesystem.
#   This option is BASICALLY UNTESTED, too. And UNIMPLEMENTED. :-)
# * 'Hooks' for a relaxed model: The directories are world-writable and push
#   permission control is purely hook-driven. This is INSECURE and works only
#   when you trust all your users; on the other hand, the attack vectors are
#   mostly just DoS or fully-traceable tinkering.
our $permission_control = 'Group';

# Path to alternate screen multiuser acl file (see screen/README, undef for none)
our $screen_acl_file = undef;

# Reserved project name and user name suffixes.
#
# Note that with personal mob branches enabled, a user name can end up being a
# file name after having a 'mob.' prefix added or a directory name after having
# a 'mob_' prefix added.  If there is ANY possibility that a file with a
# .suffix name may need to be served by the web server, lc(suffix) SHOULD be in
# this hash!  Pre-existing project names or user names with a suffix in this
# table can continue to be used, but no new projects or users can be created
# that have a suffix (case-insensitive) listed here.
our %reserved_suffixes = (
	# Entries must be lowercase WITHOUT a leading '.'
	bmp => 1,
	bz2 => 1,
	cer => 1,
	cgi => 1,
	crt => 1,
	css => 1,
	dmg => 1,
	fcgi => 1,
	gif => 1,
	gz => 1,
	htm => 1,
	html => 1,
	ico => 1,
	jp2 => 1,
	jpeg => 1,
	jpg => 1,
	jpg2 => 1,
	js => 1,
	pdf => 1,
	pem => 1,
	php => 1,
	png => 1,
	sig => 1,
	shtml => 1,
	svg => 1,
	svgz => 1,
	tar => 1,
	text => 1,
	tgz => 1,
	tif => 1,
	tiff => 1,
	txt => 1,
	xbm => 1,
	xht => 1,
	xhtml => 1,
	xz => 1,
	zip => 1,
);


#
##  -------------------------
##  sendmail.pl configuration
##  -------------------------
#

# Full information on available sendmail.pl settings can be found by running
# ../bin/sendmail.pl -v -h

# These settings will only be used if $sendmail_bin is set to 'sendmail.pl'

# sendmail.pl host name
#$ENV{'SENDMAIL_PL_HOST'} = 'localhost'; # localhost is the default

# sendmail.pl port name
#$ENV{'SENDMAIL_PL_PORT'} = '25'; # port 25 is the default

# sendmail.pl nc executable
#$ENV{'SENDMAIL_PL_NCBIN'} = "$chroot/bin/nc.openbsd"; # default is nc found in $PATH

# sendmail.pl nc options
# multiple options may be included, e.g. '-4 -X connect -x 192.168.100.10:8080'
#$ENV{'SENDMAIL_PL_NCOPT'} = '-4'; # force IPv4, default is to allow IPv4 & IPv6


#
##  -------------------------
##  Obscure Tuneable Features
##  -------------------------
#

# Throttle classes configured here override the defaults for them that
# are located in taskd/taskd.pl.  See comments in that file for more info.
our @throttle_classes = ();

# Any tag names listed here will be allowed even if they would otherwise not be.
# Note that @allowed_tags takes precedence over @blocked_tags.
our @allowed_tags = (qw( ));

# Any tag names listed here will be disallowed in addition to the standard
# list of nonsense words etc. that are blocked as tags.
our @blocked_tags = (qw( ));

# Case folding tags
# If this setting is true, then tags that differ only in case will always use
# the same-cased version.  If this setting is enabled and the tag is present in
# @allowed_tags (or the embedded white list in Util.pm) then the case of the
# tag will match the white list entry otherwise it will be all lowercased.
# If this setting is disabled (false) tags are used with their case left as-is.
# RECOMMENDED VALUE: 1 (true)
our $foldtags = 1;

# If there are no more than this many objects, then all deltas will be
# recomputed when gc takes place.  Note that this does not affect any
# fast-import created packs as they ALWAYS have their deltas recomputed.
# Also when combining small packs, if the total object count in the packs
# to be combined is no more than this then the new, combined pack will have
# its deltas recomputed during the combine operation.
# Leave undef to use the default (which should generally be fine).
# Lowering this from the default can increase disk use.
# Values less than 1000 * number of CPU cores will be silently ignored.
# The "girocco.redelta" config item can be used to modify this behavior on
# a per-repository basis.  See the description of it in gc.sh.
our $new_delta_threshold = undef;

# This setting is irrelevant unless foreign vcs mirrors that use git fast-import
# are enabled (e.g. $mirror_darcs, $mirror_bzr or $mirror_hg -- $mirror_svn does
# NOT use git fast-import and is not affected by this setting).
# The packs generated by git fast-import are very poor quality.  For this reason
# they ALWAYS have their deltas recomputed at some point.  Normally this is
# delayed until the next full (or mini) gc takes place.  For this reason a full
# gc is always scheduled immediately after a fresh mirror clone completes.
# However, in the case of normal mirror updates, several git fast-import created
# packs may exist as a result of changes fetched during the normal mirror update
# process.  These packs will persist (with their git fast-import poor quality)
# until the next full (or mini) gc triggers.  The bad deltas in these update
# packs could be sent down to clients who fetch updates before the next gc
# triggers.  To reduce (i.e. practically eliminate) the likelihood of this
# occurring, this setting can be changed to a false (0 or undef) value in which
# case after each mirror update of a git fast-import mirror, any newly created
# git fast-import packs (as a result of the mirror update running) will have
# their deltas recomputed shortly thereafter instead of waiting for the next gc.
# Recomputing deltas immediately (almost immediately) will result in an extra
# redeltification step (with associated CPU cost) that would otherwise not
# occur and, in some cases (mainly large repositories), could ultimately result
# in slightly less efficient deltas being retained.
# RECOMMENDED VALUE: 1
our $delay_gfi_redelta = 1;

# If this is set to a true value, then core.packedGitWindowSize will be set
# to 1 MiB (the same as if Git was compiled with NO_MMAP set).  If this is NOT
# set, core.packedGitWindowSize will be set to 32 MiB (even on 64-bit) to avoid
# memory blowout.  If your Git was built with NO_MMAP set and will not work
# without NO_MMAP set, you MUST set this to a true value!
our $git_no_mmap = undef;

# If set to a true value, the "X-Girocco: $gitweburl" header included in all
# Girocco-generated emails will be suppressed.
our $suppress_x_girocco = undef;

# Number of days to keep reflogs around
# May be set to a value between 1 and 30 (inclusive)
# The default of one day should normally suffice unless there's a need to
# support a "Where's the undo?  WHERE IS THE UNDO?!!!" option ;)
our $reflogs_lifetime = 1;

# The pack.window size to use with git upload-pack
# When Git is creating a pack to send down to a client, if it needs to send
# down objects that are deltas against objects it is not sending and that it
# does not know the client already has, it must undelta and recompute deltas
# for those objects.  This is the remote's "Compressing objects" phase the
# client sees during a fetch or clone.  If this value is unset, the normal
# Git default of 10 will be used for the window size during these operations.
# This value may be set to a number between 2 and 50 inclusive to change the
# window size during upload pack operations.  A window size of 2 provides the
# fastest response at the expense of less efficient deltas for the objects
# being recompressed (meaning more data to send to the client).  A window
# size of 5 typically reduces the compression time by almost half and is
# usually nearly as fast as a window size of 2 while providing better deltas.
# A window size of 50 will increase the time spent in the "Compressing objects"
# phase by as much as 5 times but will produce deltas similar to those that
# Girocco generates when it performs garbage collection.
# RECOMMENDED VALUE: undef or 5
our $upload_pack_window = undef;

# If this is true then remote fetching of refs/stash and refs/tgstash will
# be allowed.  Git does not allow single-level ref names to be pushed so the
# only way they could get in there is if a linked working tree (or, gasp, a
# non-bare Girocco repository) created them or they arrived via a non-clean
# mirror fetch.  The client almost certainly does not want to see them.
# Unless this config item is true they will also be left out of the bundle too.
# Since both stash and tgstash are used with their ref logs and there's no way
# for a remote to fetch ref logs, the "log --walk-reflogs" feature could not be
# used with them by a remote that fetched them anyway.
#
# NOTE: The reason this doesn't just control all single-level refs is that the
# "hideRefs" configuration mechanism isn't flexible enough to hide all
# single-level refs without knowing their names.  In addition, it hides the
# entire refs hierarchy so refs/stash/foo will also be hidden along with
# refs/stash, but Git doesn't actually support ref names that introduce a
# directory/file confict (aka D/F conflict) and "refs/stash" represents an
# official Git ref name therefore any refs/stash/... names really aren't
# allowed in the first place so it's no problem if they're incidentally hidden
# along with refs/stash itself.
#
# NOTE: Git 1.8.2 or later is required to actually hide the refs from fetchers
# over the "git:" protocol and Git 2.3.5 or later is required to properly hide
# them over the smart "http:" protocol (Girocco will not attempt to "hide" them
# on a smart HTTP fetch if Git is < 2.3.5 to avoid Git bugs.)  They will never
# be hidden via the non-smart HTTP fetch or any other non-smart protocols that
# also make use of the $gitdir/info/refs file as they are not excluded from it.
# Nor will they be hidden when accessed via any non-Girocco mechanism.
# They will, however, always be excluded from the primary (aka .bitmap) pack
# and bundle no matter what version of Git is used unless this is set to a
# true value.  It's only the server's Git version that matters when hiding refs.
#
# RECOMMENDED VALUE: undef or 0
our $fetch_stash_refs = undef;

# When set to a true value, Girocco will attempt to pick up ref changes made
# outside of Girocco itself and process them using the usual Girocco
# notification mechanism.  Git lacks any "post-ref-change" hook capability that
# could facilitate this.  This feature is primarily intended to detect running
# of "git fetch" in linked working trees of a Girocco repository.  In many
# cases after running a command Git runs "git gc --auto".  With the correct
# encouragement we can always induce Git to run our pre-auto-gc hook at that
# time.  "git fetch" invokes "git gc --auto" after the fetch.  Girocco needs
# to do additional maintenance to make this work properly so do not enable this
# unless it's really needed.  Additionally, there are a number of commands
# (such as "git commit") that do not invoke "git gc --auto".  Even with this
# enabled, additional hooks for post-rewrite and post-checkout
# would really be needed to catch more things and even then there are some
# Git commands that would never be caught ("git filter-branch",
# "git update-ref", "git reset", etc.) so this is hardly a complete solution.
# But it WILL catch "git fetch" changes although the hashes it uses for the
# "old" ref values may not be all that recent, the new ref values will be.
# When this is false, the hack is completely disabled.
# When this is true, the hack is enabled by default for all repositories,
# but can be controlled on an individual repository basis by setting the
# girocco.autogchack value explicitly to true (enable) or false (disable).
# If this is set to the special value "mirror" then it will behave as true
# for mirrors and false for non-mirrors thereby completely eliminating any
# overhead for push projects but detecting external "git fetch"s for mirrors.
# If this is enabled for a project, any third party script/tool can trigger
# the Girocco ref notification mechanism simply by making a ref change and
# then running "git gc --auto --quiet" on the project.  In a capitulation to
# use of linked working trees, Girocco installs a post-commit hook that will
# trigger these notifications as well when this is enabled.
our $autogchack = 0;

# When set to a true value the initial setting for core.hooksPath will point
# to the repository's own hooks directory instead of $reporoot/_global/hooks.
# Due to the unfortunate implementation of core.hooksPath, Girocco must always
# ensure the value gets set in each repository's config file.  Normally it
# just sets it to $reporoot/_global/hooks and that's that.  However, the
# update-all-config script will also tolerate it pointing at the repository's
# own hooks directory -- Girocco maintains symbolic links in there to the
# global hooks to make sure they get run when using older versions of Git;
# therefore that setting is basically equivalent.  The difference is that
# repository-specific hooks can be added when hooksPath is pointing at the
# repository's hooks directory but not when it's pointing at _global/hooks.
# A repository's setting can be changed manually (and it will stick), but
# sometimes it may be desirable to always just default to pointing at the
# repository's own hooks directory from the start.  Perhaps linked working
# trees will be in use and software that needs to set repository-specific hooks
# will be in use.  If $autogchack has been set to true this may very likely be
# the case.
our $localhooks = 0;

# If this is set to a true value changes to single-level refs (e.g. refs/stash)
# will be passed through to the notification machinery.
# Usually this is NOT wanted, especially when linked working trees are being
# used with the repository.
# However, in the unlikely event that changes to such ref names should NOT be
# ignored, this value may be set to any true value.
# RECOMMENDED VALUE: 0
our $notify_single_level = 0;

# If this is set to a non-empty value it will become the default value for
# all repositories' girocco.notifyHook value.
# Whenever taskd.pl receives a batch of ref changes for processing, it first
# sends them off to any configured "girocco.notifyHook" (same semantics as
# a post-receive hook except it also gets four command-line arguments like
# so: cat ref-changes | notifyhook $projname $user $linecount $contextlinecount
# There is no default notify hook, but each repository may set its own by
# setting the `girocco.notifyHook` config value which will be eval'd by the
# shell (like $GIT_EDITOR is) with the current directory set to the
# repository's git-dir and the changes on standard input.
# Note that normal notification processing does not take place until after
# this command (if it's not null) gets run (regardless of its result code).
our $default_notifyhook = undef;

# UNIX group owning the repositories' htmlcache subdirectory
# If not defined defaults to $owning_group
# If gitweb access is provided but only on a read-only basis, then setting
# this to a group to which Both $cgi_user and $mirror_user belong will still
# allow summary page caching.
# $mirror_user should always belong to this group
our $htmlcache_owning_group = undef;

# UNIX group owning the repositories' ctags subdirectory
# If not defined defaults to $owning_group
# If gitweb access is provided but only on a read-only basis, then setting
# this to a group to which Both $cgi_user and $mirror_user belong will still
# allow tags to be added to the repository in gitweb (provided that feature
# is enabled in gitweb/gitweb_config.perl).
# $mirror_user should always belong to this group
our $ctags_owning_group = undef;

# When using pack bitmaps and computing data to send to clients over a fetch,
# having pack.writeBitmapHashCache set to true produces better deltas (thereby
# potentially reducing the amount of data that needs to be sent).  However,
# JGit does not understand this extra data, so if JGit needs to use the bitmaps
# generated when Girocco runs Git, this setting needs to be set to a true value
# so that the hash cache is excluded when Git generates the bitmaps thereby
# making them compatible with JGit.
# Note that changes to this setting will not take effect until the next time
# gc is scheduled to run on a project and then only if gc actually takes place.
# Use the $basedir/toolbox/make-all-gc-eligible.sh script to force all projects
# to actually do a gc the next time they are scheduled for one.
# RECOMMENDED VALUE: undef or 0
our $jgit_compatible_bitmaps = 0;

# Set the default value of receive.maxInputSize
# This is only effective for receives (aka an incoming push) and causes the
# push to abort if the incoming pack (which is generally thin and does not
# have any index) exceeds this many bytes in size (a 'k', 'm' or 'g' suffix
# may be used on the value).  If undef or set to 0 there is no limit.  This
# limit is only effective when Girocco is running Git v2.11.0 or later.
our $max_receive_size = undef;

# Select the sshd to be installed into the chroot
# If set this MUST be an absolute path
# Ignored unless a chroot is actually being created
# Leaving this undef will find sshd in "standard" system locations and
# is the recommended value.  Only set this if you need to override the
# "standard" sshd for some reason.
# RECOMMENDED VALUE: undef
our $sshd_bin = undef;


#
##  ------------------------
##  Sanity checks & defaults
##  ------------------------
#
#  Changing anything in this section can result in unexpected breakage

# Couple of sanity checks and default settings (do not change these)
use Digest::MD5 qw(md5);
use MIME::Base64 qw(encode_base64);
$name =~ s/\s+/_/gs;
$nickname = lc((split(/[.]/, $name))[0]) unless $nickname;
$nickname =~ s/\s+/_/gs;
our $tmpsuffix = substr(encode_base64(md5($name.':'.$nickname)),0,6);
$tmpsuffix =~ tr,+/,=_,;
($mirror_user) or die "Girocco::Config: \$mirror_user must be set even if to current user";
($basedir) or die "Girocco::Config: \$basedir must be set";
($sendmail_bin) or die "Girocco::Config: \$sendmail_bin must be set";
$sendmail_bin = "$basedir/bin/sendmail.pl" if $sendmail_bin eq "sendmail.pl";
$screen_acl_file = "$basedir/screen/giroccoacl" unless $screen_acl_file;
$jailreporoot =~ s,^/+,,;
($reporoot) or die "Girocco::Config \$reporoot must be set";
($jailreporoot) or die "Girocco::Config \$jailreporoot must be set";
$disable_jailsetup = $disable_jailsetup ? 1 : '';
$notify_single_level = $notify_single_level ? 1 : '';
$fetch_stash_refs = $fetch_stash_refs ? 1 : '';
(not $mob or $mob eq 'mob') or die "Girocco::Config \$mob must be undef (or '') or 'mob'";
(not $min_key_length or $min_key_length =~ /^[1-9][0-9]*$/)
	or die "Girocco::Config \$min_key_length must be undef or numeric";
(! defined $max_readme_size || $max_readme_size =~ /^[0-9]+$/)
	or die "Girocco::Config \$max_readme_size must be a whole number";
(defined $mailsh_sizelimit and $mailsh_sizelimit =~ /^[1-9][0-9]*$/)
	or die "Girocco::Config \$mailsh_sizelimit must be a positive number";
$admincc = $admincc ? 1 : 0;
$rootcert = "$certsdir/girocco_root_crt.pem" if $httpspushurl && !$rootcert;
$clientcert = "$certsdir/girocco_client_crt.pem" if $httpspushurl && !$clientcert;
$clientkey = "$certsdir/girocco_client_key.pem" if $httpspushurl && !$clientkey;
$clientcertsuffix = "$certsdir/girocco_client_suffix.pem" if $httpspushurl && !$clientcertsuffix;
$mobusercert = "$certsdir/girocco_mob_user_crt.pem" if $httpspushurl && $mob && !$mobusercert;
$mobuserkey = "$certsdir/girocco_mob_user_key.pem" if $httpspushurl && $mob && !$mobuserkey;
our $mobpushurl = $pushurl;
$mobpushurl =~ s,^ssh://,ssh://mob@,i if $mobpushurl;
$disable_dsa = 1 unless $pushurl;
$disable_dsa = $disable_dsa ? 1 : '';
our $httpdnsname = ($gitweburl =~ m,https?://([A-Za-z0-9.-]+),i) ? lc($1) : undef if $gitweburl;
our $httpsdnsname = ($httpspushurl =~ m,https://([A-Za-z0-9.-]+),i) ? lc($1) : undef if $httpspushurl;
$SmartHTTPOnly = $SmartHTTPOnly ? 1 : '';
$TLSHost = $TLSHost ? 1 : '';
$pretrustedroot = $pretrustedroot ? 1 : '';
($mirror or $push) or die "Girocco::Config: neither \$mirror nor \$push is set?!";
(not $push or ($pushurl or $httpspushurl or $gitpullurl or $httppullurl)) or die "Girocco::Config: no pull URL is set";
(not $push or ($pushurl or $httpspushurl)) or die "Girocco::Config: \$push set but \$pushurl and \$httpspushurl are undef";
(not $mirror or $mirror_user) or die "Girocco::Config: \$mirror set but \$mirror_user is undef";
($manage_users == $chrooted) or die "Girocco::Config: \$manage_users and \$chrooted must be set to the same value";
(not $chrooted or $permission_control ne 'ACL') or die "Girocco::Config: resolving uids for ACL not supported when using chroot";
(grep { $permission_control eq $_ } qw(Group Hooks)) or die "Girocco::Config: \$permission_control must be set to Group or Hooks";
($chrooted or not $mob) or die "Girocco::Config: mob user supported only in the chrooted mode";
(not $httpspushurl or $httpsdnsname) or die "Girocco::Config invalid \$httpspushurl does not start with https://domainname";
(not $svn_log_window_size or $svn_log_window_size =~ /^[1-9][0-9]*$/)
	or die "Girocco::Config \$svn_log_window_size must be undef or numeric";
(not $posix_sh_bin or $posix_sh_bin !~ /\s/) or die "Girocco::Config: \$posix_sh_bin must not contain any whitespace";
(not $perl_bin or $perl_bin !~ /\s/) or die "Girocco::Config: \$perl_bin must not contain any whitespace";
!$delay_gfi_redelta and $delay_gfi_redelta = undef;
!$git_no_mmap and $git_no_mmap = undef;
!$suppress_x_girocco and $suppress_x_girocco = undef;
!$jgit_compatible_bitmaps and $jgit_compatible_bitmaps = undef;
!$autogchack and $autogchack = undef;
(not $reflogs_lifetime or $reflogs_lifetime !~ /^[1-9][0-9]*$/) and $reflogs_lifetime = 1;
$reflogs_lifetime = 0 + $reflogs_lifetime;
$reflogs_lifetime >= 0 or $reflogs_lifetime = 1;
$reflogs_lifetime <= 30 or $reflogs_lifetime = 30;
(not defined $upload_pack_window or $upload_pack_window =~ /^[1-9][0-9]*$/)
	or die "Girocco::Config \$upload_pack_window must be undef or numeric";
(not defined $upload_pack_window or $upload_pack_window >= 2 && $upload_pack_window <= 50)
	or die "Girocco::Config \$upload_pack_window must be in range 2..50";
(not defined $max_receive_size or $max_receive_size =~ /^\d+[kKmMgG]?$/)
	or die "Girocco::Config \$max_receive_size setting is invalid";
defined($ENV{'SENDMAIL_PL_HOST'}) and our $sendmail_pl_host = $ENV{'SENDMAIL_PL_HOST'};
defined($ENV{'SENDMAIL_PL_PORT'}) and our $sendmail_pl_port = $ENV{'SENDMAIL_PL_PORT'};
defined($ENV{'SENDMAIL_PL_NCBIN'}) and our $sendmail_pl_ncbin = $ENV{'SENDMAIL_PL_NCBIN'};
defined($ENV{'SENDMAIL_PL_NCOPT'}) and our $sendmail_pl_ncopt = $ENV{'SENDMAIL_PL_NCOPT'};
defined($ENV{'PYTHON'}) and our $python = $ENV{'PYTHON'};
my $op; BEGIN {$op = $ENV{'PATH'}}
defined($op) && defined($ENV{'PATH'}) && $op ne $ENV{'PATH'} and our $path=$ENV{'PATH'};

# jailreporoot MUST NOT be absolute
defined($jailreporoot) && substr($jailreporoot, 0, 1) ne "/" or
	die "\$jailreporoot MUST NOT be an absolute path\n";

# webreporoot can be undef
!defined($webreporoot) || substr($webreporoot, 0, 1) eq "/" or
	die "\$webreporoot MUST be an absolute path if not undef\n";

# All these MUST be absolute paths
{
	no strict 'refs';
	defined(${$_}) && substr(${$_}, 0, 1) eq "/" or
		die "\$$_ MUST be an absolute path\n"
	foreach qw(basedir certsdir reporoot chroot webroot cgiroot projlist_cache_dir);
}

# Make sure Git has a consistent and reproducible environment

$ENV{'XDG_CONFIG_HOME'} = $chroot.'/var/empty';
$ENV{'HOME'} = $chroot.'/etc/girocco';
$ENV{'TMPDIR'} = '/tmp';
$ENV{'GIT_CONFIG_NOSYSTEM'} = 1;
$ENV{'GIT_ATTR_NOSYSTEM'} = 1;
$ENV{'GIT_NO_REPLACE_OBJECTS'} = 1;
$ENV{'GIT_TERMINAL_PROMPT'} = 0;
$ENV{'GIT_ASKPASS'} = $basedir.'/bin/git-askpass-password';
delete $ENV{'GIT_USER_AGENT'};
$ENV{'GIT_USER_AGENT'} = $git_client_ua if defined($git_client_ua);
delete $ENV{'GIT_HTTP_USER_AGENT'};
delete $ENV{'GIT_CONFIG_PARAMETERS'};
delete $ENV{'GIT_ALTERNATE_OBJECT_DIRECTORIES'};
delete $ENV{'GIT_CONFIG'};
delete $ENV{'GIT_DIR'};
delete $ENV{'GIT_GRAFT_FILE'};
delete $ENV{'GIT_INDEX_FILE'};
delete $ENV{'GIT_OBJECT_DIRECTORY'};
delete $ENV{'GIT_NAMESPACE'};

# Guarantee a sane umask for Girocco

umask(umask() & ~0770);

1;
