# capature_command.pl -- alternative to IPC::Open3
# Copyright (C) 2015 Kyle J. McKay.  All rights reserved

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

use strict;
use warnings;

sub _set_nonblock {
	use Fcntl ();
	my $fd = shift;
	my $flags = fcntl($fd, &Fcntl::F_GETFL, 0);
	defined($flags) or die "fcntl failed: $!";
	fcntl($fd, &Fcntl::F_SETFL, $flags | &Fcntl::O_NONBLOCK)
		or die "fcntl failed: $!";
}

# Return the entire output sent to stdout and/or stderr from running a command.
# Input is redirected to input or /dev/null.  Noncaptured output is totally
# discarded.  Returns ($status, $output) where $status will be undef if there
## was a problem running the command (see $!) otherwise $status will be the
# full waitpid $? result.  $output will contain the captured output unless
# $status is undefined.
# First argument is:
#  0 => discard stdout and stderr, only return command status
#  1 => capture stdout, discard stderr, return command status
#  2 => capture stderr, discard stdout, return command status
#  3 => capture stdout+stderr, return command status
# Second argument is undef, '' or string to send to command's stdin.
# Subsequent arguments are command and arguments for pipe open call.
# All I/O is byte-oriented, input MUST NOT be in internal UTF-8 format.
# Note that if fd 0, 1, or 2 are NOT currently open when this function
# is called, they WILL be opened to File::Spec->devnull afterwards.
sub capture_command {
	# We avoid using STDIN/STDOUT in order to be compatible with FCGI mode
	use Errno ();
	use File::Spec ();
	use POSIX ();
	my $flags = shift;
	my $input = shift;
	defined($flags) or $flags = 1;
	defined($input) or $input = '';
	local $^F = 2; # just in case
	defined(my $nullfd = POSIX::open(File::Spec->devnull, &POSIX::O_RDWR))
		or die "couldn't open devnull: $!";
	while ($nullfd <= 2) {
		# paranoia
		defined($nullfd = POSIX::dup($nullfd))
			or die "couldn't dup devnull: $!";
	}
	my ($pipe_inp_r, $pipe_inp_w);
	my $pipe_inp_w_fd = -1;
	if ($input ne '') {
		pipe($pipe_inp_r, $pipe_inp_w) or die "pipe failed: $!";
		select((select($pipe_inp_w),$|=1)[0]);
		_set_nonblock($pipe_inp_w);
		$pipe_inp_w_fd = fileno($pipe_inp_w);
	}
	my ($pipe_out_r, $pipe_out_w);
	my $pipe_out_r_fd = -1;
	if ($flags & 0x3) {
		pipe($pipe_out_r, $pipe_out_w) or die "pipe failed: $!";
		select((select($pipe_out_r),$|=1)[0]);
		_set_nonblock($pipe_out_r);
		$pipe_out_r_fd = fileno($pipe_out_r);
	}
	my ($pipe_stat_r, $pipe_stat_w);
	pipe($pipe_stat_r, $pipe_stat_w) or die "pipe failed: $!";
	select((select($pipe_stat_w),$|=1)[0]);
	my $new0 = $input ne '' ? fileno($pipe_inp_r) : $nullfd;
	my $new1 = ($flags & 0x01) ? fileno($pipe_out_w) : $nullfd;
	my $new2 = ($flags & 0x02) ? fileno($pipe_out_w) : $nullfd;
	my $piped = 0;
	my $eofed = 0;
	my $oldsigchld = $SIG{'CHLD'};
	my $oldsigpipe = $SIG{'PIPE'};
	$SIG{'CHLD'} = sub {};
	$SIG{'PIPE'} = sub {$piped = 1};
	my $pid = fork();
	if (defined($pid) && !$pid) {
		# child here
		{
			defined(POSIX::dup2($new0, 0)) or last;
			defined(POSIX::dup2($new1, 1)) or last;
			defined(POSIX::dup2($new2, 2)) or last;
			POSIX::close($nullfd);
			$SIG{'CHLD'} = sub {};
			$SIG{'PIPE'} = 'DEFAULT';
			exec @_;
		}
		# dup2 or exec failed if we get here
		print $pipe_stat_w (0+$!),"\n";
		POSIX::_exit 127;
	}
	POSIX::close($nullfd);
	close($pipe_inp_r) if $input ne '';
	close($pipe_out_w) if $flags & 0x03;
	close($pipe_stat_w);
	my $status;
	my $output;
	if (defined($pid)) {{
		my $childstat = <$pipe_stat_r>;
		close($pipe_stat_r);
		if (defined($childstat) && $childstat ne '') {
			# reap the failed child
			waitpid($pid, 0);
			chomp $childstat;
			$! = Errno::EIO;
			$! = 0 + $childstat if $childstat =~ /^[1-9][0-9]*$/;
			last;
		}
		$output = "";
		my $inpoff = 0;
		my ($rv, $wv, $ev) = ("","","");
		vec($rv,0,1) = 0;
		vec($wv,0,1) = 0;
		vec($rv,$pipe_out_r_fd,1) = 1 if $pipe_out_r_fd >= 0;
		vec($wv,$pipe_inp_w_fd,1) = 1 if $pipe_inp_w_fd >= 0;
		$ev = $rv | $wv;
		if ($pipe_out_r_fd >= 0 || $pipe_inp_w_fd >= 0) {{
			my ($rout, $wout, $ready);
			my $n = select(($rout=$rv),($wout=$wv),($ready=$ev),undef);
			die "select failed: $!" unless defined($n) || $!{EINTR} || $!{EAGAIN};
			redo unless defined($n);
			$ready = $ready | $rout | $wout;
			if ($pipe_out_r_fd >= 0 && vec($ready, $pipe_out_r_fd, 1)) {
				my $c;
				do {
					$c = sysread($pipe_out_r, $output, 32768, length($output));
				} while defined($c) && $c > 0;
				if ((defined($c) && !$c) || (!defined($c) && !($!{EINTR} || $!{EAGAIN}))) {
					vec($rv,$pipe_out_r_fd,1) = 0;
					vec($ev,$pipe_out_r_fd,1) = 0;
					$eofed = 1;
					close($pipe_out_r);
				}
			}
			if ($pipe_inp_w_fd >= 0 && vec($ready, $pipe_inp_w_fd, 1)) {
				my $c;
				do {
					$c = syswrite($pipe_inp_w, $input, length($input)-$inpoff, $inpoff);
					$inpoff += $c if defined($c);
					$piped = 1 if $inpoff >= length($input);
				} while (!$piped && defined($c) && $c > 0);
				if ($piped || (defined($c) && !$c) || (!defined($c) && !($!{EINTR} || $!{EAGAIN}))) {
					vec($wv,$pipe_inp_w_fd,1) = 0;
					vec($ev,$pipe_inp_w_fd,1) = 0;
					$piped = 1;
					close($pipe_inp_w);
				}
			}
			redo unless ($piped || $pipe_inp_w_fd < 0) && ($eofed || $pipe_out_r_fd < 0);
		}}
		# Wait for child to finish
		my $w = waitpid($pid, 0);
		$status = $? if $w == $pid;
	}}
	$SIG{'CHLD'} = defined($oldsigchld) ? $oldsigchld : sub {};
	$SIG{'PIPE'} = defined($oldsigpipe) ? $oldsigpipe : 'DEFAULT';
	return ($status, $output);
}

1;
