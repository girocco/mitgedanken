# Girocco::HashUtil.pm -- HMAC SHA-1 Utility Functions
# Copyright (c) 2013 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

package Girocco::HashUtil;

use strict;
use warnings;

use base qw(Exporter);
our @EXPORT;
our $VERSION;

BEGIN {
	@EXPORT = qw(hmac_sha1 crypt_sha1 scrypt_sha1);
	*VERSION = \'1.0';
}

use MIME::Base64;
BEGIN {
	eval {
		require Digest::SHA;
		Digest::SHA->import(
			qw(sha1)
	);1} ||
	eval {
		require Digest::SHA1;
		Digest::SHA1->import(
			qw(sha1)
	);1} ||
	eval {
		require Digest::SHA::PurePerl;
		Digest::SHA::PurePerl->import(
			qw(sha1)
	);1} ||
	die "One of Digest::SHA or Digest::SHA1 or Digest::SHA::PurePerl "
	. "must be available\n";
}

my %_b64convert;
BEGIN {
	# A table that converts a standard base 64 encoding using this string:
	#   "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	# to the alternate crypt encoding using this string:
	#   "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	%_b64convert = (
	'A'=>'.','B'=>'/','C'=>'0','D'=>'1','E'=>'2','F'=>'3','G'=>'4','H'=>'5',
	'I'=>'6','J'=>'7','K'=>'8','L'=>'9','M'=>'A','N'=>'B','O'=>'C','P'=>'D',
	'Q'=>'E','R'=>'F','S'=>'G','T'=>'H','U'=>'I','V'=>'J','W'=>'K','X'=>'L',
	'Y'=>'M','Z'=>'N','a'=>'O','b'=>'P','c'=>'Q','d'=>'R','e'=>'S','f'=>'T',
	'g'=>'U','h'=>'V','i'=>'W','j'=>'X','k'=>'Y','l'=>'Z','m'=>'a','n'=>'b',
	'o'=>'c','p'=>'d','q'=>'e','r'=>'f','s'=>'g','t'=>'h','u'=>'i','v'=>'j',
	'w'=>'k','x'=>'l','y'=>'m','z'=>'n','0'=>'o','1'=>'p','2'=>'q','3'=>'r',
	'4'=>'s','5'=>'t','6'=>'u','7'=>'v','8'=>'w','9'=>'x','+'=>'y','/'=>'z'
	);
}

# Like MIME::Base64::encode except that the crypt Base64 string is used
# instead and no \n or = characters are generated and each 4-character output
# sequence is reversed.  To make the input an even multiple of 3-character
# sequences, the first 1 or 2 bytes of it may be repeated on the end.
sub _encode_base64_alt {
	use bytes;
	my $val = shift || '';
	$val .= substr($val.$val, 0, (3-(length($val)%3))) if length($val)%3;
	my $b64 = encode_base64($val, '');
	$b64 =~ s/=+//;
	$b64 =~ s/(.)/$_b64convert{$1}/g;
	my $out = '';
	while (length($b64)) {
		$out .= substr($b64,3,1).substr($b64,2,1).
			substr($b64,1,1).substr($b64,0,1);
		substr($b64,0,4) = '';
	}
	return $out;
}

# As defined in RFC 2104 for H = SHA-1
sub hmac_sha1 {
	use bytes;
	my $key = shift || '';
	my $text = shift || '';

	# HMAC is defined as H(K XOR opad, H(K XOR ipad, text))
	# where ipad is always 0x36 and opad is always 0x5C

	# Reduce a key > 64 to 64
	$key = sha1($key) if length($key) > 64;

	# (1) Pad with zeros if necessary
	$key .= pack('H2', '00') x (64 - length($key)) if length($key) < 64;

	# (2) Create the step 4 data for the hash starting with $key XOR 0x36
	my $data4 = $key;
	$data4 =~ s/(.)/chr(ord($1)^0x36)/ge;

	# (3) Append the text
	$data4 .= $text;

	# (4) Apply H to $data
	$data4 = sha1($data4);

	# (5) Create the step 5 data for the hash starting with $key XOR 0x5C
	my $data5 = $key;
	$data5 =~ s/(.)/chr(ord($1)^0x5C)/ge;

	# (6) Append step 4 result to step 5 result
	$data5 .= $data4;

	# (7) Return result of H applied to step 6 result
	return sha1($data5);
}

# An 8-byte salt is considered sufficient
# We take the first 6 bytes of the sha1 hash of the rand output and pass
# that through _encode_base64_alt to get a compatible 8-byte salt
sub _random_salt {
	use bytes;
	return _encode_base64_alt(substr(sha1(rand()), 0, 6));
}

# Return an iteration value that has a random amount of upto 1/4 its value
# subtracted from it to avoid rainbow tables.  Practically this means that
# iteration values 1-4 will be returned unchanged.
sub _random_iterations {
	my $count = shift || 0;
	$count = 24680 unless $count > 0;
	$count -= int(rand($count / 4));
	return $count;
}

# As defined in __crypt_sha1() from NetBSD's crypt-sha1.c which uses the
# PBKDF1 function defined in RFC 2898 but with more convenient args and a
# salt restricted to at most 64 bytes.  To pin the number of iterations
# exactly a negative value must be passed in for iterations.  For example,
# passing -10 as iterations will force exactly 10 iterations.
# Note that the output of this function IS identical to the output of the
# NetBSD __crypt_sha1() function provided the same $pw, $salt and $iterations
# values are used.
sub crypt_sha1 {
	use bytes;
	use constant SHA1_MAGIC => '$sha1$';
	my $pw = shift || '';
	my $salt = shift || _random_salt;
	$salt = substr($salt, 0, 64);
	my $iterations = shift || 0;
	$iterations = $iterations < 0 ?
		-$iterations : _random_iterations($iterations || 24680);

	# Create the starting value
	my $data = sprintf("%s%s%u", $salt, SHA1_MAGIC, $iterations);

	# Do the initial HMAC where $pw is the KEY and $data is the TEXT
	$data = hmac_sha1($pw, $data);

	# Perform any additional iterations requested
	for (my $i = 1; $i < $iterations; ++$i) {
		# Again $pw is the KEY and $data is the TEXT
		$data = hmac_sha1($pw, $data);
	}

	return SHA1_MAGIC.$iterations.'$'.$salt.'$'._encode_base64_alt($data);
}

# A convenience function similar to scrypt but producing a crypt_sha1 result.
# Note that while 32 rounds is rather small, it's enough to allow some variation
# in the number of rounds while still not taxing the CPU running Perl hmac_sha1.
sub scrypt_sha1 {
	my $pw = shift || '';
	return crypt_sha1($pw, '', 32);
}

1;
