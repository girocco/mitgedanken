package Girocco::CGI;

use strict;
use warnings;

use Girocco::Config;
use Girocco::Util;

BEGIN {
	our $VERSION = '0.1';
	our @ISA = qw(Exporter);
	our @EXPORT = qw(html_esc);

	use CGI qw(:standard :escapeHTML -nosticky);
	use CGI::Util qw(unescape);
	use CGI::Carp qw(fatalsToBrowser);
	eval 'sub CGI::multi_param {CGI::param(@_)}'
		unless CGI->can("multi_param");
}

my @_randlens;
my @_randchars;
BEGIN {
	@_randlens = (
	# the prime numbers >= 1024 and < 2048
1031, 1033, 1039, 1049, 1051, 1061, 1063, 1069, 1087, 1091, 1093, 1097, 1103,
1109, 1117, 1123, 1129, 1151, 1153, 1163, 1171, 1181, 1187, 1193, 1201, 1213,
1217, 1223, 1229, 1231, 1237, 1249, 1259, 1277, 1279, 1283, 1289, 1291, 1297,
1301, 1303, 1307, 1319, 1321, 1327, 1361, 1367, 1373, 1381, 1399, 1409, 1423,
1427, 1429, 1433, 1439, 1447, 1451, 1453, 1459, 1471, 1481, 1483, 1487, 1489,
1493, 1499, 1511, 1523, 1531, 1543, 1549, 1553, 1559, 1567, 1571, 1579, 1583,
1597, 1601, 1607, 1609, 1613, 1619, 1621, 1627, 1637, 1657, 1663, 1667, 1669,
1693, 1697, 1699, 1709, 1721, 1723, 1733, 1741, 1747, 1753, 1759, 1777, 1783,
1787, 1789, 1801, 1811, 1823, 1831, 1847, 1861, 1867, 1871, 1873, 1877, 1879,
1889, 1901, 1907, 1913, 1931, 1933, 1949, 1951, 1973, 1979, 1987, 1993, 1997,
1999, 2003, 2011, 2017, 2027, 2029, 2039
	);
	@_randchars = (
	# IMPORTANT: The '-' MUST be the last character in the array so we can
	# use one less than the array length to randomly replace the second '-'
	# in any generated '--' sequence.
9, 10, 13, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 46, 47, 48, 49,
50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107,
108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123,
124, 125, 126, 45 # '-' (45/0x2D) MUST be last
	);
}

sub _randpad {
	# return 1K - 2K of random padding that is a random length which
	# happens to be prime and is suitable for inclusion as an XHTML comment
	# (the comment delimiters are NOT added)
	use bytes;
	my $len = $_randlens[int(rand(@_randlens))];
	my $ccnt = @_randchars;
	my $str = '';
	for (my $i=1; $i<$len; ++$i) {
		$str .= chr($_randchars[int(rand($ccnt))]);
	}
	$str =~ s/--/'-'.chr($_randchars[int(rand($ccnt-1))])/gse;
	return $str;
}

sub _vulnpad {
	# Return suitably commented vulnerability mitigation padding if applicable

	# If https is enabled (HTTPS == "on") attempt to avoid the compression
	# vulnerability as described in VU#987798/CVE-2013-3587 (aka BREACH).
	# This only need be done for POST requests as nothing else has sensitive data.
	# See http://www.kb.cert.org/vuls/id/987798 for further information.

	my $vulnrandpad = "";
	if (($ENV{'HTTPS'} && lc($ENV{'HTTPS'}) eq 'on') &&
	    ($ENV{'REQUEST_METHOD'} && lc($ENV{'REQUEST_METHOD'}) eq 'post')) {
		# Add some random padding to mitigate the vulnerability
		$vulnrandpad = "<!-- Mitigate VU#987798/CVE-2013-3587 with random padding -->\n";
		$vulnrandpad .= "<!-- " . _randpad . " -->\n";
	}
	return $vulnrandpad;
}

sub new {
	my $class = shift;
	my ($heading, $section, $extraheadhtml, $sectionlink) = @_;
	my $gcgi = {};
	my $vulnrandpad = _vulnpad;

	$heading = CGI::escapeHTML($heading || '');
	$section = CGI::escapeHTML($section || 'administration');
	$section = "<a href=\"$sectionlink\">$section</a>" if $sectionlink;
	# $extraheadhtml is optional RAW html code to include, DO NOT escapeHTML it!
	$extraheadhtml = $extraheadhtml || '';
	my $name = CGI::escapeHTML($Girocco::Config::name || '');

	$gcgi->{cgi} = CGI->new;

	my $cgiurl = $gcgi->{cgi}->url(-absolute => 1);
	($gcgi->{srcname}) = ($cgiurl =~ m#^.*/\([a-zA-Z0-9_.\/-]+?\.cgi\)$#); #
	$gcgi->{srcname} = "cgi/".$gcgi->{srcname} if $gcgi->{srcname};

	print $gcgi->{cgi}->header(-type=>'text/html', -charset => 'utf-8');

	print <<EOT;
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">

<head>
<title>$name :: $heading</title>
<link rel="stylesheet" type="text/css" href="@{[url_path($Girocco::Config::gitwebfiles)]}/gitweb.css"/>
<link rel="stylesheet" type="text/css" href="@{[url_path($Girocco::Config::gitwebfiles)]}/girocco.css"/>
<link rel="shortcut icon" href="@{[url_path($Girocco::Config::gitwebfiles)]}/git-favicon.png" type="image/png"/>
<script src="@{[url_path($Girocco::Config::gitwebfiles)]}/mootools.js" type="text/javascript"></script>
<script src="@{[url_path($Girocco::Config::gitwebfiles)]}/girocco.js" type="text/javascript"></script>
$extraheadhtml$vulnrandpad</head>

<body>

<div class="page_header">
<a href="http://git-scm.com/" title="Git homepage"><img src="@{[url_path($Girocco::Config::gitwebfiles)]}/git-logo.png" width="72" height="27" alt="git" style="float:right; border-width:0px;"/></a>
<a href="@{[url_path($Girocco::Config::gitweburl,1)]}">$name</a> / $section / $heading
</div>

EOT

	bless $gcgi, $class;
}

sub DESTROY {
	my $self = shift;
	my $vulnrandpad = _vulnpad;
	if ($self->{srcname} and $Girocco::Config::giroccourl) {
		my $hb = $Girocco::Config::giroccobranch ?
			"hb=$Girocco::Config::giroccobranch;" : "";
		print <<EOT;
<div align="right">
<a href="@{[url_path($Girocco::Config::giroccourl)]}?a=blob;${hb}f=$self->{srcname}">(view source)</a>
</div>
EOT
	}
	print <<EOT;
</body>
$vulnrandpad</html>
EOT
}

sub cgi {
	my $self = shift;
	$self->{cgi};
}

# return previous value of $self->{errprelude}
# if at least one argument is given, then set $self->{errprelude} to the first arg
# if $self->{errprelude} is non-empty at the time the first err call happens then
#   $self->{errprelude} will be output just before the first error message
sub err_prelude {
	my $self = shift;
	my $result = $self->{errprelude};
	$self->{errprelude} = $_[0] if @_ >= 1;
	return $result;
}

sub err {
	my $self = shift;
	print $self->{errprelude} if !$self->{err} && defined($self->{errprelude});
	print "<p style=\"color: #c00000; word-wrap: break-word\">@_</p>\n";
	$self->{err}++;
}

sub ok {
	my $self = shift;
	my $err = $self->{err}||0;
	return $err == 0;
}

sub err_check {
	my $self = shift;
	my $err = $self->{err}||0;
	my $s = $err == 1 ? '' : 's';
	$err and print "<p style=\"font-weight: bold\">Operation aborted due to $err error$s.</p>\n";
	$err;
}

sub wparam {
	my $self = shift;
	my ($param) = @_;
	my $val = $self->{cgi}->param($param);
	defined $val and $val =~ s/^\s*(.*?)\s*$/$1/;
	$val;
}

sub srcname {
	my $self = shift;
	my ($srcname) = @_;
	$self->{srcname} = $srcname if $srcname;
	$self->{srcname};
}

sub html_esc($;$) {
	my $str = shift;
	my $charentityokay = shift;
	defined($str) or $str = '';
	if ($charentityokay) {
		$str =~ s/&(?!#(?:[xX][a-fA-F0-9]+|\d+);)/&amp;/g;
	} else {
		$str =~ s/&/&amp;/g;
	}
	$str =~ s/</&lt;/g; $str =~ s/>/&gt;/g;
	$str =~ s/[""]/&quot;/g; $str =~ s/['']/&apos;/g;
	$str;
}

sub print_form_fields {
	my $self = shift;
	my ($fieldmap, $valuemap, @fields) = @_;

	foreach my $field (map { $fieldmap->{$_} } @fields) {
		defined($field->[2]) && $field->[2] ne 'placeholder' or next;
		my $title='';
		if (defined($field->[3]) && $field->[3] ne '') {
			$title=' title="'.html_esc($field->[3], 1).'"'
		}
		print '<tr'.$title.'><td class="formlabel">'.$field->[0].':</td>';
		if ($field->[2] eq 'text') {
			print '<td><input type="text" name="'.$field->[1].'" size="80"';
			print ' value="'.$valuemap->{$field->[1]}.'"' if $valuemap;
			print ' />';
		} elsif ($field->[2] eq 'checkbox') {
			print '<td class="formdatatd"><input type="checkbox" name="'.$field->[1].'"';
			print ' checked="checked"' if $valuemap && $valuemap->{$field->[1]};
			printf ' value="%s"', ($valuemap && $valuemap->{$field->[1]} ? $valuemap->{$field->[1]} : "1");
			print ' />';
		} else {
			print '<td><textarea name="'.$field->[1].'" rows="5" cols="80">';
			print $valuemap->{$field->[1]} if $valuemap;
			print '</textarea>';
		}
		print "</td></tr>\n";
	}
}


1;
