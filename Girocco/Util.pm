package Girocco::Util;

use 5.008;
use strict;
use warnings;

use Girocco::Config;
use Time::Local;
use Encode;

BEGIN {
	use base qw(Exporter);
	our @EXPORT = qw(get_git scrypt jailed_file sendmail_pipe mailer_pipe
			 lock_file unlock_file valid_tag rand_adjust
			 filedb_atomic_append filedb_atomic_edit filedb_grep
			 filedb_atomic_grep valid_email valid_email_multi
			 valid_repo_url valid_web_url url_base url_path url_server
			 projects_html_list parse_rfc2822_date parse_any_date
			 extract_url_hostname is_dns_hostname is_our_hostname
			 get_cmd online_cpus sys_pagesize sys_memsize
			 calc_windowmemory to_utf8 capture_command human_size
			 calc_bigfilethreshold has_reserved_suffix human_duration
			 noFatalsToBrowser calc_redeltathreshold
			 clean_email_multi read_HEAD_symref read_config_file
			 read_config_file_hash is_git_dir git_bool util_path
			 is_shellish read_HEAD_ref);
}

my $encoder;
BEGIN {
	$encoder = Encode::find_encoding('Windows-1252') ||
		   Encode::find_encoding('ISO-8859-1') or
		   die "failed to load ISO-8859-1 encoder\n";
}

sub to_utf8($;$) {
	my ($str, $encode) = @_;
	return undef unless defined $str;
	my $ans;
	if (Encode::is_utf8($str) || utf8::decode($str)) {
		$ans = $str;
	} else {
		$ans = $encoder->decode($str, Encode::FB_DEFAULT);
	}
	utf8::encode($ans) if $encode;
	return $ans;
}

BEGIN {require "Girocco/extra/capture_command.pl"}

# Return the entire output sent to stdout from running a command
# Any output the command sends to stderr is discarded
# Returns undef if there was an error running the command (see $!)
sub get_cmd {
	my ($status, $result) = capture_command(1, undef, @_);
	return defined($status) && $status == 0 ? $result : undef;
}

# Same as get_cmd except configured git binary is automatically provided
# as the first argument to get_cmd
sub get_git {
	return get_cmd($Girocco::Config::git_bin, @_);
}

sub scrypt {
	my ($pwd) = @_;
	crypt($pwd||'', join ('', ('.', '/', 0..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64]));
}

sub jailed_file {
	my ($filename) = @_;
	$filename =~ s,^/,,;
	$Girocco::Config::chroot."/$filename";
}

sub lock_file {
	my ($path) = @_;

	$path .= '.lock';

	use Errno qw(EEXIST);
	use Fcntl qw(O_WRONLY O_CREAT O_EXCL);
	use IO::Handle;
	my $handle = new IO::Handle;

	unless (sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
		my $cnt = 0;
		while (not sysopen($handle, $path, O_WRONLY|O_CREAT|O_EXCL)) {
			($! == EEXIST) or die "$path open failed: $!";
			($cnt++ < 16) or die "$path open failed: cannot open lockfile";
			sleep(1);
		}
	}
	# XXX: filedb-specific
	chmod 0664, $path or die "$path g+w failed: $!";

	$handle;
}

sub _is_passwd_file {
	return defined($_[0]) && $_[0] eq jailed_file('/etc/passwd');
}

sub _run_update_pwd_db {
	my ($path, $updatearg) = @_;
	my @cmd = ($Girocco::Config::basedir.'/bin/update-pwd-db', "$path");
	push(@cmd, $updatearg) if $updatearg;
	system(@cmd) == 0 or die "update-pwd-db failed: $?";
}

sub unlock_file {
	my ($path, $noreplace, $updatearg) = @_;

	if (!$noreplace) {
		_run_update_pwd_db("$path.lock", $updatearg)
			if $Girocco::Config::update_pwd_db && _is_passwd_file($path);
		rename "$path.lock", $path or die "$path unlock failed: $!";
	} else {
		unlink "$path.lock" or die "$path unlock failed: $!";
	}
}

sub filedb_atomic_append {
	my ($file, $line, $updatearg) = @_;
	my $id = 65536;

	open my $src, '<', $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		my $aid = (split /:/)[2];
		$id = $aid + 1 if ($aid >= $id);

		print $dst $_ or die "$file(l) write failed: $!";
	}

	$line =~ s/\\i/$id/g;
	print $dst "$line\n" or die "$file(l) write failed: $!";

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file, 0, $updatearg);

	$id;
}

sub filedb_atomic_edit {
	my ($file, $fn, $updatearg) = @_;

	open my $src, '<', $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		print $dst $fn->($_) or die "$file(l) write failed: $!";
	}

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file, 0, $updatearg);
}

sub filedb_atomic_grep {
	my ($file, $fn) = @_;
	my @results = ();

	open my $src, '<', $file or die "$file open for reading failed: $!";
	my $dst = lock_file($file);

	while (<$src>) {
		my $result = $fn->($_);
		push(@results, $result) if $result;
	}

	close $dst or die "$file(l) close failed: $!";
	close $src;

	unlock_file($file, 1);
	return @results;
}

sub filedb_grep {
	my ($file, $fn) = @_;
	my @results = ();

	open my $src, '<', $file or die "$file open for reading failed: $!";

	while (<$src>) {
		my $result = $fn->($_);
		push(@results, $result) if $result;
	}

	close $src;

	return @results;
}

sub valid_email {
	my $email = shift;
	defined($email) or $email = '';
	return $email =~ /^[a-zA-Z0-9+._-]+@[a-zA-Z0-9.-]+$/;
}

sub clean_email_multi {
	my $input = shift;
	defined($input) or $input = '';
	$input =~ s/^\s+//; $input =~ s/\s+$//;
	my %seen = ();
	my @newlist = ();
	foreach (split(/\s*,\s*/, $input)) {
		next if $_ eq "";
		$seen{lc($_)} = 1, push(@newlist, $_) unless $seen{lc($_)};
	}
	return join(",", @newlist);
}

sub valid_email_multi {
	# each email address must be a valid_email but we silently
	# ignore extra spaces at the beginning/end and around any comma(s)
	foreach (split(/,/, clean_email_multi(shift))) {
		return 0 unless valid_email($_);
	}
	return 1;
}

sub valid_web_url {
	my $url = shift;
	defined($url) or $url = '';
	return $url =~
		/^https?:\/\/[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~:?&=;-]*)?(#[a-zA-Z0-9._-]+)?$/;
}

sub valid_repo_url {
	my $url = shift || '';
	# Currently neither username nor password is allowed in the URL (except for svn)
	# and IPv6 literal addresses are not accepted either.
	$Girocco::Config::mirror_svn &&
		$url =~ /^svn(\+https?)?:\/\/([^\@\/\s]+\@)?[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/os
		and return 1;
	$Girocco::Config::mirror_darcs &&
		$url =~ /^darcs(?:\+https?)?:\/\/[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/os
		and return 1;
	$Girocco::Config::mirror_bzr &&
		$url =~ /^bzr:\/\/[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/os
		and return 1;
	$Girocco::Config::mirror_hg &&
		$url =~ /^hg\+https?:\/\/[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/os
		and return 1;
	return $url =~ 	/^(https?|git):\/\/[a-zA-Z0-9.:-]+(\/[_\%a-zA-Z0-9.\/~-]*)?$/;
}

sub extract_url_hostname {
	my $url = shift || '';
	if ($url =~ m,^bzr://,) {
		$url =~ s,^bzr://,,;
		return 'launchpad.net' if $url =~ /^lp:/;
	}
	return undef unless $url =~ m,^[A-Za-z0-9+.-]+://[^/],;
	$url =~ s,^[A-Za-z0-9+.-]+://,,;
	$url =~ s,^([^/]+).*$,$1,;
	$url =~ s/:[0-9]*$//;
	$url =~ s/^[^\@]*[\@]//;
	return $url ? $url : undef;
}

# See these RFCs:
#   RFC 1034 section 3.5
#   RFC 1123 section 2.1
#   RFC 1738 section 3.1
#   RFC 2606 sections 2 & 3
#   RFC 3986 section 3.2.2
sub is_dns_hostname {
	my $host = shift;
	defined($host) or $host = '';
	return 0 if $host eq '' || $host =~ /\s/;
	# first remove a trailing '.'
	$host =~ s/\.$//;
	return 0 if length($host) > 255;
	my $octet = '(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])';
	return 0 if $host =~ /^$octet\.$octet\.$octet\.$octet$/o;
	my @labels = split(/[.]/, $host, -1);
	return 0 unless @labels && @labels >= $Girocco::Config::min_dns_labels;
	# now check each label
	foreach my $label (@labels) {
		return 0 unless length($label) > 0 && length($label) <= 63;
		return 0 unless $label =~ /^[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?$/;
	}
	# disallow RFC 2606 names provided at least two labels are present
	if (@labels >= 2) {
		my $tld = lc($labels[-1]);
		return 0 if
		    $tld eq 'test' ||
		    $tld eq 'example' ||
		    $tld eq 'invalid' ||
		    $tld eq 'localhost';
		my $sld = lc($labels[-2]);
		return 0 if $sld eq 'example' &&
		    ($tld eq 'com' || $tld eq 'net' || $tld eq 'org');
	}
	return 1;
}

sub is_our_hostname {
	my $test = shift || '';
	$test =~ s/\.$//;
	my %names = ();
	my @urls = (
		$Girocco::Config::gitweburl,
		$Girocco::Config::gitwebfiles,
		$Girocco::Config::webadmurl,
		$Girocco::Config::bundlesurl,
		$Girocco::Config::htmlurl,
		$Girocco::Config::httppullurl,
		$Girocco::Config::httpbundleurl,
		$Girocco::Config::httpspushurl,
		$Girocco::Config::gitpullurl,
		$Girocco::Config::pushurl
	);
	foreach my $url (@urls) {
		if ($url) {
			my $host = extract_url_hostname($url);
			if (defined($host)) {
				$host =~ s/\.$//;
				$names{lc($host)} = 1;
			}
		}
	}
	return $names{lc($test)} ? 1 : 0;
}

my (%_oktags, %_badtags, %_canontags, $_canontagscreated, @_whitetags);
BEGIN {
	# These are always okay (a "whitelist") even if they would
	# otherwise not be allowed
	@_whitetags = (qw(
		.net 2d 3d 6502 68000 68008 68010 68020 68030 68040 68060
		8086 80286 80386 80486 80586 c cc make www x
	));
	map({$_oktags{lc($_)}=1} @_whitetags, @Girocco::Config::allowed_tags);
	# entries MUST be all lowercase to be effective
	%_badtags = (
	# These are "nonsense" or pointless tags
	about=>1, after=>1, all=>1, also=>1, an=>1, and=>1, another=>1, any=>1,
	are=>1, as=>1, at=>1, be=>1, because=>1, been=>1, before=>1, being=>1,
	between=>1, both=>1, but=>1, by=>1, came=>1, can=>1, come=>1, could=>1,
	did=>1, do=>1, each=>1, for=>1, from=>1, get=>1, got=>1, had=>1, has=>1,
	have=>1, he=>1, her=>1, here=>1, him=>1, himself=>1, his=>1, how=>1,
	if=>1, in=>1, into=>1, is=>1, it=>1, like=>1, make=>1, many=>1, me=>1,
	might=>1, more=>1, most=>1, much=>1, must=>1, my=>1, never=>1, now=>1,
	of=>1, oh=>1, on=>1, only=>1, or=>1, other=>1, our=>1, out=>1, over=>1,
	said=>1, same=>1, see=>1, should=>1, since=>1, some=>1, still=>1,
	such=>1, take=>1, than=>1, that=>1, the=>1, their=>1, them=>1, then=>1,
	there=>1, these=>1, they=>1, this=>1, those=>1, through=>1, to=>1,
	too=>1, under=>1, up=>1, very=>1, was=>1, way=>1, we=>1, well=>1,
	were=>1, what=>1, where=>1, which=>1, while=>1, who=>1, with=>1,
	would=>1, yea=>1, yeah=>1, you=>1, your=>1, yup=>1
	);
	# These are "offensive" tags with at least one letter escaped to
	# avoid having this file trigger various safe-scan robots
	$_badtags{"a\x73\x73"} = 1;
	$_badtags{"a\x73\x73hole"} = 1;
	$_badtags{"b\x30\x30b"} = 1;
	$_badtags{"b\x30\x30bs"} = 1;
	$_badtags{"b\x6f\x6fb"} = 1;
	$_badtags{"b\x6f\x6fbs"} = 1;
	$_badtags{"b\x75tt"} = 1;
	$_badtags{"b\x75ttd\x69\x63k"} = 1;
	$_badtags{"c\x6f\x63k"} = 1;
	$_badtags{"c\x75\x6e\x74"} = 1;
	$_badtags{"d\x69\x63k"} = 1;
	$_badtags{"d\x69\x63kb\x75tt"} = 1;
	$_badtags{"f\x75\x63k"} = 1;
	$_badtags{"in\x63\x65st"} = 1;
	$_badtags{"ph\x75\x63k"} = 1;
	$_badtags{"p\x6f\x72n"} = 1;
	$_badtags{"p\x6f\x72no"} = 1;
	$_badtags{"p\x6f\x72nographic"} = 1;
	$_badtags{"p\x72\x30n"} = 1;
	$_badtags{"p\x72\x6fn"} = 1;
	$_badtags{"r\x61\x70e"} = 1;
	$_badtags{"s\x65\x78"} = 1;
	map({$_badtags{lc($_)}=1} @Girocco::Config::blocked_tags);
}

# A valid tag must only have [a-zA-Z0-9:.+#_-] characters, must start with a
# letter, must not be a noise word, must be more than one character long,
# must not be a repeated letter and must be no more than 32 characters long.
# However, anything in %_oktags is explicitly allowed even if it otherwise
# would violate the rules (except that none of [,\s\\\/] are allowed in tags).
# Returns the canonical name for the tag if the tag is valid otherwise undef.
sub valid_tag {
	local $_ = $_[0];
	return undef unless defined($_) && $_ ne "" && !/[,\s\/\\]/;
	my $fold = $Girocco::Config::foldtags;
	if ($fold && !$_canontagscreated) {
		local $_;
		%_canontags = ();
		$_canontags{lc($_)} = $_ foreach sort({$b cmp $a} @_whitetags, @Girocco::Config::allowed_tags);
		$_canontagscreated = 1;
	}
	return $_canontags{lc($_)} if $fold && exists($_canontags{lc($_)});
	return ($fold ? lc($_) : $_) if $_oktags{lc($_)};
	return undef unless /^[a-zA-Z][a-zA-Z0-9:.+#_-]+$/;
	return undef if $_badtags{lc($_)};
	return undef if /^(.)\1+$/;
	return length($_) <= 32 ? ($fold ? lc($_) : $_) : undef;
}

# If the passed in argument looks like a URL, return only the stuff up through
# the host:port part otherwise return the entire argument.
sub url_base {
	my $url = shift || '';
	# See RFC 3968
	$url = $1.$2.$3.$4 if $url =~ m,^( [A-Za-z][A-Za-z0-9+.-]*: ) # scheme
		( // )		# // separator
		((?:[^\@]+\@)?)	# optional userinfo
		( [^/?#]+ )	# host and port
		(?:[/?#].*)?$,x; # path and optional query string and/or anchor
	return $url;
}

# If the passed in argument looks like a URL, return only the stuff following
# the host:port part otherwise return the entire argument.
# If the optional second argument is true, the returned value will have '/'
# appended if it does not already end in '/'.
sub url_path {
	my $url = shift || '';
	my $add_slash = shift || 0;
	# See RFC 3968
	$url = $1 if $url =~ m,^(?: [A-Za-z][A-Za-z0-9+.-]*: ) # scheme
		(?: // )	# // separator
		(?: [^\@]+\@ )?	# optional userinfo
		(?: [^/?#]+ )	# host and port
		((?:[/?#].*)?)$,x; # path and optional query string and/or anchor
	$url .= '/' if $add_slash && $url !~ m|/$|;
	return $url;
}

# If both SERVER_NAME and SERVER_PORT are set pass the argument through url_path
# and then prefix it with the appropriate scheme (HTTPS=?on), host and port and
# return it.  If a something that doesn't look like it could be the start of a
# URL path comes back from url_path or SERVER_NAME is a link-local IPv6 address
# then just return the argument unchanged.
sub url_server {
	my $url = shift || '';
	my $path = url_path($url);
	return $url unless $path eq '' || $path =~ m|^[/?#]|;
	return $url unless $ENV{'SERVER_NAME'} && $ENV{'SERVER_PORT'} &&
		$ENV{'SERVER_PORT'} =~ /^[1-9][0-9]{0,4}$/;
	return $url if $ENV{'SERVER_NAME'} =~ /^[[]?fe80:/i;
	my $server = $ENV{'SERVER_NAME'};
	# Deal with Apache bug where IPv6 literal server names do not include
	# the required surrounding '[' and ']' characters
	$server = '[' . $server . ']' if $server =~ /:/ && $server !~ /^[[]/;
	my $ishttps = $ENV{'HTTPS'} && $ENV{'HTTPS'} =~ /^on$/i;
	my $portnum = 0 + $ENV{'SERVER_PORT'};
	my $port = '';
	if (($ishttps && $portnum != 443) || (!$ishttps && $portnum != 80)) {
		$port = ':' . $portnum;
	}
	return 'http' . ($ishttps ? 's' : '') . '://' . $server . $port . $path;
}

# Returns the number rounded to the nearest tenths.  The ".d" part will be
# excluded if it's ".0" unless the optional second argument is true
sub _tenths {
	my $v = shift;
	my $use0 = shift;
	$v *= 10;
	$v += 0.5;
	$v = int($v);
	return '' . int($v/10) unless $v % 10 || $use0;
	return '' . int($v/10) . '.' . ($v%10);
}

# Returns a human-readable size string (e.g. '1.5 MiB') for the value
# (in bytes) passed in.  Returns '0' for undefined or 0 or not all digits.
# Otherwise returns '1 KiB' for < 1024, or else a number rounded to the
# nearest tenths of a KiB, MiB or GiB.
sub human_size {
	my $v = shift || 0;
	return "0" unless $v && $v =~ /^\d+$/;
	return "1 KiB" unless $v > 1024;
	$v /= 1024;
	return _tenths($v) . " KiB" if $v < 1024;
	$v /= 1024;
	return _tenths($v) . " MiB" if $v < 1024;
	$v /= 1024;
	return _tenths($v) . " GiB";
}

# Returns a human duration string (e.g. 1h10m5s for the value (in secs)
# passed in.  Returns the value unchanged if it's not defined or <= 0.
sub human_duration {
        my $secs = shift;
        return $secs unless defined($secs) && $secs >= 0;
        $secs = int($secs);
        my $ans = ($secs % 60) . 's';
        return $ans if $secs < 60;
        $secs = int($secs / 60);
        $ans = ($secs % 60) . 'm' . $ans;
        return $ans if $secs < 60;
        $secs = int($secs / 60);
        $ans = ($secs % 24) . 'h' . $ans;
        return $ans if $secs < 24;
        $secs = int($secs / 24);
        return $secs . 'd' . $ans;
}

sub _escapeHTML {
	my $str = shift;
	$str =~ s/\&/\&amp;/gs;
	$str =~ s/\</\&lt;/gs;
	$str =~ s/\>/\&gt;/gs;
	$str =~ s/\"/\&quot;/gs; #"
	return $str;
}

# create relative time string from passed in age in seconds
sub _rel_age {
	my $age = shift;
	my $age_str;

	if ($age > 60*60*24*365*2) {
		$age_str = (int $age/60/60/24/365);
		$age_str .= " years ago";
	} elsif ($age > 60*60*24*(365/12)*2) {
		$age_str = int $age/60/60/24/(365/12);
		$age_str .= " months ago";
	} elsif ($age > 60*60*24*7*2) {
		$age_str = int $age/60/60/24/7;
		$age_str .= " weeks ago";
	} elsif ($age > 60*60*24*2) {
		$age_str = int $age/60/60/24;
		$age_str .= " days ago";
	} elsif ($age > 60*60*2) {
		$age_str = int $age/60/60;
		$age_str .= " hours ago";
	} elsif ($age > 60*2) {
		$age_str = int $age/60;
		$age_str .= " mins ago";
	} elsif ($age > 2) {
		$age_str = int $age;
		$age_str .= " secs ago";
	} elsif ($age >= 0) {
		$age_str = "right now";
	} else {
		$age_str = "future time";
	}
	return $age_str;
}

# create relative time string from passed in idle in seconds
sub _rel_idle {
	my $idle_str = _rel_age(shift);
	$idle_str =~ s/ ago//;
	$idle_str = "not at all" if $idle_str eq "right now";
	return $idle_str;
}

sub _strftime {
	use POSIX qw(strftime);
	my ($fmt, $secs, $zonesecs) = @_;
	my ($S,$M,$H,$d,$m,$y) = gmtime($secs + $zonesecs);
	$zonesecs = int($zonesecs / 60);
	$fmt =~ s/%z/\$z/g;
	my $ans = strftime($fmt, $S, $M, $H, $d, $m, $y, -1, -1, -1);
	my $z;
	if ($zonesecs < 0) {
		$z = "-";
		$zonesecs = -$zonesecs;
	} else {
		$z = "+";
	}
	$z .= sprintf("%02d%02d", int($zonesecs/60), $zonesecs % 60);
	$ans =~ s/\$z/$z/g;
	return $ans;
}

# Take a list of project names and produce a nicely formated table that
# includes owner links and descriptions.  If the list is empty returns ''.
# The first argument may be a hash ref that contains options.  The following
# options are available:
#   target  -- sets the target value of the owner link
#   emptyok -- if true returns an empty table rather than ''
#   sizecol -- if true include a human-readable size column
#   typecol -- if true include type column with hover info
#   changed -- if true include a changed and idle column
sub projects_html_list {
	my $options = {};
	if (defined($_[0]) && ref($_[0]) eq 'HASH') {
		$options = shift;
	}
	return '' unless @_ || (defined($options->{emptyok}) && $options->{emptyok});
	require Girocco::Project;
	my $count = 0;
	my $target = '';
	$target = " target=\""._escapeHTML($options->{target})."\""
		if defined($options->{target});
	my $withsize = defined($options->{sizecol}) && $options->{sizecol};
	my $withtype = defined($options->{typecol}) && $options->{typecol};
	my $withchanged = defined($options->{changed}) && $options->{changed};
	my $sizehead = '';
	$sizehead = substr(<<EOT, 0, -1) if $withsize;
<th class="sizecol"><span class="hover">Size<span><span class="head" _data="Size"></span
/><span class="none" /><br />(</span>Fork size excludes objects borrowed from the parent.<span class="none">)</span></span></span></th
>
EOT
	my $typehead = '';
	$typehead = '<th>Type</th>' if $withtype;
	my $chghead = '';
	$chghead = substr(<<EOT, 0, -1) if $withchanged;
<th><span class="hover">Changed<span><span class="head" _data="Changed"></span
/><span class="none" /><br />(</span>The last time a ref change was received by this site.<span class="none">)</span></span></span></th
><th><span class="hover">Idle<span><span class="head" _data="Idle"></span
/><span class="none" /><br />(</span>The most recent committer time in <i>refs/heads</i>.<span class="none">)</span></span></span></th
>
EOT
	my $html = <<EOT;
<table class='projectlist'><tr valign="top" align="left"><th>Project</th>$sizehead$typehead$chghead<th class="desc">Description</th></tr>
EOT
	my $trclass = ' class="odd"';
	foreach (sort({lc($a) cmp lc($b)} @_)) {
		if (Girocco::Project::does_exist($_, 1)) {
			my $proj = Girocco::Project->load($_);
			my $projname = $proj->{name}.".git";
			my $projdesc = $proj->{desc}||'';
			utf8::decode($projdesc) if utf8::valid($projdesc);
			my $sizecol = '';
			if ($withsize) {
				my $psize = $proj->{reposizek};
				$psize = undef unless defined($psize) && $psize =~ /^\d+$/;
				$psize = 0 if !defined($psize) && $proj->is_empty;
				if (!defined($psize)) {
					$psize = 'unknown';
				} elsif (!$psize) {
					$psize = 'empty';
				} else {
					$psize = human_size($psize * 1024);
					$psize =~ s/ /\&#160;/g;
				}
				$sizecol = '<td class="sizecol">'.$psize.'</td>';
			}
			my $typecol = '';
			if ($withtype) {
				if ($proj->{mirror}) {
					my $url = _escapeHTML($proj->{url});
					$typecol = substr(<<EOT, 0, -1);
<td class="type"><span class="hover">mirror<span class="nowrap"><span class="before" _data="$url"><span class="none"> <a href="$url" rel="nofollow">(URL)</a></span></span></span></span></td>
EOT
				} else {
					my $users = @{$proj->{users}};
					$users .= ' user';
					$users .= 's' unless @{$proj->{users}} == 1;
					my $userlist = join(', ', sort({lc($a) cmp lc($b)} @{$proj->{users}}));
					my $spncls = length($userlist) > 25 ? '' : ' class="nowrap"';
					$typecol = $userlist ? substr(<<EOT, 0, -1) : substr(<<EOT, 0, -1);
<td class="type"><span class="hover">$users<span$spncls><br class="none" />$userlist</span></span></td>
EOT
<td class="type">$users</td>
EOT
				}
			}
			my $changecol = '';
			if ($withchanged) {
				my $rel = '';
				my $changetime = $proj->{lastchange};
				if ($changetime) {
					my ($ts, $tz);
					$ts = parse_rfc2822_date($changetime, \$tz);
					my $ct = _strftime("%Y-%m-%d %T %z", $ts, $tz);
					$rel = "<span class=\"hover\">" .
						_rel_age(time - $ts) .
						"<span class=\"nowrap\"><span class=\"before\" _data=\"$changetime\"></span><span class=\"none\"><br />$ct</span></span></span>";
				} else {
					$rel = "no commits";
				}
				$changecol = substr(<<EOT, 0, -1);
<td class="change">$rel</td>
EOT
				my $idletime = $proj->{lastactivity};
				my ($idlesecs, $tz);
				$idlesecs = parse_any_date($idletime, \$tz) if $idletime;
				if ($idlesecs) {
					my $idle2822 = _strftime("%a, %d %b %Y %T %z", $idlesecs, $tz);
					my $ct = _strftime("%Y-%m-%d %T %z", $idlesecs, $tz);
					$rel = "<span class=\"hover\">" .
						_rel_idle(time - $idlesecs) .
						"<span class=\"nowrap\"><span class=\"before\" _data=\"$idle2822\"></span><span class=\"none\"><br />$ct</span></span></span>";
				} else {
					$rel = "no commits";
				}
				$changecol .= substr(<<EOT, 0, -1);
<td class="idle">$rel</td>
EOT
			}
			$html .= <<EOT;
<tr valign="top"$trclass><td><a href="@{[url_path($Girocco::Config::gitweburl)]}/$projname"$target
>@{[_escapeHTML($projname)]}</td>$sizecol$typecol$changecol<td>@{[_escapeHTML($projdesc)]}</td></tr>
EOT
			$trclass = $trclass ? '' : ' class="odd"';
			++$count;
		}
	}
	$html .= <<EOT;
</table>
EOT
	return ($count || (defined($options->{emptyok}) && $options->{emptyok})) ? $html : '';
}

my %_month_names;
BEGIN {
	%_month_names = (
		jan => 0, feb => 1, mar => 2, apr => 3, may => 4, jun => 5,
		jul => 6, aug => 7, sep => 8, oct => 9, nov => 10, dec => 11
	);
}

# Should be in "date '+%a, %d %b %Y %T %z'" format as saved to lastgc, lastrefresh and lastchange
# The leading "%a, " is optional, returns undef if unrecognized date.  This is also known as
# RFC 2822 date format and git's '%cD', '%aD' and --date=rfc2822 format.
# If the second argument is a SCALAR ref, its value will be set to the TZ offset in seconds
sub parse_rfc2822_date {
	my $dstr = shift || '';
	my $tzoff = shift || '';
	$dstr = $1 if $dstr =~/^[^\s]+,\s*(.*)$/;
	return undef unless $dstr =~
		/^\s*(\d{1,2})\s+([A-Za-z]{3})\s+(\d{4})\s+(\d{1,2}):(\d{2}):(\d{2})\s+([+-]\d{4})\s*$/;
	my ($d,$b,$Y,$H,$M,$S,$z) = ($1,$2,$3,$4,$5,$6,$7);
	my $m = $_month_names{lc($b)};
	return undef unless defined($m);
	my $seconds = timegm(0+$S, 0+$M, 0+$H, 0+$d, 0+$m, $Y-1900);
	my $offset = 60 * (60 * (0+substr($z,1,2)) + (0+substr($z,3,2)));
	$offset = -$offset if substr($z,0,1) eq '-';
	$$tzoff = $offset if ref($tzoff) eq 'SCALAR';
	return $seconds - $offset;
}

# Will parse any supported date format.  Actually there are three formats
# currently supported:
#   1. RFC 2822 (uses parse_rfc2822_date)
#   2. RFC 3339 / ISO 8601 (T may be ' ' or '_', 'Z' is optional or may be 'UTC', ':' optional in TZ)
#   3. Same as #2 except no colons or hyphens allowed and hours MUST be 2 digits
#   4. unix seconds since epoch with optional +/- trailing TZ (may not have a ':')
# Returns undef if unsupported date.
# If the second argument is a SCALAR ref, its value will be set to the TZ offset in seconds
sub parse_any_date {
	my $dstr = shift || '';
	my $tzoff = shift || '';
	if ($dstr =~ /^\s*([-+]?\d+)(?:\s+([-+]\d{4}))?\s*$/) {
		# Unix timestamp
		my $ts = 0 + $1;
		my $off = 0;
		if ($2) {
			my $z = $2;
			$off = 60 * (60 * (0+substr($z,1,2)) + (0+substr($z,3,2)));
			$off = -$off if substr($z,0,1) eq '-';
		}
		$$tzoff = $off if ref($tzoff) eq 'SCALAR';
		return $ts;
	}
	if ($dstr =~ /^\s*(\d{4})-(\d{2})-(\d{2})[Tt _](\d{1,2}):(\d{2}):(\d{2})(?:[ _]?([Zz]|[Uu][Tt][Cc]|(?:[-+]\d{1,2}:?\d{2})))?\s*$/ ||
	    $dstr =~ /^\s*(\d{4})(\d{2})(\d{2})[Tt _](\d{2})(\d{2})(\d{2})(?:[ _]?([Zz]|[Uu][Tt][Cc]|(?:[-+]\d{2}\d{2})))?\s*$/) {
		my ($Y,$m,$d,$H,$M,$S,$z) = ($1,$2,$3,$4,$5,$6,$7||'');
		my $seconds = timegm(0+$S, 0+$M, 0+$H, 0+$d, $m-1, $Y-1900);
		defined($z) && $z ne '' or $z = 'Z';
		$z = uc($z);
		$z =~ s/://;
		substr($z,1,0) = '0' if length($z) == 4;
		my $off = 0;
		if ($z ne 'Z' && $z ne 'UTC') {
			$off = 60 * (60 * (0+substr($z,1,2)) + (0+substr($z,3,2)));
			$off = -$off if substr($z,0,1) eq '-';
		}
		$$tzoff = $off if ref($tzoff) eq 'SCALAR';
		return $seconds - $off;
	}
	return parse_rfc2822_date($dstr, $tzoff);
}

# Input is a number such as a minute interval
# Return value is a random number between the input and 1.25*input
# This can be used to randomize the update and gc operations a bit to avoid
# having them all end up all clustered together
sub rand_adjust {
	my $input = shift || 0;
	return $input unless $input;
	return $input + int(rand(0.25 * $input));
}

# Open a pipe to a new sendmail process.  The '-i' option is always passed to
# the new process followed by any addtional arguments passed in.  Note that
# the sendmail process is only expected to understand the '-i', '-t' and '-f'
# options.  Using any other options via this function is not guaranteed to work.
# A list of recipients may follow the options.  Combining a list of recipients
# with the '-t' option is not recommended.
sub sendmail_pipe {
	return undef unless @_;
	die "\$Girocco::Config::sendmail_bin is unset or not executable!\n"
		unless $Girocco::Config::sendmail_bin && -x $Girocco::Config::sendmail_bin;
	my $result = open(my $pipe, '|-', $Girocco::Config::sendmail_bin, '-i', @_);
	return $result ? $pipe : undef;
}

# Open a pipe that works similarly to a mailer such as /usr/bin/mail in that
# if the first argument is '-s', a subject line will be automatically added
# (using the second argument as the subject).  Any remaining arguments are
# expected to be recipient addresses that will be added to an explicit To:
# line as well as passed on to sendmail_pipe.  In addition an
# "Auto-Submitted: auto-generated" header is always added as well as a suitable
# "From:" header.
sub mailer_pipe {
	my $subject = undef;
	if (@_ >= 2 && $_[0] eq '-s') {
		shift;
		$subject = shift;
	}
	my $tolist = join(", ", @_);
	unshift(@_, '-f', $Girocco::Config::sender) if $Girocco::Config::sender;
	my $pipe = sendmail_pipe(@_);
	if ($pipe) {
		print $pipe "From: \"$Girocco::Config::name\" ",
			"($Girocco::Config::title) ",
			"<$Girocco::Config::admin>\n";
		print $pipe "To: $tolist\n";
		print $pipe "Subject: $subject\n" if defined($subject);
		print $pipe "MIME-Version: 1.0\n";
		print $pipe "Content-Type: text/plain; charset=utf-8; format=fixed\n";
		print $pipe "Content-Transfer-Encoding: 8bit\n";
		print $pipe "X-Girocco: $Girocco::Config::gitweburl\n"
			unless $Girocco::Config::suppress_x_girocco;
		print $pipe "Auto-Submitted: auto-generated\n";
		print $pipe "\n";
	}
	return $pipe;
}

sub _goodval {
	my $val = shift;
	return undef unless defined($val);
	$val =~ s/[\r\n]+$//s;
	return undef unless $val =~ /^\d+$/;
	$val = 0 + $val;
	return undef unless $val >= 1;
	return $val;
}

# Returns the number of "online" cpus or undef if undetermined
sub online_cpus {
	my @confcpus = $^O eq "linux" ?
		qw(_NPROCESSORS_ONLN NPROCESSORS_ONLN) :
		qw(NPROCESSORS_ONLN _NPROCESSORS_ONLN) ;
	my $cpus = _goodval(get_cmd('getconf', $confcpus[0]));
	return $cpus if $cpus;
	$cpus = _goodval(get_cmd('getconf', $confcpus[1]));
	return $cpus if $cpus;
	if ($^O ne "linux") {
		my @sysctls = qw(hw.ncpu);
		unshift(@sysctls, qw(hw.availcpu)) if $^O eq "darwin";
		foreach my $mib (@sysctls) {
			$cpus = _goodval(get_cmd('sysctl', '-n', $mib));
			return $cpus if $cpus;
		}
	}
	return undef;
}

# Returns the system page size in bytes or undef if undetermined
# This should never fail on a POSIX system
sub sys_pagesize {
	use POSIX ":unistd_h";
	my $pagesize = sysconf(_SC_PAGESIZE);
	return undef unless defined($pagesize) && $pagesize =~ /^\d+$/;
	$pagesize = 0 + $pagesize;
	return undef unless $pagesize >= 256;
	return $pagesize;
}

# Returns the amount of available physical memory in bytes
# This may differ from the actual amount of physical memory installed
# Returns undef if this cannot be determined
sub sys_memsize {
	my $pagesize = sys_pagesize;
	if ($pagesize && $^O eq "linux") {
		my $pages = _goodval(get_cmd('getconf', '_PHYS_PAGES'));
		return $pagesize * $pages if $pages;
	}
	if ($^O ne "linux") {
		my @sysctls = qw(hw.physmem64);
		unshift(@sysctls, qw(hw.memsize)) if $^O eq "darwin";
		foreach my $mib (@sysctls) {
			my $memsize = _goodval(get_cmd('sysctl', '-n', $mib));
			return $memsize if $memsize;
		}
		my $memsize32 = _goodval(get_cmd('sysctl', '-n', 'hw.physmem'));
		return $memsize32 if $memsize32 && $memsize32 <= 2147483647;
		if ($pagesize) {
			my $pages = _goodval(get_cmd('sysctl', '-n', 'hw.availpages'));
			return $pagesize * $pages if $pages;
		}
		return 2147483647 + 1 if $memsize32;
	}
	return undef;
}

sub _get_max_conf_suffixed_size {
	my $conf = shift;
	return undef unless defined $conf && $conf =~ /^(\d+)([kKmMgG]?)$/;
	my ($val, $suffix) = (0+$1, lc($2));
	$val *= 1024 if $suffix eq 'k';
	$val *= 1024 * 1024 if $suffix eq 'm';
	$val *= 1024 * 1024 * 1024 if $suffix eq 'g';
	return $val;
}

sub _make_suffixed_size {
	my $size = shift;
	return $size if $size % 1024;
	$size /= 1024;
	return "${size}k" if $size % 1024;
	$size /= 1024;
	return "${size}m" if $size % 1024;
	$size /= 1024;
	return "${size}g";
}

# Return the value to pass to --window-memory= for git repack
# If the system memory or number of CPUs cannot be determined, returns "1g"
# Otherwise returns one third the available memory divided by the number of CPUs
# but never more than 1 gigabyte or max_gc_window_memory_size.
sub calc_windowmemory {
	my $cpus = online_cpus;
	my $memsize = sys_memsize;
	my $max = 1024 * 1024 * 1024;
	if ($cpus && $memsize) {
		$max = int($memsize / 3 / $cpus);
		$max = 1024 * 1024 * 1024 if $max >= 1024 * 1024 * 1024;
	}
	my $maxconf = _get_max_conf_suffixed_size($Girocco::Config::max_gc_window_memory_size);
	$max = $maxconf if defined($maxconf) && $maxconf && $max > $maxconf;
	return _make_suffixed_size($max);
}

# Return the value to set as core.bigFileThreshold for git repack
# If the system memory cannot be determined, returns "256m"
# Otherwise returns the available memory divided by 16
# but never more than 512 megabytes or max_gc_big_file_threshold_size.
sub calc_bigfilethreshold {
	my $memsize = sys_memsize;
	my $max = 256 * 1024 * 1024;
	if ($memsize) {
		$max = int($memsize / 16);
		$max = 512 * 1024 * 1024 if $max >= 512 * 1024 * 1024;
	}
	my $maxconf = _get_max_conf_suffixed_size($Girocco::Config::max_gc_big_file_threshold_size);
	$max = $maxconf if defined($maxconf) && $maxconf && $max > $maxconf;
	return _make_suffixed_size($max);
}

# Return the value to use when deciding whether or not to re-calculate object deltas
# If there are no more than this many objects then deltas will be recomputed in
# order to create more efficient pack files.  The new_delta_threshold value
# is constrained to be at least 1000 * cpu cores and no more than 100000.
# The default is sys_memsize rounded up to the nearest multiple of 256 MB and
# then 5000 per 256 MB or 50000 if we cannot determine memory size but never
# more than 100000 or less than 1000 * cpu cores.
sub calc_redeltathreshold {
	my $cpus = online_cpus || 1;
	if (defined($Girocco::Config::new_delta_threshold) &&
	    $Girocco::Config::new_delta_threshold =~ /^\d+/) {
		my $ndt = 0 + $Girocco::Config::new_delta_threshold;
		if ($ndt >= $cpus * 1000) {
			return $ndt <= 100000 ? $ndt : 100000;
		}
	}
	my $calcval = 50000;
	my $memsize = sys_memsize;
	if ($memsize) {
		my $quantum = 256 * 1024 * 1024;
		$calcval = 5000 * int(($memsize + ($quantum - 1)) / $quantum);
		$calcval = 1000 * $cpus if $calcval < 1000 * $cpus;
		$calcval = 100000 if $calcval > 100000;
	}
	return $calcval;
}

# $1 => thing to test
# $2 => optional directory, if given and -e "$2/$1$3", then return false
# $3 => optional, defaults to ''
sub has_reserved_suffix {
	no warnings; # avoid silly 'unsuccessful stat on filename with \n' warning
	my ($name, $dir, $ext) = @_;
	$ext = '' unless defined $ext;
	return 0 unless defined $name && $name =~ /\.([^.]+)$/;
	return 0 unless exists $Girocco::Config::reserved_suffixes{lc($1)};
	return 0 if defined $dir && -e "$dir/$name$ext";
	return 1;
}

# mostly undoes effect of `use CGI::Carp qw(fatalsToBrowser);`
# mostly undoes effect of `use CGI::Carp qw(warningsToBrowser);`
sub noFatalsToBrowser {
	delete $SIG{__DIE__};
	delete $SIG{__WARN__};
	undef *CORE::GLOBAL::die;
	*CORE::GLOBAL::die = sub {
		no warnings;
		my $ec = $! || ($? >> 8) || 255;
		my (undef, $fn, $li) = caller(0);
		my $loc = " at " . $fn . " line " . $li . ".\n";
		my $msg = "";
		$msg = join("", @_) if @_;
		$msg = "Died" if $msg eq "";
		$msg .= $loc unless $msg =~ /\n$/;
		die $msg if $^S;
		printf STDERR "%s", $msg;
		exit($ec);
	};
	undef *CORE::GLOBAL::warn;
	*CORE::GLOBAL::warn = sub {
		no warnings;
		my (undef, $fn, $li) = caller(0);
		my $loc = " at " . $fn . " line " . $li . ".\n";
		my $msg = "";
		$msg = join("", @_) if @_;
		$msg = "Warning: something's wrong" if $msg eq "";
		$msg .= $loc unless $msg =~ /\n$/;
		printf STDERR "%s", $msg;
	};
}

# mimics Git's symref reading but only for HEAD
# returns undef on failure otherwise an string that is
# either an all-hex (lowercase) value or starts with "refs/"
sub read_HEAD_ref {
	my $headpath = $_[0] . "/HEAD";
	if (-l $headpath) {
		my $rl = readlink($headpath);
		return defined($rl) && $rl =~ m,^refs/[^\x00-\x1f \x7f~^:\\*?[]+$, ? $rl : undef;
	}
	open my $fd, '<', $headpath or return undef;
	my $hv;
	{
		local $/ = undef;
		$hv = <$fd>;
	}
	close $fd;
	defined($hv) or return undef;
	chomp $hv;
	$hv =~ m,^ref:\s*(refs/[^\x00-\x1f \x7f~^:\\*?[]+)$, and return $1;
	$hv =~ m/^[0-9a-fA-F]{40,}$/ and return lc($hv);
	return undef;
}

# same as read_HEAD_ref but returns undef
# unless the result starts with "refs/"
sub read_HEAD_symref {
	my $hv = read_HEAD_ref(@_);
	return defined($hv) && $hv =~ m,^refs/., ? $hv : undef;
}

my $cf_unesc;
BEGIN {
	my %escvals = (
		b    => "\b",
		t    => "\t",
		n    => "\n",
		'"'  => '"',
		'\\' => '\\'
	);
	$cf_unesc = sub {
		$_[0] =~ s/\\([btn\042\\])/$escvals{$1}/g;
		$_[0];
	};
}

# mimics Git's config.c git_parse_source function behavior
# returns array of arrayref of key and value
# except that valueless booleans have a value of undef
sub read_config_file {
	local $_;
	my ($fn, $warn) = @_;
	my $li = 0;
	my $section = "";
	my @vals = ();
	open my $fh, '<', $fn or
		$warn && warn("could not open \"$fn\": $!\n"), return(undef);
	binmode($fh);
	my $bad = sub {
		close $fh;
		warn "bad config line $li in file $fn\n" if $warn;
		return undef;
	};
	while (<$fh>) {
		++$li;
		s/(?:\r\n|\n)$//;
		$_ = to_utf8($_);
		s/^\x{feff}// if $li == 1;
		utf8::encode($_);
		if (/^\s*\[/gc) {
			if (/\G([.a-zA-Z0-9-]+)\]/gc) {
				$section = lc($1) . ".";
			} elsif (/\G([.a-zA-Z0-9-]*)\s+"((?:[^\042\\\n]|\\.)*)"\]/gc) {
				$section = lc($1) . "." .
					&{sub{my $x=shift; $x =~ s/\\(.)/$1/g; $x}}($2) . ".";
			} else {
				return &$bad;
			}
		}
		/\G\s+/gc;
		next if /\G(?:[;#]|$)/;
		if (/\G([a-zA-Z][a-zA-Z0-9-]*)[ \t]*/gc) {
			my $k = $section . lc($1);
			my $v;
			if (/\G$/) {
				$v = undef;
			} elsif (/\G=\s*/gc) {
				$v = "";
				my $qt = 0;
				{
					if (/\G$/) {
						last if !$qt;
						return &$bad;
					}
					if (!$qt && /\G((?:[^"\\\n;#]|\\[btn"\\])+)/gc) {
						my $a = $1;
						if (/\G[;#]/) {
							$_ = "";
							$a =~ s/\s+$//;
						}
						$a =~ s/\s/ /g;
						$v .= &$cf_unesc($a);
					} elsif ($qt && /\G((?:[^"\\\n]|\\[btn"\\])+)/gc) {
						my $a = $1;
						$v .= &$cf_unesc($a);
					} elsif (/\G\042/gc) {
						$qt = !$qt;
					} elsif (!$qt && /\G[;#]/gc) {
						$_ = "";
					} elsif (/\G\\$/) {
						$_ = <$fh>;
						if (defined($_)) {
							++$li;
							s/(?:\r\n|\n)$//;
							$_ = to_utf8($_, 1);
							/^\s+/gc unless $v ne "" || $qt;
						} else {
							$_ = "";
						}
					} else {
						return &$bad;
					}
					redo;
				}
			} else {
				return &$bad;
			}
			push(@vals, [$k, $v]);
		} else {
			return &$bad;
		}
	}
	close $fh;
	return \@vals;
}

# Same as read_config_file except that a hashref is returned and
# subsequent same-key-name values replace earlier ones.
# Also valueless booleans are given the value 1
sub read_config_file_hash {
	my $result = read_config_file(@_);
	return undef unless defined($result);
	my %config = map {($$_[0], defined($$_[1])?$$_[1]:1)} @$result;
	return \%config;
}

# similar to Git's test except that GIT_OBJECT_DIRECTORY is ignored
sub is_git_dir {
	my $gd = shift;
	defined($gd) && $gd ne "" && -d $gd or return undef;
	-d "$gd/objects" && -x "$gd/objects" or return 0;
	-d "$gd/refs" && -x "$gd/refs" or return 0;
	if (-l "$gd/HEAD") {
		my $rl = readlink("$gd/HEAD");
		defined($rl) && $rl =~ m,^refs/., or return 0;
		-e "$gd/HEAD" or return 1;
	}
	open my $fd, '<', "$gd/HEAD" or return 0;
	my $hv;
	{
		local $/;
		$hv = <$fd>;
	}
	close $fd;
	defined $hv or return 0;
	chomp $hv;
	$hv =~ m,^ref:\s*refs/., and return 1;
	return $hv =~ /^[0-9a-f]{40}/;
}

# Returns 0 for false, 1 for true, undef for unrecognized or undef
# Unless the optional second argument is true in which case undef returns 1
sub git_bool {
	defined($_[0]) or return $_[1] ? 1 : undef;
	my $v = lc($_[0]);
	return 0 if $v eq 'false' || $v eq 'off' || $v eq 'no' || $v eq '' || $v =~ /^[-+]?0+$/;
	return 1 if $v eq 'true' || $v eq 'on' || $v eq 'yes' || $v =~ /^[-+]?0*[1-9][0-9]*$/;
	return undef;
}

# Returns a PATH properly prefixed which guarantees that Git is found and the
# basedir/bin utilities are found as intended.  $ENV{PATH} is LEFT UNCHANGED!
# Caller is responsible for assigning result to $ENV{PATH} or otherwise
# arranging for it to be used.  If $ENV{PATH} already has the proper prefix
# then it's returned as-is (making this function idempotent).
# Will die if it cannot determine a suitable full PATH.
# Result is cached so all calls after the first are practically free.
my $var_git_exec_path;
sub util_path {
	if (!defined($var_git_exec_path)) {
		defined($Girocco::Config::basedir) && $Girocco::Config::basedir ne "" &&
		-d $Girocco::Config::basedir && -r _ && -x _ or
			die "invalid \$Girocco::Config::basedir setting: $Girocco::Config::basedir\n";
		my $varsfile = $Girocco::Config::basedir . "/shlib_vars.sh";
		if (-f $varsfile && -r _) {
			my $vars;
			if (open $vars, '<', $varsfile) {
				# last value for var_git_exec_path wins
				while (<$vars>) {
					chomp;
					substr($_, 0, 19) eq "var_git_exec_path=\"" or next;
					substr($_, -1, 1) eq "\"" or next;
					my $xd = substr($_, 19, -1);
					$var_git_exec_path = $xd if -d $xd && -r _ && -x _;
				}
				close $vars;
			}
		}
		if (!defined($var_git_exec_path)) {
			my $xd = get_git("--exec-path");
			$var_git_exec_path = $xd if defined($xd) &&
				(chomp $xd, $xd) ne "" && -d $xd && -r _ && -x _;
		}
		defined($var_git_exec_path) or
			die "could not determine \$(git --exec-path) value\n"
	}
	my $prefix = "$var_git_exec_path:$Girocco::Config::basedir/bin:";
	if (substr($ENV{PATH}, 0, length($prefix)) eq $prefix) {
		return $ENV{PATH};
	} else {
		return $prefix . $ENV{PATH};
	}
}

# Note that Perl performs a "shellish" test in the Perl_do_exec3 function from doio.c,
# but it has slightly different semantics in that whitespace does not automatically
# make something "shellish".  The semantics used here more closely match Git's
# semantics so that Girocco will provide an interpretation more similar to Git's.
sub is_shellish {
	return unless defined(local $_ = shift);
	return 1 if m#[][\$&*(){}'";:=\\|?<>~`\#\s]#; # contains metacharacters
	return 0; # probably not shellish
}

1;
