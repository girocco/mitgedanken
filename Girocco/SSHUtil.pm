# Girocco::SSHUtil.pm -- OpenSSH public key utility
# Copyright (c) 2013 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

package Girocco::SSHUtil;

use strict;
use warnings;

use base qw(Exporter);
our @EXPORT;
our $VERSION;

BEGIN {
	@EXPORT = qw(sshpub_validate);
	*VERSION = \'1.0';
}

use MIME::Base64;
use Digest::MD5 qw(md5_hex);

# Input: binary sequence of one or more 4-byte big-endian length + contents
#        sequences
# Output: array of contents or () if invalid
sub _splitparts($)
{
	my $data = shift;
	my @parts = ();
	while (length($data) >= 4) {
		my $len = unpack('N',substr($data,0,4));
		my $value = '';
		if ($len > 0) {
			return () if $len + 4 > length($data);
			$value = substr($data,4,$len);
		}
		push(@parts, $value);
		substr($data, 0, 4+$len) = '';
	}
	return () unless length($data) == 0;
	return @parts;
}

# Input: binary key data
# Output: how many key bits are present
sub _countbits($)
{
	my $data = shift;
	return undef if length($data) < 1;
	my $bits = 8 * length($data);
	# but leading zero bits must be subtracted
	my $byte = unpack('C',substr($data,0,1));
	if (!$byte) {
		$bits -= 8;
	} else {
		return undef if $byte & 0x80; # negative is not valid
		while (!($byte & 0x80)) {
			--$bits;
			$byte <<= 1;
		}
	}
	return $bits;
}

# sshpub_validate
#   Input: A single-line ssh public key such as the contents of id_rsa.pub
#   Output:
#     () (if invalid key)
#     -OR-
#     array of 4 elements:
#       key type (either 'ssh-dss' or 'ssh-rsa')
#       key size (integer such as 1024, 2048, 3072, 4096 etc.)
#       key fingerprint (string as shown by ssh-keygen -l)
#       key comment (may be '' if none)
sub sshpub_validate($)
{
	my $raw = shift;
	return () if !$raw;
	my @fields = split(/[ ]/, $raw, 3);
	return () if @fields < 2;
	return () if $fields[0] ne 'ssh-dss' && $fields[0] ne 'ssh-rsa';
	return () if $fields[1] !~ m,^[0-9A-Za-z+/=]+$,;
	$fields[2] = '' if !$fields[2];
	$fields[2] = join(' ', split(' ', $fields[2]));
	my $data = decode_base64($fields[1]);
	return () if !$data || length($data) < 21;
	my @parts = _splitparts($data);
	return () if @parts < 3 || $parts[0] ne $fields[0];
	# ssh-rsa is 3 fields: type, e, n
	# ssh-dss is 5 fields: type, p, q, g, y
	my $bitlength;
	if ($parts[0] eq 'ssh-dss') {
		return () if @parts != 5;
		$bitlength = _countbits($parts[1]);
	} elsif ($parts[0] eq 'ssh-rsa') {
		return () if @parts != 3;
		$bitlength = _countbits($parts[2]);
	} else {
		return ();
	}
	return () if !$bitlength || $bitlength < 512; # probably should be 2048
	my $fingerprint = md5_hex($data);
	$fingerprint =~ s/(..)(?=.)/$1:/g;
	return ($fields[0], $bitlength, $fingerprint, $fields[2]);
}

1;
