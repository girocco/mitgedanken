package Girocco::ProjPerm;

# This is base class for various permission models; these control updating
# permissions of appropriate files in push-enabled repositories.
# The /etc/group file is maintained by Girocco::Project itself.

# The chosen subclass of this class is used as base class of Girocco::Project.

use strict;
use warnings;

use Girocco::Config;

BEGIN {
	our @interesting = qw(refs info objects);
}

sub perm_initialize {
	my ($proj) = @_;
	die "unimplemented";
}

sub perm_user_add {
	my ($proj, $username, $uid) = @_;
	die "unimplemented";
}

sub perm_user_del {
	my ($proj, $username, $uid) = @_;
	die "unimplemented";
}

sub shared_mode {
	my ($proj) = @_;
	# The --shared= argument for git init
	die "unimplemented";
}

1;


package Girocco::ProjPerm::Group;

# This is the naive permission model: on init, we just chgrp the relevant
# pieces to our group and make them group-writable. But oh, we cannot do that
# since we are not root; so we just sit happily and do nothing else.
# Then, we can just sit happily and do nothing else either.

use strict;
use warnings;

BEGIN {
	use base qw(Girocco::ProjPerm);
}

sub perm_initialize {
	my ($proj) = @_;

	# adjust group and other permissions
	system("chmod", "-R", "ug+rw,o+r", $proj->{path}) == 0 or die "Running chmod failed: $!";

	1;
}

sub perm_user_add {
	my ($proj, $username, $uid) = @_;
	1;
}

sub perm_user_del {
	my ($proj, $username, $uid) = @_;
	1;
}

sub shared_mode {
	my ($proj) = @_;
	"group";
}

sub can_user_push {
	my ($proj, $username) = @_;
	grep { $_ eq $username } @{$proj->{users}};
}

1;


package Girocco::ProjPerm::Hooks;

# This is the "soft-security" permission model: we keep the repository
# world-writable and check if the user is allowed to push only within
# the update hook that will call our can_user_push() method.

use strict;
use warnings;

BEGIN {
	use base qw(Girocco::ProjPerm);
}

sub perm_initialize {
	my ($proj) = @_;
	1;
}

sub perm_user_add {
	my ($proj, $username, $uid) = @_;
	1;
}

sub perm_user_del {
	my ($proj, $username, $uid) = @_;
	1;
}

sub shared_mode {
	my ($proj) = @_;
	"0777";
}

sub can_user_push {
	my ($proj, $username) = @_;
	grep { $_ eq $username } @{$proj->{users}};
}

1;
