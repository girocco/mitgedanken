package Girocco::User;

use strict;
use warnings;

use Digest::MD5 qw(md5);

use Girocco::Config;
use Girocco::CGI;
use Girocco::Util;
use Girocco::SSHUtil;

BEGIN {
	eval {
		require Digest::SHA;
		Digest::SHA->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA1;
		Digest::SHA1->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA::PurePerl;
		Digest::SHA::PurePerl->import(
			qw(sha1_hex)
	);1} ||
	die "One of Digest::SHA or Digest::SHA1 or Digest::SHA::PurePerl "
	. "must be available\n";
}

sub _gen_uuid {
	my $self = shift;

	$self->{uuid} = '' unless $self->{uuid};
	my @md5;
	{
		no warnings;
		@md5 = unpack('C*', md5(time . $$ . rand() . join(':',%$self)));
	}
	$md5[6] = 0x40 | ($md5[6] & 0x0F); # Version 4 -- random
	$md5[8] = 0x80 | ($md5[8] & 0x3F); # RFC 4122 specification
	return sprintf(
		'%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x',
		@md5);
}

sub _remove_ssh_leftovers {
	my $self = shift;
	my @files;
	system('rm', '-f', "$Girocco::Config::chroot/etc/sshkeys/$self->{name}");
	@files = glob("'$Girocco::Config::chroot/etc/sshcerts/${Girocco::Config::nickname}_$self->{name}'_user_*.pem");
	system('rm', '-f', @files) if @files;
	system('rm', '-f', "$Girocco::Config::chroot/etc/sshactive/$self->{name}");
	@files = glob("'$Girocco::Config::chroot/etc/sshactive/$self->{name}',*");
	system('rm', '-f', @files) if @files;
}

sub _passwd_add {
	use POSIX qw(strftime);
	my $self = shift;
	my (undef, undef, $gid) = getgrnam($Girocco::Config::owning_group||'');
	my $owngroupid = $gid ? $gid : 65534;
	Girocco::User->load($self->{name}) and die "User $self->{name} already exists";
	$self->{uuid} = $self->_gen_uuid;
	my ($S,$M,$H,$d,$m,$y) = gmtime(time());
	$self->{creationtime} = strftime("%Y%m%d_%H%M%S", $S, $M, $H, $d, $m, $y, -1, -1, -1);
	my $email_uuid_etc = join ',', $self->{email}, $self->{uuid}, $self->{creationtime};
	filedb_atomic_append(jailed_file('/etc/passwd'),
		join(':', $self->{name}, 'x', '\i', $owngroupid, $email_uuid_etc, '/', '/bin/git-shell-verify'),
		$self->{name});
	$self->_remove_ssh_leftovers;
}

sub _passwd_update {
	my $self = shift;
	filedb_atomic_edit(jailed_file('/etc/passwd'),
		sub {
			$_ = $_[0];
			chomp;
			if ($self->{name} eq (split /:/)[0]) {
				# preserve all but login name and first 2 fields of comment field
				# creating a uuid (field 2 of comment field) if one is not already present
				my @fields=split(/:/, $_, -1);
				$fields[0] = $self->{name};
				my @subfields = split(',', $fields[4]||'', -1);
				$self->{uuid} = $subfields[1] || '';
				$self->{uuid} or $self->{uuid} = $self->_gen_uuid;
				$subfields[0] = $self->{email};
				$subfields[1] = $self->{uuid};
				$fields[4] = join(',', @subfields);
				return join(':', @fields)."\n";
			} else {
				return "$_\n";
			}
		},
		$self->{name}
	);
}

sub _passwd_remove {
	my $self = shift;
	$self->_remove_ssh_leftovers;
	filedb_atomic_edit(jailed_file('/etc/passwd'),
		sub {
			$self->{name} ne (split /:/)[0] and return $_;
		},
		$self->{name}
	);
}

sub _sshkey_path {
	my $self = shift;
	'/etc/sshkeys/'.$self->{name};
}

sub _sshkey_load {
	my $self = shift;
	open my $fd, '<', jailed_file($self->_sshkey_path) or die "sshkey load failed: $!";
	my @keys = ();
	my $auth = '';
	my $authtype = '';
	while (<$fd>) {
		chomp;
		if (/^(?:no-pty )?(ssh-(?:dss|rsa) .*)$/) {
			push @keys, $1;
		} elsif (/^# ([A-Z]+)AUTH ([0-9a-f]+) (\d+)/) {
			my $expire = $3;
			$auth = $2 unless (time >= $expire);
			$authtype = $1 if $auth;
		}
	}
	close $fd;
	my $keys = join("\n", @keys); chomp $keys;
	($keys, $auth, $authtype);
}

sub _trimkeys($) {
	my $keys = shift;
	my @lines = ();
	foreach (split /\r\n|\r|\n/, $keys) {
		next if /^[ \t]*$/ || /^[ \t]*#/;
		push(@lines, $_);
	}
	return join("\n", @lines);
}

sub _sshkey_save {
	my $self = shift;
	$self->{keys} = _trimkeys($self->{keys} || '');
	open my $fd, '>', jailed_file($self->_sshkey_path) or die "sshkey save failed: $!";
	if (defined($self->{auth}) && $self->{auth}) {
		my $expire = time + 24 * 3600;
		my $typestr = $self->{authtype} ? uc($self->{authtype}) : 'REPO';
		print $fd "# ${typestr}AUTH $self->{auth} $expire\n";
	}
	print $fd map("no-pty $_\n", split(/\n/, $self->{keys}));
	close $fd;
	chmod 0664, jailed_file($self->_sshkey_path);
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name, $rsrv_sfx_ok) = @_;
	Girocco::User::valid_name($name, $rsrv_sfx_ok ? $Girocco::Config::chroot."/etc/sshkeys" : undef)
		or die "refusing to create user with invalid name ($name)!";
	my $proj = { name => $name };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual user not connected to disk record
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name) = @_;
	my $self = $class->_new($name);
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my ($name) = @_;

	open my $fd, '<', jailed_file("/etc/passwd") or die "user load failed: $!";
	my $r = qr/^\Q$name\E:/;
	foreach (grep /$r/, <$fd>) {
		chomp;

		my (undef, undef, $uid, undef, $email_uuid_etc) = split /:/;
		my $self = $class->_new($name, 1);
		$self->{uid} = $uid;
		($self->{keys}, $self->{auth}, $self->{authtype}) = $self->_sshkey_load;
		($self->{email}, $self->{uuid}, $self->{creationtime}) = split ',', $email_uuid_etc;

		close $fd;
		$self->{uuid} or !valid_email($self->{email}) or $self->_passwd_update;
		return $self;
	}
	close $fd;
	undef;
}

# public constructor #2
sub load_by_uid {
	my $class = shift;
	my ($uid) = @_;

	open my $fd, '<', jailed_file("/etc/passwd") or die "user load failed: $!";
	my $r = qr/^[^:]+:[^:]*:\Q$uid\E:/;
	foreach (grep /$r/, <$fd>) {
		chomp;

		my ($name, undef, undef, undef, $email_uuid_etc) = split /:/;
		my $self = $class->_new($name, 1);
		$self->{uid} = $uid;
		($self->{keys}, $self->{auth}, $self->{authtype}) = $self->_sshkey_load;
		($self->{email}, $self->{uuid}, $self->{creationtime}) = split ',', $email_uuid_etc;

		close $fd;
		$self->{uuid} or $self->_passwd_update;
		return $self;
	}
	close $fd;
	undef;
}

# $user may not be in sane state if this returns false!
sub cgi_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;

	$self->{name} = $gcgi->wparam('name');
	Girocco::User::valid_name($self->{name})
		or $gcgi->err("Name contains invalid characters.");

	length($self->{name}) <= 64
		or $gcgi->err("Your user name is longer than 64 characters. Do you really need that much?");

	$self->{email} = $gcgi->wparam('email');
	valid_email($self->{email})
		or $gcgi->err("Your email sure looks weird...?");
	length($self->{email}) <= 96
		or $gcgi->err("Your email is longer than 96 characters. Do you really need that much?");

	$self->keys_fill($gcgi);
}

sub update_email {
	my $self = shift;
	my $gcgi = shift;
	my $email = shift || '';

	if (valid_email($email)) {
		$self->{email} = $email;
		$self->_passwd_update;
	} else {
		$gcgi->err("Your email sure looks weird...?");
	}

	not $gcgi->err_check;
}

sub keys_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;

	$self->{keys} = _trimkeys($cgi->param('keys'));
	length($self->{keys}) <= 9216
		or $gcgi->err("The list of keys is more than 9kb. Do you really need that much?");
	foreach my $key (split /\r?\n/, $self->{keys}) {
		my ($type, $bits, $fingerprint, $comment);
		($type, $bits, $fingerprint, $comment) = sshpub_validate($key)
			if $key =~ /^ssh-(?:dss|rsa) [0-9A-Za-z+\/=]+ \S+$/;
		if (!$type) {
			my $keyval = CGI::escapeHTML($key);
			my $dsablurb = '';
			$dsablurb = ' or ssh-dss' unless $Girocco::Config::disable_dsa;
			$gcgi->err(<<EOT);
Your ssh key ("$keyval") appears to have an invalid format
(does not start with ssh-rsa$dsablurb or does not end with a whitespace-free comment) -
maybe your browser has split a single key onto multiple lines?
EOT
		} elsif ($Girocco::Config::disable_dsa && $type eq 'ssh-dss') {
			my $keyval = CGI::escapeHTML($key);
			$gcgi->err(<<EOT);
Your ssh key ("$keyval") appears to be of type dsa but only rsa keys are
supported - please generate an rsa key (starts with ssh-rsa) and try again
EOT
		} elsif ($bits > 16384) {
			my $keyval = CGI::escapeHTML($key);
			$gcgi->err(<<EOT);
Your ssh key ("$keyval") appears to have more than 16384 bits, please don't
unnecessarily overburden our processors - please limit yourself to keys of
16384 bits or less.
EOT
		} elsif ($Girocco::Config::min_key_length && $bits < $Girocco::Config::min_key_length) {
			my $keyval = CGI::escapeHTML($key);
			$gcgi->err(<<EOT);
Your ssh key ("$keyval") appears to have only $bits bit(s) but at least
$Girocco::Config::min_key_length are required - please generate a longer key
EOT
		}
	}

	not $gcgi->err_check;
}

sub keys_save {
	my $self = shift;

	$self->_sshkey_save;
}

sub keys_html_list {
	my $self = shift;
	my @keys = split(/\r?\n/, $self->{keys});
	return '' if !@keys;
	my $html = "<ol>\n";
	my %types = ('ssh-dss' => 'DSA', 'ssh-rsa' => 'RSA');
	my $line = 0;
	foreach (@keys) {
		++$line;
		my ($type, $bits, $fingerprint, $comment) = sshpub_validate($_);
		next unless $type && $types{$type};
		my $euser = CGI::escapeHTML(CGI::Util::escape($self->{name}));
		$html .= "<li>$bits <tt>$fingerprint</tt> ($types{$type}) $comment";
		$html .= "<br /><a target=\"_blank\" ".
			"href=\"@{[url_path($Girocco::Config::webadmurl)]}/usercert.cgi/$euser/$line/".
			$Girocco::Config::nickname."_${euser}_user_$line.pem\">".
			"download https push user authentication certificate</a> <sup class=\"sup\"><span>".
			"<a target=\"_blank\" href=\"@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html\">".
			"(learn more)</a></span></sup>"
			if $type eq 'ssh-rsa' && $Girocco::Config::httpspushurl &&
				$Girocco::Config::clientcert &&
				$Girocco::Config::clientkey;
		$html .= "</li>\n";
	}
	$html .= "</ol>\n";
	return $html;
}

sub gen_auth {
	my $self = shift;
	my ($type) = @_;
	$type = 'REPO' unless $type && $type =~ /^[A-Z]+$/;

	$self->{authtype} = $type;
	$self->{auth} = sha1_hex(time . $$ . rand() . $self->{keys});
	$self->_sshkey_save;
	$self->{auth};
}

sub del_auth {
	my $self = shift;

	delete $self->{auth};
	delete $self->{authtype};
}

sub get_projects {
	my $self = shift;

	return @{$self->{projects}} if defined($self->{projects});
	my @projects = filedb_atomic_grep(jailed_file('/etc/group'),
		sub {
			$_ = $_[0];
			chomp;
			my ($group, $users) = (split /:/)[0,3];
			$group if $users && $users =~ /(^|,)\Q$self->{name}\E(,|$)/;
		}
	);
	$self->{projects} = \@projects;
	@{$self->{projects}};
}

sub conjure {
	my $self = shift;

	$self->_passwd_add;
	$self->_sshkey_save;
}

sub remove {
	my $self = shift;

	require Girocco::Project;
	foreach ($self->get_projects) {
		if (Girocco::Project::does_exist($_, 1)) {
			my $project = Girocco::Project->load($_);
			$project->update if $project->remove_user($self->{name});
		}
	}

	$self->_passwd_remove;
}

### static methods

# Note that 'mob' and 'everyone' are NOT reserved names per se, but they are
# names with special semantics and they should be allowed in project membership
# lists so they are therefore NOT included in the reservedusernames list.
# 'git', 'lock' and 'bundle'  are reserved so that the personal mob names 'mob.git',
# 'mob.lock' or 'mob.bundle' are not needed as they would be invalid.
our %reservedusernames = (
	root => 1,
	sshd => 1,
	_sshd => 1,
	nobody => 1,
	lc($Girocco::Config::cgi_user) => 1,
	lc($Girocco::Config::mirror_user) => 1,
	git => 1,
	lock => 1,
	bundle => 1,
);

sub valid_name {
	$_ = $_[0];
	/^[a-zA-Z0-9][a-zA-Z0-9+._-]*$/
	and (not m#\.\.#)
	and (not m#\.$#)
	and (not m#\.git$#i)
	and (not m#\.lock$#i)
	and (not m#\.bundle$#i)
	and (not m#^mob[._]#i)
	and !exists($reservedusernames{lc($_)})
	and !has_reserved_suffix($_, $_[1]);
}

sub does_exist {
	my ($name, $nodie) = @_;
	if (!Girocco::User::valid_name($name, $Girocco::Config::chroot."/etc/sshkeys")) {
		die "tried to query for user with invalid name $name!" unless $nodie;
		return 0;
	}
	(-e jailed_file("/etc/sshkeys/$name"));
}

sub resolve_uid {
	my ($name) = @_;
	$Girocco::Config::chrooted and undef; # TODO for ACLs within chroot
	scalar(getpwnam($name));
}


1;
