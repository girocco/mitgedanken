# Girocco::CLIUtil.pm -- Command Line Interface Utility Functions
# Copyright (C) 2016 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

#
## IMPORTANT
##
## This package MUST NOT be used by any CGI script as it cancels
## the effect of CGI::Carp::fatalsToBrowser which could result in the
## output of a CGI script becoming unparseable by the web server!
#

package Girocco::CLIUtil;

use strict;
use warnings;

use base qw(Exporter);
our ($VERSION, @EXPORT, @EXPORT_OK);

BEGIN {
	@EXPORT = qw(
		diename recreate_file strict_bool
		is_yes is_no is_yesno valid_bool clean_bool
		prompt prompt_or_die
		ynprompt ynprompt_or_die
		prompt_noecho prompt_noecho_or_die
		prompt_noecho_nl prompt_noecho_nl_or_die
		yes_to_continue yes_to_continue_or_die
		get_all_users get_user get_all_projects get_project
		get_full_users nice_me
	);
	@EXPORT_OK = qw(
		_parse_options _prompt_rl _prompt_rl_or_die
		check_passwd_match _which
	);
	*VERSION = \'1.0';
}

use File::Basename;
use File::Spec;
use POSIX qw(:fcntl_h);
use Girocco::Config;
use Girocco::Util;
use Girocco::HashUtil;
use Girocco::CGI;
BEGIN {noFatalsToBrowser}

my $have_rl;
BEGIN {eval{
	require Term::ReadLine;
	$have_rl = 1;
}}

{
	package Girocco::CLIUtil::NoEcho;

	sub new {
		my $class = shift; # ignored
		my $self = bless {};
		my $fd = shift || 0;
		$self->{fd} = $fd;
		$self->{ios} = POSIX::Termios->new;
		$self->{ios}->getattr($self->{fd});
		my $noecho = POSIX::Termios->new;
		$noecho->getattr($fd);
		$noecho->setlflag($noecho->getlflag & ~(&POSIX::ECHO));
		$noecho->setattr($fd, &POSIX::TCSANOW);
		$self;
	}

	sub DESTROY {
		my $self = shift;
		$self->{ios}->setattr($self->{fd}, &POSIX::TCSANOW);
	}
}

my $diename;
BEGIN {$diename = ""}
sub diename {
	my $result = $diename;
	$diename = join(" ", @_) if @_;
	$result;
}

# Parse Options
#
# Remove any leading options matching the given specs from the @ARGV array and
# store them as indicated.  Parsing stops when an unknown option is encountered,
# "--" is encountered (in which case it's removed) or a non-option is encountered.
# Note that "-" by itself is considered a non-option argument.
#
# Option bundling for single-letter options is NOT supported.
#
# Optional first arg is CODE ref:
#   sub {my ($err, $opt) = @_; ...}
# with $err of '?' meaning $opt is unknown
# with $err of ':' meaning $opt is missing its argument
# $opt is the full option as given on the command line (including leading - etc.)
# the default if omitted dies with an error
# If the sub returns, _parse_options exits immediately with 0
#
# The rest of the arguments form pairs:
#   "spec" => ref
# where ref must be either a SCALAR ref or a CODE ref, if it's neither
# then the "spec" => ref pair is silently ignored.
# "spec" can be:
#   "name"   -- an incrementing flag (matches -name and --name)
#   ":name"  -- an option with a value (matches -name=val and --name=val)
# Using option "--name" matches spec "name" if given otherwise matches spec
# ":name" if given and there's at least one more argument (if not the ':' error
# happens).
# Using option "--name=val" only matches spec ":name" (but "val" can be "").
# For flags, a SCALAR ref is incremented, a CODE ref is called with no arguments.
# For values (":name" specs) a SCALAR ref is assigned the value a CODE ref is
# called with the value as its single argument.
#
# _parse_options returns 1 as long as there were no errors
sub _parse_options {
	local $_;
	my $failsub = sub {die((($_[0]eq'?')?"unrecognized":"missing argument for")." option \"$_[1]\"\n")};
	$failsub = shift if @_ && ref($_[0]) eq "CODE";
	my %opts = ();
	while (@_ >= 2) {
		if (defined($_[0]) && $_[0] =~ /^:?[^-:\s]/ &&
		    defined($_[1]) && (ref($_[1]) eq "SCALAR" || ref($_[1]) eq "CODE")) {
			$opts{$_[0]} = $_[1];
		}
		shift;
		shift;
	}
	while (@ARGV && $ARGV[0] =~ /^--?[^-:\s]/) {
		my $opt = shift @ARGV;
		my $sopt = $opt;
		$sopt =~ s/^--?//;
		if ($sopt =~ /^([^=]+)=(.*)$/) {
			my ($name, $val) = ($1, $2);
			if ($opts{":$name"}) {
				${$opts{":$name"}} = $val if ref($opts{":$name"}) eq "SCALAR";
				&{$opts{":$name"}}($val) if ref($opts{":$name"}) eq "CODE";
			} else {
				&$failsub('?', $opt);
				return 0;
			}
		} elsif ($opts{$sopt}) {
			++${$opts{$sopt}} if ref($opts{$sopt}) eq "SCALAR";
			&{$opts{$sopt}}() if ref($opts{$sopt}) eq "CODE";
		} elsif ($opts{":$sopt"}) {
			&$failsub(':', $opt),return(0) unless @ARGV;
			my $val = shift @ARGV;
			${$opts{":$sopt"}} = $val if ref($opts{":$sopt"} eq "SCALAR");
			&{$opts{":$sopt"}}($val) if ref($opts{":$sopt"} eq "CODE");
		} else {
			&$failsub('?', $opt);
			return 0;
		}
	}
	if (@ARGV && $ARGV[0] eq "--") {
		shift @ARGV;
		return 1;
	}
	if (@ARGV && $ARGV[0] =~ /^-./) {
		&$failsub('?', $ARGV[0]);
		return 0;
	}
	return 1;
}

sub recreate_file {
	open F, '>', $_[0] or die "failed to create $_[0]: $!\n";
	close F;
}

sub is_yes {
	my $b = shift;
	my $strict = shift;
	defined ($b) or $b = "";
	return lc($b) eq "yes" || (!$strict && lc($b) eq "y");
}

sub is_no {
	my $b = shift;
	my $strict = shift;
	defined ($b) or $b = "";
	return lc($b) eq "no" || (!$strict && lc($b) eq "n");
}

sub is_yesno {
	return is_yes(@_) || is_no(@_);
}

my %boolvals;
BEGIN {
	%boolvals = (
		true  => 1,
		on    => 1,
		yes   => 1,
		y     => 1,
		1     => 1,

		false => 0,
		off   => 0,
		no    => 0,
		n     => 0,
		0     => 0,
	);
}

sub valid_bool {
	exists($boolvals{lc($_[0])});
}

sub clean_bool {
	my $b = shift || 0;
	return $boolvals{lc($b)} || 0;
}

sub _prompt_rl {
	my ($norl, $prompt, $default, $promptsfx) = @_;
	! -t STDIN and $norl = 1;
	defined($promptsfx) or $promptsfx = ': ';
	defined($prompt) or $prompt = '';
	my $ds = '';
	$ds = " [" . $default . "]" if defined($default);
	if ($have_rl && !$norl) {
		my $rl = Term::ReadLine->new(basename($0), \*STDIN, \*STDOUT);
		$rl->ornaments(0);
		$_ = $rl->readline($prompt . $ds . $promptsfx);
		$rl->addhistory($_) if defined($_) && $_ =~ /\S/;
	} else {
		print $prompt, $ds, $promptsfx;
		$_ = <STDIN>;
	}
	return undef unless defined($_);
	chomp;
	return $_ eq '' && defined($default) ? $default : $_;
}

sub prompt {
	return _prompt_rl(undef, @_);
}

sub ynprompt {
	my $result;
	my @args = @_;
	$args[2] = "? " unless defined$args[2];
	{
		$result = prompt(@args);
		return undef unless defined($result);
		redo unless is_yesno($result);
	}
	return clean_bool($result);
}

sub _prompt_rl_or_die {
	my $result = _prompt_rl(@_);
	unless (defined($result)) {
		my $nm = $diename;
		defined($nm) or $nm = "";
		$nm eq "" or $nm .= " ";
		die "\n${nm}aborted\n";
	}
	$result;
}

sub prompt_or_die {
	return _prompt_rl_or_die(undef, @_);
}

sub ynprompt_or_die {
	my $result = ynprompt(@_);
	unless (defined($result)) {
		my $nm = $diename;
		defined($nm) or $nm = "";
		$nm eq "" or $nm .= " ";
		die "\n${nm}aborted\n";
	}
	$result;
}

sub prompt_noecho {
	my $ne = Girocco::CLIUtil::NoEcho->new;
	_prompt_rl(1, @_);
}

sub prompt_noecho_or_die {
	my $ne = Girocco::CLIUtil::NoEcho->new;
	_prompt_rl_or_die(1, @_);
}

sub prompt_noecho_nl {
	my $result = prompt_noecho(@_);
	print "\n";
	$result;
}

sub prompt_noecho_nl_or_die {
	my $result = prompt_noecho_or_die(@_);
	print "\n";
	$result;
}

sub yes_to_continue {
	return !!ynprompt(($_[0]||"Continue (enter \"yes\" to continue)"), "no");
}

sub yes_to_continue_or_die {
	unless (ynprompt_or_die(($_[0]||"Continue (enter \"yes\" to continue)"), "no")) {
		my $nm = $diename;
		defined($nm) or $nm = "";
		$nm .= " " if $nm ne "";
		die "${nm}aborted\n";
	}
	return 1;
}

my @user_list;
my $user_list_loaded;
my @full_user_list;
my $full_user_list_loaded;

# If single argument is true, return ALL passwd entries not just "...@..." ones
sub _get_all_users_internal {
	my $full = shift || 0;
	if ($full) {
		return @full_user_list if $full_user_list_loaded;
	} else {
		return @user_list if $user_list_loaded;
	}
	my $passwd_file = jailed_file("/etc/passwd");
	open my $fd, '<', $passwd_file or die "could not open \"$passwd_file\": $!\n";
	my $line = 0;
	my @users;
	if ($full) {
		@users = map {/^([^:\s#][^:\s]*):[^:]*:(-?\d+):(-?\d+)(:|$)/
			 ? [++$line,split(':',$_,-1)] : ()} <$fd>;
	} else {
		@users = map {/^([^:_\s#][^:\s#]*):[^:]+:(\d{5,}):(\d+):([^:,][^:]*)/
			 ? [++$line,$1,$2,$3,split(',',$4)] : ()} <$fd>;
	}
	close $fd;
	if ($full) {
		$$_[5] = [split(',', $$_[5])] foreach @users;
		@full_user_list = @users;
		$full_user_list_loaded = 1;
	} else {
		@users = grep({$$_[4] =~ /\@/} @users);
		@user_list = @users;
		$user_list_loaded = 1;
	}
	@users;
}

# Return array of arrayref where each arrayref has:
#   [0] = ordering ordinal from $chroot/etc/passwd
#   [1] = user name
#   [2] = user id number
#   [3] = user group number
#   [4] = user email
#   [5] = user UUID (text as 8x-4x-4x-4x-12x) or undef if none
#   [6] = user creation date as YYYYMMDD_HHMMSS (UTC) or undef if none
sub get_all_users { return _get_all_users_internal; }

# Return array of arrayref where each arrayref has:
#   [0] = ordering ordinal from $chroot/etc/passwd
#   [1] = user name
#   [2] = user password field (usually "x")
#   [3] = user id number
#   [4] = user group number
#   [5] = [info fields] from passwd line (usually email,uuid,creation)
#   [6] = home dir field
#   [7] = shell field
#   [...] possibly more, but [7] is usually max
sub get_full_users { return _get_all_users_internal(1); }

# Result of Girocco::User->load or fatal die if that fails
# Returns undef if passed undef or ""
sub get_user {
	my $username = shift;
	defined($username) && $username ne "" or return undef;
	Girocco::User::does_exist($username, 1) or die "No such user: \"$username\"\n";
	my $user;
	eval {
		$user = Girocco::User->load($username);
		1;
	} && $user->{uid} or die "Could not load user \"$username\"\n";
	$user;
}

my @project_list;
my $project_list_loaded;

# Return array of arrayref where each arrayref has:
#   [0] = ordering ordinal from $chroot/etc/group
#   [1] = group name
#   [2] = group password hash
#   [3] = group id number
#   [4] = owner from gitproj.list
#   [5] = list of comma-separated push user names (can be "") or ":" if mirror
sub get_all_projects {
	return @project_list if $project_list_loaded;
	my $fd;
	my $projlist_file = $Girocco::Config::projlist_cache_dir."/gitproj.list";
	open $fd, '<', $projlist_file or die "could not open \"$projlist_file\": $!\n";
	my $chomper = sub {chomp(my $x = shift); $x;};
	my %owners = map {(split(/\s+/, &$chomper($_), 3))[0,2]} <$fd>;
	close $fd;
	my $group_file = jailed_file("/etc/group");
	open $fd, '<', $group_file or die "could not open \"$group_file\": $!\n";
	my $line = 0;
	my $trimu = sub {
		my $list = shift;
		return ':' if $list =~ /^:/;
		$list =~ s/:.*$//;
		$list;
	};
	my $defu = sub {defined($_[0])?$_[0]:""};
	my @projects = map {/^([^:_\s#][^:\s#]*):([^:]*):(\d{5,}):(.*)$/
			    ? [++$line,$1,$2,$3,&$defu($owners{$1}),&$trimu($4)] : ()} <$fd>;
	close $fd;
	@project_list = @projects;
	$project_list_loaded = 1;
	@project_list;
}

# Result of Girocco::Project->load or fatal die if that fails
# Returns undef if passed undef or ""
sub get_project {
	my $projname = shift;
	$projname =~ s/\.git$//i if defined($projname);
	defined($projname) && $projname ne "" or return undef;
	Girocco::Project::does_exist($projname, 1) or die "No such project: \"$projname\"\n";
	my $project;
	eval {
		$project = Girocco::Project->load($projname);
		1;
	} && $project->{loaded} or die "Could not load project \"$projname\"\n";
	$project;
}

# return true if $enc_passwd is a match for $plain_passwd
sub check_passwd_match {
	my ($enc_passwd, $plain_passwd) = @_;
	defined($enc_passwd) or $enc_passwd = '';
	defined($plain_passwd) or $plain_passwd = '';
	# $enc_passwd may be crypt or crypt_sha1
	if ($enc_passwd =~ m(^\$sha1\$(\d+)\$([./0-9A-Za-z]{1,64})\$[./0-9A-Za-z]{28}$)) {
		# It's using sha1-crypt
		return $enc_passwd eq crypt_sha1($plain_passwd, $2, -(0+$1));
	} else {
		# It's using crypt
		return $enc_passwd eq crypt($plain_passwd, $enc_passwd);
	}
};

sub _which {
	my $cmd = shift;
	foreach (File::Spec->path()) {
		my $p = File::Spec->catfile($_, $cmd);
		return $p if -x $p && -f _;
	}
	return undef;
}

# apply maximum nice and ionice
my $ionice;
sub nice_me {
	my $niceval = shift;
	if (defined($niceval) && $niceval =~ /^\d+$/ && 0 + $niceval >= 1) {
		my $oldval = POSIX::nice(0);
		POSIX::nice($niceval - $oldval) if $oldval && $niceval > $oldval;
	} else {
		POSIX::nice(20);
	}
	defined($ionice) or $ionice = _which("ionice");
	defined($ionice) or $ionice = "";
	if ($ionice ne "") {
		my $devnullfd = POSIX::open(File::Spec->devnull, O_RDWR);
		defined($devnullfd) && $devnullfd >= 0 or die "cannot open /dev/null: $!";
		my ($dupin, $dupout, $duperr);
		open $dupin, '<&0' or die "cannot dup STDIN_FILENO: $!";
		open $dupout, '>&1' or die "cannot dup STDOUT_FILENO: $!";
		open $duperr, '>&2' or die "cannot dup STDERR_FILENO: $!";
		POSIX::dup2($devnullfd, 0) or die "cannot dup2 STDIN_FILENO: $!";
		POSIX::dup2($devnullfd, 1) or die "cannot dup2 STDOUT_FILENO: $!";
		POSIX::dup2($devnullfd, 2) or POSIX::dup2(fileno($duperr), 2), die "cannot dup2 STDERR_FILENO: $!";
		POSIX::close($devnullfd);
		system $ionice, "-c", "3", "-p", $$;
		POSIX::dup2(fileno($duperr), 2) or die "cannot dup2 STDERR_FILENO: $!";
		POSIX::dup2(fileno($dupout), 1) or die "cannot dup2 STDOUT_FILENO: $!";
		POSIX::dup2(fileno($dupin), 0) or die "cannot dup2 STDIN_FILENO: $!";
		close $duperr;
		close $dupout;
		close $dupin;
	}
}

1;
