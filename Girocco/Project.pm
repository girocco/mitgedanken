package Girocco::Project;

use strict;
use warnings;

BEGIN {
	use Girocco::CGI;
	use Girocco::User;
	use Girocco::Util;
	use Girocco::HashUtil;
	use Girocco::ProjPerm;
	use Girocco::Config;
	use base ('Girocco::ProjPerm::'.$Girocco::Config::permission_control); # mwahaha
}

BEGIN {
	eval {
		require Digest::SHA;
		Digest::SHA->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA1;
		Digest::SHA1->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA::PurePerl;
		Digest::SHA::PurePerl->import(
			qw(sha1_hex)
	);1} ||
	die "One of Digest::SHA or Digest::SHA1 or Digest::SHA::PurePerl "
	. "must be available\n";
}

our $metadata_fields = {
	cleanmirror => ['Mirror refs', 'cleanmirror', 'placeholder'],
	homepage => ['Homepage URL', 'hp', 'text'],
	shortdesc => ['Short description', 'desc', 'text'],
	README => ['<span style="display:inline-block;vertical-align:top">'.
		'README (HTML, &lt; 8 KiB)<br />leave blank for automatic</span>',
		'README', 'textarea', 'Enter only &#x201c;<!-- comments -->&#x201d; '.
		'to completely suppress any README'],
	notifymail => ['Commit notify &#x2013; mail to', 'notifymail', 'text',
		'comma separated address list'],
	reverseorder => ['Show oldest first', 'reverseorder', 'checkbox',
		'show new revisions in oldest to newest order (instead of the default newest to oldest older)'.
		' in &#x201c;Commit notify&#x201d; email when showing new revisions'],
	summaryonly => ['Summaries only', 'summaryonly', 'checkbox',
		'suppress patch/diff output in &#x201c;Commit notify&#x201d; email when showing new revisions'],
	notifytag => ['Tag notify &#x2013; mail to', 'notifytag', 'text',
		'comma separated address list &#x2013; if not empty, tag '.
		'notifications are sent here INSTEAD of to '.
		'&#x201c;Commit notify &#x2013; mail to&#x201d; address(es)'],
	notifyjson => ['Commit notify &#x2013; '.
		'<a title="'.html_esc('single field name is &#x201c;payload&#x201d;', 1).'" href="'.
		'https://developer.github.com/v3/activity/events/types/#pushevent'.
		'">POST JSON</a> at', 'notifyjson', 'text'],
	notifycia =>  ['Commit notify &#x2013; <a href="http://cia.vc/doc/">CIA project</a> name',
		'notifycia', 'text', 'CIA is defunct &#x2013; this value is ignored'],
};

sub _mkdir_forkees {
	my $self = shift;
	my @pelems = split('/', $self->{name});
	pop @pelems; # do not create dir for the project itself
	my $path = $self->{base_path};
	foreach my $pelem (@pelems) {
		$path .= "/$pelem";
		(-d "$path") or mkdir $path or die "mkdir $path: $!";
		chmod 02775, $path; # ok if fails (dir may already exist and be owned by someone else)
	}
}

# With a leading ':' get from project local config replacing ':' with 'gitweb.'
# With a leading '%' get from project local config after removing '%'
# With a leading '!' get boolean from project local config after removing '!' (prefixed with 'gitweb.' if no '.')
# With a leading [:%!] a trailing ':defval' may be added to select the default value to use if unset
# Otherwise it's a project file name to be loaded
# %propmapro entries are loaded but never written
# %propmapromirror entries are loaded only for mirrors but never written

our %propmap = (
	url => ':baseurl',
	email => ':owner',
	desc => 'description',
	README => 'README.html',
	hp => ':homepage',
	notifymail => '%hooks.mailinglist',
	notifytag => '%hooks.announcelist',
	notifyjson => '%hooks.jsonurl',
	notifycia => '%hooks.cianame',
	cleanmirror => '!girocco.cleanmirror',
	statusupdates => '!statusupdates:1',
	reverseorder => '!hooks.reverseorder',
	summaryonly => '!hooks.summaryonly',
);

our %propmapro = (
	lastchange => ':lastchange',
	lastactivity => 'info/lastactivity',
	lastgc => ':lastgc',
	lastreceive => ':lastreceive',
	lastparentgc => ':lastparentgc',
	lastrefresh => ':lastrefresh',
	creationtime => '%girocco.creationtime',
	reposizek => '%girocco.reposizek',
	notifyhook => '%girocco.notifyhook:undef',
	origurl => ':baseurl',
);

our %propmapromirror = (
	bangcount => '%girocco.bang.count',
	bangfirstfail => '%girocco.bang.firstfail',
	bangmessagesent => '!girocco.bang.messagesent',
	showpush => '!showpush',
);

# Projects with any of these names will be disallowed to avoid possible
# collisions with cgi script paths or chroot paths
# NOTE: names are checked after using lc on them, so all entries MUST be lowercase
our %reservedprojectnames = (
	admin => 1,	# /admin/ links
	alternates => 1, # .git/objects/info/alternates
	b => 1,		# /b/ -> bundle.cgi
	blog => 1,	# /blog/ links
	c => 1,		# /c/ -> cgit
	'git-receive-pack' => 1, # smart HTTP
	'git-upload-archive' => 1, # smart HTTP
	'git-upload-pack' => 1, # smart HTTP
	h => 1,		# /h/ -> html.cgi
	head => 1,	# .git/HEAD
	'http-alternates' => 1, # .git/objects/info/http-alternates
	info => 1,	# .git/info
	objects => 1,	# .git/objects
	packs => 1,	# .git/objects/info/packs
	r => 1,		# /r/ -> git http
	refs => 1,	# .git/refs
	w => 1,		# /w/ -> gitweb
	wiki => 1,	# /wiki/ links
	srv => 1,	# /srv/git/ -> chroot ssh git repositories
);

sub _update_index {
	my $self = shift;
	system("$Girocco::Config::basedir/gitweb/genindex.sh", $self->{name});
}

sub _readlocalconfigfile {
	my $self = shift;
	my $undefonerr = shift || 0;
	delete $self->{configfilehash};
	my $confighash = read_config_file_hash($self->{path} . "/config");
	my $result = 1;
	defined($confighash) || $undefonerr or $result = 0, $confighash = {};
	return undef unless defined($confighash);
	$self->{configfilehash} = $confighash;
	return $result;
}

# @_[0]: argument to convert to boolean result (0 or 1)
# @_[1]: value to use if argument is undef (default is 0)
# Returns 0 or 1
sub _boolval {
	my ($val, $def) = @_;
	defined($def) or $def = 0;
	defined($val) or $val = $def;
	$val =~ s/\s+//gs;
	$val = lc($val);
	return 0 if $val eq '' || $val eq 'false' || $val eq 'off' || $val eq 'no' || $val =~ /^[-+]?0+$/;
	return 1;
}

sub _property_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/'.$name;
}

sub _property_fget {
	my $self = shift;
	my ($name, $nodef) = @_;
	my $pname = $propmap{$name};
	$pname = $propmapro{$name} unless $pname;
	$pname = $propmapromirror{$name} unless $pname;
	$pname or die "unknown property: $name";
	if ($pname =~ /^([:%!])([^:]+)(:.*)?$/) {
		my ($where, $pname, $defval) = ($1, lc($2), substr(($3||":"),1));
		$defval = undef if $defval eq "undef";
		$defval = '' if $nodef;
		$self->_readlocalconfigfile
			unless ref($self->{configfilehash}) eq 'HASH';
		$pname = "gitweb." . $pname if $where eq ':' or $where eq '!' && $pname !~ /[.]/;
		my $val = $self->{configfilehash}->{$pname};
		defined($val) or $val = $defval;
		chomp $val if defined($val);
		$val = _boolval($val, $defval) if $where eq '!';
		return $nodef && !exists($self->{configfilehash}->{$pname}) ? undef : $val;
	}

	open my $p, '<', $self->_property_path($pname) or return undef;
	my @value = <$p>;
	close $p;
	my $value = join('', @value); chomp $value;
	$value;
}

sub _prop_is_same {
	my $self = shift;
	my ($name, $value) = @_;
	my $existing = $self->_property_fget($name, 1);
	defined($value) or $value = '';
	return defined($existing) && $existing eq $value;
}

sub _property_fput {
	my $self = shift;
	my ($name, $value, $nosetsame) = @_;
	my $pname = $propmap{$name};
	$pname or die "unknown property: $name";
	my $defval = '';
	($pname, $defval) = ($1, substr(($2||":"),1)) if $pname =~ /^([:%!][^:]+)(:.*)?$/;
	defined($value) or $value = $defval;
	if ($pname =~ s/^://) {
		return if $nosetsame && $self->_prop_is_same($name, $value);
		system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', "gitweb.$pname", $value);
		return;
	} elsif ($pname =~ s/^%//) {
		return if $nosetsame && $self->_prop_is_same($name, $value);
		system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', $pname, $value);
		return;
	} elsif ($pname =~ s/^!//) {
		$pname = "gitweb." . $pname unless $pname =~ /[.]/;
		$value = _boolval($value, $defval);
		return if $nosetsame && $self->_prop_is_same($name, $value);
		system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', '--bool', $pname, $value);
		return;
	}

	my $P = lock_file($self->_property_path($pname));
	chomp $value;
	$value ne '' and print $P "$value\n";
	close $P;
	unlock_file($self->_property_path($pname));
}

sub _cleanup_readme {
	my $self = shift;
	defined($self->{README}) or $self->{README} = '';
	$self->{README} =~ s/\r\n?/\n/gs;
	$self->{README} =~ s/^\s+//s;
	$self->{README} =~ s/\s+$//s;
	$self->{README} eq '' or $self->{README} .= "\n";
}

sub _lint_readme {
	my $self = shift;
	my $htmlfrag = shift;
	defined($htmlfrag) or $htmlfrag = 1;
	return 0 unless defined($self->{README}) && $self->{README} ne '';
	my $test = '<html xmlns="http://www.w3.org/1999/xhtml"><body><div>';
	$test .= $self->{README};
	$test .= '</div></body></html>';
	my ($code, $errors) = capture_command(2, $test, 'xmllint', '--nonet',
		'--noout', '--nowarning', '-');
	return 0 unless $code;
	my $cnt = 0;
	my @errs = ();
	for my $line (split(/\n+/, $errors)) {
		$line = html_esc($line) if $htmlfrag;
		$line =~ s/ /\&#160;/gs if $htmlfrag;
		++$cnt, $line = 'README'.$1 if $line =~ /^-(:\d+:.*)$/;
		if ($htmlfrag) {
			push @errs, '<tt>' . $line . '</tt>';
		} else {
			push @errs, $line . "\n";
		}
	}
	if ($htmlfrag) {
		return ($cnt, join("<br />\n", @errs));
	} else {
		return ($cnt, join("", @errs));
	}
}

sub _properties_load {
	my $self = shift;
	my $setprop = sub {
		my $propval = $self->_property_fget($_);
		defined($propval) and $self->{$_} = $propval;
	};
	&$setprop foreach keys %propmap;
	&$setprop foreach keys %propmapro;
	do {&$setprop foreach keys %propmapromirror} if $self->{mirror};
	$self->_readlocalconfigfile
		unless ref($self->{configfilehash}) eq 'HASH';
	delete $self->{auth};
	my $val = $self->{configfilehash}->{'gitweb.repoauth'};
	defined($val) or $val = '';
	chomp $val;
	if ($val =~ /^# ([A-Z]+)AUTH ([0-9a-f]+) (\d+)/) {
		my $expire = $3;
		if (time < $expire) {
			$self->{authtype} = $1;
			$self->{auth} = $2;
		}
	}
	if ($Girocco::Config::autogchack && ($self->{mirror} || $Girocco::Config::autogchack ne "mirror")) {
		if (defined($self->{configfilehash}->{'girocco.autogchack'})) {
			$self->{autogchack} = _boolval($self->{configfilehash}->{'girocco.autogchack'});
		}
	}
	$self->_cleanup_readme;
	delete $self->{configfilehash};
}

sub _set_bangagain {
	my $self = shift;
	my $fd;
	if ($self->{mirror} && defined($self->{origurl}) && $self->{url} &&
	    $self->{origurl} ne $self->{url} && -e $self->_banged_path) {
		if (open($fd, '>', $self->_bangagain_path)) {
			close $fd;
			chmod(0664, $self->_bangagain_path);
		}
	}
}

sub _properties_save {
	my $self = shift;
	delete $self->{configfilehash};
	foreach my $prop (keys %propmap) {
		$self->_property_fput($prop, $self->{$prop}, 1);
	}
	$self->_set_bangagain;
}

sub _nofetch_path {
	my $self = shift;
	$self->_property_path('.nofetch');
}

sub _nofetch {
	my $self = shift;
	my ($nofetch) = @_;
	my $nf = $self->_nofetch_path;
	if ($nofetch) {
		open my $x, '>', $nf or die "nofetch failed: $!";
		close $x;
	} else {
		! -e $nf or unlink $nf or die "yesfetch failed: $!";
	}
}

sub _banged_path {
	my $self = shift;
	$self->_property_path('.banged');
}

sub _bangagain_path {
	my $self = shift;
	$self->_property_path('.bangagain');
}

sub _gcp_path {
	my $self = shift;
	$self->_property_path('.gc_in_progress');
}

sub _clonelog_path {
	my $self = shift;
	$self->_property_path('.clonelog');
}

sub _clonefail_path {
	my $self = shift;
	$self->_property_path('.clone_failed');
}

sub _clonep_path {
	my $self = shift;
	$self->_property_path('.clone_in_progress');
}

sub _clonep {
	my $self = shift;
	my ($nofetch) = @_;
	my $np = $self->_clonep_path;
	if ($nofetch) {
		open my $x, '>', $np or die "clonep failed: $!";
		close $x;
	} else {
		unlink $np or die "clonef failed: $!";
	}
}
sub _alternates_setup {
	use POSIX qw(:fcntl_h);
	my $self = shift;
	return unless $self->{name} =~ m#/#;
	my $forkee_name = get_forkee_name($self->{name});
	my $forkee_path = get_forkee_path($self->{name});
	return unless -d $forkee_path;
	mkdir $self->{path}.'/refs'; chmod 02775, $self->{path}.'/refs';
	mkdir $self->{path}.'/objects'; chmod 02775, $self->{path}.'/objects';
	mkdir $self->{path}.'/objects/info'; chmod 02775, $self->{path}.'/objects/info';
	mkdir $self->{path}.'/objects/pack'; chmod 02775, $self->{path}.'/objects/pack';

	# If somehow either our objects/pack or the prospective alternate's pack
	# directory does not exist decline to set up any alternates
	my $altpath = "$forkee_path/objects";
	-d $self->{path}.'/objects/pack' && -d $altpath.'/pack' or return;

	# If our objects/pack and the prospective alternate's pack directory
	# do not share the same device then decline to set up any alternates
	my ($selfdev) = stat($self->{path}.'/objects/pack');
	my ($altdev) = stat($altpath.'/pack');
	defined($selfdev) && defined($altdev) && $selfdev ne "" && $altdev ne "" && $selfdev == $altdev or return;

	# We set up both alternates and http_alternates since we cannot use
	# relative path in alternates - that doesn't work recursively.

	my $filename = $self->{path}.'/objects/info/alternates';
	open my $x, '>', $filename or die "alternates failed: $!";
	print $x "$altpath\n";
	close $x;
	chmod 0664, $filename or warn "cannot chmod $filename: $!";

	if ($Girocco::Config::httppullurl) {
		$filename = $self->{path}.'/objects/info/http-alternates';
		open my $x, '>', $filename or die "http-alternates failed: $!";
		my $upfork = $forkee_name;
		do { print $x "$Girocco::Config::httppullurl/$upfork.git/objects\n"; } while ($upfork =~ s#/?.+?$## and $upfork); #
		close $x;
		chmod 0664, $filename or warn "cannot chmod $filename: $!";
	}

	# copy lastactivity from the parent project
	if (open $x, '<', $forkee_path.'/info/lastactivity') {{
		my $activity = <$x>;
		close $x;
		last unless $activity;
		open $x, '>', $self->{path}.'/info/lastactivity' or last;
		print $x $activity;
		close $x;
		chomp $activity;
		$self->{'lastactivity'} = $activity;
	}}

	# copy refs from parent project
	my $dupout;
	open $dupout, '>&1' or
		die "could not dup STDOUT_FILENO: $!";
	my $packedrefsfd = POSIX::open("$self->{path}/packed-refs", O_WRONLY|O_TRUNC|O_CREAT, 0664);
	defined($packedrefsfd) && $packedrefsfd >= 0 or die "could not open fork's packed-refs file for writing: $!";
	POSIX::dup2($packedrefsfd, 1) or
		die "could not dup2 STDOUT_FILENO: $!";
	POSIX::close($packedrefsfd);
	my $result = system($Girocco::Config::git_bin, "--git-dir=$forkee_path", 'for-each-ref', '--format=%(objectname) %(refname)');
	my $resultstr = $!;
	POSIX::dup2(fileno($dupout), 1);
	close($dupout);
	$result == 0 or die "could not create fork's packed-refs file data: $resultstr";
	unlink("$self->{path}/.delaygc") if -s "$self->{path}/packed-refs";

	# initialize HEAD
	my $HEAD = get_git("--git-dir=$forkee_path", 'symbolic-ref', 'HEAD');
	defined($HEAD) && $HEAD =~ m,^refs/heads/., or $HEAD = 'refs/heads/master';
	chomp $HEAD;
	system($Girocco::Config::git_bin, "--git-dir=$self->{path}", 'symbolic-ref', 'HEAD', $HEAD);
	chmod 0664, "$self->{path}/packed-refs", "$self->{path}/HEAD";
}

sub _set_changed {
	my $self = shift;
	my $fd;
	open $fd, '>', "$self->{path}/htmlcache/changed" and close $fd;
}

sub _set_forkchange {
	my $self = shift;
	my $changedtoo = shift;
	return unless $self->{name} =~ m#/#;
	my $forkee_path = get_forkee_path($self->{name});
	return unless -d $forkee_path;
	# mark forkee as changed
	my $fd;
	open $fd, '>', $forkee_path.'/htmlcache/changed' and close $fd if $changedtoo;
	open $fd, '>', $forkee_path.'/htmlcache/forkchange' and close $fd;
	return if -e $forkee_path.'/htmlcache/summary.forkchange';
	open $fd, '>', $forkee_path.'/htmlcache/summary.forkchange' and close $fd;
}

sub _ctags_setup {
	my $self = shift;
	my $perms = $Girocco::Config::permission_control eq 'Hooks' ? 02777 : 02775;
	mkdir $self->{path}.'/ctags'; chmod $perms, $self->{path}.'/ctags';
}

sub _group_add {
	my $self = shift;
	my ($xtra) = @_;
	$xtra .= join(',', @{$self->{users}});
	my $crypt = $self->{crypt};
	defined($crypt) or $crypt = 'unknown';
	filedb_atomic_append(jailed_file('/etc/group'),
		join(':', $self->{name}, $crypt, '\i', $xtra));
}

sub _group_update {
	my $self = shift;
	my $xtra = join(',', @{$self->{users}});
	filedb_atomic_edit(jailed_file('/etc/group'),
		sub {
			$_ = $_[0];
			chomp;
			if ($self->{name} eq (split /:/)[0]) {
				# preserve readonly flag
				s/::([^:]*)$/:$1/ and $xtra = ":$xtra";
				return join(':', $self->{name}, $self->{crypt}, $self->{gid}, $xtra)."\n";
			} else {
				return "$_\n";
			}
		}
	);
}

sub _group_remove {
	my $self = shift;
	filedb_atomic_edit(jailed_file('/etc/group'),
		sub {
			$self->{name} ne (split /:/)[0] and return $_;
		}
	);
}

sub _hook_path {
	my $self = shift;
	my ($name) = @_;
	$self->{path}.'/hooks/'.$name;
}

sub _hook_install {
	my $self = shift;
	my ($name) = @_;
	my $hooksdir = $self->{path}.'/hooks';
	my $oldmask = umask();
	umask($oldmask & ~0070);
	-d $hooksdir or mkdir $hooksdir or
	  die "hooks directory does not exist and unable to create it for project " . $self->{name} . ": $!";
	umask($oldmask);
	my $globalhooks = $Girocco::Config::reporoot . "/_global/hooks";
	-f "$globalhooks/$name" && -r _ or die "cannot find hook $name: $!";
	! -e $self->_hook_path($name) || unlink $self->_hook_path($name) && ! -e $self->_hook_path($name) or
	  die "hooks directory contains unremovable pre-existing hook $name: $!";
	symlink("$globalhooks/$name", $self->_hook_path($name)) or
	  die "cannot create hook $name symlink: $!";
}

sub _hooks_install {
	my $self = shift;
	foreach my $hook ('pre-auto-gc', 'pre-receive', 'post-commit', 'post-receive', 'update') {
		$self->_hook_install($hook);
	}
}

# private constructor, do not use
sub _new {
	my $class = shift;
	my ($name, $base_path, $path, $orphan, $optp) = @_;
	does_exist($name,1) || valid_name($name, $orphan, $optp) or die "refusing to create project with invalid name ($name)!";
	$path ||= "$base_path/$name.git";
	my $proj = { name => $name, base_path => $base_path, path => $path };

	bless $proj, $class;
}

# public constructor #0
# creates a virtual project not connected to disk image
# you can conjure() it later to disk
sub ghost {
	my $class = shift;
	my ($name, $mirror, $orphan, $optp) = @_;
	my $self = $class->_new($name, $Girocco::Config::reporoot, undef, $orphan, $optp);
	$self->{users} = [];
	$self->{mirror} = $mirror;
	$self->{email} = $self->{orig_email} = '';
	$self;
}

# public constructor #1
sub load {
	my $class = shift;
	my $name = shift || '';

	open my $fd, '<', jailed_file("/etc/group") or die "project load failed: $!";
	my $r = qr/^\Q$name\E:/;
	foreach (grep /$r/, <$fd>) {
		chomp;

		my $self = $class->_new($name, $Girocco::Config::reporoot);
		(-d $self->{path} && $self->_readlocalconfigfile(1))
			or die "invalid path (".$self->{path}.") for project ".$self->{name};

		my $ulist;
		(undef, $self->{crypt}, $self->{gid}, $ulist) = split /:/;
		$ulist ||= '';
		$self->{users} = [split /,/, $ulist];
		$self->{HEAD} = $self->get_HEAD;
		$self->{orig_HEAD} = $self->{HEAD};
		$self->{orig_users} = [@{$self->{users}}];
		$self->{mirror} = ! -e $self->_nofetch_path;
		$self->{banged} = -e $self->_banged_path if $self->{mirror};
		$self->{gc_in_progress} = -e $self->_gcp_path;
		$self->{clone_in_progress} = -e $self->_clonep_path;
		$self->{clone_logged} = -e $self->_clonelog_path;
		$self->{clone_failed} = -e $self->_clonefail_path;
		$self->{ccrypt} = $self->{crypt};

		$self->_properties_load;
		$self->{orig_email} = $self->{email};
		$self->{loaded} = 1; # indicates self was loaded from etc/group file
		close $fd;
		return $self;
	}
	close $fd;
	undef;
}

# $proj may not be in sane state if this returns false!
# fields listed in %$metadata_fields that are NOT also
# in @Girocco::Config::project_fields are totally ignored!
sub cgi_fill {
	my $self = shift;
	my ($gcgi) = @_;
	my $cgi = $gcgi->cgi;
	my %allowedfields = map({$_ => 1} @Girocco::Config::project_fields);
	my $field_enabled = sub {
		!exists($metadata_fields->{$_[0]}) || exists($allowedfields{$_[0]})};

	my $pwd = $cgi->param('pwd');
	my $pwd2 = $cgi->param('pwd2');
	# in case passwords are disabled
	defined($pwd) or $pwd = ''; defined($pwd2) or $pwd2 = '';
	if ($Girocco::Config::project_passwords and not $self->{crypt} and $pwd eq '' and $pwd2 eq '') {
		$gcgi->err("Empty passwords are not permitted.");
	}
	if ($pwd ne '' or not $self->{crypt}) {
		$self->{crypt} = scrypt_sha1($pwd);
	}
	if (($pwd ne '' || $pwd2 ne '') and $pwd ne $pwd2) {
	 	$gcgi->err("Our high-paid security consultants have determined that the admin passwords you have entered do not match each other.");
	}

	$self->{cpwd} = $cgi->param('cpwd');

	my ($forkee,$project) = ($self->{name} =~ m#^(.*/)?([^/]+)$#);
	my $newtype = $forkee ? 'fork' : 'project';
	length($project) <= 64
		or $gcgi->err("The $newtype name is longer than 64 characters. Do you really need that much?");

	if ($Girocco::Config::project_owners eq 'email') {
		$self->{email} = $gcgi->wparam('email');
		valid_email($self->{email})
			or $gcgi->err("Your email sure looks weird...?");
		length($self->{email}) <= 96
			or $gcgi->err("Your email is longer than 96 characters. Do you really need that much?");
	}

	# No setting the url unless we're either new or an existing mirror!
	unless ($self->{loaded} && !$self->{mirror}) {
		$self->{url} = $gcgi->wparam('url') ;
		if ($field_enabled->('cleanmirror')) {
			$self->{cleanmirror} = $gcgi->wparam('cleanmirror') || 0;
		}
	}
	# Always validate the url if we're an existing mirror
	if ((defined($self->{url}) && $self->{url} ne '') || ($self->{loaded} && $self->{mirror})) {{
		# Always allow the url to be left unchanged without validation when editing
		last if $self->{loaded} && defined($self->{origurl}) && defined($self->{url}) && $self->{origurl} eq $self->{url};
		valid_repo_url($self->{url})
			or $gcgi->err("Invalid URL. Note that only HTTP and Git protocols are supported. If the URL contains funny characters, contact me.");
		if ($Girocco::Config::restrict_mirror_hosts) {
			my $mh = extract_url_hostname($self->{url});
			is_dns_hostname($mh)
				or $gcgi->err("Invalid URL. Note that only DNS names are allowed, not IP addresses.");
			!is_our_hostname($mh)
				or $gcgi->err("Invalid URL. Mirrors from this host are not allowed, please create a fork instead.");
		}}
	}

	if ($field_enabled->('desc')) {
		$self->{desc} = to_utf8($gcgi->wparam('desc'), 1);
		length($self->{desc}) <= 1024
			or $gcgi->err("<b>Short</b> description length &gt; 1kb!");
	}

	if ($field_enabled->('README')) {
		$self->{README} = to_utf8($gcgi->wparam('README'), 1);
		$self->_cleanup_readme;
		length($self->{README}) <= 8192
			or $gcgi->err("README length &gt; 8kb!");
		my ($cnt, $err) = (0);
		($cnt, $err) = $self->_lint_readme if $gcgi->ok && $Girocco::Config::xmllint_readme;
		$gcgi->err($err), $gcgi->{err} += $cnt-1 if $cnt;
	}

	if ($field_enabled->('hp')) {
		$self->{hp} = $gcgi->wparam('hp');
		if ($self->{hp}) {
			valid_web_url($self->{hp})
				or $gcgi->err("Invalid homepage URL. Note that only HTTP protocol is supported. If the URL contains funny characters, contact me.");
		}
	}

	# No mucking about with users unless we're a push project
	if (($self->{loaded} && !$self->{mirror}) ||
	    (!$self->{loaded} && (!defined($self->{url}) || $self->{url} eq ''))) {
		my %users = ();
		my @users = ();
		foreach my $user ($cgi->multi_param('user')) {
			if (!exists($users{$user})) {
				$users{$user} = 1;
				push(@users, $user) if Girocco::User::does_exist($user, 1);
			}
		}
		$self->{users} = \@users;
	}

	# HEAD can only be set with editproj, NOT regproj (regproj sets it automatically)
	# It may ALWAYS be set to what it was (even if there's no such refs/heads/...)
	my $newhead;
	if (defined($self->{orig_HEAD}) && $self->{orig_HEAD} ne '' &&
	    defined($newhead = $cgi->param('HEAD')) && $newhead ne '') {
		if ($newhead eq $self->{orig_HEAD} ||
		    get_git("--git-dir=$self->{path}", 'rev-parse', '--verify', '--quiet', 'refs/heads/'.$newhead)) {
			$self->{HEAD} = $newhead;
		} else {
			$gcgi->err("Invalid default branch (no such ref)");
		}
	}

	# schedule deletion of tags (will be committed by update() after auth)
	$self->{tags_to_delete} = [$cgi->multi_param('tags')];

	if ($field_enabled->('notifymail')) {
		my $newaddrs = clean_email_multi($gcgi->wparam('notifymail'));
		if ($newaddrs eq "" or (valid_email_multi($newaddrs) and length($newaddrs) <= 512)) {
			$self->{notifymail} = $newaddrs;
		} else {
			$gcgi->err("Invalid notify e-mail address. Use mail,mail to specify multiple addresses; total length must not exceed 512 characters, however.");
		}
		if ($field_enabled->('reverseorder')) {
			$self->{reverseorder} = $gcgi->wparam('reverseorder') || 0;
		}
		if ($field_enabled->('summaryonly')) {
			$self->{summaryonly} = $gcgi->wparam('summaryonly') || 0;
		}
	}

	if ($field_enabled->('notifytag')) {
		my $newaddrs = clean_email_multi($gcgi->wparam('notifytag'));
		if ($newaddrs eq "" or (valid_email_multi($newaddrs) and length($newaddrs) <= 512)) {
			$self->{notifytag} = $newaddrs;
		} else {
			$gcgi->err("Invalid notify e-mail address. Use mail,mail to specify multiple addresses; total length must not exceed 512 characters, however.");
		}
	}

	if ($field_enabled->('notifyjson')) {
		$self->{notifyjson} = $gcgi->wparam('notifyjson');
		if ($self->{notifyjson}) {
			valid_web_url($self->{notifyjson})
				or $gcgi->err("Invalid JSON notify URL. Note that only HTTP protocol is supported. If the URL contains funny characters, contact me.");
		}
	}

	if ($field_enabled->('notifycia')) {
		$self->{notifycia} = $gcgi->wparam('notifycia');
		if ($self->{notifycia}) {
			$self->{notifycia} =~ /^[a-zA-Z0-9._-]+$/
				or $gcgi->err("Overly suspicious CIA notify project name. If it's actually valid, don't contact me, CIA is defunct.");
		}
	}

	if ($cgi->param('setstatusupdates')) {
		my $val = $gcgi->wparam('statusupdates') || '0';
		$self->{statusupdates} = $val ? 1 : 0;
	}

	not $gcgi->err_check;
}

sub form_defaults {
	my $self = shift;
	(
		name => $self->{name},
		email => $self->{email},
		url => $self->{url},
		cleanmirror => $self->{cleanmirror},
		desc => html_esc($self->{desc}),
		README => html_esc($self->{README}),
		hp => $self->{hp},
		users => $self->{users},
		notifymail => html_esc($self->{notifymail}),
		reverseorder => $self->{reverseorder},
		summaryonly => $self->{summaryonly},
		notifytag => html_esc($self->{notifytag}),
		notifyjson => html_esc($self->{notifyjson}),
		notifycia => html_esc($self->{notifycia}),
	);
}

# return true if $enc_passwd is a match for $plain_passwd
my $_check_passwd_match = sub {
	my $enc_passwd = shift;
	my $plain_passwd = shift;
	defined($enc_passwd) or $enc_passwd = '';
	defined($plain_passwd) or $plain_passwd = '';
	# $enc_passwd may be crypt or crypt_sha1
	if ($enc_passwd =~ m(^\$sha1\$(\d+)\$([./0-9A-Za-z]{1,64})\$[./0-9A-Za-z]{28}$)) {
		# It's using sha1-crypt
		return $enc_passwd eq crypt_sha1($plain_passwd, $2, -(0+$1));
	} else {
		# It's using crypt
		return $enc_passwd eq crypt($plain_passwd, $enc_passwd);
	}
};

sub authenticate {
	my $self = shift;
	my ($gcgi) = @_;

	$self->{ccrypt} or die "Can't authenticate against a project with no password";
	defined($self->{cpwd}) or $self->{cpwd} = '';
	unless ($_check_passwd_match->($self->{ccrypt}, $self->{cpwd})) {
		$gcgi->err("Your admin password does not match!");
		return 0;
	}
	return 1;
}

# return true if the password from the file is empty or consists of all the same
# character.  However, if the project was NOT loaded from the group file
# (!self->{loaded}) then the password is never locked.
# This function does NOT check $Girocco::Config::project_passwords, the caller
# is responsible for doing so if desired.  Same for $self->{email}.
sub is_password_locked {
	my $self = shift;

	$self->{loaded} or return 0;
	my $testcrypt = $self->{ccrypt}; # The value from the group file
	defined($testcrypt) or $testcrypt = '';
	$testcrypt ne '' or return 1; # No password at all
	$testcrypt =~ /^(.)\1*$/ and return 1; # Bogus crypt value
	return 0; # Not locked
}

sub _setup {
	use POSIX qw(strftime);
	my $self = shift;
	my ($pushers) = @_;
	my $fd;

	defined($self->{path}) && $self->{path} ne "" or die "invalid setup call";
	$self->_mkdir_forkees unless $self->{adopt};

	my $gid;
	$self->{adopt} || mkdir($self->{path}) or die "mkdir $self->{path} failed: $!";
	-d $self->{path} or die "unable to setup nonexistent $self->{path}";
	if ($Girocco::Config::owning_group) {
		$gid = scalar(getgrnam($Girocco::Config::owning_group));
		chown(-1, $gid, $self->{path}) or die "chgrp $gid $self->{path} failed: $!";
		chmod(02775, $self->{path}) or die "chmod 02775 $self->{path} failed: $!";
	} else {
		chmod(02777, $self->{path}) or die "chmod 02777 $self->{path} failed: $!";
	}
	delete $ENV{GIT_OBJECT_DIRECTORY};
	$ENV{'GIT_TEMPLATE_DIR'} = $Girocco::Config::chroot.'/var/empty';
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'init', '--quiet', '--bare', '--shared='.$self->shared_mode()) == 0
		or die "git init $self->{path} failed: $?";
	# we don't need these two, remove them (they will normally be created empty) if they exist
	rmdir $self->{path}."/branches";
	rmdir $self->{path}."/remotes";
	-d $self->{path}."/info" or mkdir $self->{path}."/info"
		or die "info directory does not exist and unable to create it: $!";
	-d $self->{path}."/hooks" or mkdir $self->{path}."/hooks"
		or die "hooks directory does not exist and unable to create it: $!";
	# clean out any kruft that may have come in from the initial template directory
	foreach my $cleandir (qw(hooks info)) {
		if (opendir my $hooksdir, $self->{path}.'/'.$cleandir) {
			unlink map "$self->{path}/$_", grep { $_ ne '.' && $_ ne '..' } readdir $hooksdir;
			closedir $hooksdir;
		}
	}
	if ($Girocco::Config::owning_group) {
		chown(-1, $gid, $self->{path}."/hooks") or die "chgrp $gid $self->{path}/hooks failed: $!";
	}
	# hooks never world writable
	chmod 02775, $self->{path}."/hooks" or die "chmod 02775 $self->{path}/hooks failed: $!";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'core.compression', '5') == 0
		or die "setting core.compression failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'core.logAllRefUpdates', 'false') == 0
		or die "disabling core.logAllRefUpdates failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'core.ignoreCase', 'false') == 0
		or die "disabling core.ignoreCase failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'core.hooksPath',
		($Girocco::Config::localhooks ? $self->{path}."/hooks" : $Girocco::Config::reporoot . "/_global/hooks")) == 0
		or die "setting core.hooksPath failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'extensions.preciousObjects', 'true') == 0
		or die "setting extensions.preciousObjects failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'transfer.fsckObjects', 'true') == 0
		or die "enabling transfer.fsckObjects failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'transfer.unpackLimit', '1') == 0
		or die "setting transfer.unpackLimit failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.fsckObjects', 'true') == 0
		or die "enabling receive.fsckObjects failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.denyNonFastForwards', 'false') == 0
		or die "disabling receive.denyNonFastForwards failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.denyDeleteCurrent', 'warn') == 0
		or die "disabling receive.denyDeleteCurrent failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.autogc', '0') == 0
		or die "disabling receive.autogc failed: $?";
	my ($S,$M,$H,$d,$m,$y) = gmtime(time());
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'receive.updateServerInfo', 'true') == 0
		or die "enabling receive.updateServerInfo failed: $?";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'repack.writeBitmaps', 'true') == 0
		or die "enabling repack.writeBitmaps failed: $?";
	$self->{creationtime} = strftime("%Y-%m-%dT%H:%M:%SZ", $S, $M, $H, $d, $m, $y, -1, -1, -1);
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'girocco.creationtime', $self->{creationtime}) == 0
		or die "setting girocco.creationtime failed: $?";
	-d $self->{path}."/htmlcache" or mkdir $self->{path}."/htmlcache"
		or die "htmlcache directory does not exist and unable to create it: $!";
	-d $self->{path}."/bundles" or mkdir $self->{path}."/bundles"
		or die "bundles directory does not exist and unable to create it: $!";
	-d $self->{path}."/reflogs" or mkdir $self->{path}."/reflogs"
		or die "reflogs directory does not exist and unable to create it: $!";
	foreach my $file (qw(info/lastactivity .delaygc)) {
		if (open $fd, '>', $self->{path}."/".$file) {
			close $fd;
			chmod 0664, $self->{path}."/".$file;
		}
	}

	# /info must have right permissions,
	# and git init didn't do it for some reason.
	# config must have correct permissions.
	# and Git 2.1.0 - 2.2.1 incorrectly add +x for some reason.
	# also make sure /refs, /objects and /htmlcache are correct too.
	my ($dmode, $dmodestr, $fmode, $fmodestr);
	if ($Girocco::Config::owning_group) {
		($dmode, $dmodestr) = (02775, '02775');
		($fmode, $fmodestr) = (0664,  '0664');
	} else {
		($dmode, $dmodestr) = (02777, '02777');
		($fmode, $fmodestr) = (0666,  '0666');
	}
	foreach my $dir (qw(info refs objects htmlcache bundles reflogs)) {
		chmod($dmode, $self->{path}."/$dir") or die "chmod $dmodestr $self->{path}/$dir failed: $!";
	}
	foreach my $file (qw(config)) {
		chmod($fmode, $self->{path}."/$file") or die "chmod $fmodestr $self->{path}/$file failed: $!";
	}
	# these ones are probably not strictly required but are nice to have
	foreach my $dir (qw(refs/heads refs/tags objects/info objects/pack)) {
		-d $self->{path}."/$dir" or mkdir $self->{path}."/$dir";
		chmod($dmode, $self->{path}."/$dir");
	}
	if ($Girocco::Config::owning_group && defined($gid) && $Girocco::Config::htmlcache_owning_group) {
		my $htmlgid = scalar(getgrnam($Girocco::Config::htmlcache_owning_group));
		if (defined($htmlgid) && $htmlgid ne $gid) {
			chown(-1, $htmlgid, $self->{path}."/htmlcache") or die "chgrp $htmlgid $self->{path}/htmlcache failed: $!";
			chmod($dmode, $self->{path}."/htmlcache") or die "chmod $dmodestr $self->{path}/htmlcache failed: $!";
		}
	}
	if ($Girocco::Config::owning_group && defined($gid) && $Girocco::Config::ctags_owning_group) {
		my $ctagsgid = scalar(getgrnam($Girocco::Config::ctags_owning_group));
		if (defined($ctagsgid) && $ctagsgid ne $gid) {
			chown(-1, $ctagsgid, $self->{path}."/ctags") or die "chgrp $ctagsgid $self->{path}/ctags failed: $!";
			chmod($dmode, $self->{path}."/ctags") or die "chmod $dmodestr $self->{path}/ctags failed: $!";
		}
	}

	$self->_properties_save;
	$self->_alternates_setup unless $self->{noalternates} || $self->{adopt};
	$self->_ctags_setup;
	$self->_group_remove;
	$self->_group_add($pushers);
	$self->_hooks_install;
	$self->_update_index;
	$self->_set_changed;
	$self->_set_forkchange(1);
}

sub premirror {
	my $self = shift;

	delete $self->{adopt};
	$self->_setup(':');
	$self->_clonep(1);
	if ($Girocco::Config::autogchack) {
		system("$Girocco::Config::basedir/jobd/maintain-auto-gc-hack.sh", $self->{name}) == 0
			or die "maintain-auto-gc-hack.sh $self->{name} failed";
	}
	$self->perm_initialize;
}

sub conjure {
	my $self = shift;

	delete $self->{adopt};
	$self->_setup;
	$self->_nofetch(1);
	if ($Girocco::Config::autogchack && $Girocco::Config::autogchack ne "mirror") {
		system("$Girocco::Config::basedir/jobd/maintain-auto-gc-hack.sh", $self->{name}) == 0
			or die "maintain-auto-gc-hack.sh $self->{name} failed";
	}
	if ($Girocco::Config::mob && $Girocco::Config::mob eq "mob") {
		system("$Girocco::Config::basedir/bin/create-personal-mob-area", $self->{name}) == 0
			or die "create-personal-mob-area $self->{name} failed";
	}
	$self->perm_initialize;
}

sub clone {
	my $self = shift;

	unlink ($self->_clonefail_path()); # Ignore EEXIST error
	unlink ($self->_clonelog_path()); # Ignore EEXIST error

	use IO::Socket;
	my $sock = IO::Socket::UNIX->new($Girocco::Config::chroot.'/etc/taskd.socket') or die "cannot connect to taskd.socket: $!";
	select((select($sock),$|=1)[0]);
	$sock->print("clone ".$self->{name}."\n");
	# Just ignore reply, we are going to succeed anyway and the I/O
	# would apparently get quite hairy.
	$sock->flush();
	sleep 2; # *cough*
	$sock->close();
}

# call this after ghost instead of conjure or premirror+clone to adopt a pre-existing Git dir
# ghost must be called with the proper value of $mirror for the to-be-adopted project
# for mirrors the $proj->{url} needs to be set and for push projects the $proj->{users} array ref
sub adopt {
	my $self = shift;
	my $name = $self->{name};

	# Sanity check first
	defined($name) && $name ne "" && !$self->{loaded} or return undef;
	does_exist($name, 1) or return undef;
	defined($self->{path}) && $self->{path} eq $Girocco::Config::reporoot."/$name.git" or return undef;
	defined($self->{base_path}) && $self->{base_path} eq $Girocco::Config::reporoot or return undef;
	defined($self->{email}) && defined($self->{orig_email}) && defined($self->{mirror}) && ref($self->{users}) eq 'ARRAY'
		or return undef;
	!defined(Girocco::Project->load($name)) or return undef;
	is_git_dir($self->{path}) or return undef;
	my $config = read_config_file_hash($self->{path}."/config");
	defined($config) && _boolval($config->{"core.bare"}) or die "refusing to adopt non-bare repository";
	defined(read_HEAD_symref($self->{path})) or die "refusing to adopt non-symref HEAD repository";

	# Adopt the project by creating a new $chroot/etc/group entry and setting up anything that's missing
	$self->_nofetch(!$self->{mirror});
	$self->{adopt} = 1;
	$self->_setup($self->{mirror} ? ":" : "");
	delete $self->{adopt};
	if ($Girocco::Config::autogchack && ($self->{mirror} || $Girocco::Config::autogchack ne "mirror")) {
		system("$Girocco::Config::basedir/jobd/maintain-auto-gc-hack.sh", $self->{name}) == 0
			or die "maintain-auto-gc-hack.sh $self->{name} failed";
	}
	if (!$self->{mirror} && $Girocco::Config::mob && $Girocco::Config::mob eq "mob") {
		system("$Girocco::Config::basedir/bin/create-personal-mob-area", $self->{name}) == 0
			or die "create-personal-mob-area $self->{name} failed";
	}
	my $result = $self->perm_initialize;
	unlink("$self->{path}/.delaygc") unless $self->is_empty;
	# Pick up any pre-existing settings
	my $p = Girocco::Project->load($name);
	%$self = %$p if defined($p) && $p->{loaded};
	$result;
}

sub _update_users {
	my $self = shift;

	$self->_group_update;
	my @users_add = grep { $a = $_; not scalar grep { $a eq $_ } $self->{orig_users} } $self->{users};
	my @users_del = grep { $a = $_; not scalar grep { $a eq $_ } $self->{users} } $self->{orig_users};
	$self->perm_user_add($_, Girocco::User::resolve_uid($_)) foreach (@users_add);
	$self->perm_user_del($_, Girocco::User::resolve_uid($_)) foreach (@users_del);
}

sub update {
	my $self = shift;

	$self->_properties_save;
	$self->_update_users;

	if (exists($self->{tags_to_delete})) {
		$self->delete_ctag($_) foreach(@{$self->{tags_to_delete}});
	}

	$self->set_HEAD($self->{HEAD}) unless $self->{orig_HEAD} eq $self->{HEAD};

	$self->_update_index if $self->{email} ne $self->{orig_email};
	$self->{orig_email} = $self->{email};
	$self->_set_changed;

	1;
}

sub update_password {
	my $self = shift;
	my ($pwd) = @_;

	$self->{crypt} = scrypt_sha1($pwd);
	$self->_group_update;
}

# You can explicitly do this just on a ghost() repository too.
sub delete {
	my $self = shift;

	if (-d $self->{path}) {
		system('rm', '-rf', $self->{path}) == 0
			or die "rm -rf $self->{path} failed: $?";
	}
	# attempt to clean up any empty fork directories by removing them
	my @pelems = split('/', $self->{name});
	while (@pelems > 1) {
		pop @pelems;
		# okay to fail
		rmdir join('/', $Girocco::Config::reporoot, @pelems) or last;
	}
	$self->_group_remove;
	$self->_update_index;
	$self->_set_forkchange(1);

	1;
}

# If the project's directory actually exists archive it before deleting it
# Return full path to archived project ("" if none)
sub archive_and_delete {
	my $self = shift;

	unless (-d $self->{path}) {
		$self->delete;
		return "";
	}

	# archive the project before deletion
	use POSIX qw(strftime);
	my $destdir = $self->{base_path};
	$destdir =~ s,(?<=[^/])/+$,,;
	$destdir .= "/_recyclebin/";
	$destdir .= $1 if $self->{name} =~ m,^(.*/)[^/]+$,;
	my $destbase = $self->{name};
	$destbase = $1 if $destbase =~ m,^.*/([^/]+)$,;
	my $oldmask = umask();
	umask($oldmask & ~0070);
	system('mkdir', '-p', $destdir) == 0 && -d $destdir
		or die "mkdir -p \"$destdir\" failed: $?";
	umask($oldmask);
	my $suffix = '';
	if (-e "$destdir$destbase.git") {
		$suffix = 1;
		while (-e "$destdir$destbase~$suffix.git") {
			++$suffix;
			last if $suffix >= 10000; # don't get too carried away
		}
		$suffix = '~'.$suffix;
	}
	not -e "$destdir$destbase$suffix.git"
		or die "Unable to compute suitable archive path";
	system('mv', $self->{path}, "$destdir$destbase$suffix.git") == 0
		or die "mv \"$self->{path}\" \"$destdir$destbase$suffix.git\" failed: $?";
	if (!$self->{mirror} && @{$self->{users}}) {
		# Remember the user list at recycle time
		system($Girocco::Config::git_bin, '--git-dir='.$destdir.$destbase.$suffix.".git",
			'config', 'girocco.recycleusers', join(",", @{$self->{users}}));
	}
	my ($S,$M,$H,$d,$m,$y) = gmtime(time());
	my $recycletime = strftime("%Y-%m-%dT%H:%M:%SZ", $S, $M, $H, $d, $m, $y, -1, -1, -1);
	# We ought to do something if this fails, but the project has already been moved
	# so there's really nothing to be done at this point.
	system($Girocco::Config::git_bin, '--git-dir='.$destdir.$destbase.$suffix.".git",
		'config', 'girocco.recycletime', $recycletime);

	$self->delete;
	return $destdir.$destbase.$suffix.".git";
}

sub _contains_files {
	my $dir = shift;
	(-d $dir) or return 0;
	opendir(my $dh, $dir) or die "opendir $dir failed: $!";
	while (my $entry = readdir($dh)) {
		next if $entry eq '' || $entry eq '.' || $entry eq '..';
		closedir($dh), return 1
			if -f "$dir/$entry" ||
			-d "$dir/$entry" && _contains_files("$dir/$entry");
	}
	closedir($dh);
	return 0;
}

sub has_forks {
	my $self = shift;

	return _contains_files($Girocco::Config::reporoot.'/'.$self->{name});
}

# Returns an array of 0 or more array refs, one for each bundle:
#  ->[0]: bundle creation time (seconds since epoch)
#  ->[1]: bundle name (e.g. foo-xxxxxxxx.bundle)
#  ->[2]: bundle size in bytes
sub bundles {
	my $self = shift;
	use Time::Local;

	return @{$self->{bundles}} if ref($self->{bundles}) eq 'ARRAY';
	my @blist = ();
	if (-d $self->{path}.'/bundles') {{
		my $prefix = $self->{name};
		$prefix =~ s|^.*[^/]/([^/]+)$|$1|;
		opendir(my $dh, $self->{path}.'/bundles') or last;
		while (my $bfile = readdir($dh)) {
			next unless $bfile =~ /^\d{8}_\d{6}-[0-9a-f]{8}$/;
			my $ctime = eval {timegm(
				0+substr($bfile,13,2), 0+substr($bfile,11,2), 0+substr($bfile,9,2),
				0+substr($bfile,6,2), 0+substr($bfile,4,2)-1, 0+substr($bfile,0,4)-1900)};
			next unless $ctime;
			open(my $bh, '<', $self->{path}.'/bundles/'.$bfile) or next; 
			my $f1 = <$bh>;
			my $f2 = <$bh>;
			$f1 = $self->{path}.'/objects/pack/'.$f1 if $f1 && $f1 !~ m|^/|;
			$f2 = $self->{path}.'/objects/pack/'.$f2 if $f2 && $f2 !~ m|^/|;
			close($bh);
			next unless $f1 && $f2 && $f1 ne $f2;
			chomp $f1;
			chomp $f2;
			next unless -e $f1 && -e $f2;
			my $s1 = -s $f1 || 0;
			my $s2 = -s $f2 || 0;
			next unless $s1 || $s2;
			push(@blist, [$ctime, "$prefix-".substr($bfile,16,8).".bundle", $s1+$s2]);
		}
		closedir($dh);
	}}
	@blist = sort({$b->[0] <=> $a->[0]} @blist);
	my %seen = ();
	my @result = ();
	foreach my $bndl (@blist) {
		next if $seen{$bndl->[1]};
		$seen{$bndl->[1]} = 1;
		push(@result, $bndl);
	}
	$self->{bundles} = \@result;
	return @result;
}

sub has_alternates {
	my $self = shift;
	my $af = $self->{path}.'/objects/info/alternates';
	-f $af && -s _ or return 0;
	my $nb = 0;
	open my $fh, '<', $af or return 1;
	while (my $line = <$fh>) {
		next if $line =~ /^$/ || $line =~ /^#/;
		$nb = 1;
		last;
	}
	close $fh;
	return $nb;
}

sub has_bundle {
	my $self = shift;

	return scalar($self->bundles);
}

sub _has_notifyhook {
	my $self = shift;
	my $val = $Girocco::Config::default_notifyhook;
	defined($self->{'notifyhook'}) and $val = $self->{'notifyhook'};
	return (defined($val) && $val ne "") ? $val : undef;
}

# returns true if any of the notify fields are non-empty
sub has_notify {
	my $self = shift;
	# We do not ckeck notifycia since it's defunct
	return
		$self->{'notifymail'} || $self->{'notifytag'} ||
		$self->{'notifyjson'} ||
		!!$self->_has_notifyhook;
}

sub is_empty {
	# A project is considered empty if the git repository does not
	# have any refs.  This means packed-refs does not exist or is
	# empty or only has lines starting with '#' AND there are no
	# files in the refs subdirectory hierarchy (no matter how deep).

	my $self = shift;

	(-d $self->{path}) or return 0;
	if (-e $self->{path}.'/packed-refs') {
		open(my $pr, '<', $self->{path}.'/packed-refs')
			or die "open $self->{path}./packed-refs failed: $!";
		my $foundref = 0;
		while (my $ref = <$pr>) {
			next if $ref =~ /^#/;
			$foundref = 1;
			last;
		}
		close($pr);
		return 0 if $foundref;
	}
	(-d $self->{path}.'/refs') or return 1;
	return !_contains_files($self->{path}.'/refs');
}

# returns array:
# [0]: earliest possible scheduled next gc, undef if lastgc not set
# [1]: approx. latest possible scheduled next gc, undef if lastgc not set
# Both values (if not undef) are seconds since epoch
# Result only considers lastgc and min_gc_interval nothing else
sub next_gc {
	my $self = shift;
	my $lastgcepoch = parse_any_date($self->{lastgc});
	return (undef, undef) unless defined $lastgcepoch;
	return ($lastgcepoch + $Girocco::Config::min_gc_interval,
		int($lastgcepoch + 1.25 * $Girocco::Config::min_gc_interval +
				$Girocco::Config::min_mirror_interval));
}

# returns boolean (0 or 1)
# Attempts to determine whether or not a gc (and corresponding build
# of a .bitmap/.bndl file) will actually take place at the next_gc
# time (as returned by next_gc).  Whether or not a .bitmap and .bndl
# end up being built depends on whether or not the local object graph
# is complete.  In general if has_alternates is true then a .bitmap/.bndl
# is not possible because the object graph will be incomplete,
# but it *is* possible that even though the repository has_alternates,
# it does not actually borrow any objects so a .bitmap/.bndl will build
# in spite of the presence of alternates -- but this is expected to be rare.
# The result is a best guess and false return value is not an absolute
# guarantee that gc will not take place at the next interval, but it probably
# will not if nothing changes in the meantime.
sub needs_gc {
	my $self = shift;
	my $lgc = parse_any_date($self->{lastgc});
	my $lrecv = parse_any_date($self->{lastreceive});
	my $lpgc = parse_any_date($self->{lastparentgc});
	return 1 unless defined($lgc) && defined($lrecv);
	if ($self->has_alternates) {
		return 1 unless defined($lpgc);
		return 1 unless $lpgc < $lgc;
	}
	return 1 unless $lrecv < $lgc;
	# We don't try running any "is_dirty" check, so if somehow the
	# repository became dirty without updating lastreceive we might
	# incorrectly return false instead of true.
	return 0;
}

sub delete_ctag {
	my $self = shift;
	my ($ctag) = @_;

	# sanity check, disallow filenames starting with . .. or /
	unlink($self->{path}.'/ctags/'.$ctag)
		unless !defined($ctag) || $ctag =~ m|^/| || $ctag =~ m{(?:^|/)(?:\.\.?)(?:/|$)};
}

# returns new tag count value on success (will always be >= 1) otherwise undef
sub add_ctag {
	my $self = shift;
	my $ctag = valid_tag(shift);
	my $nochanged = shift;

	# sanity check, disallow filenames starting with . .. or /
	return undef if !defined($ctag) || $ctag =~ m|^/| || $ctag =~ m{(?:^|/)(?:\.\.?)(?:/|$)};

	my $val = 0;
	my $ct;
	if (open $ct, '<', $self->{path}."/ctags/$ctag") {
		my $count = <$ct>;
		close $ct;
		defined $count or $count = '';
		chomp $count;
		$val = $count =~ /^[1-9]\d*$/ ? $count : 1;
	}
	++$val;
	my $oldmask = umask();
	umask($oldmask & ~0060);
	open $ct, '>', $self->{path}."/ctags/$ctag" and print $ct $val."\n" and close $ct;
	$self->_set_changed unless $nochanged;
	$self->_set_forkchange unless $nochanged;
	umask($oldmask);
	return $val;
}

sub get_ctag_names {
	my $self = shift;
	my @ctags = ();
	opendir(my $dh, $self->{path}.'/ctags')
		or return @ctags;
	@ctags = grep { -f "$self->{path}/ctags/$_" } readdir($dh);
	closedir($dh);
	return sort({lc($a) cmp lc($b)} @ctags);
}

sub get_heads {
	my $self = shift;
	my $fh;
	my $heads = get_git("--git-dir=$self->{path}", 'for-each-ref', '--format=%(objectname) %(refname)', 'refs/heads');
	defined($heads) or $heads = '';
	chomp $heads;
	my @res = ();
	foreach (split(/\n/, $heads)) {
		chomp;
		next if !m#^[0-9a-f]{40}\s+refs/heads/(.+)$ #x;
		push @res, $1;
	}
	push(@res, $self->{orig_HEAD}) if !@res && defined($self->{orig_HEAD}) && $self->{orig_HEAD} ne '';
	@res;
}

sub get_HEAD {
	my $self = shift;
	my $HEAD = read_HEAD_ref($self->{path});
	defined($HEAD) && $HEAD ne "" or die "could not get HEAD";
	return $1 if $HEAD =~ m{^refs/heads/(.+)$};
	return "[other]" if $HEAD =~ m{^refs/};
	return "[detached]" if $HEAD =~ m{^[0-9a-fA-F]{4,}$};
	return "[invalid]";
}

sub set_HEAD {
	my $self = shift;
	my $newHEAD = shift;
	# Cursory checks only -- if you want to break your HEAD, be my guest
	if ($newHEAD =~ /^\/|^\.|[\x00-\x1f \x7f\[~^'<>*?\\:]|\@\{|\.\.|\.lock$|\.$|\/$/) {
		die "grossly invalid new HEAD: $newHEAD";
	}
	system($Girocco::Config::git_bin, "--git-dir=$self->{path}", 'symbolic-ref', 'HEAD', "refs/heads/$newHEAD");
	die "could not set HEAD" if ($? >> 8);
	! -d "$self->{path}/mob" || $Girocco::Config::mob ne 'mob'
		or system('cp', '-p', '-f', "$self->{path}/HEAD", "$self->{path}/mob/HEAD") == 0;
}

sub gen_auth {
	my $self = shift;
	my ($type) = @_;
	$type = 'REPO' unless $type && $type =~ /^[A-Z]+$/;

	$self->{authtype} = $type;
	{
		no warnings;
		$self->{auth} = sha1_hex(time . $$ . rand() . join(':',%$self));
	}
	my $expire = time + 24 * 3600;
	my $propval = "# ${type}AUTH $self->{auth} $expire";
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', 'gitweb.repoauth', $propval);
	$self->{auth};
}

sub del_auth {
	my $self = shift;

	delete $self->{auth};
	delete $self->{authtype};
	system($Girocco::Config::git_bin, '--git-dir='.$self->{path}, 'config', '--unset', 'gitweb.repoauth');
}

sub remove_user {
	my $self = shift;
	my ($username) = @_;

	my $before_count = @{$self->{users}};
	$self->{users} = [grep { $_ ne $username } @{$self->{users}}];
	return @{$self->{users}} != $before_count;
}

### static methods

sub get_forkee_name {
	local $_ = $_[0];
	(m#^(.*)/.*?$#)[0]; #
}

sub get_forkee_path {
	no warnings; # avoid silly 'unsuccessful stat on filename with \n' warning
	my $forkee = $Girocco::Config::reporoot.'/'.get_forkee_name($_[0]).'.git';
	-d $forkee ? $forkee : '';
}

# Ultimately the full project/fork name could end up being part of a Git ref name
# when a project's forks are combined into one giant repository for efficiency.
# That means that the project/fork name must satisfy the Git ref name requirements:
#
#  1. Characters with an ASCII value less than or equal to 32 are not allowed
#  2. The character with an ASCII value of 0x7F is not allowed
#  3. The characters '~', '^', ':', '\', '*', '?', and '[' are not allowed
#  4. The character '/' is a separator and is not allowed within a name
#  5. The name may not start with '.' or end with '.'
#  6. The name may not end with '.lock'
#  7. The name may not contain the '..' sequence
#  8. The name may not contain the '@{' sequence
#  9. If multiple components are used (separated by '/'), no empty '' components
#
# We also prohibit a trailing '.git' on any path component and futher restrict
# the allowed characters to alphanumeric and [+._-] where names must start with
# an alphanumeric.

sub _valid_name_characters {
	local $_ = $_[0];
	(not m#^[/+._-]#)
		and (not m#//#)
		and (not m#\.\.#)
		and (not m#/[+._-]#)
		and (not m#\./#)
		and (not m#\.$#)
		and (not m#\.git/#i)
		and (not m#\.git$#i)
		and (not m#\.idx/#i)
		and (not m#\.idx$#i)
		and (not m#\.lock/#i)
		and (not m#\.lock$#i)
		and (not m#\.pack/#i)
		and (not m#\.pack$#i)
		and (not m#\.bundle/#i)
		and (not m#\.bundle$#i)
		and (not m#/$#)
		and (not m/^[a-fA-F0-9]{38}$/)
		and m#^[a-zA-Z0-9/+._-]+$#
		and !has_reserved_suffix($_, $_[1], $_[2]);
}

# $_[0] => prospective project name (WITHOUT trailing .git)
# $_[1] => true to allow orphans (i.e. two-or-more-level-deep projects without a parent)
#          (the directory in which the orphan will be created must, however, already exist)
# $_[2] => true to allow orphans w/o needed directory if $_[1] also true (like mkdir -p)
sub valid_name {
	no warnings; # avoid silly 'unsuccessful stat on filename with \n' warning
	local $_ = $_[0];
	_valid_name_characters($_) and not exists($reservedprojectnames{lc($_)})
		and @{[m#/#g]} <= 5 # maximum fork depth is 5
		and ((not m#/#) or -d get_forkee_path($_) or ($_[1] and ($_[2] or -d $Girocco::Config::reporoot.'/'.get_forkee_name($_))))
		and (! -f $Girocco::Config::reporoot."/$_.git");
}

# It's possible that some forks have been kept but the forkee is gone.
# In this case the standard valid_name check is too strict.
sub does_exist {
	no warnings; # avoid silly 'unsuccessful stat on filename with \n' warning
	my ($name, $nodie) = @_;
	my $okay = (
		_valid_name_characters($name, $Girocco::Config::reporoot, ".git")
		and ((not $name =~ m#/#)
			or -d get_forkee_path($name)
			or -d $Girocco::Config::reporoot.'/'.get_forkee_name($name)));
	(!$okay && $nodie) and return undef;
	!$okay and die "tried to query for project with invalid name $name!";
	(-d $Girocco::Config::reporoot."/$name.git");
}

sub get_full_list {
	open my $fd, '<', jailed_file("/etc/group") or die "getting project list failed: $!";
	my @projects = map {/^([^:_\s#][^:\s#]*):[^:]*:\d{5,}:/ ? $1 : ()} <$fd>;
	close $fd;
	@projects;
}


1;
