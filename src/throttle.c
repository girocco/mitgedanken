/*

throttle.c -- throttle utility to make use of taskd.pl throttle services
Copyright (C) 2015 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  This utility provides a means to make use of taskd.pl's throttle services.
  It is simply started with options specifying the throttle class, the item
  description and an optional supplementary message to included in case the
  request is throttled.

  If the request is allowed to proceed, the non-option arguments specify a
  command and arguments to be execvp'd.  If the request is throttled, a result
  suitable for a CGI is written to stdout with a 503 status.  Alternatively
  on failure a non-zero exit code can be returned in lieu of CGI output.
*/

#ifndef SOCKET_FILE
#error SOCKET_FILE must be defined to full path to taskd.pl socket
#endif

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809
#endif

#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/un.h>

#ifndef FD_COPY
#define FD_COPY(src,dst) ((*dst)=(*src))
#endif

#define USAGE \
"Usage: throttle [-t] [-p] [-e] -c <class> -d <desc> [-m <msg>] command [arg]...\n" \
"  -h                show this help\n" \
"  -e                exit with error (3 = exec fail, 10 = connect fail,\n" \
"                    11 = request fail, 20 = request error, 21 = throttled)\n" \
"                    on failure but do not output anything to standard output\n" \
"  -t                throttle on connect failure (default is proceed)\n" \
"  -p                proceed on throttle request error (default is throttle)\n" \
"  -c <class>        required throttle class\n" \
"  -d <desc>         required description of item in class\n" \
"  -m <msg>          supplementary message to include with 503 error\n" \
"  command [arg]...  command and arguments to execvp on proceed\n"

static int ok_connect_fail = 1;
static int ok_request_fail = 0;
static int err_on_failure = 0;

static void
die(
    const char *msg)
{
    fprintf(stderr, "fatal: %s\n", msg ? msg : "error");
    exit(EXIT_FAILURE);
}

static void
diefailure(
    const char *msg)
{
    fprintf(stderr, "fatal: %s: %s\n", msg ? msg : "failed", strerror(errno));
    exit(EXIT_FAILURE);
}

static void
dieusage(
    const char *msg)
{
    fprintf(stderr, "fatal: %s\n", msg ? msg : "error");
    fprintf(stderr, "%s", USAGE);
    exit(EXIT_FAILURE);
}

static int
isalphach(
    char ch)
{
    return ('A' <= ch && ch <= 'Z') ||
        ('a' <= ch && ch <= 'z');
}

static size_t
get_escxml_size(
    const char *str)
{
    size_t ans = 0;
    char ch;

    if (!str)
        return ans;
    while ((ch = *str++) != 0) {
        switch (ch) {
            case '<':
            case '>':
                ans += 4;
                break;
            case '&':
                ans += 5;
                break;
            case '\n':
                if (*str == '\n') {
                    while (*++str == '\n')
                        ;
                    ans += 10; /* strlen("\n</p>\n<p>\n") */
                    break;
                }
                /* fall through */
            default:
                ++ans;
                break;
        }
    }
    return ans;
}

static char *
xml_escape(
    const char *msg)
{
    size_t bl;
    char *ans;
    char ch;
    char *p;

    if (!msg)
        return NULL;
    bl = get_escxml_size(msg);
    ans = (char *)malloc(bl + 1);
    if (!ans)
        diefailure("malloc failed");
    p = ans;
    while ((ch = *msg++) != 0) {
        switch(ch) {
            case '<':
                strncpy(p, "&lt;", 4);
                p += 4;
                break;
            case '>':
                strncpy(p, "&gt;", 4);
                p += 4;
                break;
            case '&':
                strncpy(p, "&amp;", 5);
                p += 5;
                break;
            case '\n':
                if (*msg == '\n') {
                    while (*++msg == '\n')
                        ;
                    strncpy(p, "\n</p>\n<p>\n", 10);
                    p += 10;
                    break;
                }
                /* fall through */
            default:
                *p++ = ch;
                break;
        }
    }
    *p = 0;
    return ans;
}

static sig_atomic_t piped = 0;
static int wakeup[2] = {-1, -1};
#define wakeup_r (wakeup[0])
#define wakeup_w (wakeup[1])

static void
sigpipe(
    int sig)
{
    ssize_t ignore;

    (void)sig;
    piped = 1;
    ignore = write(wakeup_w, "!", 1);
    (void)ignore;
}

static void
setcloexec(
    int fd)
{
    if (fcntl(fd, F_SETFD, FD_CLOEXEC))
        diefailure("fcntl F_SETFD failed");
}

static void
setnonblock(
    int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        diefailure("fcntl F_GETFL failed");
    if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1)
        diefailure("fcntl F_SETFL O_NONBLOCK failed");
}

static void
setblock(
    int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        diefailure("fcntl F_GETFL failed");
    if (fcntl(fd, F_GETFL, flags & ~O_NONBLOCK) == -1)
        diefailure("fcntl F_SETFL !O_NONBLOCK failed");
}

static ssize_t
writeall(
    int fd,
    const void *_buf,
    size_t nbyte)
{
    const char *buf = (const char *)_buf;
    size_t off = 0;
    ssize_t written;
    size_t count = nbyte;

    if (!nbyte)
        return 0;
    do {
        written = write(fd, buf+off, count);
        if (written > 0) {
            count -= (size_t)written;
            off += (size_t)written;
        }
    } while(!piped && count && (written > 0 || errno == EINTR));
    if (!count && !piped)
        return nbyte;
    if (!written)
        errno = EIO;
    if (piped)
        errno = EPIPE;
    return -1;
}

#define GENERIC_MESSAGE \
"The requested service is temporarily unavailable, please try again later."

void
send_failure(
    int err, const char *msg)
{
    if (err_on_failure)
        exit(err);
    printf("%s\r\n", "Status: 503 Service Temporarily Unavailable");
    printf("%s\r\n", "Expires: Fri, 01 Jan 1980 00:00:00 GMT");
    printf("%s\r\n", "Pragma: no-cache");
    printf("%s\r\n", "Cache-Control: no-cache,max-age=0,must-revalidate");
    printf("%s\r\n", "Content-Type: text/html; charset=utf-8");
    printf("%s\r\n", "");
    printf("%s\n%s\n%s\n", "<p>", GENERIC_MESSAGE, "</p>");
    if (msg && *msg)
        printf("%s\n%s\n%s\n", "<p>", msg, "</p>");
    fflush(stdout);
    exit(0);
}

int
main(
    int argc,
    char *argv[])
{
    int ch;
    const char *classname = NULL;
    const char *desc = NULL;
    char *escmsg = NULL;
    char **args;
    union {
        struct sockaddr sa;
        struct sockaddr_un sun;
    } su;
    int fd, fd2;
    char request[1024];

    while ((ch = getopt(argc, argv, "htpec:d:m:")) != -1) {
        switch(ch) {
            case 'h':
                printf("%s", USAGE);
                return 0;
                break;
            case 't':
                ok_connect_fail = 0;
                break;
            case 'p':
                ok_request_fail = 1;
                break;
            case 'e':
                err_on_failure = 1;
                break;
            case 'c':
                if (!optarg || !*optarg)
                    dieusage("-c: invalid class name");
                classname = optarg;
                break;
            case 'd':
                if (!optarg || !*optarg)
                    dieusage("-c: invalid description");
                desc = optarg;
                break;
            case 'm':
                if (!optarg || !*optarg)
                    dieusage("-m: invalid message");
                if (escmsg)
                    free(escmsg);
                escmsg = xml_escape(optarg);
                break;
            case '?':
            default:
                dieusage("invalid option");
                break;
        }
    }
    if (!classname || !desc)
        dieusage("-c <class> and -d <desc> are required");
    if (strlen(classname) > 64)
        die("classname must be 64 characters or less in length");
    if (strlen(desc) > 512)
        die("description must be 512 characters or less in length");
    argv += optind;
    argc -= optind;
    if (!argc || !*argv)
        dieusage("no command specified");
    args = (char **)malloc((argc + 1) * sizeof(char *));
    if (!args)
        diefailure("malloc failed");
    memcpy(args, argv, argc * sizeof(char *));
    args[argc] = NULL;

    if (pipe(wakeup))
        diefailure("pipe failed");
    setcloexec(wakeup_w);
    setcloexec(wakeup_r);
    setnonblock(wakeup_w);
    setnonblock(wakeup_r);

    memset(&su.sun, 0, sizeof(su.sun));
    su.sun.sun_family = AF_UNIX;
    strncpy(su.sun.sun_path, SOCKET_FILE, sizeof(su.sun.sun_path)-1);
    fd2 = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd2 < 0)
        diefailure("socket failed");
    fd = fcntl(fd2, F_DUPFD, 16);
    if (fd < 0)
        diefailure("fcntl F_DUPFD failed");
    close(fd2);
    if (connect(fd, &su.sa, sizeof(su.sun))) {
        if (errno == ECONNREFUSED || errno == EACCES ||
            errno == ENOENT || errno == ENOTDIR) {

            if (!ok_connect_fail)
                send_failure(10, escmsg);
            else
                close(fd), fd=-1;
        } else {
            diefailure("connect failed");
        }
    }

    signal(SIGPIPE, sigpipe);

    if (fd >= 0) {
        snprintf(request, sizeof(request), "throttle %u %s %s\n",
            (unsigned)getpid(), classname, desc);
        if (writeall(fd, request, strlen(request)) == -1) {
            if (!ok_request_fail)
                send_failure(11, escmsg);
            else
                close(fd), fd=-1;
        }
    }

    if (fd >= 0) {
        fd_set fds;
        int nfds = wakeup_r + 1;
        size_t off = 0;
        int requesterr = 0;
        int proceed = 0;
        int throttled = 0;

        if (fd > wakeup_r)
            nfds = fd + 1;
        setnonblock(fd);
        FD_ZERO(&fds);
        FD_SET(wakeup_r, &fds);
        FD_SET(fd, &fds);
        while (!piped && !requesterr && !proceed && !throttled) {
            fd_set rset, eset;
            struct timeval tv;
            ssize_t readcnt;

            FD_COPY(&fds, &rset);
            FD_COPY(&fds, &eset);
            tv.tv_sec = 30;
            tv.tv_usec = 0;
            select(nfds, &rset, NULL, &eset, &tv);
            do {
                readcnt = read(fd, request + off, sizeof(request) - 1 - off);
                if (readcnt > 0) {
                    char *nl;
                    off += (size_t)readcnt;
                    request[off] = 0;
                    if ((nl = strchr(request, '\n')) != NULL) {
                        if (!strcmp(request, "proceed\n"))
                            proceed = 1;
                        else if (nl > request && isalphach(request[0]))
                            throttled = 1;
                        else
                            requesterr = 1;
                    }
                }
            } while (readcnt > 0 && !proceed && !throttled && !requesterr);
            if ((!readcnt && !proceed && !throttled) ||
                (readcnt == -1 && errno != EINTR && errno != EAGAIN) ||
                (off + 1 >= sizeof(request) && !proceed && !throttled)) {

                requesterr = 1;
            }
            setblock(fd);
            if (writeall(fd, "keepalive\n", 10) == -1)
                piped = 1;
            setnonblock(fd);
        }
        if (requesterr && !ok_request_fail)
            send_failure(20, escmsg);
        else if (throttled)
            send_failure(21, escmsg);
    }

    close(wakeup_r);
    close(wakeup_w);
    signal(SIGPIPE, SIG_DFL);

    execvp(args[0], args);
    send_failure(3, escmsg);
    return EXIT_FAILURE;
}
