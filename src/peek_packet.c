/*

peek_packet.c -- peek_packet utility to peek at incoming git-daemon request
Copyright (C) 2015 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  This utility is intended to be used by a script front end to git daemon
  running in inetd mode.  The first thing the script does is call this utility
  which attempts to peek the first incoming Git packet off the connection
  and then output contents after the initial 4-character hex length upto but
  excluding the first \0 character.

  At that point the script can validate the incoming request and if it chooses
  to allow it then exec git daemon to process it.  Since the packet was peeked
  it's still there to be read by git daemon.

  Note that there is a hard-coded timeout of 30 seconds and a hard-coded limit
  of PATH_MAX for the length of the initial packet.

  On failure a non-zero exit code is returned.  On success a 0 exit code is
  returned and peeked text is output to stdout.

  The connection to be peeked must be fd 0 and will have SO_KEEPALIVE set on it.

  This utility does not take any arguments and ignores any that are given.

  The output of a successful peek should be one of these:
  
    git-upload-pack /<...>
    git-upload-archive /<...>
    git-receive-pack /<...>

  where "<...>" is replaced with the repository path so, for example, doing
  "git ls-remote git://example.com/foo.git" would result in this output:
  
    git-upload-pack /foo.git

  Note that the first character could be a ~ instead of a /, but it's
  probably best to reject those.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>

/* Note that mod_reqtimeout has a default configuration of 20 seconds
 * maximum to wait for the first byte of the initial request line and
 * then no more than 40 seconds total, but after the first byte is
 * received the rest must arrive at 500 bytes/sec or faster.  That
 * means 10000 bytes minimum in 40 seconds.  We do not allow the
 * initial Git packet to be longer than PATH_MAX (which is typically
 * either 1024 or 4096).  And since 20 + 1024/500 = 22.048 and
 * 20 + 4096/500 = 28.192 using 30 seconds for a total timeout is
 * quite reasonable in comparison to mod_reqtimeout's default conf.
 */

#define TIMEOUT_SECS 30 /* no more than 30 seconds for initial packet */

#define POLL_QUANTUM 100000U /* how often to poll in microseconds */

static int xdig(char c)
{
	if ('0' <= c && c <= '9')
		return c - '0';
	if ('a' <= c && c <= 'f')
		return c - 'a' + 10;
	if ('A' <= c && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

static char buffer[PATH_MAX];
static time_t expiry;

/* Ideally we could just use MSG_PEEK + MSG_WAITALL, and that works nicely
 * on BSD-type distros.  Unfortunately very bad things happen on Linux with
 * that combination -- a CPU core runs at 100% until all the data arrives.
 * So instead we omit the MSG_WAITALL and poll every POLL_QUANTUM interval
 * to see if we've satisfied the requested amount yet.
 */
static int recv_peekall(int fd, void *buff, size_t len)
{
	int ans;
	while ((ans = recv(fd, buff, len, MSG_PEEK)) > 0 && (size_t)ans < len) {
		if (time(NULL) > expiry)
			exit(2);
		usleep(POLL_QUANTUM);
	}
	return ans < 0 ? -1 : (int)len;
}

static void handle_sigalrm(int s)
{
	(void)s;
	_exit(2);
}

static void clear_alarm(void)
{
	alarm(0);
}

int main(int argc, char *argv[])
{
	int len;
	int xvals[4];
	char hexlen[4];
	size_t pktlen;
	const char *nullptr;
	int optval;

	(void)argc;
	(void)argv;

	/* Ideally calling recv with MSG_PEEK would never, ever hang.  However
	 * even with MSG_PEEK, recv still waits for at least the first message
	 * to arrive on the socket (unless it's non-blocking).  For this reason
	 * we set an alarm timer at TIMEOUT_SECS + 2 to make sure we don't
	 * remain stuck in the recv call waiting for the first message.
	 */
	signal(SIGALRM, handle_sigalrm);
	alarm(TIMEOUT_SECS + 2); /* Some slop as this shouldn't be needed */
	atexit(clear_alarm);  /* Probably not necessary, but do it anyway */

	expiry = time(NULL) + TIMEOUT_SECS;

	optval = 1;
	if (setsockopt(0, SOL_SOCKET, SO_KEEPALIVE, &optval, sizeof(optval)))
		return 1;

	len = recv_peekall(0, hexlen, 4);
	if (len != 4)
		return 1;

	if ((xvals[0]=xdig(hexlen[0])) < 0 ||
		(xvals[1]=xdig(hexlen[1])) < 0 ||
		(xvals[2]=xdig(hexlen[2])) < 0 ||
		(xvals[3]=xdig(hexlen[3])) < 0)
		return 1;
	pktlen = ((unsigned)xvals[0] << 12) |
		((unsigned)xvals[1] << 8) |
		((unsigned)xvals[2] << 4) |
		(unsigned)xvals[3];
	if (pktlen < 22 || pktlen > sizeof(buffer))
		return 1;

	len = recv_peekall(0, buffer, pktlen);
	if (len != (int)pktlen)
		return 1;

	if (memcmp(buffer+4, "git-", 4)) /* sanity check */
		return 1;
	nullptr = (const char *)memchr(buffer+4, 0, pktlen-4);
	if (!nullptr || nullptr < (buffer+21))
		return 1;
	puts(buffer + 4);

	return 0;
}
