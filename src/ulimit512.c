/*

ulimit512.c - provide ulimit -f with 512-byte units on the command line
Copyright (C) 2018 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#define VERSION \
"ulimit512 version 1.0.0\n" \
"Copyright (C) 2018 Kyle J. McKay <mackyle at gmail dot com>\n" \
"License GPLv2+: GNU GPL version 2 or later.\n" \
"<http://gnu.org/licenses/gpl2.html>\n" \
"This is free software: you are free to change and redistribute it.\n" \
"There is NO WARRANTY, to the extent permitted by law.\n"

#define USAGE \
"Usage: ulimit512 [option...] [<command> [<arg>...]]\n" \
"  --help/-h     show this help\n" \
"  --version/-V  show version/license info\n" \
"  -f <blks>     set UL_SETFSIZE to <blks> (dec/oct/hex # of 512-byte units)\n" \
"  -i            ignore UL_SETFSIZE error if UL_GETFSIZE is <= <blks> value\n" \
"  --            terminate options, next arg is command even if it starts with -\n" \
"  <command>     if omitted do nothing other than attempt to set ulimit fsize\n" \
"  <arg>...      zero or more optional args to pass to <command>\n" \
"First sets ulimit UL_SETFSIZE value if any -f option given then uses execvp to\n" \
"run the <command> with any given <arg> values.  A failure of UL_SETFSIZE or\n" \
"execvp will always result in an error.  Note that the value give for -f ALWAYS\n" \
"specifies the limit in number of 512-byte units as required by POSIX and that\n" \
"the maximum value is the largest positive value that fits in a `long` type.\n" \
"Any -f values starting with `0x` are taken to be hexadecimal, starting with `0`\n" \
"means octal otherwise the value is decimal.  It may NOT be negative.\n" \
"Note that using a <blks> value of 0 means zero bytes and is in no way special.\n"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ulimit.h>
#include <unistd.h>

int main(int argc, char *argv[])
{

	int arg = 1;
	long fsize = -1;
	int ignore = 0;

	while (arg < argc && *argv[arg] == '-') {
		if (!strcmp(argv[arg], "--help") || !strcmp(argv[arg], "-h")) {
			printf("%s", USAGE);
			return 0;
		}
		if (!strcmp(argv[arg], "--version") || !strcmp(argv[arg], "-V")) {
			printf("%s", VERSION);
			return 0;
		}
		if (!strcmp(argv[arg], "-i")) {
			ignore = 1;
			++arg;
			continue;
		}
		if (!strcmp(argv[arg], "-f")) {
			char *end;

			if (++arg >= argc || !*argv[arg]) {
				fprintf(stderr, "ulimit512: missing -f value\n");
				return 1;
			}
			fsize = strtol(argv[arg], &end, 0);
			if (*end) {
				fprintf(stderr, "ulimit512: invalid number: %s\n",
					argv[arg]);
				return 2;
			}
			if (fsize < 0) {
				fprintf(stderr, "ulimit512: invalid limit %ld "
					"is < 0\n", fsize);
				return 2;
			}
			++arg;
			continue;
		}
		if (!strcmp(argv[arg], "--")) {
			++arg;
			break;
		}
		fprintf(stderr, "ulimit512: invalid option: %s (see ulimit512 -h)\n",
			argv[arg]);
		return 1;
	}
	if (fsize >= 0) {
		long result;

		errno = 0;
		result = ulimit(UL_SETFSIZE, fsize);
		if (result == -1 && errno != 0) {
			int olderr = errno;
			if (ignore) {
				result = 0;
				result = ulimit(UL_GETFSIZE);
				if (result < 0 || result > fsize)
					ignore = 0;
			}
			if (!ignore) {
				fprintf(stderr, "ulimit512: error: "
					"UL_SETFSIZE %ld failed: %s\n", fsize,
					strerror(olderr));
				return 1;
			}
		}
	}

	if (arg < argc) {
		int i, cnt = argc - arg;
		char **cmd = (char **)malloc((cnt + 1) * sizeof(char *));

		if (!cmd) {
			fprintf(stderr, "ulimit512: out of memory\n");
			return 1;
		}
		for (i = 0; i < cnt; ++i) {
			cmd[i] = argv[arg + i];
		}
		cmd[i] = NULL;
		if (execvp(cmd[0], cmd) == -1) {
			fprintf(stderr, "ulimit512: error: execvp failed: %s\n",
				strerror(errno));
			return 1;
		}
		/* It's an error if execvp returns even if it's not -1 */
		return 1;
	}

	/* No error if nothing to do or just UL_SETFSIZE and that succeeded */
	return 0;
}
