/*

strftime.c -- provide strftime functionality on the command line
Copyright (C) 2016,2017 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

#define VERSION \
"strftime version 1.2.0\n" \
"Copyright (C) 2016,2017 Kyle J. McKay <mackyle at gmail dot com>\n" \
"License GPLv2+: GNU GPL version 2 or later.\n" \
"<http://gnu.org/licenses/gpl2.html>\n" \
"This is free software: you are free to change and redistribute it.\n" \
"There is NO WARRANTY, to the extent permitted by law.\n"

#define USAGE \
"Usage: strftime [option...] [\"strftime(3) format\" [<epochsecs> [<offset>]]]\n" \
"  --help/-h     show this help\n" \
"  --version/-V  show version/license info\n" \
"  --locale/-l   use default locale rather than \"C\" locale\n" \
"  --adjust/-a n add n (which may be negative) to <epochsecs> before formatting\n" \
"  --            terminate options, next arg is format even if it starts with -\n" \
"  <epochsecs>   if omitted or empty string ('') use current time\n" \
"  <offset>      must be [+|-]HH[MM[SS]] same meaning as strftime(3) '%z' value\n" \
"If \"strftime(3) format\" is omitted (or '') then \"%a, %e %b %Y %T %z\" is used.\n" \
"If <offset> is omitted default timezone (taking TZ into account) will be used.\n" \
"If <offset> is NOT omitted then a strftime(3) '%Z' value will format as either\n" \
"\"unknown\" or \"UTC\" (for a zero offset).  Note that except for the time zone\n" \
"name, `date` and `strftime '%a %b %e %T %Z %Y' $(date +'%s %z')` should match.\n" \
"If --locale is NOT used the default format strips leading spaces from %e value.\n" \
"The %N format specifier is also supported for a 9-digit nanoseconds but the\n" \
"last three digits will always be zero in this implementation and only the first\n" \
"occurrence of %N will be replaced.  Additionally, if the current time is used\n" \
"along with %N then a 1 ms sleep will occur before and after gettimeofday().\n"

#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#define DEFAULT_FORMAT "%a, %e %b %Y %T %z"
#define MAXLENGTH 4096

static char buff[MAXLENGTH];
static char tzval[20]; /* TZ=unknown+HH:MM:SS\0 */

int main(int argc, char *argv[])
{
	int arg = 1;
	char *bp = buff;
	struct timeval tv;
	char *fmt = DEFAULT_FORMAT;
	int defaultformat = 1;
	int dosetlocale = 0;
	int needtime = 1;
	struct tm localvals;
	long adjust = 0;
	size_t fmtlen, i, firstN;

	tv.tv_usec = 0;
	while (arg < argc && *argv[arg] == '-') {
		if (!strcmp(argv[arg], "--help") || !strcmp(argv[arg], "-h")) {
			printf("%s", USAGE);
			return 0;
		}
		if (!strcmp(argv[arg], "--version") || !strcmp(argv[arg], "-V")) {
			printf("%s", VERSION);
			return 0;
		}
		if (!strcmp(argv[arg], "--locale") || !strcmp(argv[arg], "-l")) {
			dosetlocale = 1;
			++arg;
			continue;
		}
		if (!strcmp(argv[arg], "--adjust") || !strcmp(argv[arg], "-a")) {
			char *end;

			if (++arg >= argc || !*argv[arg]) {
				fprintf(stderr, "strftime: missing --adjust value "
					"(see strftime -h)\n");
				return 1;
			}
			adjust = strtol(argv[arg], &end, 10);
			if (*end) {
				fprintf(stderr, "strftime: invalid number: %s\n",
					argv[arg]);
				return 2;
			}
			++arg;
			continue;
		}
		if (!strcmp(argv[arg], "--")) {
			++arg;
			break;
		}
		fprintf(stderr, "strftime: invalid option: %s (see strftime -h)\n",
			argv[arg]);
		return 1;
	}
	if (argc - arg > 3) {
		fprintf(stderr, "strftime: invalid arguments (see strftime -h)\n");
		return 1;
	}
	if (argc - arg >= 1) {
		if (*argv[arg]) {
			fmt = argv[arg];
			defaultformat = 0;
		}
		if (argc - arg >= 2) {
			if (*argv[arg+1]) {
				char *end;
				long l = strtol(argv[arg+1], &end, 10);

				if (*end) {
					fprintf(stderr, "strftime: invalid number: %s\n",
						argv[arg+1]);
					return 2;
				}
				tv.tv_sec = (time_t)l;
				needtime = 0;
			}
			if (argc - arg >= 3) {
				const char *o = argv[arg+2];
				size_t l = strlen(o);
				char tzsign = '-';
				char *d = tzval;

				if (*o == '+' || *o == '-') {
					tzsign = (*o == '+') ? '-' : '+';
					++o;
					--l;
				}
				if (l < 2 || l > 6 || (l & 0x1) || l != strspn(o, "0123456789")) {
					fprintf(stderr, "strftime: invalid offset: %s\n",
						argv[arg+2]);
					return 2;
				}
				if (l == strspn(o, "0")) {
					memcpy(d, "TZ=UTC", 6);
					d += 6;
				} else {
					memcpy(d, "TZ=unknown", 10);
					d += 10;
					*d++ = tzsign;
					*d++ = *o++;
					*d++ = *o++;
					if (*o) {
						*d++ = ':';
						*d++ = *o++;
						*d++ = *o++;
					}
					if (*o) {
						*d++ = ':';
						*d++ = *o++;
						*d++ = *o++;
					}
				}
				*d = '\0';
				putenv(tzval);
			}
		}
	}
	if (dosetlocale) {
		setlocale(LC_ALL, "");
	}
	tzset();
	fmtlen = strlen(fmt);
	firstN = 0;
	for (i=0; i<fmtlen; ++i) {
		if (fmt[i] != '%')
			continue;
		++i;
		if (fmt[i] == '%')
			continue;
		if (fmt[i] == 'N') {
			fmt[i-1] = '\0';
			firstN = i + 1;
			break;
		}
	}
	if (needtime) {
		struct timespec milli;
		if (firstN) {
			milli.tv_sec = 0;
			milli.tv_nsec = 1000;
			nanosleep(&milli, NULL);
		}
		gettimeofday(&tv, NULL);
		if (firstN)
			nanosleep(&milli, NULL);
	}
	tv.tv_sec += (time_t)adjust;
	localvals = *localtime(&tv.tv_sec);
	buff[0] = '\0';
	if (*fmt && !strftime(buff, sizeof(buff), fmt, &localvals)) {
		fprintf(stderr, "strftime: format string too long\n");
		return 3;
	}
	if (firstN) {
		size_t left = sizeof(buff) - (fmtlen = strlen(buff));
		if (left < 10) {
			fprintf(stderr, "strftime: format string too long\n");
			return 3;
		}
		snprintf(buff + fmtlen, left, "%06u000", (unsigned)tv.tv_usec);
		fmtlen += 9;
		left -= 9;
		fmt += firstN;
		if (*fmt) {
			if (!strftime(buff + fmtlen, left, fmt, &localvals)) {
				fprintf(stderr, "strftime: format string too long\n");
				return 3;
			}
		}
	}
	if (defaultformat && !dosetlocale && buff[3] == ',' && buff[5] == ' ') {
		memmove(buff+1, buff, 4);
		++bp;
	}
	printf("%s\n", bp);
	return 0;
}
