/*

rangecgi.c -- rangecgi utility to serve multiple files as one with range support
Copyright (C) 2014,2015,2016,2019 Kyle J. McKay
All rights reserved

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  This utility serves multiple files (currently exactly two) as though they
  were one file and allows a continuation download using the "Range:" header.

  Only GET and HEAD requests are supported with either no "Range:" header or
  a "Range:" header with exactly one range.

  USAGE:
    rangecgi ([--etag] | [-c <content-type>] [-e <days>]) [-m 0|1|2] file1 file2

  If --etag is given then all environment variables are ignored and the
  computed ETag value (with the "", but without the "ETag:" prefix part) is
  output to standard output on success.  Otherwise there is no output and the
  exit code will be non-zero.

  If --etag is given then no other options except -m are allowed.  If
  -c <content-type> is given then the specified content type will be used as-is
  for the returned item.  If -e <days> is given then a cache-control and expires
  header will be output with the expiration set that many days into the future.

  The default is "-m 0" which means use the latest mtime of the given files
  when computing the ETag value.  With "-m 1" always use the mtime from the
  first file and with "-m 2" always use the mtime from the second file.

  Other CGI parameters MUST be passed as environment variables in particular
  REQUEST_METHOD MUST be set and to request a range, HTTP_RANGE MUST be set.
  HTTP_IF_RANGE MAY be set.  No other environment variables are examined.

  Exit code 0 for CGI success (Status: header etc. output)
  Exit code 1 for no REQUEST_METHOD.
  Exit code 2 for file1 and/or file2 not given or wrong type or bad option.
  Exit code 3 for --etag mode and file1 and/or file2 could not be opened.

  If file1 and/or file2 is not found a 404 status will be output.

  Normally a front end script will begin processing the initial CGI request
  from the web server and then pass it on to this utility as appropriate.

  If a "Range:" header is present and a non-empty "If-Range:" header is also
  present then if the value in the "If-Range:" header does not exactly match
  the computed "ETag:" value then the "Range:" header will be silently ignored.
*/

#undef _FILE_OFFSET_BITS
#define _FILE_OFFSET_BITS 64
#include <sys/types.h>
#include <sys/uio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>
#ifdef __APPLE__
#include <AvailabilityMacros.h>
#ifndef MAC_OS_X_VERSION_10_5
#define MAC_OS_X_VERSION_10_5 1050
#endif
#if MAC_OS_X_VERSION_MIN_REQUIRED < MAC_OS_X_VERSION_10_5
typedef struct stat statrec;
#define fstatfunc(p,b) fstat(p,b)
#else
typedef struct stat64 statrec;
#define fstatfunc(p,b) fstat64(p,b)
#endif
#else
typedef struct stat statrec;
#define fstatfunc(p,b) fstat(p,b)
#endif
typedef unsigned long long bignum;

static void errorfail_(unsigned code, const char *status, const char *extrahdr)
{
  (void)code;
  (void)status;
  (void)extrahdr;
  exit(3);
}

static void errorexit_(unsigned code, const char *status, const char *extrahdr)
{
  printf("Status: %u %s\r\n", code, status);
  printf("%s\r\n", "Expires: Fri, 01 Jan 1980 00:00:00 GMT");
  printf("%s\r\n", "Pragma: no-cache");
  printf("%s\r\n", "Cache-Control: no-cache,max-age=0,must-revalidate");
  printf("%s\r\n", "Accept-Ranges: bytes");
  if (extrahdr)
    printf("%s\r\n", extrahdr);
  printf("%s\r\n", "Content-Type: text/plain; charset=utf-8; format=fixed");
  printf("%s\r\n", "");
  printf("%s\n", status);
  fflush(stdout);
  exit(0);
}

static void emithdrs(const char *ct, int exp, time_t lm, const char *etag,
                     bignum tl, int isr, bignum r1, bignum r2)
{
  struct tm gt;
  char dtstr[32];
  const char *xtra = "";

  if (isr)
    if (isr > 0)
      printf("Status: %u %s\r\n", 206, "Partial Content");
    else
      printf("Status: %u %s\r\n", 416, "Requested Range Not Satisfiable");
  else
    printf("Status: %u %s\r\n", 200, "OK");
  if (exp > 0) {
    time_t epsecs = time(NULL);
    long esecs = 86400 * exp;
    gt = *gmtime(&epsecs);
    strftime(dtstr, sizeof(dtstr), "%a, %d %b %Y %H:%M:%S GMT", &gt);
    printf("Date: %s\r\n", dtstr);
    epsecs += esecs;
    gt = *gmtime(&epsecs);
    strftime(dtstr, sizeof(dtstr), "%a, %d %b %Y %H:%M:%S GMT", &gt);
    printf("Expires: %s\r\n", dtstr);
    printf("Cache-Control: public,max-age=%ld\r\n", esecs);
  } else if (!exp) {
    printf("%s\r\n", "Expires: Fri, 01 Jan 1980 00:00:00 GMT");
    printf("%s\r\n", "Pragma: no-cache");
    printf("%s\r\n", "Cache-Control: no-cache,max-age=0,must-revalidate");
  }
  printf("%s\r\n", "Accept-Ranges: bytes");
  gt = *gmtime(&lm);
  strftime(dtstr, sizeof(dtstr), "%a, %d %b %Y %H:%M:%S GMT", &gt);
  printf("Last-Modified: %s\r\n", dtstr);
  if (etag)
    printf("ETag: %s\r\n", etag);
  if (!isr) {
    printf("Content-Length: %llu\r\n", tl);
  } else if (isr > 0) {
    printf("Content-Length: %llu\r\n", r2 - r1 + 1);
    printf("Content-Range: bytes %llu-%llu/%llu\r\n", r1, r2, tl);
  } else {
    printf("Content-Range: bytes */%llu\r\n", tl);
  }
  if (isr >= 0) {
    if (!ct || !*ct)
      ct = "application/octet-stream";
    printf("Content-Type: %s\r\n", ct);
    printf("%s\r\n", "Vary: Accept-Encoding");
  } else {
    printf("%s\r\n", "Content-Type: text/plain; charset=utf-8; format=fixed");
    xtra = "Requested Range Not Satisfiable\n";
  }
  printf("\r\n%s", xtra);
}

static void error416(time_t lm, const char *etag, bignum tl)
{
  emithdrs(NULL, -1, lm, etag, tl, -1, 0, 0);
  fflush(stdout);
  exit(0);
}

static void die(const char *msg)
{
  fprintf(stderr, "%s\n", msg);
  fflush(stderr);
  exit(2);
}

static void readx(int fd, void *buf, size_t count)
{
  char *buff = (char *)buf;
  int err = 0;
  while (count && (!err || err == EINTR || err == EAGAIN || err == EWOULDBLOCK)) {
    ssize_t amt = read(fd, buff, count);
    if (amt == -1) {
      err = errno;
      continue;
    }
    err = 0;
    if (!amt)
      break;
    buff += (size_t)amt;
    count -= (size_t)amt;
  }
  if (count) {
    if (err)
      die("failed reading file (error)");
    else
      die("failed reading file (EOF)");
  }
}

static void writex(int fd, const void *buf, size_t count)
{
  const char *buff = (const char *)buf;
  int err = 0;
  while (count && (!err || err == EINTR || err == EAGAIN || err == EWOULDBLOCK)) {
    ssize_t amt = write(fd, buff, count);
    if (amt == -1) {
      err = errno;
      continue;
    }
    err = 0;
    if (!amt)
      break;
    buff += (size_t)amt;
    count -= (size_t)amt;
  }
  if (count) {
    if (err)
      die("failed writing file (error)");
    else
      die("failed writing file (EOF)");
  }
}

#define SIZEPWR 15
static char dumpbuff[1U << SIZEPWR];
void dumpfile(int fd, bignum start, bignum len)
{
  off_t loc = lseek(fd, (off_t)start, SEEK_SET);
  size_t maxread;
  if (loc != (off_t)start)
    die("lseek failed");
  if (start & ((bignum)(sizeof(dumpbuff) - 1)))
    maxread = sizeof(dumpbuff) - (size_t)(start & ((bignum)(sizeof(dumpbuff) - 1)));
  else
    maxread = sizeof(dumpbuff);
  while (len) {
    size_t cnt = len > (bignum)maxread ? maxread : (size_t)len;
    readx(fd, dumpbuff, cnt);
    writex(STDOUT_FILENO, dumpbuff, cnt);
    len -= (bignum)cnt;
    maxread = sizeof(dumpbuff);
  }
}
#undef SIZEPWR

int main(int argc, char *argv[])
{
  int isetag = 0;
  void (*errorexit)(unsigned,const char *,const char *);
  statrec f1, f2;
  int e1, e2, i;
  bignum l1, l2, tl;
  bignum r1=0, r2=0;
  bignum start, length;
  time_t lm;
  const char *rm = NULL;
  const char *hr = NULL;
  const char *hir = NULL;
  const char *ct = NULL;
  int expdays = -1, opt_e = 0;
  /* "inode_inode-size-time_t_micros" each in hex up to 8 bytes gives */
  /* "16bytes_16bytes-16bytes-16bytes" plus NUL = 70 bytes (including "") */
  char etag[70];
  int fd1 = -1, fd2 = -1;
  int mno = 0;

  opterr = 0;
  for (;;) {
    int ch;
    if (optind < argc && !strcmp(argv[optind], "--etag")) {
      ch = -2;
      ++optind;
    } else {
      ch = getopt(argc, argv, "c:e:m:");
    }
    if (ch == -1)
      break;
    switch (ch) {
      case -2:
        isetag = 1;
        break;
      case 'c':
        ct = optarg;
        break;
      case 'e':
      {
        int v, n;
        if (sscanf(optarg, "%i%n", &v, &n) != 1 || n != (int)strlen(optarg))
          exit(2);
        expdays = v;
        opt_e = 1;
        break;
      }
      case 'm':
        if (!optarg[0] || optarg[1])
          exit(2);
        if (optarg[0] != '0' && optarg[0] != '1' && optarg[0] != '2')
          exit(2);
        mno = optarg[0] - '0';
        break;
      default:
        exit(2);
    }
  }
  if (argc - optind != 2)
    exit(2);
  i = optind;

  if (isetag) {
    if (ct || opt_e)
      exit(2);
    errorexit = errorfail_;
  } else {
    rm = getenv("REQUEST_METHOD");
    if (!rm || !*rm)
      exit(1);
    hr = getenv("HTTP_RANGE");
    if (hr)
      hir = getenv("HTTP_IF_RANGE");
    errorexit = errorexit_;
    if (strcmp(rm, "GET") && strcmp(rm, "HEAD"))
      errorexit(405, "Method Not Allowed", "Allow: GET,HEAD");
  }

  fd1 = open(argv[i], O_RDONLY);
  e1 = fd1 >= 0 ? 0 : errno;
  fd2 = open(argv[i+1], O_RDONLY);
  e2 = fd2 >= 0 ? 0 : errno;
  if (e1 == EACCES || e2 == EACCES)
    errorexit(403, "Forbidden", NULL);
  if (e1 == ENOENT || e1 == ENOTDIR || e2 == ENOENT || e2 == ENOTDIR)
    errorexit(404, "Not Found", NULL);
  e1 = fstatfunc(fd1, &f1) ? errno : 0;
  e2 = fstatfunc(fd2, &f2) ? errno : 0;
  if (e1 || e2)
    errorexit(500, "Internal Server Error", NULL);
  if (!S_ISREG(f1.st_mode) || !S_ISREG(f2.st_mode))
    errorexit(500, "Internal Server Error", NULL);
  if (mno == 1)
    lm = f1.st_mtime;
  else if (mno == 2)
    lm = f2.st_mtime;
  else if (f1.st_mtime >= f2.st_mtime)
    lm = f1.st_mtime;
  else
    lm = f2.st_mtime;
  l1 = f1.st_size;
  l2 = f2.st_size;
  tl = l1 + l2;
  sprintf(etag, "\"%llx_%llx-%llx-%llx\"", (unsigned long long)f1.st_ino,
    (unsigned long long)f2.st_ino, tl, (unsigned long long)lm * 1000000U);

  if (isetag) {
    close(fd2);
    close(fd1);
    printf("%s\n", etag);
    exit(0);
  }

  if (hir && *hir && strcmp(etag, hir))
    hr = NULL;

  if (hr && !tl)
    error416(lm, etag, tl);  /* Range: not allowed on zero length content */

  if (hr) {
    /* Only one range may be specified and it must be bytes */
    /* with a 2^64 value we could have "Range: bytes = 20-digit - 20-digit" */
    int pos = -1;
    int s = sscanf(hr, " %*[Bb]%*[Yy]%*[Tt]%*[Ee]%*[Ss] = %n", &pos);
    if (s != 0 || pos < 6 || strchr(hr, ','))
      errorexit(400, "Bad Request", NULL);
    hr += pos;
    if (*hr == '-') {
      bignum trail;
      ++hr;
      /* It's a request for the trailing part */
      if (strchr(hr, '-'))
        errorexit(400, "Bad Request", NULL);
      pos = -1;
      s = sscanf(hr, " %llu%n", &trail, &pos);
      if (s != 1 || pos < 1 || hr[pos])
        errorexit(400, "Bad Request", NULL);
      if (!trail || trail > tl)
        error416(lm, etag, tl);
      r1 = tl - trail;
      r2 = tl - 1;
    } else {
      pos = -1;
      s = sscanf(hr, "%llu - %n", &r1, &pos);
      if (s != 1 || pos < 2)
        errorexit(400, "Bad Request", NULL);
      hr += pos;
      if (*hr) {
        if (*hr == '-')
          errorexit(400, "Bad Request", NULL);
        pos = -1;
        s = sscanf(hr, "%llu %n", &r2, &pos);
        if (s != 1 || pos < 1 || hr[pos])
          errorexit(400, "Bad Request", NULL);
      } else {
        r2 = tl - 1;
      }
      if (r1 > r2 || r2 >= tl)
        error416(lm, etag, tl);
    }
    start = r1;
    length = r2 - r1 + 1;
  } else {
    start = 0;
    length = tl;
  }
  
  emithdrs(ct, expdays, lm, etag, tl, hr?1:0, r1, r2);
  fflush(stdout);

  if (strcmp(rm, "HEAD")) {
    if (start < l1) {
      bignum dl = l1 - start;
      if (dl > length) dl = length;
      dumpfile(fd1, start, dl);
      start += dl;
      length -= dl;
    }
    if (length && start >= l1) {
      bignum dl;
      start -= l1;
      dl = l2 - start;
      if (dl > length) dl = length;
      dumpfile(fd2, start, dl);
    }
  }

  close(fd2);
  close(fd1);
  return 0;
}
