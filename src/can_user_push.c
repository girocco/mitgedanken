/*

can_user_push.c -- check project membership in group file
Copyright (c) 2013 Kyle J. McKay.  All rights reserved.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

/*
  USAGE: can_user_push groupname [username]
  Exit code 0 for yes, non-zero for no
  If username is an explicit member of groupname returns yes
  If username is omitted, the LOGNAME environment variable will be used
  Wrong number of or bad arguments exits with non-zero
*/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <grp.h>

int main(int argc, char *argv[])
{
  const char *group;
  const char *user;
  struct group *ent;
  int found = 0;

  if (argc < 2 || argc > 3)
    return EXIT_FAILURE;

  group = argv[1];
  user = (argc >= 3) ? argv[2] : getenv("LOGNAME");
  if (!group || !*group || !user || !*user)
    return EXIT_FAILURE;

  ent = getgrnam(group);
  if (ent) {
    char **memb;
    memb = ent->gr_mem;
    if (memb) {
      for (; *memb; ++memb) {
        if (strcmp(*memb, user) == 0) {
          found = 1;
          break;
        }
      }
    }
  }

  return found ? EXIT_SUCCESS : EXIT_FAILURE;
}
