#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::Config;
use Girocco::CGI;
use Girocco::Project;
use Girocco::Util;
use CGI;

our $cgi = CGI->new;
$cgi->charset('UTF-8');

my $pname = to_utf8($cgi->param('p'), 1);
my $ctags = to_utf8($cgi->param('t'), 1);
defined $pname or $pname = '';
defined $ctags or $ctags = '';
$pname =~ s/\.git$//;

if ($cgi->request_method ne 'POST' || $pname eq '') {
	print $cgi->header(-status=>403);
	print "<p>Invalid data. Go away, sorcerer.</p>\n";
	exit;
}

my $proj = Girocco::Project::does_exist($pname, 1) && Girocco::Project->load($pname);
if (not $proj) {
	print $cgi->header(-status=>404);
	print "<p>Project \"".html_esc($pname)."\" does not exist.</p>";
	exit;
}

if ($ctags =~ /[^ a-zA-Z0-9:.+#_-]/) {
	print $cgi->header(-status=>403);
	print "<p>Content tag(s) '".html_esc($ctags)."' contain evil characters.</p>";
	exit;
}

my $oldmask = umask();
umask($oldmask & ~0060);
my $changed;
foreach my $ctag (split(/ /, $ctags)) {
	$changed = 1 if $proj->add_ctag($ctag, 1);
}
if ($changed) {
	$proj->_set_changed;
	$proj->_set_forkchange;
}
umask($oldmask);

print $cgi->header(-status=>303, -location=>"@{[url_path($Girocco::Config::gitweburl)]}/$pname.git");
