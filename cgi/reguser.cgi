#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('User Registration');
my $cgi = $gcgi->cgi;

unless ($Girocco::Config::manage_users) {
	print "<p>I don't manage users.</p>";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

my $y0 = $cgi->param('y0') || '';
if ($cgi->param('name') && $y0 eq 'Register' && $cgi->request_method eq 'POST') {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $gcgi->wparam('name');
	if (!Girocco::User::valid_name($name)) {
		my $htmlname = html_esc($name);
		$gcgi->err(
			"Invalid user name \"$htmlname\" ".
			"(contains bad characters or is a reserved user name). ".
			"See <a href=\"@{[url_path($Girocco::Config::htmlurl)]}/names.html#user\">names</a>.");
		$gcgi->err_check;
	} elsif (Girocco::User::does_exist($name)) {
		$gcgi->err("A user with that name already exists.");
		$gcgi->err_check;
	} else {
		my $user = Girocco::User->ghost($name);
		if ($user->cgi_fill($gcgi)) {
			$user->conjure;
			my $keysdiv = '';
			my $keylist = $user->keys_html_list;
			if ($keylist) {
				$keysdiv = <<EOT;

<div id="keys"><p>The following keys have been registered for user $name as
shown below along with their <tt>ssh-keygen -l</tt> fingerprint:</p>
$keylist</div>
EOT
			}
			print <<EOT;
<p>User $name successfully registered.</p>
<p>Project administrators can now give you push access to their projects.</p>
<p>Congratulations, and have a lot of fun!</p>$keysdiv
EOT
			exit;
		}
	}
}

my $httpspara = '';
$httpspara = <<EOT if $Girocco::Config::httpspushurl;
<p>Please be sure to include at least one RSA key (starts with the <tt>ssh-rsa</tt> prefix) in
order to enable HTTPS pushing. <sup class="sup"><span><a href="@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html">(learn more)</a></span></sup><br />
X.509 (e.g. OpenSSL) format public keys can be converted to SSH .pub format with the
<a href="http://repo.or.cz/w/ezcert.git/blob/master:/ConvertPubKey">ConvertPubKey</a> utility thus obviating the
need for OpenSSH if all pushing is to be done using HTTPS (see the example in the TIPS section of the <tt>ConvertPubKey -h</tt> output).</p>
EOT
my $dsablurb = '';
$dsablurb = ' or <tt>~/.ssh/id_dsa.pub</tt>' unless $Girocco::Config::disable_dsa;
print <<EOT;
<p>Here you can register a user.
You need to register a user so that you can push to the hosted projects.</p>
<p>If you already have a user account you may
<a href="@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi">edit user</a> instead.</p>
<p>SSH (the <tt>ssh</tt> protocol) or HTTPS is used for pushing, your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected;
use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt>$dsablurb.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.
You can paste multiple keys in the box below, each on a separate line.
Paste each key <em>including</em> the <tt>ssh-</tt>whatever prefix and email-like postfix.</p>
$httpspara<p>We won't bother to verify your email contact,
but fill in something sensible in your own interest
so that we may contact you or confirm your identity should the need arise.
We also need to send you an e-mail if you want to update your SSH keys later.</p>
$Girocco::Config::legalese
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/reguser.cgi">
<table class="form">
<tr><td class="formlabel">Login:</td><td><input type="text" name="name" /></td></tr>
<tr><td class="formlabel">Email:</td><td><input type="text" name="email" /></td></tr>
<tr><td class="formlabel">Public SSH key(s):</td><td><textarea wrap="off" name="keys" rows="5" cols="80"></textarea></td></tr>
<tr style="display:none"><td class="formlabel">Anti-captcha (leave empty!):</td><td><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Register" /></td></tr>
</table>
</form>
EOT
