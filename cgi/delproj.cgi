#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Removal');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1) && !Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name,1)) {
	print "<p>Sorry but this project does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
if (!$proj) {
	print "<p>not found project $name, that's really weird!</p>\n";
	exit;
}
my $escname = $name;
$escname =~ s/[+]/%2B/g;
$proj->{cpwd} = $cgi->param('cpwd');
my $isempty = !$proj->{mirror} && $proj->is_empty;

if ($proj->has_forks()) {
	print "<p>Sorry but this project has forks associated. Such projects cannot be removed. Please tell the administrator if you really want to.</p>\n";
	exit;
}

my $y0 = $cgi->param('y0') || '';
if ($y0 && $cgi->request_method eq 'POST' && $proj->authenticate($gcgi)) {
	# submitted
	if (!$proj->{mirror} && !$isempty && !$cgi->param('auth')) {
		if ($y0 ne 'Send authorization code') {
			print "<p>Invalid data. Go away, sorcerer.</p>\n";
			exit;
		}

		valid_email($proj->{email}) or die "Sorry, this project cannot be removed.";

		my $auth = $proj->gen_auth('DEL');

		# Send auth mail
		defined(my $MAIL = mailer_pipe '-s', "[$Girocco::Config::name] Project removal authorization", $proj->{email}) or
			die "Sorry, could not send authorization code: $!";
		print $MAIL <<EOT;
Hello,

Somebody requested a project removal authorization code to be sent for
project $name on $Girocco::Config::name.  Since you are the project admin,
you receive the authorization code.  If you don't want to actually remove
project $name, just ignore this e-mail.  Otherwise, use this code
within 24 hours:

$auth

In case you did not request the removal authorization code, we apologize.

Should you run into any problems, please let us know.

Have fun!
EOT
		close $MAIL;

		print <<EOT;
<p>The project admin should shortly receive an e-mail containing a project
removal authorization code.  Please enter this code below to remove project
$name from $Girocco::Config::name.  The code will expire in 24 hours or after
you have used it.</p>
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi">
<input type="hidden" name="name" value="$name" />
<input type="hidden" name="cpwd" value="$proj->{cpwd}" />
<p>Authorization code: <input name="auth" size="50" /></p>
<p><input type="submit" name="y0" value="Remove" /></p>
</form>
EOT
		exit;
	}
	if ($y0 ne "Remove") {
		print "<p>Invalid data. Go away, sorcerer.</p>\n";
		exit;
	}
	if (!$proj->{mirror} && !$isempty) {
		$proj->{auth} && $proj->{authtype} && $proj->{authtype} eq 'DEL' or do {
			print <<EOT;
<p>There currently isn't any project removal authorization code on file for
project $name.  Please <a href="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi?name=$escname"
>generate one</a>.</p>
EOT
			exit;
		};
		my $auth = $gcgi->wparam('auth');
		if ($auth ne $proj->{auth}) {
			print <<EOT;
<p>Invalid authorization code, please re-enter or
<a href="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi?name=$escname"
>generate a new one</a>.</p>
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi">
<input type="hidden" name="name" value="$name" />
<input type="hidden" name="cpwd" value="$proj->{cpwd}" />
<p>Authorization code: <input name="auth" size="50" /></p>
<p><input type="submit" name="y0" value="Remove" /></p>
</form>
EOT
			exit;
		}
		$proj->del_auth;
	}
	if (!$proj->{mirror} && !$isempty) {
		# archive the non-empty, non-mirror project before calling delete
		$proj->archive_and_delete;
	} else {
		$proj->delete;
	}
	print "<p>Project successfully removed. Have a nice day.</p>\n";
	exit;
}

my $fetchy = $Girocco::Config::gitpullurl || $Girocco::Config::httppullurl ||
	$Girocco::Config::pushurl || $Girocco::Config::httpspushurl || $Girocco::Config::gitweburl;
my $url = $proj->{mirror} ? $proj->{url} : "$fetchy/$name.git";
my $type = $proj->{mirror} ? "mirrored " : ($isempty ? "empty " : "");
my $label = ($proj->{mirror} || $isempty) ? "Remove" : "Send authorization code";

print <<EOT;
<p>Please confirm that you are going to remove ${type}project
$name ($url) from the site.</p>
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/delproj.cgi">
<input type="hidden" name="name" value="$name" />
EOT
if ($Girocco::Config::project_passwords) {
	print <<EOT;
<p>Admin password: <input type="password" name="cpwd" /> <sup class="sup"><span><a
href="@{[url_path($Girocco::Config::webadmurl)]}/pwproj.cgi?name=$escname">(forgot password?)</a></span></sup></p>
EOT
}
print <<EOT;
<p><input type="submit" name="y0" value="$label" /></p>
</form>
EOT
