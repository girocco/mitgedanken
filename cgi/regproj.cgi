#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Registration');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');
defined($name) or $name = '';

my $fork = $cgi->param('fork');
if (defined($fork)) {
	$fork =~ s/\.git$//;
	$name = "$fork/$name";
}
my $escname = $name;
$escname =~ s/[+]/%2B/g;
my $mirror_mode_set = 1;
if ($Girocco::Config::mirror && $Girocco::Config::push) {
	$mirror_mode_set = 0 unless ($Girocco::Config::initial_regproj_mode||'') eq 'mirror';
}
my %values = (
	desc => '',
	email => '',
	hp => '',
	mirror => $mirror_mode_set,
	cleanmirror => 1,
	notifymail => '',
	reverseorder => 1,
	summaryonly => '',
	notifytag => '',
	notifyjson => '',
	notifycia => '',
	README => '',
	source => 'Anywhere',
	url => '',
	Anywhere_url => '',
	GitHub_i0 => '',
	GitHub_i1 => '',
	Gitorious_i0 => '',
	Gitorious_i1 => '',
);
$values{'mirror'} = 0 unless $Girocco::Config::mirror;
$values{'mirror'} = 0 if $Girocco::Config::push && $name =~ m#/#;
if (@{[$name =~ m#/#g]} > 5) {
	$gcgi->err("Unable to create a fork more than five levels deep, please fork the parent project instead.");
	exit;
}
my $y0 = $cgi->param('y0') || '';
if ($cgi->param('mode') && $y0 eq 'Register' && $cgi->request_method eq 'POST') {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $validname = 1;
	if (Girocco::Project::valid_name($name)) {
		Girocco::Project::does_exist($name,1)
			and $gcgi->err("Project with the name '$name' already exists.");
	} else {
		$validname = 0;
		if ($name =~ /^(.*)\.git$/i && Girocco::Project::valid_name($1)) {
			$gcgi->err("Project name should not end with <tt>.git</tt> - I'll add that automagically.");
		} else {
			my $htmlname = html_esc($name);
			$gcgi->err(
				"Invalid project name \"$htmlname\" ".
				"(contains bad characters or is a reserved project name). ".
				"See <a href=\"@{[url_path($Girocco::Config::htmlurl)]}/names.html\">names</a>.");
		}
	}

	my $check = $cgi->param('mail');
	$check =~ tr/ \t/ /s; $check =~ s/^ //; $check =~ s/ $//;
	if ($check !~ /^(?:(?:(?:the )?sun)|(?:sol))$/i) {
		$gcgi->err("Sorry, invalid captcha check.");
	}

	foreach my $key (keys(%values)) {
		$values{$key} = html_esc($cgi->param($key));
	}
	my $mirror = ($cgi->param('mode')||'') eq 'mirror';
	$values{'mirror'} = $Girocco::Config::mirror && $mirror ? 1 : 0;

	if ($mirror and $Girocco::Config::mirror_sources and not $cgi->param('url')) {
		my $src = $cgi->param('source'); $src ||= '';
		my $source; $src and $source = (grep { $_->{label} eq $src } @$Girocco::Config::mirror_sources)[0];
		$source or $gcgi->err("Invalid or no mirror source $src specified");

		my $n = $source->{label};
		my $u = $source->{url};
		if ($source->{inputs}) {
			for my $i (0..$#{$source->{inputs}}) {
				my $v = $cgi->param($n.'_i'.$i);
				unless ($v) {
					$gcgi->err("Source specifier '".$source->{inputs}->[$i]->{label}."' not filled.");
					next;
				}
				my $ii = $i + 1;
				$u =~ s/%$ii/$v/g;
			}
		} else {
			$u = $cgi->param($n.'_url');
			$u or $gcgi->err("Source URL not specified");
		}
		$cgi->param('url', $u);
	}

	my $proj = Girocco::Project->ghost($name, $mirror) if $validname;
	if ($validname && $proj->cgi_fill($gcgi)) {
		if ($mirror) {
			unless ($Girocco::Config::mirror) {
				$gcgi->err("Mirroring mode is not enabled at this site.");
				exit;
			}
			$proj->premirror;
			$proj->clone;
			print "<p>Please <a href=\"@{[url_path($Girocco::Config::webadmurl)]}/mirrorproj.cgi?name=$escname\">pass onwards</a>.</p>\n";
			print "<script language=\"javascript\">document.location='@{[url_path($Girocco::Config::webadmurl)]}/mirrorproj.cgi?name=$escname'</script>\n";

		} else {
			unless ($Girocco::Config::push) {
				$gcgi->err("Push mode is not enabled at this site.");
				exit;
			}
			$proj->conjure;
			print <<EOT;
<p>Project <a href="@{[url_path($Girocco::Config::gitweburl)]}/$name.git">$name</a> successfully set up.</p>
EOT
			my @pushurls = ();
			push(@pushurls, "<tt>$Girocco::Config::pushurl/$name.git</tt>") if $Girocco::Config::pushurl;
			push(@pushurls, "<tt>$Girocco::Config::httpspushurl/$name.git</tt> " .
				"<sup class=\"sup\"><span><a href=\"@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html\">(learn more)</a></span></sup>")
				if $Girocco::Config::httpspushurl;
			print "<p>The push URL(s) for the project: " . join(", ", @pushurls) . "</p>" if @pushurls;
			my @pullurls = ();
			push(@pullurls, $Girocco::Config::gitpullurl) if $Girocco::Config::gitpullurl;
			push(@pullurls, $Girocco::Config::httppullurl) if $Girocco::Config::httppullurl;
			print "<p>The read-only URL(s) for the project: <tt>" .
				join("/$name.git</tt>, <tt>", @pullurls) .
				"/$name.git</tt></p>" if @pullurls;
			my $regnotice = '';
			if ($Girocco::Config::manage_users) {
				$regnotice = <<EOT;
Everyone who wants to push must <a href="@{[url_path($Girocco::Config::webadmurl)]}/reguser.cgi">register oneself as a user</a> first.
(One user can have push access to multiple projects and multiple users can have push access to one project.)
EOT
			}
			my $pushy = $Girocco::Config::pushurl || $Girocco::Config::httpspushurl;
			my $pushyhint = '';
			$pushyhint = " # <span style='font-family:sans-serif;font-size:smaller;position:relative;bottom:1pt'>" .
				"<a href=\"@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html\">(learn more)</a></span>"
				if $pushy =~ /^https:/i;
			print <<EOT;
<p>You can <a href="@{[url_path($Girocco::Config::webadmurl)]}/editproj.cgi?name=$escname">assign users</a> now
- don't forget to assign yourself as a user as well if you want to push!
$regnotice
</p>
<p>Note that you cannot clone an empty repository since it contains no branches; you need to make the first push from an existing repository.
To import a new project, the procedure is roughly as follows:
<pre>
  \$ git init
  \$ git add
  \$ git commit
  \$ git remote add origin $pushy/$name.git$pushyhint
  \$ git push --all origin
</pre>
</p>
<p>Enjoy yourself, and have a lot of fun!</p>
EOT
		}
		exit;
	}
}

my $mirror_mode = {
	name => 'mirror',
	desc => 'our dedicated git monkeys will check another repository at a given URL every hour and mirror any new updates',
	pwpurp => 'mirroring URL'
};
my $push_mode = {
	name => 'push',
	desc => 'registered users with appropriate permissions will be able to push to the repository',
	pwpurp => 'list of users allowed to push'
};

my $me = $Girocco::Config::mirror ? $mirror_mode : undef;
my $pe = $Girocco::Config::push ? $push_mode : undef;
if ($me and $pe) {
	print <<EOT;
<p>At this site, you can host a project in one of two modes: $me->{name} mode and $pe->{name} mode.
In the <b>$me->{name} mode</b>, $me->{desc}.
In the <b>$pe->{name} mode</b>, $pe->{desc}.
You currently cannot switch freely between those two modes;
if you want to switch from mirroring to push mode or vice versa just delete and recreate
the project.</p>
EOT
} else {
	my $mode = $me ? $me : $pe;
	print "<p>This site will host your project in a <b>$mode->{name} mode</b>: $mode->{desc}.</p>\n";
}

my @pwpurp = ();
push @pwpurp, $me->{pwpurp} if $me;
push @pwpurp, $pe->{pwpurp} if $pe;
my $pwpurp = join(', ', @pwpurp);

if ($Girocco::Config::project_passwords) {
	print <<EOT;
<p>You will need the admin password to adjust the project settings later
($pwpurp, project description, ...).</p>
EOT
}

unless ($name =~ m#/#) {
	print <<EOT;
<p>Note that if your project is a <strong>fork of an existing project</strong>
(this does not mean anything socially bad), please instead go to the project's
gitweb page and click the 'fork' link in the top bar. This way, all of us
will save bandwidth and more importantly, your project will be properly categorized.</p>
EOT
	$me and print <<EOT;
<p>If your project is a fork but the existing project is not registered here yet, please
consider registering it first; you do not have to be involved in the project
in order to register it here as a mirror.</p>
EOT
} else {
	my $xname = $name; $xname =~ s#/$#.git#; #
	my ($pushnote1, $pushnote2);
	if ($pe) {
		$pushnote1 = " and you will need to push only the data <em>you</em> created, not the whole project";
		$pushnote2 = <<EOT;
 (That will be done automagically, you do not need to specify any extra arguments during the push.)
EOT
	}
	print <<EOT;
<p>Great, your project will be created as a subproject of the '$xname' project.
This means that it will be properly categorized$pushnote1.$pushnote2</p>
EOT
}

my $modechooser;
my $mirrorentry = '';
if ($me) {
	$mirrorentry = '<tr id="mirror_url"><td class="formlabel" style="vertical-align:middle">Mirror source:</td><td>';
	if (!$Girocco::Config::mirror_sources) {
		$mirrorentry .= "<input type='text' name='url' value='%values{'url'}' />";
	} else {
		$mirrorentry .= "<table>"."\n";
		foreach my $source (@$Girocco::Config::mirror_sources) {
			my $n = $source->{label};
			$mirrorentry .= '<tr><td class="formlabel">';
			$mirrorentry .= '<p><label><input type="radio" class="mirror_sources" name="source" value="'.$n.'"'.
				($n eq $values{'source'} ? ' checked="checked"' : '').' />';
			$mirrorentry .= $n;
			$mirrorentry .= '</label></p></td><td>';
			if ($source->{desc}) {
				$mirrorentry .= '<p>';
				$source->{link} and $mirrorentry .= '<a href="'.$source->{link}.'">';
				$mirrorentry .= $source->{desc};
				$source->{link} and $mirrorentry .= '</a>';
				$mirrorentry .= '</p>';
			}
			if (!$source->{inputs}) {
				$mirrorentry .= '<p>URL: <input type="text" name="'.$n.'_url" '.
					'value="'.$values{$n.'_url'}.
					'" onchange="set_mirror_source('."'".$n."'".')" /></p>';
			} else {
				$mirrorentry .= '<p>';
				my $i = 0;
				foreach my $input (@{$source->{inputs}}) {
					$mirrorentry .= $input->{label};
					my ($l, $v) = ($n.'_i'.$i, '');
					if ($cgi->param($l)) {
						$v = ' value="'.html_esc($cgi->param($l)).'"';
					}
					$mirrorentry .= ' <input type="text" name="'.$l.'"'.$v.
						' onchange="set_mirror_source('."'".$n."'".')" />';
					$mirrorentry .= $input->{suffix} if $input->{suffix};
					$mirrorentry .= '&#160; &#160;';
				} continue { $i++; }
				$mirrorentry .= '</p>';
			}
			$mirrorentry .= '</td></tr>'."\n";
		}
		$mirrorentry .= "</table>";
	}
	$mirrorentry .= '</td></tr>'."\n";;
	$mirrorentry .= '<tr id="mirror_refs"><td class="formlabel">Mirror refs:</td><td class="formdatatd">'.
			'<label title="Unchecking this will mirror the entire refs namespace which is usually unnecessary.  '.
			'Non-git sources always mirror the entire refs namespace regardless of this setting.">'.
			'<input type="checkbox" name="cleanmirror" value="1" '.($values{'cleanmirror'} ? 'checked="checked" ' : '').
			'style="vertical-align:middle" /><span style="vertical-align:middle; margin-left:0.5ex">'.
			'Only mirror <code>refs/heads/*</code>, <code>refs/tags/*</code> and <code>refs/notes/*</code></span></label></td></tr>'."\n"
		if grep(/cleanmirror/, @Girocco::Config::project_fields);
}
if ($me and $pe) {
	$modechooser = <<EOT;
<tr><td class="formlabel" style="vertical-align:middle">Hosting mode:</td><td><p>
<label><input type="radio" name="mode" value="mirror" id="mirror_radio"@{[$values{'mirror'}?' checked="checked"':'']} />Mirror mode</label><br />
<label><input type="radio" name="mode" value="push" id="push_radio"@{[$values{'mirror'}?'':' checked="checked"']} />Push mode</label>
</p></td></tr>
EOT
} else {
	$modechooser = '<input type="hidden" name="mode" value="'.($me ? $me->{name} : $pe->{name}).'" />';
}

my $forkentry = '';
if ($name =~ m#/#) {
	$name =~ s#^(.*)/##;
	$forkentry = '<input type="hidden" name="fork" value="'.$1.'" /><span class="formdata" style="padding-left:0.5ex">' .
		html_esc($1) . '/</span>';
}
$name = html_esc($name);

print <<EOT;
$Girocco::Config::legalese
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/regproj.cgi">
<table class="form">
<tr><td class="formlabel">Project name:</td>
<td>$forkentry<input type="text" name="name" value="$name" /><span class="formdata" style="padding-left:0">.git</span></td></tr>
EOT
if ($Girocco::Config::project_passwords) {
	print <<EOT;
<tr><td class="formlabel">Admin password (twice):</td><td><input type="password" name="pwd" /><br /><input type="password" name="pwd2" /></td></tr>
EOT
}
if ($Girocco::Config::project_owners eq 'email') {
	print <<EOT;
<tr><td class="formlabel">E-mail contact:</td><td><input type="text" name="email" value="@{[$values{'email'}]}" /></td></tr>
EOT
}
print $modechooser;
print $mirrorentry;

$gcgi->print_form_fields($Girocco::Project::metadata_fields, \%values, @Girocco::Config::project_fields);

print <<EOT;
EOT

print <<EOT;
<tr><td class="formlabel" style="line-height:inherit">Anti-captcha &#x2013; please<br />enter name of our nearest star:</td>
<td style="vertical-align:middle"><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Register" /></td></tr>
</table>
</form>
EOT
