#!/usr/bin/perl

# deluser.cgi -- support for user deletion via web
# Copyright (c) 2013 Kyle J. McKay.  All rights reserved.
# Portions (c) Petr Baudis <pasky@suse.cz> and (c) Jan Krueger <jk@jk.gs>
# License GPLv2+: GNU GPL version 2 or later.
# www.gnu.org/licenses/gpl-2.0.html
# This is free software: you are free to change and redistribute it.
# There is NO WARRANTY, to the extent permitted by law.

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;
binmode STDOUT, ':utf8';

my $gcgi = Girocco::CGI->new('User Removal');
my $cgi = $gcgi->cgi;

unless ($Girocco::Config::manage_users) {
	print "<p>I don't manage users.</p>";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

sub _auth_form {
	my ($name, $submit) = @_;
	print <<EOT;
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi">
<input type="hidden" name="name" value="$name" />
<p>Authorization code: <input name="auth" size="50" /></p>
<p><input type="submit" name="y0" value="$submit" /></p>
</form>
EOT
}

my $y0 = $cgi->param('y0') || '';
if ($cgi->param('name') && $y0 && $cgi->request_method eq 'POST') {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $gcgi->wparam('name');
	Girocco::User::does_exist($name, 1)
		or $gcgi->err("Username is not registered.");

	$gcgi->err_check and exit;

	my $user;
	($user = Girocco::User->load($name)) && valid_email($user->{email})
		or $gcgi->err("Username may not be removed.");

	$gcgi->err_check and exit;

	if (!$cgi->param('auth')) {
		if ($y0 ne 'Send authorization code') {
			print "<p>Invalid data. Go away, sorcerer.</p>\n";
			exit;
		}

		valid_email($user->{email}) or die "Sorry, this user cannot be removed.";

		my $auth = $user->gen_auth('DEL');

		# Send auth mail
		defined(my $MAIL = mailer_pipe '-s', "[$Girocco::Config::name] Account removal authorization", $user->{email}) or
			die "Sorry, could not send authorization code: $!";
		print $MAIL <<EOT;
Hello,

You have requested an authorization code be sent to you for removing
your account.  If you don't want to actually remove your account, just
ignore this e-mail.  Otherwise, use this code within 24 hours:

$auth

Should you run into any problems, please let us know.

Have fun!
EOT
		close $MAIL;

		print "<p>You should shortly receive an e-mail containing an authorization code.
			Please enter this code below to remove your account.
			The code will expire in 24 hours or after you have used it.</p>";
		_auth_form($name, "'Login'");
		exit;
	} else {
		if ($y0 ne "'Login'" && $y0 ne "Remove user account") {
			print "<p>Invalid data. Go away, sorcerer.</p>\n";
			exit;
		}

		$user->{auth} && $user->{authtype} eq 'DEL' or do {
			print "<p>There currently isn't any authorization code filed under your account.  ".
				"Please <a href=\"@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi\">generate one</a>.</p>";
			exit;
		};

		my $auth = $gcgi->wparam('auth');
		if ($auth ne $user->{auth}) {
			print "<p>Invalid authorization code, please re-enter or ".
				"<a href=\"@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi\">generate a new one</a>.</p>";
			_auth_form($name, "'Login'");
			exit;
		}

		my $conf = $gcgi->wparam('confirm') || '';
		if ($y0 ne 'Remove user account' || $conf ne $user->{name}) {
			my $blurb1 = '.';
			my $projectsinfo = '';
			my @projects = $user->get_projects;
			if (@projects) {
				$projectsinfo = projects_html_list({target=>"_blank", sizecol=>1, typecol=>1, changed=>1}, @projects);
				$blurb1 = ' and from the following projects:' if $projectsinfo;
			}
			my $ownedinfo = '';
			my @ownedprojects = filedb_grep($Girocco::Config::projlist_cache_dir.'/gitproj.list',
				sub {
					chomp;
					my ($proj, $hash, $owner) = split(/ /, $_, 3);
					if ($owner eq $user->{email}) {
						$proj;
					}
				}
			);
			if (@ownedprojects) {
				$ownedinfo = <<EOT;
<p>The following project(s) are owned by the same email address as user account '$user->{name}'
and <b>will NOT be removed</b>.  If desired, they can be removed from their project admin
page(s) (the "edit" link on the project page).</p>
EOT
				$ownedinfo .= projects_html_list(
					{target=>"_blank", sizecol=>1, typecol=>1, changed=>1}, @ownedprojects);
			}
			print <<EOT;
<p>Please confirm that you are going to remove user account '$user->{name}'
from the site$blurb1</p>$projectsinfo$ownedinfo
<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi">
<input type="hidden" name="name" value="$name" />
<input type="hidden" name="auth" value="$auth" />
<input type="hidden" name="confirm" value="$name" />
<p><input type="submit" name="y0" value="Remove user account" /></p>
</form>
EOT
			exit;
		}

		$user->remove;
		print "<p>User account successfully removed. Have a nice day.</p>\n";
		exit;
	}
}

print <<EOT;
<p>Here you can request an authorization code to remove your user account.</p>

<p>Please enter your username below;
we will send you an email with an authorization code
and further instructions.</p>

<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi">
<table class="form">
<tr><td class="formlabel">Login:</td><td><input type="text" name="name" /></td></tr>
<tr style="display:none"><td class="formlabel">Anti-captcha (leave empty!):</td><td><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Send authorization code" /></td></tr>
</table>
</form>
EOT
