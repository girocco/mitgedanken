#!/bin/sh

# authrequired.cgi -- show certification authorization instructions on 401
# Copyright (C) 2014,2016,2017 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Version 1.3.1

# We pretend like we don't exist.  Unless this was an attempt to access a push
# URL over HTTP/HTTPS in which case we return a suitable error message.

# Some of this detection requires REQUEST_URI to be set which is an Apache
# extension.  If REQUEST_URI is not set that portion of the smart detection
# will be disabled.

# Also note that we return a 403 error instead of a 401 error because we require
# a user push certificate.  Returning a 401 error and having the client then
# provide a user name and password is completely pointless since we now are
# providing copious amounts of help text.

# If the client appears to be an older version of Git that will probably not
# display a text/plain error response to the user then the error message is
# sent as an 'ERR ' packet in smart protocol format instead.  Git versions
# older than 1.8.3 as well as JGit and libgit require the 'ERR ' packet format.
# This 'ERR ' packet format support requires that REQUEST_URI be set to the
# original URI that was fetched or it will never trigger.

set -e

headers() {
	printf '%s\r\n' "Status: $1"
	printf '%s\r\n' "Expires: Fri, 01 Jan 1980 00:00:00 GMT"
	printf '%s\r\n' "Pragma: no-cache"
	printf '%s\r\n' "Cache-Control: no-cache, max-age=0, must-revalidate"
	printf '%s\r\n' "Content-Type: $2"
	[ -z "$3" ] || printf "%s\r\n" "$3"
	printf '\r\n'
}

notfound() {
	# Simulate a 404 error as though we do not exist
	headers 404 "text/html; charset=utf-8"
	SPACE=
	[ -z "$REQUEST_URI" ] || SPACE=" "
	cat <<EOF
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL $REQUEST_URI${SPACE}was not found on this server.</p>
<hr />
$SERVER_SIGNATURE
</body></html>
EOF
	exit 0
}

# Set isoldgit if we've detected an old Git client that needs an 'ERR ' packet
isoldgit=
case "$HTTP_USER_AGENT" in *[Gg]it/*)
	case "$HTTP_USER_AGENT" in
	*[Jj][Gg]it/*)
		isoldgit=1
		;;
	*)
		suffix="${HTTP_USER_AGENT##*[Gg]it/}"
		gitvers="${suffix%%[!0-9.]*}"
		gitvers="${gitvers%.}"
		case "$gitvers" in
			[1-9][0-9]*|[2-9]|[2-9].*|1.[1-9][0-9]*|1.9*| \
			1.8.[1-9][0-9]*|1.8.[3-9]|1.8.[3-9].*) :;;
			*)
				isoldgit=1
		esac
	esac
esac

needsauth=1
service=
if [ -n "$REQUEST_URI" ]; then
	# Try to detect whether or not it was something that needs auth
	needsauth=
	BASE="${REQUEST_URI%%[?]*}"
	QS="${REQUEST_URI#$BASE}"
	QS="${QS#[?]}"
	case "$BASE" in
		*/info/refs)
			case "&$QS&" in *"&service=git-receive-pack&"*)
				service=git-receive-pack
				needsauth=1
			esac
			;;
		*/git-receive-pack)
			service=git-receive-pack
			needsauth=1
	esac
fi
[ -n "$needsauth" ] || notfound

# Return a text/plain response WITHOUT any additional parameters (such as
# charset=) so that the Git client will display the result.

# If AUTHREQUIRED_MESSAGE is set, that's inserted as an extra line
xmsg=
nl='
'
if [ -n "$AUTHREQUIRED_MESSAGE" ]; then
	xmsg="$AUTHREQUIRED_MESSAGE$nl$nl"
fi

# We need some config variables
. @basedir@/shlib.sh

message="\
======================================================================
Authentication Required
======================================================================

${xmsg}In order to push using https, you must first
configure a user push certificate.

You may download a user push certificate from
the edit user page that may be accessed at:

  $cfg_webadmurl/edituser.cgi

Instructions for configuring Git to use the
downloaded push certificate can be found at:

  $cfg_htmlurl/httpspush.html

Do not forget to also configure the location
of your private key (see the above page).

======================================================================
"
if [ -n "$isoldgit" ] && [ -n "$service" ]; then
	l1=$(( 15 + ${#service} ))
	l2=$(( 9 + ${#message} ))
	headers 200 "application/x-$service-advertisement" \
		"Content-Length: $(( $l1 + 4 + $l2 ))"
	printf '%04x# service=%s\n0000%04xERR \n%s' $l1 "$service" \
		$l2 "$message"
else
	# IMPORTANT: Do NOT add any extra parameters here!
	# Older Git clients cannot handle anything other than
	# EXACTLY "text/plain"!
	headers 403 "text/plain"
	printf '%s' "$message"
fi
exit 0
