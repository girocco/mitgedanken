#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# Portions Copyright (c) Kyle J. McKay <mackyle@gmail.com>
# GPLv2

use strict;
use warnings;

use Digest::MD5 qw(md5_hex);

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Util;

BEGIN {
	eval {
		require Digest::SHA;
		Digest::SHA->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA1;
		Digest::SHA1->import(
			qw(sha1_hex)
	);1} ||
	eval {
		require Digest::SHA::PurePerl;
		Digest::SHA::PurePerl->import(
			qw(sha1_hex)
	);1} ||
	die "One of Digest::SHA or Digest::SHA1 or Digest::SHA::PurePerl "
	. "must be available\n";
}

# Ultra-trivial templating engine;
# /^@section=SECTION
# /^@heading=HEADING
# /^@header			produces HTML header based on @section and @heading
# /@@gitweburl@@/		substitute for gitweburl configuration variable
# /@@base(gitweburl)@@/		substitute for base portion of gitweburl variable
# /@@path(gitweburl)@@/		substitute for path portion of gitweburl variable
# /@@server(gitweburl)@@/	replace scheme://host:port portion of gitweburl variable
# /@@md5(relpath)@@/		produces MD5 hex of file in DOCUMENT_ROOT at relpath
# /@@sha1(relpath)@@/		produces SHA1 hex of file in DOCUMENT_ROOT at relpath
# /@@blob(relpath)@@/		produces git hash-object -t blob of file in DOCUMENT_ROOT
# /@@ifmob@@...@@end@@/		remove unless mob defined
# /@@ifssh@@...@@end@@/		remove unless pushurl defined
# /@@ifhttps@@...@@end@@/	remove unless httpspushurl defined
# /@@ifcustom@@...@@end@@/	remove if pretrustedroot defined (i.e. not custom root)
# /@@ctr()@@/			replace with 1-based up-counting value

my $pathinfo = $ENV{PATH_INFO} || '';
my $docroot = $ENV{DOCUMENT_ROOT} || '';
$pathinfo =~ s,^/,,;
unless ($pathinfo) {
	my $gcgi = Girocco::CGI->new('HTML Templater');
	print "<p>Hi, this is your friendly HTML templater speaking. Pass me template name.</p>\n";
	exit;
}

my $templatefd;
unless ($pathinfo !~ m#\./# and open($templatefd, '<', "$Girocco::Config::basedir/html/$pathinfo")) {
	my $gcgi = Girocco::CGI->new('HTML Templater');
	print "<p>Invalid template name.</p>\n";
	exit;
}

if ($pathinfo =~ /\.(png|jpe?g|gif|svgz?)$/) {
	my $kind = $1;
	$kind = 'jpeg' if $kind eq 'jpg';
	$kind = 'svg+xml' if $kind =~ /^svg/;
	print "Content-type: image/$kind\n\n";
	my $buf;
	while (read($templatefd, $buf, 32768)) {
		print $buf;
	}
	exit;
}

sub get_file_hash {
	my ($hash, $fn) = @_;
	return '' unless $docroot && $docroot ne '/' && -d $docroot;
	return '' if $hash ne 'md5' && $hash ne 'sha1' && $hash ne 'blob';
	return '' if !$fn || $fn =~ m|^[./]| || $fn =~ m|[.]/| || $fn =~ m|/[.]|;
	return '' unless -f "$docroot/$fn";
	open(HASHFILE, '<', "$docroot/$fn") or return '';
	local $/;
	undef $/;
	my $contents = <HASHFILE>;
	close(HASHFILE);
	$hash eq 'blob' and $contents = sprintf("blob %u\0", length($contents)) . $contents;
	return $hash eq 'md5' ? md5_hex($contents) : sha1_hex($contents);
}

my ($gcgi, $section, $heading);

my $template=join('', map(to_utf8($_, 1), <$templatefd>));
close $templatefd;
$template =~ s/@\@ifmob@\@(.*?)@\@end@\@/$Girocco::Config::mob?$1:''/ges;
$template =~ s/@\@ifssh@\@(.*?)@\@end@\@/$Girocco::Config::pushurl?$1:''/ges;
$template =~ s/@\@ifcustom@\@(.*?)@\@end@\@/!$Girocco::Config::pretrustedroot?$1:''/ges;
$template =~ s/@\@ifhttps@\@(.*?)@\@end@\@/$Girocco::Config::httpspushurl?$1:''/ges;

my $counter = 0;

foreach (split(/\n/, $template)) {
	chomp;
	if (s/^\@section=//) {
		$section = $_;
		next;
	} elsif (s/^\@heading=//) {
		$heading = $_;
		next;
	} elsif (s/^\@header//) {
		$gcgi = Girocco::CGI->new($heading, $section);
		print "<div class=\"htmlcgi\">";
		next;
	} else {
		s/@@(\w+?)@@/${$Girocco::Config::{$1}}/ge;
		s/@\@base\((\w+?)\)@@/url_base(${$Girocco::Config::{$1}})/ge;
		s/@\@path\((\w+?)\)@@/url_path(${$Girocco::Config::{$1}})/ge;
		s/@\@server\((\w+?)\)@@/url_server(${$Girocco::Config::{$1}})/ge;
		s/@\@(md5|sha1|blob)\(([^()]+?)\)@@/get_file_hash($1,$2)/ge;
		s/@\@ctr\(\)@@/++$counter/ge;
		print "$_\n";
	}
}
print "</div>";
$gcgi and $gcgi->srcname("html/$pathinfo");
