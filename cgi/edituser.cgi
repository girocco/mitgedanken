#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# (c) Jan Krueger <jk@jk.gs>
# GPLv2

use strict;
use warnings;

use lib "__BASEDIR__";
use Girocco::CGI;
use Girocco::Config;
use Girocco::User;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('User Email & SSH Key Update');
my $cgi = $gcgi->cgi;

unless ($Girocco::Config::manage_users) {
	print "<p>I don't manage users.</p>";
	exit;
}

if ($cgi->param('mail')) {
	print "<p>Go away, bot.</p>";
	exit;
}

sub _auth_form {
	my $name = shift;
	my $submit = shift;
	my $fields = shift;
	$fields = '' if (!$fields);
	my $auth = shift;
	my $authtag = ($auth ? qq(<input type="hidden" name="auth" value="$auth" />) :
		qq(<p>Authorization code: <input name="auth" size="50" /></p>));
	print <<EOT;

<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi">
<input type="hidden" name="name" value="$name" />
$authtag
$fields<p><input type="submit" name="y0" value="$submit" /></p>
</form>
EOT
}

my $savefail = 0;
my $y0 = $cgi->param('y0') || '';
if ($cgi->param('name') && $y0 && $cgi->request_method eq 'POST') {
	# submitted, let's see
	# FIXME: racy, do a lock
	my $name = $gcgi->wparam('name');
	Girocco::User::does_exist($name, 1)
		or $gcgi->err("Username is not registered.");

	$gcgi->err_check and exit;

	my $user;
	($user = Girocco::User->load($name)) && valid_email($user->{email})
		or $gcgi->err("Username may not be updated.");

	$gcgi->err_check and exit;

	if (!$cgi->param('auth')) {
		if ($y0 ne 'Send authorization code') {
			print "<p>Invalid data. Go away, sorcerer.</p>\n";
			exit;
		}

		valid_email($user->{email}) or die "Sorry, this user cannot be changed.";

		my $auth = $user->gen_auth;

		# Send auth mail
		defined(my $MAIL = mailer_pipe '-s', "[$Girocco::Config::name] Account update authorization", $user->{email}) or
			die "Sorry, could not send authorization code: $!";
		print $MAIL <<EOT;
Hello,

You have requested an authorization code to be sent to you for updating
your account's email and/or SSH keys. If you don't want to actually update
your email or SSH keys, just ignore this e-mail. Otherwise, use this code
within 24 hours:

$auth

Should you run into any problems, please let us know.

Have fun!
EOT
		close $MAIL;

		print "<p>You should shortly receive an e-mail containing an authorization code.
			Please enter this code below to update your SSH keys.
			The code will expire in 24 hours or after you have used it.</p>";
		_auth_form($name, "'Login'");
		exit;
	} else {
		$user->{auth} && $user->{authtype} ne 'DEL' or do {
			print "<p>There currently isn't any authorization code filed under your account.  ".
				"Please <a href=\"@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi\">generate one</a>.</p>";
			exit;
		};

		my $fields = '';
		my $email = $cgi->param('email');
		my $keys = $cgi->param('keys');

		my $auth = $gcgi->wparam('auth');
		if ($auth ne $user->{auth}) {
			print "<p>Invalid authorization code, please re-enter or ".
				"<a href=\"@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi\">generate a new one</a>.</p>";
			_auth_form($name, "'Login'");
			exit;
		}

		if (defined($email) && defined($keys)) {
			if ($y0 ne 'Update') {
				print "<p>Invalid data. Go away, sorcerer.</p>\n";
				exit;
			}

			# Auth valid, keys given -> save
			if (($email eq $user->{email} || $user->update_email($gcgi, $email)) && $user->keys_fill($gcgi)) {
				$user->del_auth;
				$user->keys_save;
				print "<p>Your Email &amp; SSH keys have been updated.</p>";
				my $keylist = $user->keys_html_list;
				if ($keylist) {
					print <<EOT;

<div id="keys"><p>The following keys have been registered for user $name as
shown below along with their <tt>ssh-keygen -l</tt> fingerprint:</p>
$keylist</div>
EOT
				}
				exit;
			}
			$y0 = "'Login'";
			$savefail = 1;
		} else {
			# Otherwise pre-fill fields
			$email = $user->{email};
			$keys = $user->{keys}."\n";
		}

		if ($y0 ne "'Login'") {
			print "<p>Invalid data. Go away, sorcerer.</p>\n";
			exit;
		}

		my $httpspara = '';
		$httpspara = <<EOT if $Girocco::Config::httpspushurl;
<p>Please be sure to include at least one RSA key (starts with the <tt>ssh-rsa</tt> prefix) in
order to enable HTTPS pushing. <sup class="sup"><span><a href="@{[url_path($Girocco::Config::htmlurl)]}/httpspush.html">(learn more)</a></span></sup><br />
An X.509 (e.g. OpenSSL) format public key can be converted to SSH .pub format with the
<a href="http://repo.or.cz/w/ezcert.git/blob/master:/ConvertPubKey">ConvertPubKey</a> utility thus obviating the
need for OpenSSH if all pushing is to be done using HTTPS (see the example in the TIPS section of the <tt>ConvertPubKey -h</tt> output).</p>
EOT
		my $emailval = CGI::escapeHTML($email);
		my $keysval = CGI::escapeHTML($keys);
		my $blurb = '';
		$blurb = 'SSH (the <tt>ssh</tt> protocol)'
			if $Girocco::Config::pushurl && !$Girocco::Config::httpspushurl;
		$blurb = 'HTTPS (the <tt>https</tt> protocol)'
			if !$Girocco::Config::pushurl && $Girocco::Config::httpspushurl;
		$blurb = 'SSH (the <tt>ssh</tt> protocol) or HTTPS (the <tt>https</tt> protocol)'
			if $Girocco::Config::pushurl && $Girocco::Config::httpspushurl;
		my $dsablurb = '';
		$dsablurb = ' or <tt>~/.ssh/id_dsa.pub</tt>' unless $Girocco::Config::disable_dsa;
		print <<EOT;
<p>Authorization code validated (for now).</p>
<p>$blurb is used for pushing, your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected;
use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt>$dsablurb.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.</p>
<p>You can paste multiple keys in the box below, each on a separate line.
Paste each key <em>including</em> the <tt>ssh-</tt>whatever prefix and email-like postfix.</p>
$httpspara<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi">
<input type="hidden" name="name" value="$name" />
<input type="hidden" name="auth" value="$auth" />
<table class="form">
<tr><td class="formlabel">Login:</td><td class="formdata">$name</td></tr>
<tr><td class="formlabel">Email:</td><td><input type="text" name="email" value="$emailval"/></td></tr>
<tr><td class="formlabel">Public SSH key(s):</td><td><textarea wrap="off" name="keys" rows="5" cols="80">$keysval</textarea></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Update" /></td></tr>
</table>
EOT
		my $keylist = $savefail ? '' : $user->keys_html_list;
		if ($keylist) {
			print <<EOT;

<div id="keys"><p>The following keys are currently registered for user $name as
shown below along with their <tt>ssh-keygen -l</tt> fingerprint:</p>
$keylist</div>
EOT
		}
		exit;
	}

}

my $blurb1 = '';
$blurb1 = 'SSH (the <tt>ssh</tt> protocol)'
	if $Girocco::Config::pushurl && !$Girocco::Config::httpspushurl;
$blurb1 = 'HTTPS (the <tt>https</tt> protocol)'
	if !$Girocco::Config::pushurl && $Girocco::Config::httpspushurl;
$blurb1 = 'SSH (the <tt>ssh</tt> protocol) or HTTPS (the <tt>https</tt> protocol)'
	if $Girocco::Config::pushurl && $Girocco::Config::httpspushurl;
my $blurb2 = '';
$blurb2 = ' and download https push user authentication certificate(s)'
	if $Girocco::Config::httpspushurl;
my $dsablurb = '';
$dsablurb = ' or <tt>~/.ssh/id_dsa.pub</tt>' unless $Girocco::Config::disable_dsa;
print <<EOT;
<p>Here you may update the email and public SSH key(s) associated with your user account$blurb2.
You may <a href="@{[url_path($Girocco::Config::webadmurl)]}/deluser.cgi">request an authorization
code in order to remove your user account from this site</a>.</p>
<p>If you do not already have a user account you may
<a href="@{[url_path($Girocco::Config::webadmurl)]}/reguser.cgi">register user</a> instead.</p>
<p>The public SSH key(s) are required for you to push to projects.
$blurb1 is used for pushing, your SSH key authenticates you -
there is no password (though we recommend that your SSH key is password-protected;
use <code>ssh-agent</code> to help your fingers).
You can find your public key in <tt>~/.ssh/id_rsa.pub</tt>$dsablurb.
If you do not have any yet, generate it using the <code>ssh-keygen</code> command.</p>

<p>Please enter your username below;
we will send you an email with an authorization code
and further instructions.</p>

<form method="post" action="@{[url_path($Girocco::Config::webadmurl)]}/edituser.cgi">
<table class="form">
<tr><td class="formlabel">Login:</td><td><input type="text" name="name" /></td></tr>
<tr style="display:none"><td class="formlabel">Anti-captcha (leave empty!):</td><td><input type="text" name="mail" /></td></tr>
<tr><td class="formlabel"></td><td><input type="submit" name="y0" value="Send authorization code" /></td></tr>
</table>
</form>
EOT
