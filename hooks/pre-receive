#!/bin/sh
#
# Keep track of the last time we modified the object store
#
# Beware, we MAY be running in a chroot!

set -e

# Make sure the current directory is where we expect to be
[ "${GIT_DIR+set}" != "set" ] || { [ -n "$GIT_DIR" ] && [ -d "$GIT_DIR" ]; } || unset GIT_DIR
[ -n "$GIT_DIR" ] || GIT_DIR="$(git rev-parse --git-dir)"
[ -n "$GIT_DIR" ] && cd -P "${GIT_DIR:-.}" || exit 1
case "${PWD%/*}" in */worktrees)
	# Gah!
	if
		# But it COULD just be a coincidence...
		[ -s commondir ] && [ -s HEAD ] &&
		_cmndir= && read -r _cmndir <commondir 2>/dev/null &&
		[ -n "$_cmndir" ] && [ -d "$_cmndir" ]
	then
		# ...it is not, fix it!
		cd -P "$_cmndir" || exit 1
	fi
esac
GIT_DIR="." GIT_PREFIX= && export GIT_DIR

# Get out of the mob
case "$PWD" in *?/mob)
	cd ..
	GIROCCO_PERSONAL_MOB=1
esac

umask 002

if [ -x @perlbin@ ]; then
	# We are NOT inside the chroot
	basedir=@basedir@
	list_packs() { command "$basedir/bin/list_packs" "$@"; }
	strftime() { command "$basedir/bin/strftime" "$@"; }
fi

git config gitweb.lastreceive "$(date '+%a, %d %b %Y %T %z')"

# Read the incoming refs and freshen old loose objects
# If we waited until post-receive a gc could have already nuked them
# We freshen the new ref in case it's being resurrected to protect it from gc
# We probably do not need to do it for new refs as Git tries to do that,
# but since we're already doing it for old refs (which Git does not do),
# it's almost no extra work for new refs, just in case.  We also attempt to
# make all packs user and group writable so they can be touched/renamed later.

# Starting with Git v2.11.0 receive-pack packs/objects end up in a quarantine
# object directory that is just discarded immediately if pre-receive declines
# to accept the push.  This is a good thing.  However, it means that the
# incoming objects are NOT located in objects/... as GIT_OBJECT_DIRECTORY and
# GIT_QUARANTINE_PATH are both set to the quarantine objects directory and the
# original objects directory is appended to GIT_ALTERNATE_OBJECT_DIRECTORIES
# (but it will just be the absolute path to objects).  The simple bottom line
# is that we should also try everything in the GIT_QUARANTINE_PATH directory if
# it's set.
[ -z "$GIT_QUARANTINE_PATH" ] || [ -d "$GIT_QUARANTINE_PATH" ] || unset GIT_QUARANTINE_PATH

# We also record changes to a ref log.  We do it here rather than in
# post-receive so that we can guarantee all potential changes are recorded in
# the log before they take place.  It's possible that the update hook will
# ultimately deny one or more updates but waiting until post-receive could
# result in updates being left out of the log.

hexdig='[0-9a-f]'
octet="$hexdig$hexdig"
octet4="$octet$octet$octet$octet"
octet20="$octet4$octet4$octet4$octet4$octet4"
_make_packs_ugw() {
	find -L "$1" -maxdepth 1 -type f ! -perm -ug+w \
		-name "pack-$octet20*.*" -exec chmod ug+w '{}' + || :
} 2>/dev/null
_make_packs_ugw objects/pack
[ -z "$GIT_QUARANTINE_PATH" ] || _make_packs_ugw "$GIT_QUARANTINE_PATH/pack"

# Trigger a mini-gc if there are at least 20 packs present.
# Our current pack that contains this push's data will have a .keep while
# this hook is running so it will be excluded from the count, but the count
# is only an approximate guide anyway and being off by one or two will not
# cause any performance problems.  We could drop the check to 19 instead to
# compensate but there could be other simultaneous pushes and it's highly
# unlikely that it would end up making any difference anyway.  We also
# deliberately ignore anything in the "$GIT_QUARANTINE_PATH/pack" area because
# at this point it will have a .keep (if it exists) and because of the way
# it's created there can never be more than one pack in there anyway.
# The mini-gc code contains the logic to sort out small packs vs. non-small
# packs and which should be combined in what order so we do not need to
# do any more complicated testing here.
if ! [ -e .needsgc ]; then
	packs=
	{ packs="$(list_packs --quiet --count --exclude-no-idx --exclude-keep objects/pack || :)" || :; } 2>/dev/null
	if [ -n "$packs" ] && [ "$packs" -ge 20 ]; then
		>.needsgc
	fi
fi

# Make sure we have a reflogs directory and abort the update if we cannot
# create one.  Normally project creation will make sure this directory exists.
[ -d reflogs ] || mkdir -p reflogs >/dev/null 2>&1 || :
[ -d reflogs ]

# Multiple push operations could be occurring simultaneously so we need to
# guarantee they do not step on one another and we do this by generating a
# unique log file name.  We use a microseconds timestamp and the current process
# id and we guarantee that this process is kept alive for the entire microsecond
# of the timestamp thereby guaranteeing that we are the only possible process
# that could use that pid during that particular microsecond (ignoring leap seconds).
# To do this we need to sleep until the microsecond turns over, grab the timestamp
# and then sleep until the microsecond turns over again.  This will introduce a
# guaranteed 2 microsecond delay into every push.  This should not generally be
# noticeable.  The strftime utility does this for us when using %N with current time.
# We always use UTC for the timestamp so that chroot and non-chroot match up.
# Log entries are the lines sent to the pre-receive hook with hhmmss prepended.
lognamets="$(TZ=UTC strftime '%Y%m%d_%H%M%S%N')"
lognamets="${lognamets%???}"
loghhmmss="${lognamets##*_}"
loghhmmss="${loghhmmss%??????}"

# We write to a temp ref log and then move it into place so that the reflogs
# collector can assume that log files with their final name are immutable
logname="reflogs/$lognamets.$$"
lognametmp="reflogs/tmp_$lognamets.$$"

while read -r old new ref; do
	echo "$loghhmmss $old $new $ref" >&3
	args=
	if [ "$old" != "0000000000000000000000000000000000000000" ]; then
		# freshen mod time on recently unref'd loose objects
		fn="${old#??}"
		shard="${old%$fn}"
		args="$args 'objects/$shard/$fn'"
		[ -z "$GIT_QUARANTINE_DIRECTORY" ] || args="$args '$GIT_QUARANTINE_DIRECTORY/$shard/$fn'"
	fi
	if [ "$new" != "0000000000000000000000000000000000000000" ]; then
		# prevent imminent pruning of a ref being resurrected
		fn="${new#??}"
		shard="${new%$fn}"
		args="$args 'objects/$shard/$fn'"
		[ -z "$GIT_QUARANTINE_DIRECTORY" ] || args="$args '$GIT_QUARANTINE_DIRECTORY/$shard/$fn'"
	fi
	eval "chmod ug+w $args" 2>/dev/null || :
	eval "touch -c $args" 2>/dev/null || :
done 3>"$lognametmp"
mv "$lognametmp" "$logname"

# While unlikely, it is conceivable that several ref updates have occurred that
# did not actually push any packs.  In that case we could build up a large
# number of log files so request a mini gc if there are 50 or more of them now.
if ! [ -e .needsgc ]; then
	logfiles=
	{ logfiles="$(($(find -L reflogs -maxdepth 1 -type f -print | wc -l || :)+0))" || :; } 2>/dev/null
	if [ -n "$logfiles" ] && [ "$logfiles" -ge 50 ]; then
		>.needsgc
	fi
fi
