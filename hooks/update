#!/bin/sh
#
# Currently, we just confine the mob user to the mob branch here.
#
# TODO: Generalized branches push permissions support.

set -e

# Make sure the current directory is where we expect to be
[ "${GIT_DIR+set}" != "set" ] || { [ -n "$GIT_DIR" ] && [ -d "$GIT_DIR" ]; } || unset GIT_DIR
[ -n "$GIT_DIR" ] || GIT_DIR="$(git rev-parse --git-dir)"
[ -n "$GIT_DIR" ] && cd -P "${GIT_DIR:-.}" || exit 1
case "${PWD%/*}" in */worktrees)
	# Gah!
	if
		# But it COULD just be a coincidence...
		[ -s commondir ] && [ -s HEAD ] &&
		_cmndir= && read -r _cmndir <commondir 2>/dev/null &&
		[ -n "$_cmndir" ] && [ -d "$_cmndir" ]
	then
		# ...it is not, fix it!
		cd -P "$_cmndir" || exit 1
	fi
esac
GIT_DIR="." GIT_PREFIX= && export GIT_DIR

if ! [ -x @perlbin@ ]; then
	# We are INSIDE the chroot

	reporoot=/@jailreporoot@
	reporoot="$(cd "$reporoot" && pwd -P)"
	webadmurl=@webadmurl@
	mob=@mob@
	proj="$(pwd -P)"; proj="${proj#$reporoot/}"
	mobdir=
	case "$proj" in *?/mob)
		mobdir=1
		proj="${proj%/mob}"
	esac
	projbare="${proj%.git}"

	if ! [ -f "$reporoot/$proj/.nofetch" ]; then
		echo "The $proj project is a mirror and may not be pushed to, sorry" >&2
		exit 3
	fi

	if [ -n "$mobdir" ] && [ "$mob" != "mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "Personal mob branches are not supported" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && [ "$LOGNAME" = "mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "The mob user may not use personal mob branches" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && ! [ -d "$reporoot/$proj/mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "The project '$proj' does not support personal mob branches" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && ! can_user_push "$projbare" mob; then
		# Should only get here if there's a misconfiguration
		echo "The user 'mob' does not have push permissions for project '$proj'" >&2
		echo "You may adjust push permissions at $webadmurl/editproj.cgi?name=$proj" >&2
		exit 3
	fi
	if [ -n "$mobdir" ]; then
		# All personal mob refs must start with refs/heads/mob.$USER,
		# refs/heads/mob_$USER/, refs/mob/mob.$USER or refs/mob/mob_$USER/
		case "$1" in
		"refs/heads/mob.$LOGNAME"	| \
		"refs/heads/mob_$LOGNAME/"?*	| \
		"refs/mob/mob.$LOGNAME"		| \
		"refs/mob/mob_$LOGNAME/"?*	) :;;
		*)
			echo "The user '$LOGNAME' does not have push permissions for project '$proj'." >&2
			echo "However '$proj' allows pushes to personal mob branches w/o push perms." >&2
			echo "The ref '$1' is not a valid personal mob branch ref name." >&2
			echo "Valid personal mob branch ref names are one of the following:" >&2
			echo "  refs/heads/mob.$LOGNAME or refs/mob/mob.$LOGNAME" >&2
			echo "or refs that start with one of the following:" >&2
			echo "  refs/heads/mob_$LOGNAME/ or refs/mob/mob_$LOGNAME/" >&2
			echo "No other personal mob branch ref names may be pushed to, sorry." >&2
			echo "You may adjust push permissions at $webadmurl/editproj.cgi?name=$proj" >&2
			exit 3
		esac
		exit 0
	fi

	if ! can_user_push "$projbare"; then
		echo "The user '$LOGNAME' does not have push permissions for project '$proj'" >&2
		echo "You may adjust push permissions at $webadmurl/editproj.cgi?name=$proj" >&2
		exit 3
	fi

	if [ "$mob" = "mob" ] && [ "$LOGNAME" = "mob" ]; then
		if [ x"$1" != x"refs/heads/mob" ]; then
			echo "The mob user may push only to the 'mob' branch, sorry" >&2
			exit 1
		fi
		if [ x"$2" = x"0000000000000000000000000000000000000000" ]; then
			echo "The mob user may not _create_ the 'mob' branch, sorry" >&2
			exit 2
		fi
		if [ x"$3" = x"0000000000000000000000000000000000000000" ]; then
			echo "The mob user may not _delete_ the 'mob' branch, smch, sorry"
			exit 3
		fi
	fi

	if [ "$mob" = "mob" ]; then
		case "$1" in
		"refs/heads/mob."?* | "refs/heads/mob_"?*)
			echo "Use of the ref '$1' is reserved for personal mob branch" >&2
			echo "users who do not have push permission to project '$proj'." >&2
			echo "Users with push permission may only access the personal" >&2
			echo "mob branches using refs that start with 'refs/mob/'." >&2
			exit 3
		esac
	fi

	exit 0
fi

# We are NOT inside the chroot

. @basedir@/shlib.sh
reporoot="$cfg_reporoot"
v_get_proj_from_dir proj
projbare="${proj%.git}"

if [ "$cfg_permission_control" = "Hooks" ]; then
	# We have some permission control to do!
	# XXX: Sanity check on project name and $USER here? Seems superfluous.
	if ! "$cfg_basedir/bin/can_user_push_http" "$projbare" "$USER"; then
		echo "The user '$USER' does not have push permissions for project '$proj'" >&2
		echo "You may adjust push permissions at $cfg_webadmurl/editproj.cgi?name=$proj" >&2
		exit 3
	fi
fi

if [ -n "$GIT_PROJECT_ROOT" ]; then
	# We are doing a smart HTTP push

	mobdir=
	case "$proj" in *?/mob)
		mobdir=1
		proj="${proj%/mob}"
		projbare="${proj%.git}"
	esac

	if ! [ -f "$reporoot/$proj/.nofetch" ]; then
		echo "The $proj project is a mirror and may not be pushed to, sorry" >&2
		exit 3
	fi

	authuser="${REMOTE_USER#/UID=}"
	authuser="${authuser#UID = }"
	authuuid="$authuser"
	authuser="${authuser%/dnQualifier=*}"
	authuser="${authuser%, dnQualifier = *}"
	authuuid="${authuuid#$authuser}"
	authuuid="${authuuid#/dnQualifier=}"
	authuuid="${authuuid#, dnQualifier = }"
	if [ -z "$authuser" ]; then
		echo "Only authenticated users may push, sorry" >&2
		exit 3
	fi
	if [ "$authuser" != "mob" ] || [ "$cfg_mob" != "mob" ]; then
		if ! useruuid="$("$cfg_basedir/bin/get_user_uuid" "$authuser")" || [ "$useruuid" != "$authuuid" ]; then
			echo "The user '$authuser' certificate being used is no longer valid."
			echo "You may download a new user certificate at $cfg_webadmurl/edituser.cgi"
			exit 3
		fi
	fi

	if [ -n "$mobdir" ] && [ "$cfg_mob" != "mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "Personal mob branches are not supported" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && [ "$authuser" = "mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "The mob user may not use personal mob branches" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && ! [ -d "$reporoot/$proj/mob" ]; then
		# Should only get here if there's a misconfiguration
		echo "The project '$proj' does not support personal mob branches" >&2
		exit 3
	fi
	if [ -n "$mobdir" ] && ! "$cfg_basedir/bin/can_user_push_http" "$projbare" "mob"; then
		# Should only get here if there's a misconfiguration
		echo "The user 'mob' does not have push permissions for project '$proj'" >&2
		echo "You may adjust push permissions at $cfg_webadmurl/editproj.cgi?name=$proj" >&2
		exit 3
	fi
	if [ -n "$mobdir" ]; then
		# All personal mob refs must start with refs/heads/mob.$USER,
		# refs/heads/mob_$USER/, refs/mob/mob.$USER or refs/mob/mob_$USER/
		case "$1" in
		"refs/heads/mob.$authuser"	| \
		"refs/heads/mob_$authuser/"?*	| \
		"refs/mob/mob.$authuser"	| \
		"refs/mob/mob_$authuser/"?*	) :;;
		*)
			echo "The user '$authuser' does not have push permissions for project '$proj'." >&2
			echo "However '$proj' allows pushes to personal mob branches w/o push perms." >&2
			echo "The ref '$1' is not a valid personal mob branch ref name." >&2
			echo "Valid personal mob branch ref names are one of the following:" >&2
			echo "  refs/heads/mob.$authuser or refs/mob/mob.$authuser" >&2
			echo "or refs that start with one of the following:" >&2
			echo "  refs/heads/mob_$authuser/ or refs/mob/mob_$authuser/" >&2
			echo "No other personal mob branch ref names may be pushed to, sorry." >&2
			echo "You may adjust push permissions at $cfg_webadmurl/editproj.cgi?name=$proj" >&2
			exit 3
		esac
		exit 0
	fi

	if ! "$cfg_basedir/bin/can_user_push_http" "$projbare" "$authuser"; then
		echo "The user '$authuser' does not have push permissions for project '$proj'" >&2
		echo "You may adjust push permissions at $cfg_webadmurl/editproj.cgi?name=$proj" >&2
		exit 3
	fi

	if [ "$cfg_mob" = "mob" ] && [ "$authuser" = "mob" ]; then
		if [ x"$1" != x"refs/heads/mob" ]; then
			echo "The mob user may push only to the 'mob' branch, sorry" >&2
			exit 1
		fi
		if [ x"$2" = x"0000000000000000000000000000000000000000" ]; then
			echo "The mob user may not _create_ the 'mob' branch, sorry" >&2
			exit 2
		fi
		if [ x"$3" = x"0000000000000000000000000000000000000000" ]; then
			echo "The mob user may not _delete_ the 'mob' branch, smch, sorry"
			exit 3
		fi
	fi

	if [ "$cfg_mob" = "mob" ]; then
		case "$1" in
		"refs/heads/mob."?* | "refs/heads/mob_"?*)
			echo "Use of the ref '$1' is reserved for personal mob branch" >&2
			echo "users who do not have push permission to project '$proj'." >&2
			echo "Users with push permission may only access the personal" >&2
			echo "mob branches using refs that start with 'refs/mob/'." >&2
			exit 3
		esac
	fi
fi

exit 0
