@section=site guide
@heading=How to Setup HTTPS Push
@header

<!-- This file is preprocessed by cgi/html.cgi -->

<h2>Overview</h2>

<div class="indent">
<p>The https push facility relies on user client authentication certificates to
enable pushing.  These certificates are automatically created whenever an
RSA SSH public key is included in the &#x201c;Public SSH Key(s)&#x201d; section
of the <a href="/reguser.cgi">Register user</a> page and may be downloaded
from the download link(s) shown on the user registration confirmation page
or the <a href="/edituser.cgi">Update user email/SSH Keys</a> page.</p>

<p>A user client certificate is <em>NOT</em> required to fetch using https@@ifcustom@@, but you will
likely need to configure the root certificate (if you haven&#x2019;t already done so).  See
<a href="rootcert.html#quick">the instructions to quickly and easily configure the root certificate</a>
if you only want to fetch over https and don&#x2019;t currently need to push@@end@@.</p>

<p style="border:thin dotted black;background-color:#eef;padding:0.5ex 1ex;max-width:90ex">An
https push user authentication certificate may be downloaded from the
<a href="/reguser.cgi">Register user</a> confirmation page or the
<a href="/edituser.cgi">Update user email/SSH Keys</a> page.</p>
</div>

<h2 id="instructions">Instructions</h2>

<div class="indent">
<p><b>Note</b>: These instructions are for modern Gits.  If you have
an ancient Git (i.e. prior to version 1.8.5) see the
<a href="#alternate">alternate instructions</a> below.</p>
</div>

<h3>0. Quick Overview</h3>
<div>
<ol>
@@ifcustom@@
<li>Download the <a href="@@path(webadmurl)@@/@@nickname@@_root_cert.pem">root certificate</a>.</li>
@@end@@
<li>Download your user certificate from the <a href="/reguser.cgi">Register user</a>
confirmation page or the <a href="/edituser.cgi">Update user email/SSH Keys</a>
page.</li>
<li>Identify the file containing your private key.</li>
<li>Perform one-time Git global configuration of the @@ifcustom@@root certificate (<tt>http.sslCAInfo</tt>),@@end@@
user certificate (<tt>http.sslCert</tt>) and private key (<tt>http.sslKey</tt>) but <em>only</em>
for URLs starting with "<tt>@@base(httpspushurl)@@</tt>".</li>
</ol>
</div>

@@ifcustom@@
<h3>@@ctr()@@. Download the root certificate</h3>

<div class="indent">
<p>Download the <a href="@@path(webadmurl)@@/@@nickname@@_root_cert.pem">root certificate</a>
(more information about it can be found <a href="@@path(htmlurl)@@/rootcert.html">here</a>).</p>

<p>Assuming the root certificate will be stored in "<tt>$HOME/certs</tt>" it may be
downloaded like so:</p>

<pre class="indent">
mkdir -p "$HOME/certs"
cd "$HOME/certs"
curl -LO "@@server(webadmurl)@@/@@nickname@@_root_cert.pem"
</pre>
</div>
@@end@@

<h3>@@ctr()@@. Download your user certificate</h3>

<div class="indent">
<p>You must register an RSA public key using either the 
<a href="/reguser.cgi">Register user</a> page or the
<a href="/edituser.cgi">Update user email/SSH Keys</a> page.</p>

<p>Your user push certificate for that RSA public key can then be
downloaded from the register user confirmation page or the edit
user page.</p>

<p>Please note that if you use ssh, you may already have a suitable RSA
public key stored in the "<tt>$HOME/.ssh/id_rsa.pub</tt>" file.</p>

<p>If you do not already have a suitable RSA public key (or you want to use
a different one for this site) you will need to
generate a new RSA key and then register the public key portion using either
the <a href="/reguser.cgi">Register user</a> page or the
<a href="/edituser.cgi">Update user email/SSH Keys</a> page.</p>

<p>A new RSA key (both public and private parts) can be generated using the
"<tt>ssh-keygen -t rsa</tt>" command (from OpenSSH) or using a combination of
the "<tt>openssl genrsa</tt>" command (from OpenSSL) and the
"<tt><a href="https://repo.or.cz/ezcert.git/blob/HEAD:/ConvertPubKey#l173">ConvertPubKey</a></tt>"
command (from <a href="https://repo.or.cz/ezcert.git">EZCert</a>).</p>

<p>Download your https push user certificate and store it in the
"<tt>$HOME/certs</tt>" directory.  The downloaded user certificate file will
have a name like "<tt>@@nickname@@_</tt><i>name</i><tt>_user_1.pem</tt>" where
"<i>name</i>" is the user name you registered the public key for (the downloaded
user certificate file may also have a suffix other than "<tt>_1</tt>" if
you&#x2019;ve registered more than one public key).</p>
</div>

<h3>@@ctr()@@. Locate your private key</h3>

<div class="indent">
<p>If you registered "<tt>$HOME/.ssh/id_rsa.pub</tt>" as your public key then
your corresponding private key can most likely be found in
"<tt>$HOME/.ssh/id_rsa</tt>".</p>

<p>If you&#x2019;re using a different RSA public key, you will need the full
path to the corresponding private key portion for the next step.</p>
</div>

<h3>@@ctr()@@. Perform Git global configuration</h3>

<div class="indent">
<p>Please note that these configuration steps will only be effective for modern Gits
(version 1.8.5 or later).  If you&#x2019;re dealing with an ancient Git see the
<a href="#alternate">alternate instructions</a>.</p>

<p>Assuming @@ifcustom@@the root certificate has been downloaded and stored in "<tt>$HOME/certs</tt>",@@end@@
the user certificate has been downloaded and stored in "<tt>$HOME/certs</tt>" and
the private key is located in "<tt>$HOME/.ssh/id_rsa</tt>", the following will
configure Git&#x2019;s @@ifcustom@@"<tt>http.sslCAInfo</tt>", @@end@@"<tt>http.sslCert</tt>" and "<tt>http.sslKey</tt>"
settings but <em>only</em> for URLs starting with "<tt>@@base(httpspushurl)@@</tt>":</p>

<pre class="indent">
@@ifcustom@@git config --global http.@@base(httpspushurl)@@.sslCAInfo \
                    "$HOME/certs/@@nickname@@_root_cert.pem"

@@end@@git config --global http.@@base(httpspushurl)@@.sslCert \
                    "$HOME/certs/@@nickname@@_<i>name</i>_user_1.pem"

git config --global http.@@base(httpspushurl)@@.sslKey \
                    "$HOME/.ssh/id_rsa"
</pre>

<p>Your git is now configured and ready to push to this site using
an https push URL (presuming your user has push permission to the project
you&#x2019;re pushing to).  See the <a href="#examples">examples</a> below.</p>

<p>If your RSA private key is password protected, you may want to also set
the following to avoid overly repetitious entering of the private key&#x2019;s
password:</p>

<pre class="indent">
git config --global http.@@base(httpspushurl)@@.sslCertPasswordProtected true
</pre>

<p><b>OS X Note</b>: Users of OS X 10.9 and later (including 10.10 etc.) please
be advised that the system&#x2019;s curl library ("<tt>/usr/lib/libcurl.4.dylib</tt>")
has <a href="http://mackyle.github.io/git-osx-installer/iscurlsick.html#problem"
>problems handling client certificates</a>.  If you&#x2019;re using a version of
Git that uses that version of the curl library (Git uses libcurl to talk https),
you will be unable to use any downloaded https user push certificate.  If you
think you might be affected, you can
<a href="http://mackyle.github.io/git-osx-installer/iscurlsick.html#gittest"
>test your Git</a> and if you have a problem, install a
<a href="http://mackyle.github.io/git-osx-installer/">Git without the problem</a>
instead.  (Reportedly this issue MAY have been addressed starting with Mac OS X 10.13,
but it doesn't hurt to <a href="http://mackyle.github.io/git-osx-installer/iscurlsick.html#gittest"
>test your Git</a> just to be sure.)</p>
</div>

<h2 id="examples">Examples</h2>

<div class="indent">
<p>It&#x2019;s possible to both fetch and push over https.  It&#x2019;s also
possible to fetch over http and push over https.  There&#x2019;s an example
of each.  Both examples assume Git has already been configured as described
in the <a href="#instructions">instructions</a>.</p>

<pre class="indent">
# clone using http
git clone @@httppullurl@@/mobexample.git mob1

# clone using https
git clone @@httpspushurl@@/mobexample.git mob2

# configure mob1 to push over https
cd /tmp/mob1
git remote set-url --push origin @@httpspushurl@@/mobexample.git
echo mob1 >> mob1
git add mob1
git commit -m mob1
# push will fail unless your user has push permission
git push --all origin

# configure mob2 to fetch and push over https
cd /tmp/mob2
# nothing needs to be done, the clone &amp; global config took care of it
echo mob2 >> mob2
git add mob2
git commit -m mob2
# push will fail unless your user has push permission
git push --all origin
</pre>
</div>

<h2 id="alternate">Alternative Git Configuration Techniques</h2>

<div class="indent">
<p>These techniques work with Git version 1.6.6 and later (versions of Git
prior to 1.6.6 lack the required smart HTTP protocol support).</p>

<pre class="indent">
# work in /tmp
cd /tmp

# clone using http
git clone @@httppullurl@@/mobexample.git mob1

# clone using https
@@ifcustom@@GIT_SSL_CAINFO=$HOME/certs/@@nickname@@_root_cert.pem \
@@end@@git clone @@httpspushurl@@/mobexample.git mob2

# configure mob1 to push over https
cd /tmp/mob1
# omitting --global makes these settings repository specific
@@ifcustom@@git config http.sslCAInfo $HOME/certs/@@nickname@@_root_cert.pem
@@end@@git config http.sslCert $HOME/certs/@@nickname@@_<i>name</i>_user_1.pem
git config http.sslKey $HOME/.ssh/id_rsa
git remote set-url --push origin @@httpspushurl@@/mobexample.git
echo mob1 >> mob1
git add mob1
git commit -m mob1
# push will fail unless your user has push permission
git push --all origin

# configure mob2 to fetch and push over https
cd /tmp/mob2
@@ifcustom@@git config http.sslCAInfo $HOME/certs/@@nickname@@_root_cert.pem
@@end@@git config http.sslCert $HOME/certs/@@nickname@@_<i>name</i>_user_1.pem
git config http.sslKey $HOME/.ssh/id_rsa
echo mob2 >> mob2
git add mob2
git commit -m mob2
# push will fail unless your user has push permission
git push --all origin
</pre>

<p>The example <tt>git push</tt> commands above will fail with a push permission
error since your user most likely does not have permission to push to the
<tt>mobexample.git</tt> project@@ifmob@@, but the mob user can push to the mob branch of
<tt>mobexample.git</tt> over https as detailed
<a href="@@path(htmlurl)@@/mob.html#httpsmobpush">here</a>@@end@@.</p>
</div>

<h2>Password Caching</h2>

<div class="indent">
<p>In the above examples, if the <tt>$HOME/.ssh/id_rsa</tt> private key is password
protected, then it&#x2019;s desirable to set <tt>http.sslCertPasswordProtected</tt>
to true like so:</p>

<pre class="indent">
# with the current directory /tmp/mob1 or /tmp/mob2
git config --bool http.sslCertPasswordProtected true
</pre>
</div>
