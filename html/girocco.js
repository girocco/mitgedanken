/* Girocco JS */


/* Hide/show various bits depending on mirror/push
 * mode in regproj.cgi: */

var tr_display1;
var tr_display2;
var has_display2;
function mirror_push__click(ev, which) {
	if (!ev) var ev = window.event;
	if (which == 'mirror') {
		document.getElementById('mirror_url').style.display = tr_display1;
		if (has_display2)
			document.getElementById('mirror_refs').style.display = tr_display2;
	} else {
		document.getElementById('mirror_url').style.display = 'none';
		if (has_display2)
			document.getElementById('mirror_refs').style.display = 'none';
	}
}
function mirror_push_prepare(mr, pr) {
	if (!mr || !pr)
		return;
	mr.onclick = function(e) { mirror_push__click(e, 'mirror') };
	pr.onclick = function(e) { mirror_push__click(e, 'push') };
	tr_display1 = document.getElementById('mirror_url').style.display;
	var display2;
	display2 = document.getElementById('mirror_refs');
	if (display2) {
		has_display2 = true;
		tr_display2 = display2.style.display;
	}
}
function set_mirror_source(which) {
	var radios = document.getElementsByTagName('input');
	for (var i = 0; i < radios.length; ++i) {
		var checkclass = ' ' + radios[i].className + ' ';
		if (checkclass.indexOf(' mirror_sources ') >= 0) {
			radios[i].checked = (radios[i].value == which) ? true : false;
		}
	}
}
window.addEvent('domready', function() {
	mirror_push_prepare(document.getElementById('mirror_radio'),
			    document.getElementById('push_radio'));
	if (document.getElementById('mirror_radio').checked) {
		mirror_push__click(null, 'mirror')
	}
	if (document.getElementById('push_radio').checked) {
		mirror_push__click(null, 'push')
	}
});
