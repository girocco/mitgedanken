# chrootsetup_dragonfly.sh

# This file SHOULD NOT be executable!  It is sourced by jailsetup.sh and
# SHOULD NOT be executed directly!

# On entry the current directory will be set to the top of the chroot
# This script must perform platform-specific chroot setup which includes
# creating any dev device entries, setting up proc (if needed), setting
# up lib64 (if needed) as well as installing a basic set of whatever libraries
# are needed for a chroot to function on this platform.

# This script must also define a pull_in_bin function that may be called to
# install an executable together with any libraries it depends on into the
# chroot.

# Finally this script must install a suitable nc.openbsd compatible version of
# netcat into the chroot jail that's available as nc.openbsd and which supports
# connections to unix sockets.

# We are designed to set up the chroot based on binaries from
# x86_64 DragonFly BSD 4.4; some things may need slight modifications if
# being run on a different distribution.

# We require update_pwd_db to be set to work properly on DragonFly BSD
[ -n "$cfg_update_pwd_db" ] && [ "$cfg_update_pwd_db" != "0" ] || {
	echo 'error: Config.pm must set $update_pwd_db to 1 to use a DragonFly BSD jail' >&2
	exit 1
}

chroot_dir="$(pwd)"

mkdir -p dev proc
chown 0:0 dev proc

# Extra directories
mkdir -p libexec var/tmp

# Install the devfsctl chroot ruleset
rm -f etc/devfs.conf
cat "$curdir/fstab/devfsctl_chroot_ruleset" >etc/devfs.conf
chown 0:0 etc/devfs.conf
chmod 0444 etc/devfs.conf

# Make sure there's an auth.conf file, empty is fine
if ! [ -e etc/auth.conf ]; then
	>etc/auth.conf
	chown 0:0 etc/auth.conf
	chmod 0444 etc/auth.conf
fi

cp_p() {
	# use cpio to avoid setting flags
	# must NOT use passthrough mode as that will set flags on newer systems
	[ "$2" = "${2%/}/" ] || ! [ -d "$chroot_dir/$2" ] || set -- "$1" "$2/"
	(cd "$(dirname "$1")" && echo "$(basename "$1")" |
	cpio -o -L 2>/dev/null | { cd "$chroot_dir/${2%/*}" && cpio -i -m -u; } 2>/dev/null)
	test $? -eq 0
	if [ "${2%/*}" != "${2%/}" ]; then
		mv -f "$chroot_dir/${2%/*}/$(basename "$1")" \
			"$chroot_dir/${2%/*}/$(basename "$2")"
	fi
}

# Bring in basic libraries:
rm -f lib/* libexec/*
# ld-elf.so.2:
cp_p /libexec/ld-elf.so.2 libexec/

pull_in_lib() {
	[ -f "$1" ] || return
	dst="${2%/}/$(basename "$1")"
	if [ ! -e "$dst" ] || [ "$1" -nt "$dst" ]; then
		cp_p "$1" "$dst"
		for llib in $(ldd "$1" | grep '=>' | LC_ALL=C awk '{print $3}'); do
			(pull_in_lib "$llib" lib)
			test $? -eq 0
		done
	fi

}

# pull_in_bin takes two arguments:
# 1: the full path to a binary to pull in (together with any library dependencies)
# 2: the destination directory relative to the current directory to copy it to which
# MUST already exist with optional alternate name if the name in the chroot should be different
# 3: optional name of binary that if already in $2 and the same as $1 hard link to instead
# for example, "pull_in_bin /bin/sh bin" will install the shell into the chroot bin directory
# for example, "pull_in_bin /bin/bash bin/sh" will install bash as the chroot bin/sh
# IMPORTANT: argument 1 must be a machine binary, NOT a shell script or other interpreted text
# IMPORTANT: text scripts can simply be copied in or installed as they don't have libraries to copy
# NOTE: it's expected that calling this function on a running chroot may cause temporary disruption
# In order to avoid a busy error while replacing binaries we first copy the binary to the
# var/tmp directory and then force move it into place after the libs have been brought in.
pull_in_bin() {
	bin="$1"; bdst="$2"
	if [ -d "${bdst%/}" ]; then
		bnam="$(basename "$bin")"
		bdst="${bdst%/}"
	else
		bnam="$(basename "$bdst")"
		bdst="${bdst%/*}"
	fi
	if [ -n "$3" ] && [ "$3" != "$bnam" ] &&
	   [ -r "$bdst/$3" ] && [ -x "$bdst/$3" ] && cmp -s "$bin" "$bdst/$3"; then
		ln -f "$bdst/$3" "$bdst/$bnam"
		return 0
	fi
	cp_p "$bin" var/tmp/
	# ...and all the dependencies.
	for lib in $(ldd "$bin" | grep '=>' | LC_ALL=C awk '{print $3}'); do
		pull_in_lib "$lib" lib
	done
	mv -f "var/tmp/$(basename "$bin")" "$bdst/$bnam"
}

# A catch all that needs to be called after everything's been pulled in
chroot_update_permissions() {
	# Be paranoid
	[ -n "$chroot_dir" ] && [ "$chroot_dir" != "/" ] || { echo bad '$chroot_dir' >&2; exit 2; }
	cd "$chroot_dir" || { echo bad '$chroot_dir' >&2; exit 2; }
	rm -rf var/tmp
	chown -R 0:0 bin lib sbin var libexec
	# bootstrap the master.passwd database
	rm -f etc/master.passwd etc/pwd.db etc/spwd.db
	LC_ALL=C awk -F ':' '{ print $1 ":" $2 ":" $3 ":" $4 "::0:0:" $5 ":" $6 ":" $7 }' <etc/passwd >etc/master.passwd
	PW_SCAN_BIG_IDS=1 pwd_mkdb -d etc etc/master.passwd 2>/dev/null
	chown $cfg_mirror_user:$cfg_owning_group etc/master.passwd etc/pwd.db etc/spwd.db
	chmod 0664 etc/master.passwd etc/pwd.db etc/spwd.db
}

# the nc.openbsd compatible utility is available as $var_nc_openbsd_bin
pull_in_bin "$var_nc_openbsd_bin" bin/nc.openbsd
