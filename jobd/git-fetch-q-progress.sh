#!/bin/sh

# git-fetch-q-progress.sh - fetch with progress but not ref noise
# Copyright (C) 2017,2018 Kyle J. McKay.  All rights reserved.

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Version 1.0.1

set -e
git rev-parse --git-dir >/dev/null || exit
PERLPROG='
#!perl
#line 27 "git-fetch-q-progress.sh"
use strict;
use warnings;
use Fcntl;
$| = 1;
my $flags = fcntl(STDIN, F_GETFL, 0);
defined($flags) or die "fcntl failed: $!";
fcntl(STDIN, F_SETFL, $flags | O_NONBLOCK) or die "fcntl failed: $!";
my $data = "";
my $cnt;
my $last = undef;
sub flush_last { print($last) and $last = undef if defined($last); }
my ($total, $creates, $deletes, $forces) = (0,0,0,0);
sub flush_results {
	if ($total) {
		my $msg = $total . " ref";
		$msg .= "s" unless $total == 1;
		$msg .= " affected";
		if ($creates) {
			$msg .= ", " . $creates . " creation";
			$msg .= "s" unless $creates == 1;
		}
		if ($deletes) {
			$msg .= ", " . $deletes . " deletion";
			$msg .= "s" unless $deletes == 1;
		}
		if ($forces) {
			$msg .= ", " . $forces . " forced update";
			$msg .= "s" unless $forces == 1;
		}
		print $msg, "\n";
	}
	$total = $creates = $deletes = $forces = 0;
}
my $sawref;
sub handle_line {
	if ($_[0] =~ /^ ([*=!tX +-]) /) {
		++$total unless $1 eq "!" || $1 eq "=" || $1 eq "X";
		if ($1 eq "-") {
			++$deletes;
		} elsif ($1 eq "*") {
			++$creates;
		} elsif ($1 eq "+") {
			++$forces;
		}
		$sawref = 1, $last = undef if !$sawref;
	} elsif ($_[0] =~ /^error:/) {
		flush_last;
		print STDERR $_[0];
	} else {
		flush_last;
		$sawref=0, flush_results if $sawref;
		$last = $_[0];
	}
}
do {
	my ($rin, $win, $ein);
	$rin = $win = "";
	vec($rin, fileno(STDIN), 1) = 1;
	$ein = $rin;
	select($rin, $win, $ein, undef);
	$cnt = read(STDIN, $data, 1024, length($data));
	my $idx = rindex($data, "\r");
	if ($idx >= 0) {
		++$idx;
		flush_last;
		print substr($data, 0, $idx);
		substr($data, 0, $idx) = "";
	}
	while (($idx = index($data, "\n")) >= 0) {
		++$idx;
		handle_line(substr($data, 0, $idx));
		substr($data, 0, $idx) = "";
	}
} while $cnt;
flush_last;
$data eq "" or print $data;
flush_results
'
exec 3>&1
ec=
read -r ec <<EOT || :
$(
	exec 4>&3 3>&1 1>&4 4>&-
	{ ec=0 && git fetch --progress "$@" 3>&- || ec=$?; echo $ec >&3; } 2>&1 |
	{ export PERLPROG && perl -e 'eval $ENV{PERLPROG}'; } >&2
)
EOT
exec 3>&-
exit ${ec:-1}
