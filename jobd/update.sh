#!/bin/sh

. @basedir@/shlib.sh

set -e

if [ $# -ne 1 ]; then
	echo "Usage: update.sh projname" >&2
	exit 1
fi

# date -R is linux-only, POSIX equivalent is '+%a, %d %b %Y %T %z'
datefmt='+%a, %d %b %Y %T %z'

git_fetch_q_progress() {
	PATH="$var_git_exec_path:$cfg_basedir/bin:$PATH" @basedir@/jobd/git-fetch-q-progress.sh "$@"
}

# freshen_loose_objects full-sha ...
# if "$n" is a loose object, set its modification time to now
# otherwise silently do nothing with no error.  To facilitate conversion
# of mirror projects to push projects we also add group write  permission.
freshen_loose_objects() {
	_list=
	for _sha; do
		_fn="${_sha#??}"
		_shard="${_sha%$_fn}"
		_list="$_list objects/$_shard/$_fn"
	done
	if [ -n "$_list" ]; then
		chmod ug+w $_list 2>/dev/null || :
		touch -c $_list 2>/dev/null || :
	fi
}

# darcs fast-export | git fast-import with error handling
git_darcs_fetch() (
	set_utf8_locale
	_err1=
	_err2=
	exec 3>&1
	{ read -r _err1 || :; read -r _err2 || :; } <<-EOT
	$(
		exec 4>&3 3>&1 1>&4 4>&-
		{
			_e1=0
			"$cfg_basedir"/bin/darcs-fast-export \
				--export-marks="$(pwd)/dfe-marks" \
				--import-marks="$(pwd)/dfe-marks" "$1" 3>&- || _e1=$?
			echo $_e1 >&3
		} |
		{
			_e2=0
			git fast-import \
				--export-marks="$(pwd)/gfi-marks" \
				--export-pack-edges="$(pwd)/gfi-packs" \
				--import-marks="$(pwd)/gfi-marks" \
				--force 3>&- || _e2=$?
			echo $_e2 >&3
		}
	)
	EOT
	exec 3>&-
	[ "$_err1" = 0 ] && [ "$_err2" = 0 ]
	return $?
)

# bzr fast-export | git fast-import with error handling
git_bzr_fetch() (
	set_utf8_locale
	BZR_LOG=/dev/null
	export BZR_LOG
	_err1=
	_err2=
	exec 3>&1
	{ read -r _err1 || :; read -r _err2 || :; } <<-EOT
	$(
		exec 4>&3 3>&1 1>&4 4>&-
		{
			_e1=0
			bzr fast-export --plain \
				--export-marks="$(pwd)/bfe-marks" \
				--import-marks="$(pwd)/bfe-marks" "$1" 3>&- || _e1=$?
			echo $_e1 >&3
		} |
		{
			_e2=0
			git fast-import \
				--export-marks="$(pwd)/gfi-marks" \
				--export-pack-edges="$(pwd)/gfi-packs" \
				--import-marks="$(pwd)/gfi-marks" \
				--force 3>&- || _e2=$?
			echo $_e2 >&3
		}
	)
	EOT
	exec 3>&-
	[ "$_err1" = 0 ] && [ "$_err2" = 0 ]
	return $?
)

[ -n "$cfg_mirror" ] || { echo "Mirroring is disabled" >&2; exit 0; }

umask 002
[ "$cfg_permission_control" != "Hooks" ] || umask 000
clean_git_env

proj="${1%.git}"
cd "$cfg_reporoot/$proj.git"

# Activate a mini-gc if needed
# We do this here as well as after a successful fetch so that if we're stuck
# in a fetch loop where fetches are succeeding in fetching new packs but the
# ref update is failing for some reason (perhaps a non-commit under refs/heads)
# and a previous invokation therefore had a "bang" exit then we will still
# get the .needsgc flag set in a timely fashion to avoid excess pack build up.
check_and_set_needsgc

trap 'if [ $? != 0 ]; then echo "update failed dir: $PWD" >&2; fi; rm -f "$bang_log"' EXIT
trap 'exit 130' INT
trap 'exit 143' TERM

if [ "${force_update:-0}" = "0" ] && check_interval lastrefresh $cfg_min_mirror_interval; then
	progress "= [$proj] update skip (last at $(config_get lastrefresh))"
	exit 0
fi
if [ -e .nofetch ]; then
	progress "x [$proj] update disabled (.nofetch exists)"
	exit 0
fi
progress "+ [$proj] update ($(date))"

# Any pre-existing FETCH_HEAD from a previous fetch, failed or not, is garbage
rm -f FETCH_HEAD

# A previous failed update attempt can leave a huge tmp_pack_XXXXXX file behind.
# Since no pushes are allowed to mirrors, we know that any such files that exist
# at this point in time are garbage and can be safely deleted, we do not even
# need to check how old they are.  A tmp_idx_XXXXXX file is also created during
# the later stages of the fetch process, so we kill any of those as well.
find -L objects/pack -maxdepth 1 -type f -name "tmp_pack_?*" -exec rm -f '{}' + || :
find -L objects/pack -maxdepth 1 -type f -name "tmp_idx_?*" -exec rm -f '{}' + || :

# Make sure we have a reflogs subdirectory and abort the update if not
# This should not count as a normal "bang" failure if unsuccessful
[ -d reflogs ] || mkdir -p reflogs >/dev/null 2>&1 || :
[ -d reflogs ]

keep_bang_log=
do_check_after_refs=1
bang_setup
bang_action="update"
bang_trap() {
	if [ -n "$1" ]; then
		# Throttle retries
		# Since gitweb shows the .last_refresh date, it's safe to update
		# gitweb.lastrefresh to throttle the updates w/o corrupting the
		# last refresh date display on the gitweb summary page
		# It's therefore important that we do NOT touch .last_refresh here
		config_set lastrefresh "$(date "$datefmt")"
	fi
}

bang echo "Project: $proj"
bang echo "   Date: $(TZ=UTC date '+%Y-%m-%d %T UTC')"
bang echo ""
mail="$(config_get owner)" || :
url="$(config_get baseurl)" || :
case "$url" in *"	"*|*" "*|"")
	bang_eval 'echo "Bad mirror URL (\"$url\")"; ! :'
	exit 1
esac
bang echo "Mirroring from URL \"$url\""
bang echo ""
statusok="$(git config --bool gitweb.statusupdates 2>/dev/null || echo true)"
mailaddrs=
[ "$statusok" = "false" ] || [ -z "$mail" ] || mailaddrs="$mail"
[ -z "$cfg_admincc" ] || [ "$cfg_admincc" = "0" ] || [ -z "$cfg_admin" ] ||
if [ -z "$mailaddrs" ]; then mailaddrs="$cfg_admin"; else mailaddrs="$mailaddrs,$cfg_admin"; fi

bang_eval "git for-each-ref --format '%(refname) %(objectname)' >.refs-temp"
bang_eval "LC_ALL=C sort -b -k1,1 <.refs-temp >.refs-before"

check_after_refs() {
	[ -n "$do_check_after_refs" ] || return 0
	bang_eval "git for-each-ref --format '%(refname) %(objectname)' >.refs-temp"
	bang_eval "LC_ALL=C sort -b -k1,1 <.refs-temp >.refs-after"
	refschanged=
	cmp -s .refs-before .refs-after || refschanged=1
	do_check_after_refs=
}

! [ -e .delaygc ] || >.allowgc || :

# Make sure we don't get any unwanted loose objects
# Starting with Git v2.10.0 fast-import can generate loose objects unless we
# tweak its configuration to prevent that
git_add_config 'fetch.unpackLimit=1'
# Note the git config documentation is wrong
# transfer.unpackLimit, if set, overrides fetch.unpackLimit
git_add_config 'transfer.unpackLimit=1'
# But not the Git v2.10.0 and later fastimport.unpackLimit which improperly uses <= instead of <
git_add_config 'fastimport.unpackLimit=0'

# remember the starting time so we can easily combine fetched loose objects
# we sleep for 1 second after creating .needspack to make sure all objects are newer
if ! [ -e .needspack ]; then
	rm -f .needspack
	>.needspack
	sleep 1
fi

case "$url" in
	svn://* | svn+http://* | svn+https://* | svn+file://* | svn+ssh://*)
		[ -n "$cfg_mirror_svn" ] || { echo "Mirroring svn is disabled" >&2; exit 0; }
		# Allow the username to be specified in the "svn-credential.svn.username"
		# property and the password in the "svn-credential.svn.password" property
		# Use an 'anonsvn' username by default as is commonly used for anonymous svn
		# Default the password to the same as the username
		# The password property will be ignored unless a username has been specified
		if svnuser="$(git config --get svn-credential.svn.username)" && [ -n "$svnuser" ]; then
			if ! svnpass="$(git config --get svn-credential.svn.password)"; then
				svnpass="$svnuser"
			fi
			url1="${url#*://}"
			url1="${url1%%/*}"
			case "$url1" in ?*"@"?*)
				urlsch="${url%%://*}"
				url="$urlsch://${url#*@}"
			esac
		else
			# As a fallback, check in the URL, just in case
			url1="${url#*://}"
			url1="${url1%%/*}"
			svnuser=
			case "$url1" in ?*"@"?*)
				urlsch="${url%%://*}"
				url="$urlsch://${url#*@}"
				url1="${url1%%@*}"
				svnuser="${url1%%:*}"
				if [ -n "$svnuser" ]; then
					svnpass="$svnuser"
					case "$url1" in *":"*)
						svnpass="${url1#*:}"
					esac
				fi
			esac
			if [ -z "$svnuser" ]; then
				svnuser="anonsvn"
				svnpass="anonsvn"
			fi
		fi
		GIT_ASKPASS_PASSWORD="$svnpass"
		export GIT_ASKPASS_PASSWORD
		# Update the git svn url to match baseurl but be cognizant of any
		# needed prefix changes.  See the comments in taskd/clone.sh about
		# why we need to put up with a prefix in the first place.
		case "$url" in svn+ssh://*) svnurl="$url";; *) svnurl="${url#svn+}";; esac
		svnurl="${svnurl%/}"
		svnurlold="$(config_get svnurl)" || :
		if [ "$svnurl" != "$svnurlold" ]; then
			# We better already have an svn-remote.svn.fetch setting
			bang test -n "$(git config --get-all svn-remote.svn.fetch)" || :
			# the only way to truly know what the proper prefix is
			# is to attempt a fresh git-svn init -s on the new url
			rm -rf svn-new-url || :
			# We require svn info to succeed on the URL otherwise it's
			# simply not a valid URL and without using -s on the init it
			# will not otherwise be tested until the fetch
			bang eval 'svn --non-interactive --username "$svnuser" --password "$svnpass" info "$svnurl" >/dev/null'
			bang mkdir svn-new-url
			GIT_DIR=svn-new-url bang git init --bare --quiet
			# We initially use -s for the init which will possibly shorten
			# the URL.  However, the shortening can fail if a password is
			# not required for the longer version but is for the shorter,
			# so try again without -s if the -s version fails.
			cmdstr='git svn init --username="$svnuser" --prefix "" -s "$svnurl" </dev/null >/dev/null 2>&1 || '
			cmdstr="$cmdstr"'git svn init --username="$svnuser" --prefix "" "$svnurl" </dev/null >/dev/null 2>&1'
			GIT_DIR=svn-new-url bang eval "$cmdstr"
			gitsvnurl="$(GIT_DIR=svn-new-url git config --get svn-remote.svn.url)" || :
			gitsvnfetch="$(GIT_DIR=svn-new-url git config --get svn-remote.svn.fetch)" || :
			gitsvnprefixnew="${gitsvnfetch%%:*}"
			gitsvnsuffixnew="${gitsvnprefixnew##*/}"
			gitsvnprefixnew="${gitsvnprefixnew%$gitsvnsuffixnew}"
			rm -rf svn-new-url || :
			# Using GIT_DIR= with bang leaves it set to svn-new-url, so reset it to .
			GIT_DIR=.
			if [ "$gitsvnurl" != "$(git config --get svn-remote.svn.url || :)" ]; then
				# The url has been changed.
				# We must update the url and replace the prefix on all config items
				gitsvnfetch="$(git config --get-all svn-remote.svn.fetch | head -1)" || :
				gitsvnprefixold="${gitsvnfetch%%:*}"
				gitsvnsuffixold="${gitsvnprefixold##*/}"
				gitsvnprefixold="${gitsvnprefixold%$gitsvnsuffixold}"
				git config --remove-section 'svn-remote.svnnew' 2>/dev/null || :
				git config 'svn-remote.svnnew.url' "$gitsvnurl"
				git config --get-regexp '^svn-remote\.svn\.' |
				while read -r sname sval; do
					case "$sname" in
					svn-remote.svn.fetch|svn-remote.svn.branches|svn-remote.svn.tags)
						sname="${sname#svn-remote.svn.}"
						sval="${sval#$gitsvnprefixold}"
						bang git config --add "svn-remote.svnnew.$sname" "${gitsvnprefixnew}$sval"
					esac
				done
				test $? -eq 0
				bang git config -f svn/.metadata svn-remote.svn.reposRoot "$gitsvnurl"
				bang git config --remove-section svn-remote.svn
				bang git config --rename-section svn-remote.svnnew svn-remote.svn
			fi
			bang config_set svnurl "$svnurl"
		fi
		# remove any stale *.lock files greater than 1 hour old in case
		# git-svn was killed on the last update because it took too long
		find -L svn -type f -name '*.lock' -mmin +60 -exec rm -f '{}' + 2>/dev/null || :
		GIT_DIR=. bang git svn fetch --log-window-size=$var_log_window_size --username="$svnuser" --quiet </dev/null
		# git svn does not preserve group permissions in the svn subdirectory
		chmod -R ug+rw,o+r svn
		# git svn also leaves behind ref turds that end with @nnn
		# We get rid of them now
		git for-each-ref --format='%(refname)' |
		LC_ALL=C sed '/^..*@[1-9][0-9]*$/!d; s/^/delete /' |
		git_updateref_stdin
		unset GIT_ASKPASS_PASSWORD
		;;
	darcs://* | darcs+http://* | darcs+https://*)
		[ -n "$cfg_mirror_darcs" ] || { echo "Mirroring darcs is disabled" >&2; exit 0; }
		case "$url" in
			darcs://*) darcsurl="http://${url#darcs://}";;
			*) darcsurl="${url#darcs+}";;
		esac
		# remove any stale lock files greater than 1 hour old in case
		# darcs_fast_export was killed on the last update because it took too long
		find -L *.darcs -maxdepth 2 -type f -name 'lock' -mmin +60 -exec rm -f '{}' + 2>/dev/null || :
		bang git_darcs_fetch "$darcsurl"
		;;
	bzr://*)
		[ -n "$cfg_mirror_bzr" ] || { echo "Mirroring bzr is disabled" >&2; exit 0; }
		bzrurl="${url#bzr://}"
		bang git_bzr_fetch "$bzrurl"
		;;
	hg+http://* | hg+https://* | hg+file://* | hg+ssh://*)
		[ -n "$cfg_mirror_hg" ] || { echo "Mirroring hg is disabled" >&2; exit 0; }
		# We just remove hg+ here, so hg+http://... becomes http://...
		hgurl="${url#hg+}"
		# Fetch any new updates
		bang hg -R "$(pwd)/repo.hg" pull
		# Do the fast-export | fast-import
		bang git_hg_fetch
		;;
	*)
		[ "$url" = "$(git config --get remote.origin.url || :)" ] || bang config_set_raw remote.origin.url "$url"
		pruneopt=--prune
		[ "$(git config --bool fetch.prune 2>/dev/null || :)" != "false" ] || pruneopt=
		if ! is_gfi_mirror_url "$url"; then
			lastwasclean=
			[ "$(git config --bool girocco.lastupdateclean 2>/dev/null || :)" != "true" ] || lastwasclean=1
			nextisclean=
			[ "$(git config --bool girocco.cleanmirror 2>/dev/null || :)" != "true" ] || nextisclean=1
			if [ "$nextisclean" != "$lastwasclean" ]; then
				if [ -n "$nextisclean" ]; then
					git config --replace-all remote.origin.fetch "+refs/heads/*:refs/heads/*"
					git config --add remote.origin.fetch "+refs/tags/*:refs/tags/*"
					git config --add remote.origin.fetch "+refs/notes/*:refs/notes/*"
					git config --add remote.origin.fetch "+refs/top-bases/*:refs/top-bases/*"
				else
					git config --replace-all remote.origin.fetch "+refs/*:refs/*"
				fi
			fi
		fi
		# remember the starting time so we can easily detect new packs for fast-import mirrors
		# we sleep for 1 second after creating .gfipack to make sure all packs are newer
		if is_gfi_mirror_url "$url" && [ ! -e .gfipack ]; then
			rm -f .gfipack
			>.gfipack
			sleep 1
		fi
		fetcharg="default"
		git config remotes.default >/dev/null 2>&1 || fetcharg="--all"
		fetchcmd="git fetch"
		[ "$show_progress" != "0" ] || fetchcmd="git fetch -q"
		if [ -n "$var_have_git_171" ] && [ "${show_progress:-0}" != "0" ]; then
			# git fetch learned --progress in v1.7.1
			case "$show_progress" in
			[2-9]*|1[0-9]*)
				# full volume progress with all the spammy noise
				fetchcmd="git fetch --progress"
				;;
			*)
				# a kinder, gentler progress that doesn't leave one
				# covered all over in exploded bits of spam afterwards
				fetchcmd="git_fetch_q_progress"
				;;
			esac
		fi
		# It's possible for a fetch to actually do something while still returning
		# a non-zero result (perhaps some of the refs were updated but some were
		# not -- a malicious Git-impersonation trying to set refs/heads/... refs
		# to non-commit objects for example).
		GIT_SSL_NO_VERIFY=1 bang_catch eval "$fetchcmd" $pruneopt --multiple "$fetcharg"
		# If we did fetch anything, don't treat it as an error, but do keep the log;
		# otherwise invoke bang_failed as for a normal failure
		if [ "${bang_errcode:-0}" != "0" ]; then
			save_bang_errcode="$bang_errcode"
			check_after_refs
			if [ -n "$refschanged" ]; then
				keep_bang_log="git fetch${pruneopt:+ $pruneopt} --multiple $fetcharg"
			else
				bang_cmd="git fetch${pruneopt:+ $pruneopt} --multiple $fetcharg"
				bang_errcode="$save_bang_errcode"
				bang_failed
			fi
		fi
		if ! is_gfi_mirror_url "$url" && [ "$nextisclean" != "$lastwasclean" ]; then
			if [ -n "$nextisclean" ]; then
				# We must manually purge the unclean refs now as even prune won't do it
				git for-each-ref --format='%(refname)' |
				LC_ALL=C sed \
					-e '/^refs\/heads\//d' \
					-e '/^refs\/tags\//d' \
					-e '/^refs\/notes\//d' \
					-e '/^refs\/top-bases\//d' \
					-e 's/^/delete /' |
				git_updateref_stdin
			fi
			git config --bool girocco.lastupdateclean ${nextisclean:-0}
		fi
		if [ -e .gfipack ] && is_gfi_mirror_url "$url"; then
			find -L objects/pack -type f -newer .gfipack -name "pack-$octet20*.pack" -print >>gfi-packs
			rm -f .gfipack
		fi
		;;
esac

# The objects subdirectories permissions must be updated now.
# In the case of a dumb http clone, the permissions will not be correct
# (missing group write) despite the core.sharedrepository=1 setting!
# The objects themselves seem to have the correct permissions.
# This problem appears to have been fixed in the most recent git versions.
perms=g+w
[ "$cfg_permission_control" != "Hooks" ] || perms=go+w
chmod $perms $(find -L objects -maxdepth 1 -type d) 2>/dev/null || :

bang git update-server-info

# We maintain the last refresh date in two places deliberately
# so that it's available as part of the config data and also
# as a standalone file timestamp that can be accessed without git.
bang config_set lastrefresh "$(date "$datefmt")"
{ >.last_refresh; } 2>/dev/null || :

# Check to see if any refs changed
check_after_refs

# Force a mini-gc if $Girocco::Config::delay_gfi_redelta is false and there's
# at least one gfi pack present now
if [ -z "$cfg_delay_gfi_redelta" ] && ! [ -e .needsgc ] &&
   [ -f gfi-packs ] && [ -s gfi-packs ] && is_gfi_mirror_url "$url"; then
	>.needsgc
fi

# Activate a mini-gc if needed
check_and_set_needsgc

# Look at which refs changed and trigger ref-change for these
sockpath="$cfg_chroot/etc/taskd.socket"
if [ -n "$refschanged" ]; then
	bang config_set lastreceive "$(date '+%a, %d %b %Y %T %z')"
	# We always use UTC for the log timestamp so that chroot and non-chroot match up.
	# We don't have to worry about multiple log files since only one update runs
	lognamets="$(TZ=UTC date '+%Y%m%d_%H%M%S')"
	loghhmmss="${lognamets##*_}"
	logname="reflogs/${lognamets%%_*}"
	# We freshen the mod time to now on any old or new ref that is a loose object
	# For old refs we do it so we will be able to keep them around for 1 day
	# For new refs we do it in case we are about to run gc and the new ref
	# actually points to an oldish loose object that had been unreachable
	# We probably do not need to do it for new refs as Git tries to do that,
	# but since we're already doing it for old refs (which Git does not do),
	# it's almost no extra work for new refs, just in case.
	{
		echo "ref-changes %$proj% $proj"
		LC_ALL=C join .refs-before .refs-after |
		LC_ALL=C sed -e '/^[^ ][^ ]* \([^ ][^ ]*\) \1$/d' |
			while read ref old new; do
				echo "$loghhmmss $old $new $ref" >&3
				freshen_loose_objects "$old" "$new"
				echo "$old $new $ref"
			done
		LC_ALL=C join -v 1 .refs-before .refs-after |
			while read ref old; do
				echo "$loghhmmss $old 0000000000000000000000000000000000000000 $ref" >&3
				freshen_loose_objects "$old"
				echo "$old 0000000000000000000000000000000000000000 $ref"
			done
		LC_ALL=C join -v 2 .refs-before .refs-after |
			while read ref new; do
				echo "$loghhmmss 0000000000000000000000000000000000000000 $new $ref" >&3
				freshen_loose_objects "$new"
				echo "0000000000000000000000000000000000000000 $new $ref"
			done
		git for-each-ref --format='%(objectname) %(objectname) %(refname)' refs/heads
		echo "done ref-changes %$proj% $proj"
	} >.refs-temp 3>>"$logname"
	if [ -S "$sockpath" ]; then
			trap ':' PIPE
			nc_openbsd -w 15 -U "$sockpath" <.refs-temp || :
			trap - PIPE
	fi
	bang config_set lastchange "$(date '+%a, %d %b %Y %T %z')"
	bang_eval "git for-each-ref --sort=-committerdate --format='%(committerdate:iso8601)' \
		--count=1 refs/heads >info/lastactivity"
	! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :
	rm -f .delaygc .allowgc
	if
		[ "${cfg_autogchack:-0}" != "0" ] &&
		[ "$(git config --get --bool girocco.autogchack 2>/dev/null)" != "false" ]
	then
		mv -f .refs-after .refs-last
	fi
fi

# If the repository does not yet have a valid HEAD symref try to set one
# If an empty repository was cloned and then later becomes unempty you just
# lose out on the fancy "symref=HEAD:" logic and get this version instead
check_and_set_head || :

rm -f .refs-before .refs-after .refs-temp FETCH_HEAD

if is_banged; then
	[ -z "$mailaddrs" ] || ! was_banged_message_sent ||
	{
		echo "$proj update succeeded - failure recovery"
		echo "this status message may be disabled on the project admin page"
	} | mailref "update@$cfg_gitweburl/$proj.git" -s "[$cfg_name] $proj update succeeded" "$mailaddrs" || :
	bang_reset
fi

if [ -n "$keep_bang_log" ] && [ -s "$bang_log" ]; then
	cat "$bang_log" >.banglog
	echo "" >>.banglog
	echo "$keep_bang_log failed with error code $save_bang_errcode" >>.banglog
fi

progress "- [$proj] update ($(date))"
