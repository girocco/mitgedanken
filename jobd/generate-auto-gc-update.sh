#!/bin/sh

# This script is designed to be run either directly giving it a project
# name or by being sourced from one of the other scripts.

# When sourced from another script the current directory must be set
# to the top-level --git-dir of the project to operate on and the
# _shlib_done variable must be set to 1 and then after sourcing the
# generate_auto_gc_update function must be called

# If GIROCCO_SUPPRESS_AUTO_GC_UPDATE is set to a non-zero value then this
# script will always exit immediately with success without doing anything

if [ -z "$_shlib_done" ]; then
	[ "${GIROCCO_SUPPRESS_AUTO_GC_UPDATE:-0}" = "0" ] || exit 0

	. @basedir@/shlib.sh

	set -e

	if [ $# -ne 1 ]; then
		echo "Usage: generate-auto-gc-update.sh projname" >&2
		exit 1
	fi

	proj="${1%.git}"
	shift
	cd "$cfg_reporoot/$proj.git"
	unset GIROCCO_SUPPRESS_AUTO_GC_UPDATE
fi

# See the table from maintain-auto-gc-hack.sh
# We test the same conditions here
generate_auto_gc_update() {
	[ "${GIROCCO_SUPPRESS_AUTO_GC_UPDATE:-0}" = "0" ] || return 0
	_hackon=
	if
		[ "${cfg_autogchack:-0}" != "0" ] &&
		{ [ "$cfg_autogchack" != "mirror" ] || ! [ -e .nofetch ]; } &&
		[ "$(git config --get --bool girocco.autogchack 2>/dev/null)" != "false" ]
	then
		_hackon=1
	fi
	[ -n "$_hackon" ] || return 0
	# It's not our responsibility to create the initial .refs-last file
	# But we do need one, so make sure it exists
	[ -f .refs-last ] || >>.refs-last
	# This works like a combination of update.sh and pre-receive and post-receive
	git for-each-ref --format '%(refname) %(objectname)' | LC_ALL=C sort -b -k1,1 >.refs-new.$$
	refschanged=
	cmp -s .refs-last .refs-new.$$ || refschanged=1
	sockpath="$cfg_chroot/etc/taskd.socket"
	if [ -n "$refschanged" ]; then
		config_set lastreceive "$(date '+%a, %d %b %Y %T %z')"
		# See comments in hooks/pre-receive about this
		lognamets="$(TZ=UTC strftime '%Y%m%d_%H%M%S%N')"
		lognamets="${lognamets%???}"
		loghhmmss="${lognamets##*_}"
		loghhmmss="${loghhmmss%??????}"
		logname="reflogs/$lognamets.$$"
		lognametmp="reflogs/tmp_$lognamets.$$"
		v_get_proj_from_dir proj && proj="${proj%.git}"
		{
			echo "ref-changes %@local% $proj"
			LC_ALL=C join .refs-last .refs-new.$$ |
			LC_ALL=C sed -e '/^[^ ][^ ]* \([^ ][^ ]*\) \1$/d' |
				while read ref old new; do
					echo "$loghhmmss $old $new $ref" >&3
					echo "$old $new $ref"
				done
			LC_ALL=C join -v 1 .refs-last .refs-new.$$ |
				while read ref old; do
					echo "$loghhmmss $old 0000000000000000000000000000000000000000 $ref" >&3
					echo "$old 0000000000000000000000000000000000000000 $ref"
				done
			LC_ALL=C join -v 2 .refs-last .refs-new.$$ |
				while read ref new; do
					echo "$loghhmmss 0000000000000000000000000000000000000000 $new $ref" >&3
					echo "0000000000000000000000000000000000000000 $new $ref"
				done
			LC_ALL=C join .refs-last .refs-new.$$ |
			LC_ALL=C sed -ne '/^refs\/heads\/[^ ][^ ]* \([^ ][^ ]*\) \1$/p' |
				while read ref old new; do
					echo "$old $new $ref"
				done
			echo "done ref-changes %@local% $proj"
		} >.refs-temp.$$ 3>>"$lognametmp"
		mv "$lognametmp" "$logname"
		if [ -S "$sockpath" ]; then
			trap ':' PIPE
			nc_openbsd -w 15 -U "$sockpath" <.refs-temp.$$ || :
			trap - PIPE
		fi
		mv -f .refs-new.$$ .refs-last
		config_set lastchange "$(date '+%a, %d %b %Y %T %z')"
		git for-each-ref --sort=-committerdate --format='%(committerdate:iso8601)' \
			--count=1 refs/heads >info/lastactivity
		! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :
		check_and_set_needsgc
		rm -f .delaygc .allowgc
	fi
	rm -f .refs-new.$$ .refs-temp.$$
}

[ -n "$_shlib_done" ] || generate_auto_gc_update
