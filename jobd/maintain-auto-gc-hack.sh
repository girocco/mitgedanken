#!/bin/sh

# This script is designed to be run either directly giving it a project
# name or by being sourced from one of the other scripts.

# When sourced from another script the current directory must be set
# to the top-level --git-dir of the project to operate on and the
# _shlib_done variable must be set to 1 and then after sourcing the
# maintain_auto_gc_hack function must be called

if [ -z "$_shlib_done" ]; then
	. @basedir@/shlib.sh

	set -e

	if [ $# -ne 1 ]; then
		echo "Usage: maintain-auto-gc-hack.sh projname" >&2
		exit 1
	fi

	proj="${1%.git}"
	shift
	cd "$cfg_reporoot/$proj.git"
fi

make_empty_git_pack() {
	if ! [ -f "objects/pack/$1.idx" ] || ! [ -f "objects/pack/$1.pack" ]; then
		rm -f "objects/pack/$1.idx" "objects/pack/$1.pack" || :
		git pack-objects -q $opts --stdout </dev/null |
		git index-pack --stdin "objects/pack/$1.pack" >/dev/null || :
	fi
	! [ -e "objects/pack/$1.keep" ] || rm -f "objects/pack/$1.keep" || :
}

# In order to get pre-auto-gc to run we maintain the following:
#   1) gc.auto=1
#   2) gc.autopacklimit=1
#   3) pre-auto-gc-1.pack (and .idx)
#   4) pre-auto-gc-2.pack (and .idx)
# In addition the following happens:
#   5) post-receive and update.sh keep .refs-last updated
#
# The current state pre-auto-gc hack is determined by the following:
#
# $Girocco::Config::autogchack | repository's girocco.autogchack config | state
# ---------------------------- | -------------------------------------- | -----
# mirror and .nofetch exists   | don't care                             | off
# false                        | don't care                             | off
# true (or mirror w/o .nofetch)| true or unset                          | on
# true (or mirror w/o .nofetch)| false                                  | off
#
# When $Girocco::Config::autogchack is the special value "mirror" it behaves
# as "false" when the .nofetch file exists and "true" when it does not.
#
# After determining what state we should be in, we make suitable adjustments
# if things are not set up properly including creating a .refs-last file
#
# Note that if the auto gc hack is disabled and nothing needs to be changed
# then only two read-only "git config" commands get run which makes this
# very low overhead considering it normally only runs at gc.sh time.
maintain_auto_gc_hack() {
	_hackon=
	if
		[ "${cfg_autogchack:-0}" != "0" ] &&
		{ [ "$cfg_autogchack" != "mirror" ] || ! [ -e .nofetch ]; } &&
		[ "$(git config --get --bool girocco.autogchack 2>/dev/null)" != "false" ]
	then
		_hackon=1
	fi
	if [ -z "$_hackon" ]; then
		# Make the hack be off, but still allow pre-auto-gc to run if there are
		# enough loose objects or packs in order to ensure that .needsgc gets set
		rm -f .refs-last
		rm -f objects/pack/pre-auto-gc-?.idx objects/pack/pre-auto-gc-?.pack || :
		[ "$(git config -f config --get gc.auto)" = "3000" ] || git config gc.auto 3000 || :
		[ "$(git config -f config --get gc.autopacklimit)" = "25" ] || git config gc.autopacklimit 25 || :
	else
		# Make the hack be on
		# This one may need a bit more work
		# Do the .refs-last file first to make sure it's available
		# before changing the settings that will cause pre-auto-gc to activate
		if ! [ -e .refs-last ]; then
			git for-each-ref --format '%(refname) %(objectname)' |
			LC_ALL=C sort -b -k1,1 >.refs-new.$$ &&
			mv -f .refs-new.$$ .refs-last || :
		fi
		# Make sure at least two pack files exist; they MUST NOT
		# have any associated .keep files for this to work!
		make_empty_git_pack "pre-auto-gc-1" || :
		make_empty_git_pack "pre-auto-gc-2" || :
		# Finally activate auto gc
		[ "$(git config -f config --get gc.autopacklimit)" = "1" ] || git config gc.autopacklimit 1 || :
		[ "$(git config -f config --get gc.auto)" = "1" ] || git config gc.auto 1 || :
	fi
}

[ -n "$_shlib_done" ] || maintain_auto_gc_hack
