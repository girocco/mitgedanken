#!/bin/sh

# combine-packs.sh -- combine Git pack files
# Copyright (C) 2016,2017,2018 Kyle J. McKay.
# All rights reserved

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Version 1.2.2

USAGE="
printf '%s\n' path-to-pack[.idx|.pack] ... |
$(basename "$0") [option]... [pack-objects option]... [pack-base-name]

NOTE: The following options MUST be given before any pack-objects options:

  --replace         on success, remove the input packs, see note below
                    (but any input packs with a .keep are never removed)

  --names           output the 40-char hex sha1 plus '\n' to stdout for each
                    newly created pack(s), if any

  --ignore-missing  silently ignore input pack file names that do not exist

  --ignore-missing-objects
                    silently ignore missing objects (explicit objects when
                    using --objects otherwise those contained in input packs)

  --loose           add the list of all currently existing loose objects in
                    the repository to the list of objects to pack

  --objects         input is a list of object hash id values instead of packs

  --envok           allow use of GIT_OBJECT_DIRECTORY otherwise it is an error
                    to run combine-packs.sh with GIT_OBJECT_DIRECTORY set

  --sort-tags-by-id
                    sort tags by object id rather than embedded tag name
                    using this option avoids using perl when tags are present

  --weak-naming     use perl to produce weaker object names (and likely larger
                    output packs) instead of naming with rev-list '--objects'
                    (this option requires tree objects contain 20-byte hashes)

If --replace is given, ALL packs to be combined MUST be located in
  the objects/pack subdirectory of the current git directory AND the output
  pack base MUST also be omitted (meaning it defaults to objects/pack/pack).

The --loose option can be used both with and without the --objects option.  If
  there are no currently existing loose objects in the repository's objects/
  directory then it's effectively silently ignored.

Note that if --objects is used then --replace and --ignore-missing are invalid.

Unless --ignore-missing-objects is given, any input objects (either given
  explicitly when using --objects otherwise those contained in the input packs)
  that are not present in the current git directory (respecting the value of
  GIT_OBJECT_DIRECTORY if --envok is given) or its alternate object
  directories, if any, will cause combine-packs to fail.
  With this option any such objects are SILENTLY SKIPPED and do NOT appear in
  the output pack(s)!

Unless the --sort-tags-by-id option is used then perl will be used if available
  and any tag objects are present in the input.  It provides the only efficient
  way to extract the embedded tag name from a batch of tag objects reliably.
  However, since the only reason the tag name is extracted is to sort the tag
  objects for better tag deltification, if the tag objects are sorted by the
  tag object id there is never any need to run perl.  In practice, tag objects
  rarely generate deltas and there are almost never enough tag objects in the
  first place for the size savings of the almost-never-happens tag
  deltification to matter anyway.  This option will be activated automatically
  if perl does not appear to be available.

Normally all commit and tree objects to be packed are 'named' using the git
  rev-list --objects command so that the best possible pack(s) can be produced.
  This requires that all tree objects referenced from commits and trees being
  packed (recursively for trees) as well as all the blobs referenced by them
  must be present in the repository or else the 'rev-list --objects' command
  used to name them will fail.  As an alternative the --weak-naming option will
  avoid use of the '--objects' option and name the contents of tree objects
  using a perl script.  The resulting names are good, but not _as_ good which
  may produce a less efficiently packed pack.  It does, however, permit packing
  completely arbitrarily selected objects without error.

A 40-char hex sha1 is taken to be objects/pack/pack-<sha-1>.idx relative to
  the current git directory (as output by \`git rev-parse --git-dir\` or
  by \`git rev-parse --git-common-dir\` for Git version 2.5 or later).

If a <pack-name> does not exist and contains no '/' characters then it is
  retried as objects/pack/<pack-name> instead.

Packs to be combined MUST have an associated .idx file.

The pack-base-name may be a relative path name and if so, is ALWAYS relative
  to the current git directory regardless of any GIT_OBJECT_DIRECTORY setting.

If not given, then the pack-base-name defaults to objects/pack/pack
  relative to the current git directory.

If GIT_OBJECT_DIRECTORY is set to a non-default location (and the --envok flag
  is given to allow it) then everywhere above where it says \"objects/\" is
  effectively replaced with the full absolute path to \"\$GIT_OBJECT_DIRECTORY/\".
  And, obviously, that location is no longer necessarily a subdirectory of the
  current git directory either.

Note that --delta-base-offset is ALWAYS passed to git pack-objects but it is
  the ONLY option that is automatically passed (but remember that --reuse-delta
  and --reuse-object are IMPLIED and must be explicitly disabled if desired).

The options --revs, --unpacked, --all, --reflog, --indexed-objects and
  --stdout are forbidden.  Although --keep-true-parents is allowed it should
  not have any effect at all.  Using --incremental is recommended only for
  wizards or with --objects as in most other cases it will result in an empty
  pack being output.  The combination of --loose --objects --incremental will
  pack up all loose objects not already in a pack (and nothing else if standard
  input is redirected to /dev/null in which case the --objects is optional).

WARNING: the move_aside logic currently only works when pack-base-name is
         completely omitted!
"

set -e

# $$ should be the same in subshells, but just in case, remember it
cp_pid=$$

perltagprog='
#!/usr/bin/perl
#line 140 "combine-packs.sh"
use strict;
use warnings;

sub discard {
  my $count = shift;
  my $x = "";
  while ($count >= 32768) {
    read(STDIN, $x, 32768);
    $count -= 32768;
  }
  read(STDIN, $x, $count) if $count;
}

my @tags = ();
binmode STDIN;
while (<STDIN>) {
  if (/^([0-9a-fA-F]+) ([^ ]+) ([0-9]+)$/) {
    my ($h, $t, $l) = ($1, $2, $3);
    my $te = 0;
    my $tn = "";
    discard(1 + $l), next unless $2 eq "tag";
    my $count = 0;
    while (<STDIN>) {
      $count += length($_);
      chomp;
      last if /^$/;
      $tn = $1 if /^tag ([^ ]+)$/;
      $te = $1 if /^tagger [^>]+> ([0-9]+)/;
      last if $tn && $te;
    }
    discard(1 + $l - $count);
    push(@tags, [$te, "$h $tn\n"]);
  }
}
print map($$_[1], sort({$$b[0] <=> $$a[0]} @tags));
'
perlnameprog='
#!/usr/bin/perl
#line 179 "combine-packs.sh"
use strict;
use warnings;

sub discard {
  my $count = shift;
  my $x = "";
  my $len;
  while ($count >= 32768) {
    $len = read(STDIN, $x, 32768);
    defined($len) && $len == 32768 or die "bad --batch output";
    $count -= 32768;
  }
  if ($count) {
    $len = read(STDIN, $x, $count);
    defined($len) && $len == $count or die "bad --batch output";
  }
}

binmode STDIN;
my $ln = 0;
while (<STDIN>) {
  if (/^([0-9a-fA-F]+) ([^ ]+) ([0-9]+)$/) {
    my ($h, $t, $l) = ($1, $2, $3);
    discard(1 + $l), next unless $2 eq "tree";
    my $tr = "";
    my $count = 0;
    $count = read(STDIN, $tr, $l) if $l;
    defined($count) && $count == $l or die "bad --batch output";
    discard(1);
    ++$ln;
    print $ln, " ", $h, " ~~~~ \n";
    my ($loc, $pos);
    for ($pos = 0;
         $pos < $l && ($loc = index($tr, "\0", $pos)) > $pos && $loc + 20 < $l; 
         $pos = $loc + 20 + 1) {
      substr($tr, $pos, $loc - $pos) =~ /^([0-7]{5,6}) (.*)$/os or die "bad --batch output";
      my ($mode, $name) = (oct($1), $2);
      $mode == 0100644 || $mode == 040000 or next;
      $name =~ tr/\n/?/;
      my $r = $name;
      $r =~ s/[\t\n\013\f\r ]+//gos;
      $r = substr(reverse($r), 0, 16);
      my $i = unpack("H*", substr($tr, $loc + 1, 20));
      ++$ln;
      if (length($r)) {
          print $ln, " ", $i, " ", $r, " ", $name, "\n";
      } else {
          print $ln, " ", $i, "  ", $name, "\n";
      }
    }
    $pos == $l or die "bad --batch output";
  }
}
'

td=
zap=
gd=
gdo=
gdop=

cleanup_on_exit() {
	ewf=
	[ -n "$td" ] && [ -e "$td/success" ] || ewf=1
	[ -z "$td" ] || ! [ -e "$td" ] || rm -rf "$td" || :
	[ -z "$gdop" ] || [ -z "$zap" ] || command find -L "$gdop" -maxdepth 1 -type f -name "*.$zap" -exec rm -f '{}' + || :
	[ -z "$ewf" ] || echo "combine_packs: exiting with failure" >&2 || :
}

trap cleanup_on_exit EXIT
trap 'exit 129' HUP
trap 'exit 130' INT
trap 'exit 131' QUIT
trap 'exit 143' TERM

die() {
	echo "combine-packs: fatal: $*" >&2 || :
	# In case we are in a sub shell force the entire command to exit
	# The trap on TERM will make sure cleanup still happens in this case
	extrapid=
	[ -z "$td" ] || ! [ -s "$td/popid" ] || extrapid="$(cat "$td/popid")" || :
	kill $cp_pid $extrapid || :
	exit 1
}

cmd_path() (
	"unset" -f unalias command "$1" >/dev/null 2>&1 || :
	"unalias" -a >/dev/null 2>&1 || :
	"command" -v "$1"
) 2>/dev/null

# This extra indirection shouldn't be necessary, but it is for some broken sh
# in order for a failure to not prematurely exit die_on_fail with set -e active
do_command() (
	# some shells do not handle "exec command ..." properly but just a
	# plain "exec ..." has the same semantics so "command" is omitted here
	LC_ALL=C exec "$@"
)

die_on_fail() {
	do_command "$@" || {
		_ec=$?
		[ -z "$td" ] || >"$td/failed" || :
		die "failed command ($_ec): $*"
	}
}

# These commands may be the non-final member of a pipe and
# MUST NOT be allowed to silently fail without consequence
awk()  { die_on_fail awk  "$@"; }
cat()  { die_on_fail cat  "$@"; }
cut()  { die_on_fail cut  "$@"; }
find() { die_on_fail find "$@"; }
git()  { die_on_fail git  "$@"; }
join() { die_on_fail join "$@"; }
perl() { die_on_fail perl "$@"; }
sed()  { die_on_fail sed  "$@"; }
sort() { die_on_fail sort "$@"; }

hexdig='[0-9a-f]'
octet="$hexdig$hexdig"
octet4="$octet$octet$octet$octet"
octet19="$octet4$octet4$octet4$octet4$octet$octet$octet"
octet20="$octet4$octet4$octet4$octet4$octet4"

names=
ignoremiss=
looselist=
objectlist=
dozap=
envok=
missok=
noperl=
weak=

while [ $# -ge 1 ]; do case "$1" in
	--names)
		names=1
		shift
		;;
	--replace)
		dozap="zap-$$"
		shift
		;;
	--ignore-missing)
		ignoremiss=1
		shift
		;;
	--ignore-missing-objects)
		missok=1
		shift
		;;
	-h|--help)
		trap - EXIT
		if [ -t 1 ] && pg="$(git var GIT_PAGER 2>/dev/null)" && [ -n "$pg" ]; then
			printf '%s' "${USAGE#?}" | eval "$pg" || :
		else
			printf '%s' "${USAGE#?}" || :
		fi
		exit 0
		;;
	--loose)
		looselist=1
		shift
		;;
	--objects)
		objectlist=1
		shift
		;;
	--envok)
		envok=1
		shift
		;;
	--sort-tags-by-id)
		noperl=1
		shift
		;;
	--weak-naming)
		weak=1
		shift
		;;
	*)
		break
		;;
esac; done
[ -z "$ignoremiss$dozap" ] || [ -z "$objectlist" ] || die "invalid options"
perlbin=
[ -n "$noperl" ] && [ -z "$weak" ] || { perlbin="$(cmd_path perl)" && [ -n "$perlbin" ]; } || noperl=1
[ -z "$weak" ] || [ -n "$perlbin" ] || die "--weak-naming requires perl"

# Always make sure we get the specified objects
GIT_NO_REPLACE_OBJECTS=1
export GIT_NO_REPLACE_OBJECTS
gd="$(git rev-parse --git-dir)" && [ -n "$gd" ] ||
	die "git rev-parse --git-dir failed"
gv="$(git --version)"
gv="${gv#[Gg]it version }"
gv="${gv%%[!0-9.]*}"
IFS=. read -r gvmaj gvmin gvpat <<EOT
$gv
EOT
: "${gvmaj:=0}" "${gvmin:=0}" "${gvpat:=0}"
# git rev-parse added --no-walk support in 1.5.3 which is required
# git cat-file added --batch-check support in 1.5.6 which is required
if [ $gvmaj -lt 1 ] || { [ $gvmaj -eq 1 ] && [ $gvmin -lt 5 ]; } ||
   { [ $gvmaj -eq 1 ] && [ $gvmin -eq 5 ] && [ $gvpat -lt 6 ]; }; then
	die "combine-packs requires at least Git version 1.5.6"
fi
tmp="$gd"
gd="$(cd "$gd" && pwd -P)" || die "cd failed: $tmp"
# git rev-parse added --git-common-dir in 2.5
if [ $gvmaj -gt 2 ] || { [ $gvmaj -eq 2 ] && [ $gvmin -ge 5 ]; }; then
	# rev-parse --git-common-dir is broken and may give an
	# incorrect result without a suitable current directory
	tmp="$gd"
	gd="$(cd "$gd" && cd "$(git rev-parse --git-common-dir)" && pwd -P)" &&
	[ -n "$gd" ] ||
		die "git rev-parse --git-common-dir failed from: $tmp"
fi
# gcfbf is Git Cat-File --Batch-check=Format Option :)
gcfbf=
if [ $gvmaj -gt 1 ] || { [ $gvmaj -eq 1 ] && [ $gvmin -gt 8 ]; } ||
   { [ $gvmaj -eq 1 ] && [ $gvmin -eq 8 ] && [ $gvpat -ge 5 ]; }; then
	gcfbf='=%(objectname) %(objecttype)'
fi
# gcfbo is Git Cat-File --Buffer Option :)
gcfbo=
if [ $gvmaj -gt 2 ] || { [ $gvmaj -eq 2 ] && [ $gvmin -ge 6 ]; }; then
	gcfbo=--buffer
fi
if [ "${GIT_OBJECT_DIRECTORY+set}" = "set" ] && [ -z "$envok" ]; then
	# GIT_OBJECT_DIRECTORY may only be set to $gd/objects without --envok
	godok=
	if [ -n "$GIT_OBJECT_DIRECTORY" ] && [ -d "$GIT_OBJECT_DIRECTORY" ] &&
	   [ -d "$gd/objects" ] && godfp="$(cd "$GIT_OBJECT_DIRECTORY" && pwd -P)" &&
	   gdofp="$(cd "$gd/objects" && pwd -P)" && [ -n "$godfp" ] && [ -n "$gdofp" ] &&
	   [ "$gdofp" = "$godfp" ]; then
		godok=1
	fi
	if [ -z "$godok" ]; then
		die "GIT_OBJECT_DIRECTORY set to non-default location without --envok"
	fi
fi
gdo="${GIT_OBJECT_DIRECTORY:-$gd/objects}"
tmp="$gdo"
gdo="$(cd "$gdo" && pwd -P)" || die "cd failed: $tmp"
[ -d "$gdo/pack" ] || die "no such directory: $gdo/pack"
gdop="$(cd "$gdo/pack" && pwd -P)" || die "cd failed: $gdo/pack"
zap="$dozap"

lastarg=
lastargopt=
packbase=
packbasearg=
nonopts=0
for arg; do
	lastarg="$arg"
	lastargopt=1
	case "$arg" in
	--replace|--names|--ignore-missing|-h|--help|--objects)
		die "invalid options"
		;;
	--revs|--unpacked|--all|--reflog|--indexed-objects)
		die "forbidden pack-objects options"
		;;
	-*)
		:
		;;
	*)
		lastargopt=
		nonopts=$(( $nonopts + 1 ))
	esac
done
if [ $# -gt 0 ] && [ $nonopts -gt 1 ] ||
   { [ $nonopts -eq 1 ] && [ -n "$lastargopt" ]; } ||
   { [ $nonopts -eq 1 ] && [ -z "$lastarg" ]; }; then
	die "invalid options"
fi
if [ $nonopts -eq 1 ]; then
	packbase="$lastarg"
else
	packbase="$gdop/pack"
fi
pbd="$(dirname "$packbase")"
case "$pbd" in /*);;*)
	pbd="$gd/$pbd"
esac
[ -e "$pbd" ] && [ -d "$pbd" ] || die "no such directory: $(dirname "$packbase")"
packbase="$(cd "$pbd" && pwd -P)/$(basename "$packbase")"
pbd="$(dirname "$packbase")"
[ -e "$pbd" ] && [ -d "$pbd" ] || die "internal failure realpathing: $packbase"
packbasecheck="$packbase"
case "$packbase" in "$gd"/?*)
	packbase="${packbase#$gd/}"
esac
[ $nonopts -eq 1 ] || packbasearg="$packbase"
[ -z "$zap" ] || [ -n "$packbasearg" ] || die "--replace does not allow specifying pack-base"
if [ -n "$zap" ] && [ "$(dirname "$packbasecheck")" != "$gdop" ] ; then
	die "--replace and pack base dir not <git-dir-objects>/pack" >&2
fi

td="$(mktemp -d "$gd/cmbnpcks-XXXXXX")"
tdmin="$(basename "$td")"
failed="$td/failed"
listok="$td/listok"
packok="$td/packok"
popid="$td/popid"
success="$td/success"
cm="$tdmin/commits"
cmo="$tdmin/ordered"
tg="$tdmin/tags"
tr="$tdmin/trees"
bl="$tdmin/blobs"
ms="$tdmin/missing"
trbl="$tdmin/treesblobs"
named="$tdmin/named"
named2="$tdmin/named2"

get_pack_base() {
	_name="$1"
	case "$_name" in
	$octet20*)
		_name="$gdop/pack-$_name"
		;;
	*.idx)
		_name="${_name%.idx}"
		;;
	*.pack)
		_name="${_name%.pack}"
		;;
	esac
	if ! [ -e "$_name.idx" ] && ! [ -e "$_name.pack" ]; then
		case "$_name" in */*) :;; *)
			_name="$gdop/$_name"
		esac
	fi
	if ! [ -f "$_name.idx" ] || ! [ -s "$_name.idx" ] ||
	   ! [ -f "$_name.pack" ] || ! [ -s "$_name.pack" ]; then
		[ -z "$ignoremiss" ] || return 0
		die "no such pack found matching: $1" >&2
	fi
	_name="$(cd "$(dirname "$_name")" && pwd -P)/$(basename "$_name")"
	if ! [ -f "$_name.idx" ] || ! [ -s "$_name.idx" ] ||
	   ! [ -f "$_name.pack" ] || ! [ -s "$_name.pack" ]; then
		die "internal failure realpathing: $1" >&2
	fi
	_namecheck="$_name"
	case "$(dirname "$_name")" in "$gd"/?*)
		_name="${_name#$gd/}"
	esac
	if [ -n "$zap" ] && [ "$(dirname "$_namecheck")" != "$gdop" ]; then
		die "--replace and pack not in <git-dir-objects>/pack: $1" >&2
	fi
	echo "$_name"
	return 0
}

# atomic cp
dupe_file() {
	_dupetmp="$(mktemp "$(dirname "$2")/packtmp-XXXXXX")"
	cp -fp "$1" "$_dupetmp"
	mv -f "$_dupetmp" "$2"
}

# add "old" prefix to passed in existing files, but be careful to hard-link
# ALL the files to be renamed to the renamed name BEFORE removing anything
move_aside() {
	for _f; do
		! [ -f "$_f" ] ||
		ln -f "$_f" "$(dirname "$_f")/old$(basename "$_f")" >/dev/null 2>&1 ||
		dupe_file "$_f" "$(dirname "$_f")/old$(basename "$_f")"
	done
	for _f; do
		if [ -f "$_f" ]; then
			rm -f "$_f"
			! test -f "$_f"
		fi
	done
	return 0
}

list_loose_objects() (
	cd "$gdo" || return 1
	objdirs="$(echo $octet)"
	[ "$objdirs" != "$octet" ] || return 0
	find -L $objdirs -mindepth 1 -maxdepth 1 -type f -name "$octet19*" -print | sed 's,/,,'
)

origdir="$PWD"
cd "$gd"
>"$cm"
>"$cmo"
>"$tr"
>"$bl"
if [ -n "$objectlist" ]; then
	gcf='git cat-file $gcfbo --batch-check"$gcfbf"'
	[ -z "$looselist" ] || gcf='{ list_loose_objects && cat; } | '"$gcf"
	eval "$gcf"
else
	[ -z "$zap" ] || command find -L "$gdop" -maxdepth 1 -type f -name "*.$zap" -exec rm -f '{}' + || :
	{
		[ -z "$looselist" ] || list_loose_objects
		while IFS=': ' read -r packraw junk; do
			pack="$(cd "$origdir" && get_pack_base "$packraw" || die "no such pack: $packraw")"
			if [ -n "$pack" ]; then
				[ -z "$zap" ] || [ -e "$pack.keep" ] || >"$pack.$zap"
				git show-index <"$pack.idx"
			fi
		done | cut -d ' ' -f 2
	} | git cat-file $gcfbo --batch-check"$gcfbf"
fi | awk '{
  if ($2=="tree") print $1
  else if ($2=="blob") print $1 >"'"$bl"'"
  else if ($2=="commit") print $1 >"'"$cm"'"
  else if ($2=="tag") print $1 >"'"$tg"'"
  else if ($2=="missing") print $1 >"'"$ms"'"
}' | sort -u >"$tr"
[ -n "$missok" ] || ! [ -s "$ms" ] || die "missing" $(wc -l <"$ms") "object(s)"
echo "g" | cat "$tr" "$bl" - | sort -u >"$trbl"
if [ -z "$weak" ]; then
	git rev-list --no-walk --objects --stdin <"$cm" |
	awk '{
	  if ($1!=$0) print NR " " $0
	  else print $0 >"'"$cmo"'"
	}' |
	sort -t " " -k2,2 |
	join -t " " -1 2 - "$trbl" >"$named"
	join -t " " -v 1 "$tr" "$named" |
	git rev-list --no-walk --objects --stdin |
	awk '{print NR " " $0}' |
	sort -t " " -k2,2 |
	join -t " " -1 2 - "$trbl" >"$named2"
else
	! [ -s "$cm" ] || git rev-list --no-walk --stdin <"$cm" >"$cmo"
	git cat-file $gcfbo --batch <"$tr" |
	perl -e "$perlnameprog" |
	sort -t " " -k2,2 |
	join -t " " -1 2 - "$trbl" >"$named"
fi
pocmd='git pack-objects --delta-base-offset "$@"'
[ -z "$packbasearg" ] || pocmd="$pocmd \"${packbasearg}tmp\""
{
	cat "$cmo"
	! [ -s "$tg" ] || {
		if [ -n "$noperl" ]; then
			sort -u "$tg"
		else
			git cat-file $gcfbo --batch <"$tg" | perl -e "$perltagprog"
		fi
	}
	if [ -z "$weak" ]; then
		{
			join -t " " "$named" "$tr" |
			sort -t " " -k2,2n
			join -t " " "$named2" "$tr" |
			sort -t " " -k2,2n
		} | sed -e 's/\([^ ][^ ]*\) [^ ][^ ]*/\1/'
		{
			join -t " " -v 1 "$named" "$tr" |
			sort -t " " -k2,2n
			join -t " " -v 1 "$named2" "$tr" |
			sort -t " " -k2,2n
		} | awk -F '[ ]' '{
			if (NF >= 3) {
				nm = substr($0, length($1) + length($2) + 3)
				sfx = nm
				gsub(/[\t\n\013\f\r ]+/, "", sfx)
				if (length(sfx)) {
					if (length(sfx) > 16) sfx = substr(sfx, length(sfx) - 15)
					else if (length(sfx) < 16) sfx = sprintf("%16s", sfx)
					split(sfx, c, "")
					r = c[16] c[15] c[14] c[13] c[12] c[11] c[10] c[9] c[8] c[7] c[6] c[5] c[4] c[3] c[2] c[1]
					sub(/[ ]+$/, "", r)
					print NR " " $1 " " r " " nm
				} else print NR " " $1 "  " nm
			} else print NR " " $1 "  "
		}' | sort -t " " -k3,3 -k1,1n | awk -F '[ ]' '{
			if (NF >= 4) {
				nm = substr($0, length($1) + length($2) + length($3) + 4)
				print $2 " " nm
			} else print $2 " "
		}'
	else
		{
			join -t " " "$named" "$tr" |
			sort -t " " -k3,3 -k2,2n
			join -t " " -v 1 "$named" "$tr" |
			sort -t " " -k3,3 -k2,2n
		} | awk -F '[ ]' '{
			if (NF >= 4) {
				nm = substr($0, length($1) + length($2) + length($3) + 4)
				print $1 " " nm
			} else print $1 " "
		}'
	fi
	sort -u "$bl"
	>"$listok"
} | {
	sh -c 'echo $$ >"$1"; pocmd="$2"; shift; shift; eval "exec $pocmd"' sh "$popid" "$pocmd" "$@" || {
		rm -f "$popid"
		die "git pack-objects failed"
	}
	rm -f "$popid"
	>"$packok"
} |
while read -r newpack; do
	if [ -n "$packbasearg" ]; then
		move_aside "$packbasearg"-$newpack.*
		ln -f "${packbasearg}tmp"-$newpack.pack "$packbasearg"-$newpack.pack
		ln -f "${packbasearg}tmp"-$newpack.idx "$packbasearg"-$newpack.idx
		rm -f "${packbasearg}tmp"-$newpack.*
	fi
	[ -z "$names" ] || echo "$newpack"
done
[ $? -eq 0 ] && ! [ -e "$failed" ] && [ -e "$listok" ] && [ -e "$packok" ] ||
	die "unspecified failure"
if [ -n "$zap" ]; then
	(cd "$gdo" && [ -d "pack" ] && find -L "pack" -maxdepth 1 -type f -name "*.$zap" -print) |
	while read -r remove; do
		rm -f "$gdo/${remove%.$zap}".*
	done
fi
>"$success"
