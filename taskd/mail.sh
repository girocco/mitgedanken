#!/bin/sh
#
# Copyright (c) 2007 Andy Parkins
#
# An example hook script to mail out commit update information.  This hook
# sends emails listing new revisions to the repository introduced by the
# change being reported.  The rule is that (for branch updates) each commit
# will appear on one email and one email only.
#
# =================
# This is Girocco-customized version. No matter what is said below, it has
# following changes:
# * Calling with arguments is same as giving them on stdin.
# * Optional fourth parameter is project name, used at most places instead
#   of description.
# * Optional fifth parameter is email sender.
# * Optional sixth parameter is extra e-mail header to add.
# * If MAIL_SH_OTHER_BRANCHES is set it is taken as branch tips of already seen
#   revisions when running show_new_revisions and mail.sh skips its own
#   computation.  If it starts with '@' the rest is the hash of a blob that
#   contains the information (one tip per line).
#   If MAIL_SH_OTHER_BRANCHES is not set, guess at what they are.
#   MAIL_SH_OTHER_BRANCHES is ignored if updates are piped in via stdin.
# * Load shlib.
# * Default subject prefix is site name.
# * Unsubscribe instructions in email footer.
# * Default showrev includes gitweb link and show -C.
# * Nicer subject line.
# * Limit mail size to 256kb.
# =================
#
# This hook is stored in the contrib/hooks directory.  Your distribution
# will have put this somewhere standard.  You should make this script
# executable then link to it in the repository you would like to use it in.
# For example, on debian the hook is stored in
# /usr/share/git-core/contrib/hooks/post-receive-email:
#
#  chmod a+x post-receive-email
#  cd /path/to/your/repository.git
#  ln -sf /usr/share/git-core/contrib/hooks/post-receive-email hooks/post-receive
#
# This hook script assumes it is enabled on the central repository of a
# project, with all users pushing only to it and not between each other.  It
# will still work if you don't operate in that style, but it would become
# possible for the email to be from someone other than the person doing the
# push.
#
# Config
# ------
# hooks.mailinglist
#   This is the list that all pushes will go to; leave it blank to not send
#   emails for every ref update.
# hooks.reverseorder
#   If true new revisions are shown in reverse order (i.e. oldest to newest)
# hooks.summaryonly
#   If true do not include the actual diff when showing new commits (the
#   summary information will still be shown).  Default is false.
# hooks.announcelist
#   This is the list that all pushes of annotated tags will go to.  Leave it
#   blank to default to the mailinglist field.  The announce emails lists
#   the short log summary of the changes since the last annotated tag.
# hooks.envelopesender
#   If set then the -f option is passed to sendmail to allow the envelope
#   sender address to be set
# hooks.emailprefix
#   All emails have their subjects prefixed with this prefix, or "[SCM]"
#   if emailprefix is unset, to aid filtering
# hooks.showrev
#   The shell command used to format each revision in the email, with
#   "%s" replaced with the commit id.  Defaults to "git rev-list -1
#   --pretty %s", displaying the commit id, author, date and log
#   message.  To list full patches separated by a blank line, you
#   could set this to "git show -C %s; echo".
#   To list a gitweb/cgit URL *and* a full patch for each change set, use this:
#     "t=%s; printf 'http://.../?id=%%s' \$t; echo;echo; git show -C \$t; echo"
#   Be careful if "..." contains things that will be expanded by shell "eval"
#   or printf.
#
# Notes
# -----
# All emails include the headers "X-Git-Refname", "X-Git-Oldrev",
# "X-Git-Newrev", and "X-Git-Reftype" to enable fine tuned filtering and
# give information for debugging.
#

# ---------------------------- Functions

#
# Function to output arguments without interpretation followed by \n
#
echol()
{
	printf '%s\n' "$*"
}

#
# Function to run git diff-tree with select warnings redirected to stdout
#
git_diff_tree()
{
	[ -n "$differrtmp" ] ||
		differrtmp="$(mktemp -u "${TMPDIR:-/tmp}/difftree-$$-XXXXXX")"
	_gdtec=0
	>"$differrtmp"
	git diff-tree "$@" 2>"$differrtmp" || _gdtec=$?
	! [ -s "$differrtmp" ] ||
		# omit anything that mentions a ".renameLimit" config item
		<"$differrtmp" LC_ALL=C grep -v -i -F '.renameLimit' || :
	rm -f "$differrtmp"
	return ${_gdtec:-0}
}

#
# Function to read a tag's (the single argument) header fields into
#   tagobject    the "object" value
#   tagtype      the "type" value
#   tagtag       the "tag" value
#   taggername   from "tagger" value before first '<'
#   taggerdate   from "tagger" value after '>' but formatted into a date string
#   taggeremail  from "tagger" value between '<' and '>'
# On return all fields will be set (to empty if failure) and the three tagger
# fields will only be set to non-empty if a "tagger" header field is present.
# If the object does not exist or is not a tag the function returns failure.
read_tag_fields()
{
	tagobject=
	tagtype=
	tagtag=
	taggername=
	taggerdate=
	taggeremail=
	if _tagfields="$(git cat-file tag "$1" | LC_ALL=C awk -F '[ ]' '
			function quotesh(sv) {
				gsub(/'\''/, "'\'\\\\\'\''", sv)
				return "'\''" sv "'\''"
			}
			function trim(sv) {
				sub(/^[ \t]+/, "", sv)
				sub(/[ \t]+$/, "", sv)
				return sv
			}
			function hval(sv) {
				sub(/^[^ ]* /, "", sv)
				return sv
			}
			NR==1,/^$/ {
				if ($1 == "object") to=hval($0)
				if ($1 == "type") ty=hval($0)
				if ($1 == "tag") tt=hval($0)
				if ($1 == "tagger") tr=hval($0)
			}
			END {
				if (tt == "" || to == "" || ty == "") exit 1
				print "tagobject=" quotesh(to)
				print "tagtype=" quotesh(ty)
				print "tagtag=" quotesh(tt)
				if (match(tr, /^[ \t]*[^ \t<][^<]*/)) {
					tn=substr(tr, RSTART, RLENGTH)
					tr=substr(tr, RSTART + RLENGTH)
					tn=trim(tn)
					if (tn != "")
						print "taggername=" quotesh(tn)
					if (match(tr, /^<.*>/)) {
						if (RLENGTH > 2)
							te=substr(tr, RSTART+1, RLENGTH-2)
						tr=substr(tr, RSTART + RLENGTH)
						te=trim(te)
						if (te != "")
							print "taggeremail=" quotesh(te)
						if (match(tr, /^[ \t]*[0-9]+([ \t]+[-+]?[0-9][0-9]([0-9][0-9]([0-9][0-9])?)?)[ \t]*$/))
							td=trim(tr)
						if (td != "")
							print "taggerdate=" quotesh(td)
					}
				}
			}
			')"; then
		eval "$_tagfields"
		[ -z "$taggerdate" ] || taggerdate="$(strftime "$sdatefmt" $taggerdate)"
		return 0
	fi
	return 1
}

#
# Function to prepare for email generation. This decides what type
# of update this is and whether an email should even be generated.
#
prep_for_email()
{
	# --- Arguments
	oldrev="$(git rev-parse --revs-only "$1" --)" || :
	newrev="$(git rev-parse --revs-only "$2" --)" || :
	[ -n "$oldrev" ] && [ -n "$newrev" ] && [ "$oldrev" != "$newrev" ] || return 1
	scratch="${newrev#????????????????}"
	newrev16="${newrev%$scratch}"
	refname="$3"
	refname_type=

	# --- Interpret
	# 0000->1234 (create)
	# 1234->2345 (update)
	# 2345->0000 (delete)
	if [ -n "$oldrev" ] && [ "${oldrev#*[!0]}" = "$oldrev" ]
	then
		change_type="create"
	else
		if [ -n "$newrev" ] && [ "${newrev#*[!0]}" = "$newrev" ]
		then
			change_type="delete"
		else
			change_type="update"
		fi
	fi

	# --- Get the revision types
	newrev_type="$(git cat-file -t "$newrev" 2>/dev/null)" || :
	oldrev_type="$(git cat-file -t "$oldrev" 2>/dev/null)" || :
	case "$change_type" in
	create|update)
		rev="$newrev"
		rev_type="$newrev_type"
		;;
	delete)
		rev="$oldrev"
		rev_type="$oldrev_type"
		;;
	esac

	# The revision type tells us what type the commit is, combined with
	# the location of the ref we can decide between
	#  - working branch
	#  - tracking branch
	#  - unannoted tag
	#  - annotated tag
	case "$refname:$rev_type" in
		refs/tags/*:commit)
			# un-annotated tag
			refname_type="tag"
			short_refname="${refname#refs/tags/}"
			;;
		refs/tags/*:tag)
			# annotated tag
			refname_type="annotated tag"
			short_refname="${refname#refs/tags/}"
			# change recipients
			if [ -n "$announcerecipients" ]; then
				recipients="$announcerecipients"
			fi
			;;
		refs/heads/*:commit|refs/heads/*:tag)
			# branch
			refname_type="branch"
			short_refname="${refname#refs/heads/}"
			;;
		refs/remotes/*:commit|refs/remotes/*:tag)
			# tracking branch
			refname_type="tracking branch"
			short_refname="${refname#refs/remotes/}"
			echol >&2 "*** Push-update of tracking branch, $refname"
			echol >&2 "***  - no email generated."
			return 1
			;;
		refs/mob/*:commit|refs/mob/*:tag)
			# personal mob ref
			refname_type="personal mob ref"
			short_refname="${refname#refs/mob/}"
			echol >&2 "*** Push-update of personal mob ref, $refname"
			echol >&2 "***  - no email generated."
			return 1
			;;
		*)
			# Anything else (is there anything else?)
			echol >&2 "*** Unknown type of update to $refname ($rev_type)"
			echol >&2 "***  - no email generated"
			return 1
			;;
	esac

	# Check if we've got anyone to send to
	if [ -z "$recipients" ]; then
		case "$refname_type" in
			"annotated tag")
				config_name="neither hooks.announcelist nor hooks.mailinglist is"
				;;
			*)
				config_name="hooks.mailinglist is not"
				;;
		esac
		echol >&2 "*** $config_name set so no email will be sent"
		echol >&2 "*** for $refname update $oldrev->$newrev"
		return 1
	fi

	return 0
}

#
# Top level email generation function.  This calls the appropriate
# body-generation routine after outputting the common header.
#
# Note this function doesn't actually generate any email output, that is
# taken care of by the functions it calls:
#  - generate_email_header
#  - generate_create_XXXX_email
#  - generate_update_XXXX_email
#  - generate_delete_XXXX_email
#  - generate_email_footer
#
# Note also that this function cannot 'exit' from the script; when this
# function is running (in hook script mode), the send_mail() function
# is already executing in another process, connected via a pipe, and
# if this function exits without, whatever has been generated to that
# point will be sent as an email... even if nothing has been generated.
#
generate_email()
{
	# Email parameters
	# The email subject will contain the best description of the ref
	# that we can build from the parameters
	describe="$(git describe $rev 2>/dev/null)" || :
	if [ -z "$describe" ]; then
		describe="$rev"
	fi

	generate_email_header

	# Call the correct body generation function
	fn_name=general
	case "$refname_type" in
	"tracking branch"|branch)
		fn_name=branch
		;;
	"annotated tag")
		fn_name=atag
		;;
	esac
	generate_${change_type}_${fn_name}_email

	generate_email_footer
}

generate_email_header()
{
	# --- Email (all stdout will be the email)
	# Generate header
	if [ -n "$emailsender" ]; then
		echol "From: $emailsender"
	fi
	cat <<-EOF
	To: $recipients
	Subject: ${emailprefix}$projectname $refname_type $short_refname ${change_type}d: $describe
	MIME-Version: 1.0
	Content-Type: text/plain; charset=utf-8; format=fixed
	Content-Transfer-Encoding: 8bit
	EOF
	[ -z "$refname" ] || echol "References: <$refname@$projurl>"
	[ -n "$cfg_suppress_x_girocco" ] || echol "X-Girocco: $cfg_gitweburl"
	[ -z "$emailextraheader" ] || echol "$emailextraheader"
	cat <<-EOF
	X-Git-Refname: $refname
	X-Git-Reftype: $refname_type
	X-Git-Oldrev: $oldrev
	X-Git-Newrev: $newrev
	Auto-Submitted: auto-generated

	This is an automated email generated because a ref change occurred in the
	git repository for project $projectname.

	The $refname_type, $short_refname has been ${change_type}d
	EOF
}

generate_email_footer()
{
	SPACE=" "
	cat <<-EOF


	${cfg_name:-mail.sh} automatic notification. Contact project admin $projectowner
	if you want to unsubscribe, or site admin ${cfg_admin:+$cfg_admin }if you receive
	no reply.
	--${SPACE}
	$projectboth
	EOF
}

# --------------- Branches

#
# Called for the creation of a branch
#
generate_create_branch_email()
{
	# This is a new branch and so oldrev is not valid
	echol "        at  $newrev ($newrev_type)"
	echol ""

	show_new_revisions_eval='
	echol "Those revisions in this branch that are new to this repository"
	echol "have not appeared on any other notification email; so we list"
	echol "those revisions in full, below."

	echol ""
	echol "$LOGBEGIN"
	'
	show_new_revisions
	if [ -n "$LAST_SHOWN_REVISION" ]; then
		echol "$LOGEND"
	else
		echol "No new revisions were added by this branch."
	fi

	# If any revisions were shown by show_new_revisions then we show
	# a diffstat from the last shown revisions's parent (or the empty tree
	# if the last revision shown is a root revision) to the new revision
	# provided the last shown revision is not a merge commit and there
	# were no more boundary commits encountered by show_new_revisions than
	# the last shown revision has parents.
	# This is to show the truth of what happened in this change.

	if [ -n "$LAST_SHOWN_REVISION" ] && [ -n "$LAST_SHOWN_NPARENTS" ] && [ -n "$LAST_SHOWN_NBOUNDARY" ] &&
	   [ "$LAST_SHOWN_NPARENTS" -le 1 ] && [ "$LAST_SHOWN_NBOUNDARY" -le "$LAST_SHOWN_NPARENTS" ]; then
		if [ "$LAST_SHOWN_NPARENTS" -eq 0 ]; then
			# Note that since Git 1.5.5 the empty tree object is ALWAYS available
			# whether or not it's actually present in the repository.

			# Set oldrev to the result of $(git hash-object -t tree --stdin </dev/null)
			oldrev="4b825dc642cb6eb9a060e54bf8d69288fbee4904"
		else
			# Set oldrev to the parent of the last shown revision
			oldrev="$LAST_SHOWN_REVISION^"
		fi
		echol ""
		echol "Summary of changes:"
		git_diff_tree --no-color --stat=72 --summary -B --find-copies-harder "$oldrev" "$newrev" --
	fi
}

#
# Called for the change of a pre-existing branch
#
generate_update_branch_email()
{
	# Consider this:
	#   1 --- 2 --- O --- X --- 3 --- 4 --- N
	#
	# O is $oldrev for $refname
	# N is $newrev for $refname
	# X is a revision pointed to by some other ref, for which we may
	#   assume that an email has already been generated.
	# In this case we want to issue an email containing only revisions
	# 3, 4, and N.  Given (almost) by
	#
	#  git rev-list N ^O --not --all
	#
	# The reason for the "almost", is that the "--not --all" will take
	# precedence over the "N", and effectively will translate to
	#
	#  git rev-list N ^O ^X ^N
	#
	# So, we need to build up the list more carefully.  git rev-parse
	# will generate a list of revs that may be fed into git rev-list.
	# We can get it to make the "--not --all" part and then filter out
	# the "^N" with:
	#
	#  git rev-parse --not --all | grep -v N
	#
	# Then, using the --stdin switch to git rev-list we have effectively
	# manufactured
	#
	#  git rev-list N ^O ^X
	#
	# This leaves a problem when someone else updates the repository
	# while this script is running.  Their new value of the ref we're
	# working on would be included in the "--not --all" output; and as
	# our $newrev would be an ancestor of that commit, it would exclude
	# all of our commits.  What we really want is to exclude the current
	# value of $refname from the --not list, rather than N itself.  So:
	#
	#  git rev-parse --not --all | grep -v $(git rev-parse $refname)
	#
	# Get's us to something pretty safe (apart from the small time
	# between refname being read, and git rev-parse running - for that,
	# I give up)
	#
	#
	# Next problem, consider this:
	#   * --- B --- * --- O ($oldrev)
	#          \
	#           * --- X --- * --- N ($newrev)
	#
	# That is to say, there is no guarantee that oldrev is a strict
	# subset of newrev (it would have required a --force, but that's
	# allowed).  So, we can't simply say rev-list $oldrev..$newrev.
	# Instead we find the common base of the two revs and list from
	# there.
	#
	# As above, we need to take into account the presence of X; if
	# another branch is already in the repository and points at some of
	# the revisions that we are about to output - we don't want them.
	# The solution is as before: git rev-parse output filtered.
	#
	# Finally, tags: 1 --- 2 --- O --- T --- 3 --- 4 --- N
	#
	# Tags pushed into the repository generate nice shortlog emails that
	# summarise the commits between them and the previous tag.  However,
	# those emails don't include the full commit messages that we output
	# for a branch update.  Therefore we still want to output revisions
	# that have been output on a tag email.
	#
	# Luckily, git rev-parse includes just the tool.  Instead of using
	# "--all" we use "--branches"; this has the added benefit that
	# "remotes/" will be ignored as well.

	# List all of the revisions that were removed by this update, in a
	# fast-forward update, this list will be empty, because rev-list O
	# ^N is empty.  For a non-fast-forward, O ^N is the list of removed
	# revisions
	fast_forward=
	rev=
	for rev in $(git rev-list "$newrev..$oldrev" --)
	do
		revtype="$(git cat-file -t "$rev")" || :
		echol "  discards  $rev ($revtype)"
	done
	if [ -z "$rev" ]; then
		fast_forward=1
	fi

	# List all the revisions from baserev to newrev in a kind of
	# "table-of-contents"; note this list can include revisions that
	# have already had notification emails and is present to show the
	# full detail of the change from rolling back the old revision to
	# the base revision and then forward to the new revision
	for rev in $(git rev-list "$oldrev..$newrev" --)
	do
		revtype="$(git cat-file -t "$rev")" || :
		echol "       via  $rev ($revtype)"
	done

	if [ "$fast_forward" ]; then
		echol "      from  $oldrev ($oldrev_type)"
	else
		#  1. Existing revisions were removed.  In this case newrev
		#     is a subset of oldrev - this is the reverse of a
		#     fast-forward, a rewind
		#  2. New revisions were added on top of an old revision,
		#     this is a rewind and addition.

		# (1) certainly happened, (2) possibly.  When (2) hasn't
		# happened, we set a flag to indicate that no log printout
		# is required.

		echol ""

		# Find the common ancestor of the old and new revisions and
		# compare it with newrev
		baserev=$(git merge-base $oldrev $newrev)
		rewind_only=
		if [ "$baserev" = "$newrev" ]; then
			echol "This update discarded existing revisions and left the branch pointing at"
			echol "a previous point in the repository history."
			echol ""
			echol " * -- * -- N ($newrev)"
			echol "            \\"
			echol "             O -- O -- O ($oldrev)"
			echol ""
			echol "The removed revisions are not necessarily gone - if another reference"
			echol "still refers to them they will stay in the repository."
			rewind_only=1
		else
			echol "This update added new revisions after undoing existing revisions.  That is"
			echol "to say, the old revision is not a strict subset of the new revision.  This"
			echol "situation occurs when you --force push a change and generate a repository"
			echol "containing something like this:"
			echol ""
			echol " * -- * -- B -- O -- O -- O ($oldrev)"
			echol "            \\"
			echol "             N -- N -- N ($newrev)"
			echol ""
			echol "When this happens we assume that you've already had alert emails for all"
			echol "of the O revisions, and so we here report only the revisions in the N"
			echol "branch from the common base, B."
		fi
	fi

	echol ""
	LAST_SHOWN_REVISION=
	if [ -z "$rewind_only" ]; then
		show_new_revisions_eval='
		echol "Those revisions listed above that are new to this repository have"
		echol "not appeared on any other notification email; so we list those"
		echol "revisions in full, below."

		echol ""
		echol "$LOGBEGIN"
		'
		show_new_revisions
	fi
	if [ -n "$LAST_SHOWN_REVISION" ]; then
		echol "$LOGEND"
	else
		echol "No new revisions were added by this update."
	fi

	# The diffstat is shown from the old revision to the new revision.
	# This is to show the truth of what happened in this change.
	# There's no point showing the stat from the base to the new
	# revision because the base is effectively a random revision at this
	# point - the user will be interested in what this revision changed
	# - including the undoing of previous revisions in the case of
	# non-fast-forward updates.
	echol ""
	echol "Summary of changes:"
	git_diff_tree --no-color --stat=72 --summary -B --find-copies-harder "$oldrev..$newrev" --
}

#
# Called for the deletion of a branch
#
generate_delete_branch_email()
{
	echol "       was  $oldrev"
	echol ""
	echol "$LOGBEGIN"
	git_diff_tree --no-color --date="$datefmt" -s --abbrev-commit --abbrev=$habbr --always --encoding=UTF-8 --pretty=oneline "$oldrev" --
	echol "$LOGEND"
}

# --------------- Annotated tags

#
# Called for the creation of an annotated tag
#
generate_create_atag_email()
{
	echol "        at  $newrev ($newrev_type)"

	generate_atag_email
}

#
# Called for the update of an annotated tag (this is probably a rare event
# and may not even be allowed)
#
generate_update_atag_email()
{
	echol "        to  $newrev ($newrev_type)"
	echol "      from  $oldrev (which is now obsolete)"

	generate_atag_email
}

#
# Called when an annotated tag is created or changed
#
generate_atag_email()
{
	# Use read_tag_fields to pull out the individual fields from the
	# tag and git cat-file --batch-check to fully peel the tag if needed
	tagrev="$newrev"
	tagrev16="$newrev16"
	read_tag_fields "$tagrev"
	if [ "$tagtype" = "tag" ]; then
		# fully peel the tag
		peeledobject=
		peeledtype=
		_po=
		_pt=
		read -r _po _pt junk <<-EOT || :
			$(git cat-file --batch-check"${var_have_git_185:+=%(objectname) %(objecttype)}" <<-ETX || :
				$tagrev^{}
				ETX
			)
			EOT
		if [ -n "$_po" ] && [ -n "$_pt" ] && [ "$_pt" != "missing" ] && [ "$_po" != "missing" ]; then
			peeledobject="$_po"
			peeledtype="$_pt"
		else
			peeledtype="missing"
		fi
	else
		peeledobject="$tagobject"
		peeledtype="$tagtype"
	fi

	echol "   tagging  $peeledobject ($peeledtype)"
	case "$peeledtype" in
	commit)

		# If the tagged object is a commit, then we assume this is a
		# release, and so we calculate which tag this tag is
		# replacing
		prevtag="$(git describe --abbrev=0 "$peeledobject^" 2>/dev/null)" || :

		if [ -n "$prevtag" ]; then
			echol "  replaces  $prevtag"
		fi
		;;
	*)
		echol "    length  $(git cat-file -s "$peeledobject" 2>/dev/null) bytes"
		;;
	esac
	echol " tagged by  $taggername"
	echol "        on  $taggerdate"

	echol ""
	echol "$LOGBEGIN"

	while
		echol "tag $tagrev"
		echol "Tag:    $tagtag"
		echol "Object: $tagobject ($tagtype)"
		[ -z "$taggername" ] ||
		echol "Tagger: $taggername"
		[ -z "$taggerdate" ] ||
		echol "Date:   $taggerdate"
		echol "URL:    <$projurl/$tagrev16>"
		echol ""

		# Show the content of the tag message; this might contain a change
		# log or release notes so is worth displaying.
		git cat-file tag "$tagrev" | LC_ALL=C sed -e '1,/^$/d;s/^/    /;'
		echol ""
		[ "$tagtype" = "tag" ]
	do
		tagrev="$tagobject"
		scratch="${tagrev#????????????????}"
		tagrev16="${tagrev%$scratch}"
		read_tag_fields "$tagrev" || break
	done

	case "$peeledtype" in
	commit)
		# Only commit tags make sense to have rev-list operations
		# performed on them
		if [ -n "$prevtag" ]; then
			# Show changes since the previous release
			git shortlog "$prevtag..$newrev" --
		else
			# No previous tag, show all the changes since time
			# began
			git shortlog "$newrev" --
		fi
		;;
	*)
		# XXX: Is there anything useful we can do for non-commit
		# objects?
		;;
	esac

	echol "$LOGEND"
}

#
# Called for the deletion of an annotated tag
#
generate_delete_atag_email()
{
	echol "       was  $oldrev"
	echol ""
	echol "$LOGBEGIN"
	git_diff_tree --no-color --date="$datefmt" -s --abbrev-commit --abbrev=$habbr --always --encoding=UTF-8 --pretty=oneline "$oldrev" --
	echol "$LOGEND"
}

# --------------- General references

#
# Called when any other type of reference is created (most likely a
# non-annotated tag)
#
generate_create_general_email()
{
	echol "        at  $newrev ($newrev_type)"

	generate_general_email
}

#
# Called when any other type of reference is updated (most likely a
# non-annotated tag)
#
generate_update_general_email()
{
	echol "        to  $newrev ($newrev_type)"
	echol "      from  $oldrev"

	generate_general_email
}

#
# Called for creation or update of any other type of reference
#
generate_general_email()
{
	# Unannotated tags are more about marking a point than releasing a
	# version; therefore we don't do the shortlog summary that we do for
	# annotated tags above - we simply show that the point has been
	# marked, and print the log message for the marked point for
	# reference purposes
	#
	# Note this section also catches any other reference type (although
	# there aren't any) and deals with them in the same way.

	echol ""
	if [ "$newrev_type" = "commit" ]; then
		if [ -n "$(git rev-list --no-walk --merges "$newrev" --)" ]; then
			pfmt12="$pfmt1$pfmt1m$pfmt2"
		else
			pfmt12="$pfmt1$pfmt2"
		fi
		echol "$LOGBEGIN"
		git_diff_tree --no-color --date="$datefmt" --root -s --always --encoding=UTF-8 --format="$pfmt12$projurlesc/$newrev16$pfmt3" --abbrev=$habbr "$newrev" --
		echol "$LOGEND"
	else
		# What can we do here?  The tag marks an object that is not
		# a commit, so there is no log for us to display.  It's
		# probably not wise to output git cat-file as it could be a
		# binary blob.  We'll just say how big it is
		echol "$newrev is a $newrev_type, and is $(git cat-file -s "$newrev") bytes long."
	fi
}

#
# Called for the deletion of any other type of reference
#
generate_delete_general_email()
{
	echol "       was  $oldrev"
	echol ""
	echol "$LOGBEGIN"
	git_diff_tree --no-color --date="$datefmt" -s --abbrev-commit --abbrev=$habbr --always --encoding=UTF-8 --pretty=oneline "$oldrev" --
	echol "$LOGEND"
}


# --------------- Miscellaneous utilities

#
# Show new revisions as the user would like to see them in the email.
#
# On return LAST_SHOWN_REVISION will be set to non-empty if any revisions shown
# and LAST_SHOWN_NPARENTS will be the number of parents it has (possibly 0)
# and LAST_SHOWN_NBOUNDARY will be the total number of boundary commits seen (possibly 0)
# So if $LAST_SHOWN_NBOUNDARY is greater than $LAST_SHOWN_NPARENTS then the
# portion of the graph shown by show_new_revisions (excluding $LAST_SHOWN_REVISION)
# includes at least one merge commit that had at least one parent excluded.
# If show_new_revisions_eval is non-empty, than just before the first output
# gets shown it will be eval'd and then set to "".
#
show_new_revisions()
{
	# This shows all log entries that are not already covered by
	# another ref - i.e. commits that are now accessible from this
	# ref that were previously not accessible
	# (see generate_update_branch_email for the explanation of this
	# command)
	LAST_SHOWN_REVISION=
	LAST_SHOWN_NPARENTS=
	LAST_SHOWN_NBOUNDARY=0

	# Revision range passed to rev-list differs for new vs. updated
	# branches.
	if [ "$change_type" = create ]
	then
		# Show all revisions exclusive to this (new) branch.
		revspec="$newrev"
	else
		# Branch update; show revisions not part of $oldrev.
		revspec="$oldrev..$newrev"
	fi

	if [ "${MAIL_SH_OTHER_BRANCHES+set}" = "set" ]; then
		case "$MAIL_SH_OTHER_BRANCHES" in
		@*)
			othertips="git cat-file blob '${MAIL_SH_OTHER_BRANCHES#@}' 2>/dev/null"
			;;
		*)
			othertips='printf "%s\n" $MAIL_SH_OTHER_BRANCHES'
			;;
		esac
	else
		othertips='git for-each-ref --format="%(refname)" refs/heads |
		    LC_ALL=C awk -v "refname=$refname" "\$1 != refname"'
	fi
#	if [ -z "$custom_showrev" ]
#	then
#		git rev-list --pretty --stdin "$revspec" --
#	else
		while read onerev mark pcnt && [ -n "$onerev" ] && [ -n "$mark" ] && [ -n "$pcnt" ]
		do
			if [ "$mark" = "-" ]; then
				LAST_SHOWN_NBOUNDARY=$(( $LAST_SHOWN_NBOUNDARY + 1 ))
				continue
			fi
			if [ -n "$show_new_revisions_eval" ]; then
				eval "$show_new_revisions_eval"
				show_new_revisions_eval=
			fi
			if [ -z "$reverseopt" ] || [ -z "$LAST_SHOWN_REVISION" ]; then
				LAST_SHOWN_REVISION="$onerev"
				LAST_SHOWN_NPARENTS="$pcnt"
			fi
			if [ -n "$custom_showrev" ]; then
				eval $(printf "$custom_showrev" $onerev)
			else
				if [ "${summaryonly:-false}" = "false" ]; then
					if [ ${pcnt:-1} -gt 1 ]; then
						opts="-p --cc"
						pfmt12="$pfmt1$pfmt1m$pfmt2"
					else
						opts="-p --stat=72 --summary"
						pfmt12="$pfmt1$pfmt2"
					fi
				else
					if [ ${pcnt:-1} -gt 1 ]; then
						opts="-s"
						pfmt12="$pfmt1$pfmt1m$pfmt2"
					else
						opts="--stat=72 --summary"
						pfmt12="$pfmt1$pfmt2"
					fi
				fi
				scratch="${onerev#????????????????}"
				onerev16="${onerev%$scratch}"
				git_diff_tree --no-color --date="$datefmt" $opts --always --encoding=UTF-8 --format="$pfmt12$projurlesc/$onerev16$pfmt3" --abbrev=$habbr -B --find-copies-harder --root "$onerev" --
				echo
			fi
		done <<EOT
$(eval "$othertips" |
git cat-file --batch-check"${var_have_git_185:+=%(objectname)}" |
LC_ALL=C sed -e '/ missing$/d' -e 's/^\([^ ][^ ]*\).*$/\1/' -e 's/^/^/' |
git --no-pager log --stdin --no-color $reverseopt --boundary --format=tformat:"%H %m %p" "$revspec" -- |
LC_ALL=C awk '{print $1 " " $2 " " NF-2}')
EOT
#	fi
}


size_limit()
{
	size=0
	while IFS= read -r line; do
		# Include size of RFC 822 trailing CR+LF on each line
		size=$(($size+${#line}+2))
		if [ $size -gt $1 ]; then
			echol "...e-mail trimmed, exceeded size limit."
			break
		fi
		echol "$line"
	done
}


sendmail_to_stdout()
{
	cat
}


send_mail()
{
	if [ -n "$cfg_sender" ]; then
		"${cfg_sendmail_bin:-/usr/sbin/sendmail}" -i -t -f "$cfg_sender"
	else
		"${cfg_sendmail_bin:-/usr/sbin/sendmail}" -i -t
	fi
}

# ---------------------------- main()

# --- Constants
LOGBEGIN="- Log -----------------------------------------------------------------"
LOGEND="-----------------------------------------------------------------------"

# --- Config
! [ -f @basedir@/shlib.sh ] || ! [ -r @basedir@/shlib.sh ] || . @basedir@/shlib.sh

# Git formatting to use
datefmt=rfc2822
habbr=12

# strftime formatting to use (empty is default rfc2822 format)
sdatefmt=

# This is --pretty=medium in four parts with a URL: line added
# The pfmt1m value must be inserted after the pfmt1 value but only for merges
# The URL must be inserted between pfmt2 and pfmt3
pfmt1='format:commit %H%n'
pfmt1m='Merge: %p%n'
pfmt2='Author: %an <%ae>%nDate:   %ad%nURL:    <'
pfmt3='>%n%n%w(0,4,4)%s%n%n%b'

GIT_CONFIG_PARAMETERS="${GIT_CONFIG_PARAMETERS:+$GIT_CONFIG_PARAMETERS }'core.abbrev=$habbr'"
export GIT_CONFIG_PARAMETERS

# Set GIT_DIR either from the working directory, or from the environment
# variable.
GIT_DIR="$(git rev-parse --git-dir 2>/dev/null)" || :
if [ -z "$GIT_DIR" ]; then
	echol >&2 "fatal: mail.sh: GIT_DIR not set"
	exit 1
fi
# Get GIT_COMMON_DIR for Git >= 2.5.0
GCD_DIR="$(cd "$GIT_DIR" && unset GIT_DIR && git rev-parse --git-common-dir 2>/dev/null)" || :
if [ -z "$GCD_DIR" ] || [ "$GCD_DIR" = "--git-common-dir" ]; then
	# Must be pre-2.5.0
	GCD_DIR="$GIT_DIR"
else
	# Might be relative to GIT_DIR so cd there first
	GCD_DIR="$(cd "$GIT_DIR" && cd "$GCD_DIR" && pwd)"
fi

projectdesc=
! [ -s "$GCD_DIR/description" ] || projectdesc="$(LC_ALL=C sed -ne '1p' "$GCD_DIR/description")"
# Check if the description is unchanged from it's default, and shorten it to
# a more manageable length if it is
case "$projectdesc" in ""|"Unnamed repository"*)
	projectdesc="UNNAMED PROJECT"
esac

# If --stdout is first argument send all output there instead of mailing
if [ "$1" = "--stdout" ]; then
	shift
	cfg_sendmail_bin="sendmail_to_stdout"
fi

projectname="${4%.git}"
if [ -n "$projectname" ]; then
	projectname="$projectname.git"
	projectowner="$(git config gitweb.owner 2>/dev/null)" || :
else
	projectname="$(cd "$GIT_DIR" && pwd)"
	projectname="${projectname%/.git}"
	projectname="$(basename "$projectname")"
fi
projectboth="$projectname (\"$projectdesc\")"
projurl="$cfg_gitweburl/$projectname"
projurlesc="$(printf '%s\n' "$projurl" | sed -e 's/%/%%/g')"

emailsender="$5"
emailextraheader="$6"

recipients="$(git config hooks.mailinglist 2>/dev/null)" || :
summaryonly="$(git config --bool hooks.summaryonly 2>/dev/null)" || :
reverseopt=
[ "$(git config --bool hooks.reverseorder 2>/dev/null || :)" != "true" ] || reverseopt=--reverse
announcerecipients="$(git config hooks.announcelist)" || :
envelopesender="$(git config hooks.envelopesender)" || :
emailprefix="$(git config hooks.emailprefix || echol "[${cfg_name:-mail.sh}] ")" || :
custom_showrev="$(git config hooks.showrev)" || :

differrtmp="$(mktemp -u "${TMPDIR:-/tmp}/difftree-$$-XXXXXX")"

# --- Main loop
# Allow dual mode: run from the command line just like the update hook, or
# if no arguments are given then run as a hook script
# If --stdout is first argument send all output there instead (handled above)
# Optional 4th (projectname), 5th (sender) and 6th (extra header) arguments
max_email_size="$(( ${cfg_mailsh_sizelimit:-256} * 1024 ))"
[ "${max_email_size:-0}" -ge "10240" ] || max_email_size="10240" # 10K minimum
if [ -n "$1" ] && [ -n "$2" ] && [ -n "$3" ]; then
	# Handle a single update rather than a batch on stdin
	# Output will still be sent to sendmail unless --stdout is used
	# Same 3 args as update hook (<refname> <old> <new>)
	if prep_for_email $2 $3 $1; then
		PAGER= generate_email | size_limit "$max_email_size" | send_mail
	fi
else
	# MAIL_SH_OTHER_BRANCHES cannot possibly be valid for multiple updates
	unset MAIL_SH_OTHER_BRANCHES
	# Same input as pre-receive hook (each line is <old> <new> <refname>)
	while read oldrev newrev refname
	do
		prep_for_email $oldrev $newrev $refname || continue
		PAGER= generate_email | size_limit "$max_email_size" | send_mail
	done
fi

exit 0
