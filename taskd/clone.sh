#!/bin/sh
#
# Invoked from taskd/taskd.pl

. @basedir@/shlib.sh

set -e

umask 002
[ "$cfg_permission_control" != "Hooks" ] || umask 000
clean_git_env

# darcs fast-export | git fast-import with error handling
git_darcs_fetch() (
	set_utf8_locale
	_err1=
	_err2=
	exec 3>&1
	{ read -r _err1 || :; read -r _err2 || :; } <<-EOT
	$(
		exec 4>&3 3>&1 1>&4 4>&-
		{
			_e1=0
			"$cfg_basedir"/bin/darcs-fast-export \
				--export-marks="$(pwd)/dfe-marks" "$1" 3>&- || _e1=$?
			echo $_e1 >&3
		} |
		{
			_e2=0
			git fast-import \
				--export-marks="$(pwd)/gfi-marks" \
				--export-pack-edges="$(pwd)/gfi-packs" \
				--force 3>&- || _e2=$?
			echo $_e2 >&3
		}
	)
	EOT
	exec 3>&-
	[ "$_err1" = 0 ] && [ "$_err2" = 0 ]
	return $?
)

# bzr fast-export | git fast-import with error handling
git_bzr_fetch() (
	set_utf8_locale
	BZR_LOG=/dev/null
	export BZR_LOG
	_err1=
	_err2=
	exec 3>&1
	{ read -r _err1 || :; read -r _err2 || :; } <<-EOT
	$(
		exec 4>&3 3>&1 1>&4 4>&-
		{
			_e1=0
			bzr fast-export --plain \
				--export-marks="$(pwd)/bfe-marks" "$1" 3>&- || _e1=$?
			echo $_e1 >&3
		} |
		{
			_e2=0
			git fast-import \
				--export-marks="$(pwd)/gfi-marks" \
				--export-pack-edges="$(pwd)/gfi-packs" \
				--force 3>&- || _e2=$?
			echo $_e2 >&3
		}
	)
	EOT
	exec 3>&-
	[ "$_err1" = 0 ] && [ "$_err2" = 0 ]
	return $?
)

send_clone_failed() {
	trap "" EXIT
	# We must now close the .clonelog file that is open on stdout and stderr
	exec >/dev/null 2>&1
	! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :
	failaddrs="$(config_get owner)" || :
	[ -z "$cfg_admincc" ] || [ "$cfg_admincc" = "0" ] || [ -z "$cfg_admin" ] ||
	if [ -z "$failaddrs" ]; then failaddrs="$cfg_admin"; else failaddrs="$failaddrs,$cfg_admin"; fi
	[ -z "$failaddrs" ] ||
	{
	      cat <<EOT
Condolences. The clone of project $proj just failed.

	* Source URL: $url
	* Project settings: $cfg_webadmurl/editproj.cgi?name=$(echo "$proj" | LC_ALL=C sed -e 's/[+]/%2B/g')

The project settings link may be used to adjust the settings
and restart the clone in order to try the clone again.
EOT
		if [ -f .clonelog ] && [ -r .clonelog ]; then
			echo ""
			echo "Log follows:"
			echo ""
			loglines=$(LC_ALL=C wc -l <.clonelog)
			if [ $loglines -le 203 ]; then
				cat .clonelog
			else
				head -n 100 .clonelog
				echo ""
				echo "[ ... elided $(( $loglines - 200 )) middle lines ... ]"
				echo ""
				tail -n 100 .clonelog
			fi
		fi
	} | mailref "clone@$cfg_gitweburl/$proj.git" -s "[$cfg_name] $proj clone failed" "$failaddrs" || :
}

# removes all leftovers from a previous failed clone attempt
cleanup_failed_clone() {

	# Remove any left-over svn-remote.svn or remote.origin config
	git config --remove-section svn-remote.svn 2>/dev/null || :
	git config --remove-section remote.origin 2>/dev/null || :

	# If there is a remote-template.origin section, pre-seed the
	# remote.origin section with its contents
	git config --get-regexp '^remote-template\.origin\..' |
	while read name value; do
		if [ -n "$name" ] && [ -n "$value" ]; then
			git config "remote${name#remote-template}" "$value"
		fi
	done

	# Any pre-existing FETCH_HEAD from a previous clone failed or not is
	# now garbage to be removed
	rm -f FETCH_HEAD

	# Remove any left-over svn dir from a previous failed attempt
	rm -rf svn

	# Remove any left-over .darcs dirs from a previous failed attempt
	rm -rf *.darcs

	# Remove any left-over repo.hg dir from a previous failed attempt
	rm -rf repo.hg

	# Remove any left-over import/export/temp files from a previous failed attempt
	rm -f bfe-marks dfe-marks hg2git-heads hg2git-mapping hg2git-marks* hg2git-state \
		gfi-marks gfi-packs .pkts-temp .refs-temp

	# We want a gc right after the clone, so re-enable that just in case.
	# There's a potential race where we could add it and gc.sh could remove
	# it, but we'll reunset lastgc just before we remove .delaygc at the end.
	[ -e .delaygc ] || >.delaygc
	git config --unset gitweb.lastgc 2>/dev/null || :

	# Remove all pre-existing refs
	rm -f packed-refs
	git for-each-ref --format='delete %(refname)' | git_updateref_stdin 2>/dev/null || :

	# The initial state before a clone starts has HEAD as a symbolic-ref to master
	git symbolic-ref HEAD refs/heads/master

	# HEAD is no longer "ok"
	git config --unset girocco.headok 2>/dev/null || :

	# We, perhaps, ought to remove any packs/loose objects now, but the next gc
	# will get rid of any extras.  Also, if we're recloning the same thing, any
	# preexisting packs/loose objects containing what we're recloning will only
	# speed up the reclone by avoiding some disk writes.  So we don't kill them.

	# It's just remotely possible that a bunch of failures in a row could
	# create a big mess that just keeps growing and growing...
	# Trigger a .needsgc if that happens.
	check_and_set_needsgc
}

proj="${1%.git}"
cd "$cfg_reporoot/$proj.git"
bang_reset

! [ -e .delaygc ] || >.allowgc || :

trap "echo '@OVER@'; touch .clone_failed; send_clone_failed" EXIT
echo "Project: $proj"
echo "   Date: $(TZ=UTC date '+%Y-%m-%d %T UTC')"
echo ""
[ -n "$cfg_mirror" ] || { echo "Mirroring is disabled" >&2; exit 1; }
url="$(config_get baseurl)" || :
case "$url" in *"	"*|*" "*|"")
	echo "Bad mirror URL (\"$url\")"
	exit 1
esac

cleanup_failed_clone

# Record original mirror type for use by update.sh
mirror_type="$(get_url_mirror_type "$url")"
git config girocco.mirrortype "$mirror_type"

echo "Mirroring from URL \"$url\""
echo ""

if [ "$cfg_project_owners" = "source" ]; then
	config set owner "$(ls -ldH "${url#file://}" 2>/dev/null | LC_ALL=C awk '{print $3}')"
fi

mailaddrs="$(config_get owner)" || :
[ -z "$cfg_admin" ] ||
if [ -z "$mailaddrs" ]; then mailaddrs="$cfg_admin"; else mailaddrs="$mailaddrs,$cfg_admin"; fi

# Make sure we don't get any unwanted loose objects
# Starting with Git v2.10.0 fast-import can generate loose objects unless we
# tweak its configuration to prevent that
git_add_config 'fetch.unpackLimit=1'
# Note the git config documentation is wrong
# transfer.unpackLimit, if set, overrides fetch.unpackLimit
git_add_config 'transfer.unpackLimit=1'
# But not the Git v2.10.0 and later fastimport.unpackLimit which improperly uses <= instead of <
git_add_config 'fastimport.unpackLimit=0'

# Initial mirror
echo "Initiating mirroring..."
headref=
showheadwarn=
warnempty=

# remember the starting time so we can easily combine fetched loose objects
# we sleep for 1 second after creating .needspack to make sure all objects are newer
if ! [ -e .needspack ]; then
	rm -f .needspack
	>.needspack
	sleep 1
fi

case "$url" in
	svn://* | svn+http://* | svn+https://* | svn+file://* | svn+ssh://*)
		[ -n "$cfg_mirror_svn" ] || { echo "Mirroring svn is disabled" >&2; exit 1; }
		# Allow the username to be specified in the "svn-credential.svn.username"
		# property and the password in the "svn-credential.svn.password" property
		# Use an 'anonsvn' username by default as is commonly used for anonymous svn
		# Default the password to the same as the username
		# The password property will be ignored unless a username has been specified
		if svnuser="$(git config --get svn-credential.svn.username)" && [ -n "$svnuser" ]; then
			if ! svnpass="$(git config --get svn-credential.svn.password)"; then
				svnpass="$svnuser"
			fi
			url1="${url#*://}"
			url1="${url1%%/*}"
			case "$url1" in ?*"@"?*)
				urlsch="${url%%://*}"
				url="$urlsch://${url#*@}"
			esac
		else
			# As a fallback, check in the URL, just in case
			url1="${url#*://}"
			url1="${url1%%/*}"
			svnuser=
			case "$url1" in ?*"@"?*)
				urlsch="${url%%://*}"
				url="$urlsch://${url#*@}"
				url1="${url1%%@*}"
				svnuser="${url1%%:*}"
				if [ -n "$svnuser" ]; then
					svnpass="$svnuser"
					case "$url1" in *":"*)
						svnpass="${url1#*:}"
					esac
				fi
			esac
			if [ -z "$svnuser" ]; then
				svnuser="anonsvn"
				svnpass="anonsvn"
			fi
		fi
		GIT_ASKPASS_PASSWORD="$svnpass"
		export GIT_ASKPASS_PASSWORD
		# We just remove svn+ here, so svn+http://... becomes http://...
		# We also remove a trailing '/' to match what git-svn will do
		case "$url" in svn+ssh://*) svnurl="$url";; *) svnurl="${url#svn+}";; esac
		svnurl="${svnurl%/}"
		# We require svn info to succeed on the URL otherwise it's
		# simply not a valid URL and without using -s on the init it
		# will not otherwise be tested until the fetch
		svn --non-interactive --username "$svnuser" --password "$svnpass" info "$svnurl" >/dev/null
		# We initially use -s for the init which will possibly shorten
		# the URL.  However, the shortening can fail if a password is
		# not required for the longer version but is for the shorter,
		# so try again without -s if the -s version fails.
		# We must use GIT_DIR=. here or ever so "helpful" git-svn will
		# create a .git subdirectory!
		GIT_DIR=. git svn init --username="$svnuser" --prefix "" -s "$svnurl" </dev/null ||
		GIT_DIR=. git svn init --username="$svnuser" --prefix "" "$svnurl" </dev/null
		# We need to remember this url so we can detect changes because
		# ever so "helpful" git-svn may shorten it!
		config_set svnurl "$svnurl"
		# At this point, since we asked for a standard layout (-s) git-svn
		# may have been "helpful" and adjusted our $svnurl to a prefix and
		# then glued the removed suffix onto the front of any svn-remote.svn.*
		# config items.  We could avoid this by not using the '-s' option
		# but then we might not get all the history.  If, for example, we
		# are cloning an http://svn.example.com/repos/public repository that
		# early in its history moved trunk => public/trunk we would miss that
		# earlier history without allowing the funky shorten+prefix behavior.
		# So we read back the svn-remote.svn.fetch configuration and compute
		# the prefix.  This way we are sure to get the correct prefix.
		gitsvnurl="$(git config --get svn-remote.svn.url)" || :
		gitsvnfetch="$(git config --get-all svn-remote.svn.fetch | tail -1)" || :
		gitsvnprefix="${gitsvnfetch%%:*}"
		gitsvnsuffix="${gitsvnprefix##*/}"
		gitsvnprefix="${gitsvnprefix%$gitsvnsuffix}"
		# Ask git-svn to store everything in the normal non-remote
		# locations being careful to use the correct prefix
		git config --replace-all svn-remote.svn.fetch "${gitsvnprefix}trunk:refs/heads/master"
		git config --replace-all svn-remote.svn.branches "${gitsvnprefix}branches/*:refs/heads/*"
		git config --replace-all svn-remote.svn.tags "${gitsvnprefix}tags/*:refs/tags/*"
		# look for additional non-standard directories to fetch
		# check for standard layout at the same time
		foundstd=
		foundfile=
		svn --non-interactive --username "$svnuser" --password "$svnpass" ls "$gitsvnurl/${gitsvnprefix}" 2>/dev/null |
		{ while read file; do case $file in
			# skip the already-handled standard ones and any with a space or tab
			*'	'*|*' '*) :;;
			trunk/|branches/|tags/) foundstd=1;;
			# only fetch extra directories from the $svnurl root (not any files)
			*?/) git config --add svn-remote.svn.fetch \
				"${gitsvnprefix}${file%/}:refs/heads/${file%/}";;
			*?) foundfile=1;;
		esac; done
		# if files found and no standard directories present use a simpler layout
		if [ -z "$foundstd" ] && [ -n "$foundfile" ]; then
			git config --unset svn-remote.svn.branches
			git config --unset svn-remote.svn.tags
			git config --replace-all svn-remote.svn.fetch ':refs/heads/master'
		fi; }
		test $? -eq 0
		# Again, be careful to use GIT_DIR=. here or else new .git subdirectory!
		GIT_DIR=. git svn fetch --log-window-size=$var_log_window_size --username="$svnuser" --quiet </dev/null
		# git svn does not preserve group permissions in the svn subdirectory
		chmod -R ug+rw,o+r svn
		# git svn also leaves behind ref turds that end with @nnn
		# We get rid of them now
		git for-each-ref --format='%(refname)' |
		LC_ALL=C sed '/^..*@[1-9][0-9]*$/!d; s/^/delete /' |
		git_updateref_stdin
		unset GIT_ASKPASS_PASSWORD
		;;
	darcs://* | darcs+http://* | darcs+https://*)
		[ -n "$cfg_mirror_darcs" ] || { echo "Mirroring darcs is disabled" >&2; exit 1; }
		case "$url" in
			darcs://*) darcsurl="http://${url#darcs://}";;
			*) darcsurl="${url#darcs+}";;
		esac
		git_darcs_fetch "$darcsurl"
		;;
	bzr://*)
		[ -n "$cfg_mirror_bzr" ] || { echo "Mirroring bzr is disabled" >&2; exit 1; }
		# we just remove bzr:// here, a typical bzr url is just
		# "lp:foo"
		bzrurl="${url#bzr://}"
		git_bzr_fetch "$bzrurl"
		;;
	hg+http://* | hg+https://* | hg+file://* | hg+ssh://*)
		[ -n "$cfg_mirror_hg" ] || { echo "Mirroring hg is disabled" >&2; exit 1; }
		# We just remove hg+ here, so hg+http://... becomes http://...
		hgurl="${url#hg+}"
		# Perform the initial hg clone
		hg clone -U "$hgurl" "$(pwd)/repo.hg"
		# Do the fast-export | fast-import
		git_hg_fetch
		;;
	*)
		# We manually add remote.origin.url and remote.origin.fetch
		# to simulate a `git remote add --mirror=fetch` since that's
		# not available until Git 1.7.5 and this way we guarantee we
		# always get exactly the intended configuration and nothing else.
		git config remote.origin.url "$url"
		if ! is_gfi_mirror_url "$url" && [ "$(git config --bool girocco.cleanmirror 2>/dev/null || :)" = "true" ]; then
			git config --replace-all remote.origin.fetch "+refs/heads/*:refs/heads/*"
			git config --add remote.origin.fetch "+refs/tags/*:refs/tags/*"
			git config --add remote.origin.fetch "+refs/notes/*:refs/notes/*"
			git config --add remote.origin.fetch "+refs/top-bases/*:refs/top-bases/*"
			git config --bool girocco.lastupdateclean true
		else
			git config --replace-all remote.origin.fetch "+refs/*:refs/*"
			git config --bool girocco.lastupdateclean false
		fi
		# Set the correct HEAD symref by using ls-remote first
		GIT_SSL_NO_VERIFY=1 GIT_TRACE_PACKET=1 git ls-remote origin >.refs-temp 2>.pkts-temp ||
		{
			# Since everything was redirected, on failure there'd be no output,
			# so let's make some failure output
			cat .pkts-temp
			echo ""
			echo "git ls-remote \"$url\" failed"
			exit 1
		}
		# Compensate for git() {} side effects
		unset GIT_TRACE_PACKET
		# If the server is running at least Git 1.8.4.3 then it will send us the actual
		# symref for HEAD.  If we are running at least Git 1.7.5 then we can snarf that
		# out of the packet trace data.
		if [ -s .refs-temp ]; then
			# Nothing to do unless the remote repository has at least 1 ref
			# See if we got a HEAD ref
			head="$(LC_ALL=C grep -E "^$octet20$hexdig*[ $tab]+HEAD\$" <.refs-temp | LC_ALL=C awk '{print $1}')"
			# If the remote has HEAD set to a symbolic ref that does not exist
			# then we will not receive a HEAD ref in the ls-remote output
			headref=
			showheadwarn=
			symrefcap=
			if [ -n "$head" ]; then
				symrefcap="$(LC_ALL=C sed -ne <.pkts-temp \
					"/packet:.*git<.*[ $tab]symref="'HEAD:refs\/heads\/'"[^ $tab]/\
					{s/^.*[ $tab]symref="'HEAD:\(refs\/heads\/'"[^ $tab][^ $tab]*"'\).*$/\1/;p;}')"
				# prefer $symrefcap (refs/heads/master if no $symrefcap) if it
				# matches HEAD otherwise take the first refs/heads/... match
				matchcnt=0
				while read ref; do
					[ -n "$ref" ] || continue
					matchcnt=$(( $matchcnt + 1 ))
					if [ -z "$headref" ] || [ "$ref" = "${symrefcap:-refs/heads/master}" ]; then
						headref="$ref"
					fi
					if [ "$headref" = "${symrefcap:-refs/heads/master}" ] && [ $matchcnt -gt 1 ]; then
						break
					fi
				done <<-EOT
				$(LC_ALL=C grep -E "^$head[ $tab]+refs/heads/[^ $tab]+\$" <.refs-temp |
					LC_ALL=C awk '{print $2}')
				EOT
				# Warn if there was more than one match and $symrefcap is empty
				# or $symrefcap is not the same as $headref since our choice might
				# differ from the source repository's HEAD
				if [ $matchcnt -ge 1 ] && [ "$symrefcap" != "$headref" ] &&
				   { [ -n "$symrefcap" ] || [ $matchcnt -gt 1 ]; }; then
					showheadwarn=1
				fi
			fi
			if [ -z "$headref" ]; then
				# If we still don't have a HEAD ref then prefer refs/heads/master
				# if it exists otherwise take the first refs/heads/...
				# We do not support having a detached HEAD.
				# We always warn now because we will be setting HEAD differently
				# than the source repository had HEAD set
				showheadwarn=1
				while read ref; do
					[ -n "$ref" ] || continue
					if [ -z "$headref" ] || [ "$ref" = "refs/heads/master" ]; then
						headref="$ref"
					fi
					[ "$headref" != "refs/heads/master" ] || break
				done <<-EOT
				$(LC_ALL=C grep -E "^$octet20$hexdig*[ $tab]+refs/heads/[^ $tab]+\$" <.refs-temp |
					LC_ALL=C awk '{print $2}')
				EOT
			fi
			# If we STILL do not have a HEAD ref (perhaps the source repository
			# contains only tags) then use refs/heads/master.  It will be invalid
			# but is no worse than we used to do by default and we'll warn about
			# it.  We do not support a HEAD symref to anything other than refs/heads/...
			[ -n "$headref" ] || headref="refs/heads/master"
			git symbolic-ref HEAD "$headref"
			pruneopt=--prune
			[ "$(git config --bool fetch.prune 2>/dev/null || :)" != "false" ] || pruneopt=
			# remember the starting time so we can easily detect new packs for fast-import mirrors
			# we sleep for 1 second after creating .gfipack to make sure all packs are newer
			if is_gfi_mirror_url "$url" && ! [ -e .gfipack ]; then
				rm -f .gfipack
				>.gfipack
				sleep 1
			fi
			GIT_SSL_NO_VERIFY=1 git remote update $pruneopt
			if [ -e .gfipack ] && is_gfi_mirror_url "$url"; then
				find -L objects/pack -type f -newer .gfipack -name "pack-$octet20*.pack" -print >>gfi-packs
				rm -f .gfipack
			fi
		else
			warnempty=1
			git symbolic-ref HEAD "refs/heads/master"
		fi
		rm -f .refs-temp .pkts-temp
		;;
esac

# The objects subdirectories permissions must be updated now.
# In the case of a dumb http clone, the permissions will not be correct
# (missing group write) despite the core.sharedrepository=1 setting!
# The objects themselves seem to have the correct permissions.
# This problem appears to have been fixed in the most recent git versions.
perms=g+w
[ "$cfg_permission_control" != "Hooks" ] || perms=go+w
chmod $perms $(find -L objects -maxdepth 1 -type d) 2>/dev/null || :

# We may have just cloned a lot of refs and they will all be
# individual files at this point.  Let's pack them now so we
# can have better performance right from the start.
git pack-refs --all

# Initialize gitweb.lastreceive, gitweb.lastchange and info/lastactivity
git config gitweb.lastreceive "$(date '+%a, %d %b %Y %T %z')"
git config gitweb.lastchange "$(date '+%a, %d %b %Y %T %z')"
git for-each-ref --sort=-committerdate --format='%(committerdate:iso8601)' \
	--count=1 refs/heads >info/lastactivity || :
! [ -d htmlcache ] || { >htmlcache/changed; } 2>/dev/null || :

# Don't leave a multi-megabyte useless FETCH_HEAD behind
rm -f FETCH_HEAD

# Last ditch attempt to get a valid HEAD for a non-git source
check_and_set_head || :

# The rest
echo "Final touches..."
git update-server-info
trap "" EXIT

# run gc now unless the clone is empty
if [ -z "$warnempty" ]; then
	git config --unset gitweb.lastgc 2>/dev/null || :
	rm -f .delaygc .allowgc
fi

emptynote=
[ -z "$warnempty" ] ||
emptynote="
WARNING:  You have mirrored an empty repository.
"
headnote=
[ -z "$showheadwarn" ] || [ -z "$headref" ] ||
headnote="
NOTE:  HEAD has been set to a symbolic ref to \"$headref\".
       Use the \"Project settings\" link to choose a different HEAD symref.
"
sizenote=
! is_gfi_mirror ||
sizenote="
NOTE:  Since this is a mirror of a non-Git source, the initial repository
       size may be somewhat larger than necessary.  This will be corrected
       shortly.  If you intend to clone this repository you may want to
       wait up to 1 hour before doing so in order to receive the more
       compact final size.
"
[ -z "$mailaddrs" ] ||
mailref "clone@$cfg_gitweburl/$proj.git" -s "[$cfg_name] $proj clone completed" "$mailaddrs" <<EOT || :
Congratulations! The clone of project $proj just completed.

	* Source URL: $url
	* GitWeb interface: $cfg_gitweburl/$proj.git
	* Project settings: $cfg_webadmurl/editproj.cgi?name=$(echo "$proj" | LC_ALL=C sed -e 's/[+]/%2B/g')
$emptynote$headnote$sizenote
Have a lot of fun.
EOT

echo "Mirroring finished successfuly!"
# In case this is a re-mirror, lastgc could have been set already so clear it now
git config --unset gitweb.lastgc || :
rm .clone_in_progress
echo "$sizenote@OVER@"
