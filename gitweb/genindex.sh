#!/bin/sh
#
# genindex - Generate gitweb project list from Girocco's

# Usage: genindex.sh [project-to-update]
#
# If project-to-update is given, then only that one will be updated

. @basedir@/shlib.sh

set -e

update="${1%.git}"

# Use the correct umask so the list file is group-writable, if owning_group set
if [ -n "$cfg_owning_group" ]; then
	umask 002
fi

# gitweb calls CGI::Util::unescape on both the path and owner, but the only
# character we allow that needs to be escaped is '+' which is allowed in
# both the owner email and in the project name.  Otherwise '+' will be
# displayed as a ' ' in the owner email and will cause a project name
# containing it to be omitted from the project list page.

if [ -z "$update" ] || [ ! -s "$cfg_projlist_cache_dir/gitproj.list" ]; then
	# Must read all the owners so don't bother with join at all
	get_repo_list | while read proj; do
		echo "$proj $(cd "$cfg_reporoot/$proj.git" && config_get owner)"
	done | perl -MDigest::MD5=md5_hex -ne \
		'@_=split;print "$_[0] ",md5_hex(lc($_[1]))," $_[1]\n";' |
	LC_ALL=C sort -k 1,1 >/tmp/gitproj.list.$$
	test $? -eq 0
else
	get_repo_list | LC_ALL=C sort -k 1,1 |
	LC_ALL=C join -a 1 - "$cfg_projlist_cache_dir/gitproj.list" |
	while read proj hash owner; do
		if [ "$proj" = "$update" ] || [ -z "$owner" ] || [ -z "$hash" ]; then
			echo "$proj recalc $(cd "$cfg_reporoot/$proj.git" && config_get owner)"
		else
			echo "$proj $hash $owner"
		fi
	done | perl -MDigest::MD5=md5_hex -ne \
		'@_=split;print "$_[0] ",$_[1] eq "recalc"?md5_hex(lc($_[2])):$_[1]," $_[2]\n";' |
	LC_ALL=C sort -k 1,1 >/tmp/gitproj.list.$$
	test $? -eq 0
fi
cut -d ' ' -f 1,3- </tmp/gitproj.list.$$ | sed -e 's/ /.git /;s/+/%2B/g' >/tmp/gitweb.list.$$

# Set the proper group, if configured, before the move
if [ -n "$cfg_owning_group" ]; then
	chgrp "$cfg_owning_group" /tmp/gitproj.list.$$ /tmp/gitweb.list.$$
fi

# Atomically move into place
mv -f /tmp/gitproj.list.$$ "$cfg_projlist_cache_dir/gitproj.list"
mv -f /tmp/gitweb.list.$$ "$cfg_projlist_cache_dir/gitweb.list"
