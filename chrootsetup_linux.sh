# chrootsetup_linux.sh

# This file SHOULD NOT be executable!  It is sourced by jailsetup.sh and
# SHOULD NOT be executed directly!

# On entry the current directory will be set to the top of the chroot
# This script must perform platform-specific chroot setup which includes
# creating any dev device entries, setting up proc (if needed), setting
# up lib64 (if needed) as well as installing a basic set of whatever libraries
# are needed for a chroot to function on this platform.

# This script must also define a pull_in_bin function that may be called to
# install an executable together with any libraries it depends on into the
# chroot.

# Finally this script must install a suitable nc.openbsd compatible version of
# netcat into the chroot jail that's available as nc.openbsd and which supports
# connections to unix sockets.

# We are designed to set up the chroot based on binaries from
# amd64 Debian lenny; some things may need slight modifications if
# being run on a different distribution.

chroot_dir="$(pwd)"

mkdir -p dev proc selinux
chown 0:0 proc selinux
rm -f lib64
ln -s lib lib64

# Seed up /dev:
rm -f dev/null dev/zero dev/random dev/urandom
mknod dev/null c 1 3
mknod dev/zero c 1 5
mknod dev/random c 1 8
mknod dev/urandom c 1 9
chmod a+rw dev/null dev/zero dev/random dev/urandom

# Extra directories
mkdir -p var/run/sshd var/tmp

has_files()
{
	for _f in "$@"; do
		test -f "$_f" || return 1
	done
	return 0
}

# Bring in basic libraries:
rm -f lib/*

# ld.so:
! [ -d /lib ] || ! has_files /lib/ld-linux*.so* || cp -p -t lib /lib/ld-linux*.so*
! [ -d /lib64 ] || ! has_files /lib64/ld-linux*64.so* || cp -p -t lib /lib64/ld-linux*64.so*
has_files lib/ld-linux*.so* || {
	echo "ERROR: could not find any ld-linux*.so* file" >&2
	exit 1
}

# Besides '=>' libs, attempt to pick up absolute path libs and create a symlink for upto one level deep
extract_libs() {
	ldd "$1" | grep -v -e linux-gate -e linux-vdso -e ld-linux | LC_ALL=C awk '{print $1 " " $2 " " $3}' |
	while read -r _f1 _f2 _f3; do
		case "$_f2" in
			"=>")
				echo "$_f3"
				;;
			"(0x"*)
				case "$_f1" in /*.so*)
					_basedir="$(dirname "$_f1")"
					_basedir="${_basedir#/}"
					_basedir="${_basedir#usr/}"
					case "$_basedir" in
						lib)
							echo "$_f1"
							;;
						lib/?*)
							_basedir="${_basedir#lib/}"
							case "$_basedir" in */*) :;; *)
								if [ ! -e "lib/$_basedir" ]; then
									ln -s . "lib/$_basedir"
								fi
								echo "$_f1"
							esac
					esac
				esac
		esac
	done
	test $? -eq 0
}

pull_in_lib() {
	[ -f "$1" ] || return
	dst="${2%/}/$(basename "$1")"
	if [ ! -e "$dst" ] || [ "$1" -nt "$dst" ]; then
		cp -p -t "$2" "$1"
		for llib in $(extract_libs "$1"); do
			(pull_in_lib "$llib" lib)
			test $? -eq 0
		done
	fi
	case "$(basename "$1")" in libc.*)
		# grab libnss_compat.so* from libc location
		! has_files "$(dirname "$1")/libnss_compat."so* ||
		for nlib in "$(dirname "$1")/libnss_compat."so*; do
			(pull_in_lib "$nlib" "$2")
			test $? -eq 0
		done
	esac
}

# pull_in_bin takes two arguments:
# 1: the full path to a binary to pull in (together with any library dependencies)
# 2: the destination directory relative to the current directory to copy it to which
# MUST already exist with optional alternate name if the name in the chroot should be different
# 3: optional name of binary that if already in $2 and the same as $1 hard link to instead
# for example, "pull_in_bin /bin/sh bin" will install the shell into the chroot bin directory
# for example, "pull_in_bin /bin/bash bin/sh" will install bash as the chroot bin/sh
# IMPORTANT: argument 1 must be a machine binary, NOT a shell script or other interpreted text
# IMPORTANT: text scripts can simply be copied in or installed as they don't have libraries to copy
# NOTE: it's expected that calling this function on a running chroot may cause temporary disruption
# In order to avoid a busy error while replacing binaries we first copy the binary to the
# var/tmp directory and then force move it into place after the libs have been brought in.
pull_in_bin() {
	bin="$1"; bdst="$2"
	if [ -d "${bdst%/}" ]; then
		bnam="$(basename "$bin")"
		bdst="${bdst%/}"
	else
		bnam="$(basename "$bdst")"
		bdst="${bdst%/*}"
	fi
	if [ -n "$3" ] && [ "$3" != "$bnam" ] &&
	   [ -r "$bdst/$3" ] && [ -x "$bdst/$3" ] && cmp -s "$bin" "$bdst/$3"; then
		ln -f "$bdst/$3" "$bdst/$bnam"
		return 0
	fi
	cp -p -t var/tmp "$bin"
	# ...and all the dependencies.
	for lib in $(extract_libs "$bin"); do
		pull_in_lib "$lib" lib
	done
	mv -f "var/tmp/$(basename "$bin")" "$bdst/$bnam"
}

# A catch all that needs to be called after everything's been pulled in
chroot_update_permissions() {
	# Be paranoid
	[ -n "$chroot_dir" ] && [ "$chroot_dir" != "/" ] || { echo bad '$chroot_dir' >&2; exit 2; }
	cd "$chroot_dir" || { echo bad '$chroot_dir' >&2; exit 2; }
	rm -rf var/tmp
	chown -R 0:0 bin dev lib sbin var
}

# the nc.openbsd compatible utility is available as $var_nc_openbsd_bin
pull_in_bin "$var_nc_openbsd_bin" bin/nc.openbsd
